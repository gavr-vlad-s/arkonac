/*
    File:    strings_to_columns.hpp
    Created: 24 May 2020 at 10:09 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef STRINGS_TO_COLUMNS_H
#define STRINGS_TO_COLUMNS_H
#   include <cstddef>
#   include <string>
#   include <vector>
struct Format{
    std::size_t indent_                 = 0; //< number of spaces before each line
    std::size_t number_of_columns_      = 2; //< required number of columns
    std::size_t spaces_between_columns_ = 0; //< number of spaces between columns
};

template<typename CharT,
         typename Traits    = std::char_traits<CharT>,
         typename Allocator = std::allocator<CharT>>
auto calculate_columns_width(std::size_t                                                     num_of_columns,
                             const std::vector<std::basic_string<CharT, Traits, Allocator>>& l)
{
    std::vector<size_t> result = std::vector<size_t>(num_of_columns);
    for(std::size_t& z : result){
        z = 0;
    }
    std::size_t counter = 0;
    for(const auto& s : l){
        std::size_t i = counter % num_of_columns;
        result[i] = std::max(result[i], s.length());
        counter++;
    }
    return result;
}

/**
 * \param [in] l   vector of the processed strings
 * \param [in] f   information about formatting
 * \param [in] d   separator
 *
 * \return strings from l are separated by d and then formatted corresponding
 *         to format information f
 */
template<typename CharT,
         typename Traits    = std::char_traits<CharT>,
         typename Allocator = std::allocator<CharT>>
auto strings_to_columns(const std::vector<std::basic_string<CharT, Traits, Allocator>>& l,
                        const Format&                                                   f,
                        CharT                                                           d = CharT{','})
{
    using string_t = std::basic_string<CharT, Traits, Allocator>;

    string_t    result;
    std::size_t num_of_strs                = l.size();
    std::size_t num_of_columns             = f.number_of_columns_;

    if(!num_of_strs || (num_of_columns <= 0)){
        return result;
    }
    if(num_of_columns > num_of_strs){
        num_of_columns = num_of_strs;
    }
    std::size_t num_of_rows                = num_of_strs / num_of_columns;
    std::size_t rest                       = num_of_strs % num_of_columns;

    using Row = std::vector<string_t>;

    string_t delim;
    size_t      delim_len = 0;
    if(d){
        delim     += d;
        delim_len =  1;
    }

    auto        begin_indent                = string_t(f.indent_, CharT{' '});
    auto        interm_indent               = string_t(f.spaces_between_columns_, CharT{' '});
    auto        column_width                = calculate_columns_width(num_of_columns, l);
    std::size_t delim_len_and_interm_indent = delim_len + f.spaces_between_columns_ + 1;
    std::size_t num_of_deleted_char         = 0;
    for(std::size_t i = 0; i < num_of_rows; i++){
        Row         current_row;
        std::size_t num_of_padded_spaces;
        for(std::size_t j = 0; j < num_of_columns; j++){
            auto current_line = l[num_of_columns * i + j];
            num_of_padded_spaces = column_width[j] - current_line.length();
            auto temp = current_line + delim + string_t(num_of_padded_spaces, CharT{' '});
            current_row.push_back(temp);
        }
        result += begin_indent;
        for(const auto& e : current_row){
            result += e + interm_indent;
        }
        result += CharT{'\n'};
        num_of_deleted_char = num_of_padded_spaces + delim_len_and_interm_indent;
    }
    if(rest){
        Row    current_row;
        std::size_t num_of_padded_spaces = 0;
        for(std::size_t i = num_of_strs - rest; i < num_of_strs; i++){
            auto current_line = l[i];
            num_of_padded_spaces = column_width[i + rest - num_of_strs] - current_line.length();
            auto temp = current_line + delim + string_t(num_of_padded_spaces, CharT{' '});
            current_row.push_back(temp);
        }
        result += begin_indent;
        for(const auto& e : current_row){
            result += e + interm_indent;
        }
        result += CharT{'\n'};
        num_of_deleted_char = num_of_padded_spaces + delim_len_and_interm_indent;
    }
    result.erase(result.end() - num_of_deleted_char, result.end());

    return result;
}
#endif