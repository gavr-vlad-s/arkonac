/*
    File:    idx_of_vector_to_string.cpp
    Created: 03 January 2020 at 13:04 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <vector>
#include <iterator>
#include "../include/idx_of_vector_to_string.h"
#include "../../strings-lib/include/join.h"
#include "../include/idx_to_string.h"

std::string idx_of_vector_to_string(const Module_name_trie_ptr&       mnt,
                                    const std::shared_ptr<Char_trie>& ids_trie,
                                    std::size_t                       idx,
                                    std::string                       default_value)
{
    std::string              result;
    if(!idx){
        return default_value;
    }
    auto                     vector_from_idx = mnt->get(idx);
    std::vector<std::string> module_name_parts;
    for(size_t idx_of_part : vector_from_idx){
        module_name_parts.push_back(idx_to_string(ids_trie, idx_of_part));
    }
    result = join(std::begin(module_name_parts),
                  std::end(module_name_parts),
                  std::string{"::"});
    return result;
}