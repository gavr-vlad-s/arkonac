/*
    File:    errors_and_tries.cpp
    Created: 21 June 2019 at 06:48 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/errors_and_tries.h"

void Errors_and_tries::print() const
{
    ec_->print();
    wc_->print();
}