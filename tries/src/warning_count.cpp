/*
    File:    warning_count.cpp
    Created: 21 June 2019 at 06:38 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstdio>
#include "../include/warning_count.h"

void Warning_count::increment_number_of_warnings()
{
    number_of_warnings_++;
}

size_t Warning_count::get_number_of_warnings() const
{
    return number_of_warnings_;
}

void Warning_count::print() const
{
    printf("\nTotal warnings: %zu\n", number_of_warnings_);
}