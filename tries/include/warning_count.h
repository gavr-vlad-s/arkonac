/*
    File:    warning_count.h
    Created: 21 June 2019 at 06:35 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef WARNING_COUNT_H
#define WARNING_COUNT_H
#   include <cstddef>
/* A class for calculating the number of warnings. */
class Warning_count{
public:
    Warning_count() : number_of_warnings_(0) {};
    void   increment_number_of_warnings();
    void   print() const;
    size_t get_number_of_warnings() const;
private:
    size_t number_of_warnings_;
};
#endif