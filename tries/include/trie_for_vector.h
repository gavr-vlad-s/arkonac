/*
    File:    trie_for_vector.h
    Created: 03 January 2020 at 12:16 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef TRIE_FOR_VECTOR_H
#define TRIE_FOR_VECTOR_H
#   include <cstddef>
#   include <cstdint>
#   include <vector>
#   include <string>
#   include "../include/trie.h"
template<typename T>
class Trie_for_vector : public Trie<T>{
public:
    virtual ~Trie_for_vector<T>()                      = default;
    Trie_for_vector<T>()                               = default;
    Trie_for_vector<T>(const Trie_for_vector<T>& orig) = default;

    /**
     *  \brief The function get on the index idx of the vector of values of
     *         type T builds the same vector, but already as std::vector< T >.
     *  \param [in] idx The index of the vectot.
     *  \return         The same vector, but already as std::vector < T >.
     */
    std::vector<T> get(size_t idx);
    size_t         insert_vector(const std::vector<T>& s);
};

template<typename T>
std::vector<T> Trie_for_vector<T>::get(size_t idx){
    if(!idx){
        return std::vector<T>{};
    }

    size_t         len     = Trie<T>::node_buffer[idx].path_len;
    std::vector<T> v       = std::vector<T>(len);
    size_t         current = idx;
    size_t         i       = len - 1;

    for( ; current; current = Trie<T>::node_buffer[current].parent){
        v[i--] = Trie<T>::node_buffer[current].c;
    }
    return v;
}

template<typename T>
size_t Trie_for_vector<T>::insert_vector(const std::vector<T>& s){
    std::basic_string<T> str;
    for(auto ch : s){
        str += ch;
    }
    size_t idx = this->insert(str);
    return idx;
}
#endif