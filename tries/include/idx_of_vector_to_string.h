/*
    File:    idx_of_vector_to_string.h
    Created: 03 January 2020 at 12:54 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef IDX_OF_VECTOR_TO_STRING_H
#define IDX_OF_VECTOR_TO_STRING_H
#   include <string>
#   include <memory>
#   include <cstddef>
#   include <cstdint>
#   include "../include/trie_for_vector.h"
#   include "../include/char_trie.h"

using Module_name_trie_ptr = std::shared_ptr<Trie_for_vector<std::size_t>>;

std::string idx_of_vector_to_string(const Module_name_trie_ptr&       mnt,
                                    const std::shared_ptr<Char_trie>& ids_trie,
                                    std::size_t                       idx,
                                    std::string                       default_value = std::string{});
#endif