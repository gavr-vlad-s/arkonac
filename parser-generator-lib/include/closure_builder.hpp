/*
    File:    closure_builder.hpp
    Created: 01 May 2020 at 21:34 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef CLOSURE_BUILDER_H
#define CLOSURE_BUILDER_H
#   include <vector>
#   include <list>
#   include "../include/items.hpp"
#   include "../include/rules.hpp"
#   include "../include/grammar.hpp"
namespace parser_generator {
    namespace slr1 {
        class Closure_builder final{
        public:
            Closure_builder(const Closure_builder& rhs) = default;
            ~Closure_builder()                          = default;

            explicit Closure_builder(const Grammar& grammar) :
                grammar_{grammar},
                number_of_non_terminals_{grammar.num_of_nonterminals_}
            {
                init(grammar);
            }

            Items_collection operator()(const Items_collection& collection);
        private:
            const Grammar&                      grammar_;
            std::size_t                         number_of_non_terminals_;
            std::vector<std::list<std::size_t>> converted_grammar_;

            void init(const Grammar& grammar);

            Items_collection build_next_collection(const Items_collection &old_collection);
        };
    };
};
#endif