/*
    File:    sets_follows.hpp
    Created: 26 April 2020 at 16:14 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SETS_FOLLOWS_H
#define SETS_FOLLOWS_H
#   include <cstddef>
#   include <cstdint>
#   include <vector>
#   include <set>
#   include <list>
#   include "../include/rules.hpp"
#   include "../include/grammar.hpp"
#   include "../include/sets_first.hpp"
#   include "../include/symbol_types.hpp"
namespace parser_generator{
    using follow_sets_t = std::vector<std::set<Symbol>>;
    class Sets_follow final{
    public:
        Sets_follow(const Sets_follow& rhs) = default;
        ~Sets_follow()                      = default;

        Sets_follow(const Grammar& grammar, const sets_first_t& firsts) :
            grammar_{grammar}, firsts_{firsts}, number_of_sets_{grammar.num_of_nonterminals_} {}

        follow_sets_t operator () ();
    private:
        const Grammar&      grammar_;
        const sets_first_t& firsts_;
        std::size_t         number_of_sets_ = 1;

        struct Converted_rule{
            Rule                          rule_;
            std::vector<std::set<Symbol>> firsts_for_suffixes_;
            std::set<std::size_t>         nonterminals_positions_;
        };

        std::list<Converted_rule>         converted_rules_;

        follow_sets_t build_next_sets(const follow_sets_t& current_sets) const;

        void build_converted_rules();
    };
};
#endif