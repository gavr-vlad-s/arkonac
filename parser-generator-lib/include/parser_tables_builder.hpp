/*
    File:    parser_tables_builder.hpp
    Created: 23 May 2020 at 14:17 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef PARSER_TABLES_BUILDER_H
#define PARSER_TABLES_BUILDER_H
#   include <map>
#   include <vector>
#   include <cstdint>
#   include <cstddef>
#   include <set>
#   include "../include/items.hpp"
#   include "../include/rules.hpp"
#   include "../include/grammar.hpp"
#   include "../include/symbol_types.hpp"
#   include "../include/closure_builder.hpp"
namespace parser_generator{
    namespace slr1{
        using Table_index = std::pair<std::size_t, Symbol>;

        struct Items_map final{
            std::map<Table_index, std::size_t> item_map_;
            std::vector<Items_collection>      item_collections_;
        };

        using symbols_t    = std::set<Symbol>;
        using collection_t = Items_collection;

        enum Action_kind {
            Shift, Reduce, OK
        };

        struct Action{
            unsigned      kind_ : 2;
            std::uint16_t idx_  : 14;
        };

        bool operator < (Action lhs, Action rhs);

        using GOTO_index   = std::pair<std::size_t, std::uint16_t>;
        using Action_index = std::pair<std::size_t, Symbol>;
        struct Parser_table{
            std::map<Action_index, std::set<Action>> actions_;
            std::map<GOTO_index, std::size_t>        gotos_;
            std::vector<Items_collection>            item_collections_;
        };

        class Parser_tables_builder final{
        public:
            Parser_tables_builder(const Parser_tables_builder& rhs) = default;
            ~Parser_tables_builder()                                = default;

            explicit Parser_tables_builder(const Grammar& grammar) :
                grammar_{grammar}, closure_builder_{grammar} {}

            Items_map    build_item_map();
            Parser_table build_actions_and_gotos();
        private:
            const Grammar&  grammar_;
            Closure_builder closure_builder_;

            using transitions_t = std::map<Symbol, Items_collection>;
            transitions_t get_transitions(const collection_t& collection) const;
            collection_t move(const collection_t& collection, const Symbol& s) const;
        };
    };
};
#endif