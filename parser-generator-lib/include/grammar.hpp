/*
    File:    grammar.hpp
    Created: 23 May 2020 at 13:39 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef GRAMMAR_H
#define GRAMMAR_H
#   include <cstdint>
#   include <cstddef>
#   include "../include/rules.hpp"
namespace parser_generator{
    struct Grammar final{
        Grammar()                   = default;
        Grammar(const Grammar& rhs) = default;
        ~Grammar()                  = default;

        Grammar(std::uint16_t target,
                const rules_t& rules,
                std::size_t num_of_terminals = 1,
                std::size_t num_of_nonterminals = 1) :
            rules_{rules},
            target_{target},
            num_of_terminals_{num_of_terminals},
            num_of_nonterminals_{num_of_nonterminals} {}

        rules_t        rules_;
        std::uint16_t  target_               = 0;
        std::size_t    num_of_terminals_     = 1;
        std::size_t    num_of_nonterminals_  = 1;
    };
};
#endif