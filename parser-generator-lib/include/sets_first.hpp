/*
    File:    sets_first.hpp
    Created: 18 April 2020 at 14:13 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SETS_FIRST_H
#define SETS_FIRST_H
#   include <cstddef>
#   include <cstdint>
#   include <vector>
#   include <set>
#   include <memory>
#   include "../include/symbol_types.hpp"
#   include "../include/rules.hpp"
#   include "../include/grammar.hpp"
namespace parser_generator{
    using sets_first_t   = std::vector<std::set<Symbol>>;

    class Sets_first final{
    public:
        Sets_first(const Sets_first& rhs) = default;
        ~Sets_first()                     = default;

        explicit Sets_first(const Grammar& grammar) : grammar_{grammar} {}

        sets_first_t operator ()() const;
    private:
        const Grammar& grammar_;
        sets_first_t build_next_sets(const sets_first_t& current_sets) const;
    };
};
#endif
