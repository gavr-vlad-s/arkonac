/*
    File:    rules.hpp
    Created: 18 April 2020 at 09:42 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef RULES_TYPES_H
#define RULES_TYPES_H
#   include<vector>
#   include "../include/symbol_types.hpp"
namespace parser_generator{
    struct Rule final{
        std::uint16_t       concept_;
        std::vector<Symbol> body_;
    };

    using rules_t = std::vector<Rule>;

    bool operator <  (const Rule& lhs, const Rule& rhs);

    bool operator == (const Rule& lhs, const Rule& rhs);

    bool operator != (const Rule& lhs, const Rule& rhs);
};
#endif