/*
    File:    symbols_builders.hpp
    Created: 23 May 2020 at 14:01 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SYMBOLS_BUILDER_H
#define SYMBOLS_BUILDER_H
#   include <cstdint>
#   include <cstddef>
#   include <map>
#   include <set>
#   include <vector>
#   include <string>
#   include "../include/symbol_types.hpp"
namespace parser_generator{
    enum class Kind{
        Terminal, Non_terminal, Epsilon
    };

    struct Names_info{
        std::vector<std::string>  terminals_mames_;
        std::vector<std::string>  nonterminals_mames_;
        std::string               target_name_;
    };

    struct Named_symbol{
        Kind        kind_;
        std::string name_;
    };

    class Symbols_builder final{
    public:
        Symbols_builder()                                        = default;
        Symbols_builder(const Symbols_builder& rhs)              = default;
        Symbols_builder& operator = (const Symbols_builder& rhs) = default;
        Symbols_builder(Symbols_builder&& rhs)                   = default;
        Symbols_builder& operator = (Symbols_builder&& rhs)      = default;

        ~Symbols_builder()                                       = default;

        explicit Symbols_builder(const Names_info& names_info);

        Symbol      build_symbol(const Named_symbol& symbol) const;
        std::string to_string(const Symbol& symbol) const;
        std::string to_string(const std::set<Symbol> &symbols) const;

        std::size_t get_number_of_nonterminals() const;
        std::size_t get_number_of_terminals() const;

    private:
        std::vector<std::string>             terminal_names_;
        std::map<std::string, std::uint16_t> terminal_name_to_code_;
        std::vector<std::string>             nonterminal_names_;
        std::map<std::string, std::uint16_t> nonterminal_name_to_code_;
    };
};
#endif