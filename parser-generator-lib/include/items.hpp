/*
    File:    items.hpp
    Created: 01 May 2020 at 21:07 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ITEMS_H
#define ITEMS_H
#   include <cstddef>
#   include <set>
#   include <map>
#   include "../include/symbol_types.hpp"
#   include "../include/rules.hpp"
namespace parser_generator{
    namespace slr1{
        struct Item{
            std::size_t idx_of_rule_  = 0;
            std::size_t dot_location_ = 0;
        };

        bool operator <  (const Item& lhs, const Item& rhs);
        bool operator == (const Item& lhs, const Item& rhs);
        bool operator != (const Item& lhs, const Item& rhs);

        using Items_collection = std::set<Item>;
    };
};

#endif