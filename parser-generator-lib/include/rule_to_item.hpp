/*
    File:    rule_to_item.hpp
    Created: 01 May 2020 at 21:16 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef RULE_TO_ITEM_H
#define RULE_TO_ITEM_H
#   include "../include/symbol_types.hpp"
#   include "../include/rules.hpp"
#   include "../include/items.hpp"
#   include "../include/grammar.hpp"
namespace parser_generator{
    namespace slr1{
        Item rule_to_item(std::size_t rule_idx, const Grammar& grammar);
    };
};
#endif