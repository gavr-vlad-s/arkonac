/*
    File:    parser_builder.hpp
    Created: 23 May 2020 at 15:58 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef PARSER_BUILDER_H
#define PARSER_BUILDER_H
#   include <string>
#   include <vector>
#   include <boost/variant.hpp>
#   include "../include/grammar.hpp"
#   include "../include/symbols_builders.hpp"
#   include "../include/grammar_builder.hpp"
#   include "../../bitset/include/bitset.hpp"
namespace parser_generator{
    namespace slr1{
        /**
         * @brief Information about generated parser class.
         *
         */
        struct Parser_class_details{
            /// Name of generated parser class.
            std::string                        parser_class_name_;
            /// Namespace for generated parser class.
            std::string                        parser_namespace_name_;
            /// Name of scanner class for generated parser.
            std::string                        scanner_class_name_;
            /// Namespace for scanner class.
            std::string                        scanner_namespace_name_;
            /// Includes and templates needed for generated parser header.
            std::string                        parser_header_preamble_;
            /// Includes, templates etc. needed for generated parser implementation.
            std::string                        parser_impl_preamble_;
            /// Name of the function that converts a token received from the scanner to terminal
            std::string                        token_to_terminal_conversion_function_name_;
            /// Type of the result of parsing process.
            std::string                        parsing_result_type_;
            /// Function that converts each token into an element of intermediate representation.
            std::string                        shift_function_name_;
            /// Type of value returned by shift function.
            std::string                        shift_function_result_type_;
            /**
             *  @brief Actions to reduce.
             *
             * @details This vector defines actions to reduce according to rules of the grammar.
             * The numbering of grammar rules begins with 1,
             * More precisely, reduce_actions_[i] defines actions to reduce according the rule with number i.
             */
            std::vector<std::string>           reduce_actions_;
            /**
             * @brief Additional arguments of a constructor of generated parser.
             * @details The generated parser constructor has one required argument, and this required argument
             * is always pointer to the scanner. But to implement the parser, we may need to specify some
             * additional arguments of constructor, e.g., a pointer to a symbol tables. For example, to specify
             * arguments arg1_name, arg2_name with types Typename1, Typename2 correspondingly, we need to specify the
             * value of additional_ctor_args_ as "Typename1 arg1_name, Typename2 arg2_name".
             */
            std::string                        additional_ctor_args_;
            /**
             * @brief The body of the parser constructor.
             * @details In some cases, we need to write a constructor body to implement parsing.
             * In such cases, this body should be in ctor_body_.
             */
            std::string                        ctor_body_;
            /**
             * @brief Additional parser members.
             * @details In some cases, we need some additional parser members to implement parsing.
             */
            std::string                        additional_parser_members_;
            /**
             * @brief Initializers for additional members.
             */
            std::string                        initializers_for_ctor_;
            /**
             * @brief Additional header files to implement parsing.
             * @details In many cases, we need some additional header (.hpp, .h, .h++) files to implement parsing.
             * This map maps such header file name to the file content.
             */
            std::map<std::string, std::string> additional_includes_;
            /**
             * @brief Additional implementation files to implement parsing.
             * @details In many cases, we need some additional implementation (.cpp, .cxx, .c++, and so on)
             * files to implement parsing. This map maps such implementation file name to the file content.
             */
            std::map<std::string, std::string> additional_srcs_;
            /**
             * @brief Mapping of names of terminals to diagnostic messages.
             * @details To display a meaningful diagnostic message when there are not entry (state, terminal)
             * in SLR(1) table ACTION, we need to know how to convert a terminal to diagnostic message.
             * This map do such conversion.
             */
            std::map<std::string, std::string> terminal_to_msg_;
            /**
             * @brief Type for token.
             */
            std::string                        token_type_;
            /**
             * @brief Postprocessing to get result of parsing.
             */
            std::string                        postprocessing_;
            std::string                        user_error_handling_;
        };

        struct Parser_info final{
            Parser_class_details class_details_;
            Grammar_info         grammar_info_;
        };

        struct Generated_texts final{
            std::map<std::string, std::string> include_; ///< mapping of name of a hpp-file to its content
            std::map<std::string, std::string> src_;     ///< mapping of name of a cpp-file to its content
        };

        struct Error_info final{
            Parser_table           tables_;
            std::set<Action_index> clashes_;
        };


        using Parser_builder_result = boost::variant<Generated_texts, Error_info>;

        class Parser_builder final{
        public:
            Parser_builder(const Parser_builder& rhs) = default;
            ~Parser_builder()                         = default;

            explicit Parser_builder(const Parser_info& p) :
                grammar_info_{p.grammar_info_},
                grammar_builder_{p.grammar_info_},
                parser_class_details_{p.class_details_}
                {}

            Parser_builder_result operator ()();

            std::string error_info_to_string(Error_info& einfo);
        private:
            const Grammar_info&      grammar_info_;
            Grammar_builder          grammar_builder_;
            Parser_class_details     parser_class_details_;

            Grammar                  grammar_;
            std::string              lower_case_name_;

            std::vector<std::string> names_of_terminals_;
            std::vector<std::string> names_of_nonterminals_;

            void correct_names();

            using actions_t     = std::map<Action_index, std::set<Action>>;
            using action_pair_t = std::pair<Action_index, Action>;
            using gotos_t       = std::map<GOTO_index, std::size_t>;

            using bitscales_t   = std::vector<bitscale::bitset>;
            using acts_table_t  = std::map<Action_index, std::set<Action>>;
            using messages_t    = std::vector<std::string>;

            std::string gotos_to_str(const gotos_t& gotos)                    const;
            std::string build_parser_header_str()                             const;
            std::string build_parser_interface()                              const;
            std::string build_parser_class_interface()                        const;
            std::string build_public_parser_class_interface()                 const;
            std::string build_private_parser_class_interface()                const;
            std::string build_acts(const actions_t& actions)                  const;
            std::string acts_pair_to_str(const action_pair_t& p)              const;

            std::string build_terminal_hpp()                                  const;
            std::string build_nonterminal_hpp()                               const;

            void build_names_of_terminals();
            void build_names_of_nonterminals();

            std::string build_content_of_array_of_targets()                   const;

            std::string build_reduction_functions_protos()                    const;

            std::string build_reduction_function_array()                      const;
            std::string build_reduction_functions()                           const;
            std::string build_reduction_functions_block()                     const;

            std::string build_call_operator()                                 const;

            std::string build_parser_implementation()                         const;

            bitscales_t build_missing_action_indices(const Parser_table& t)   const;

            messages_t build_messages(const bitscales_t& bits)                const;

            std::string build_diagnostic_messages_header()                    const;
            std::string build_diagnostic_messages_impl(const Parser_table& t) const;
        };
    };
};
#endif