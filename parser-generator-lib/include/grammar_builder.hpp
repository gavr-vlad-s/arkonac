/*
    File:    grammar_builder.hpp
    Created: 23 May 2020 at 13:43 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef GRAMMAR_BUILDER_H
#define GRAMMAR_BUILDER_H
#   include <string>
#   include <vector>
#   include <set>
#   include "../include/symbols_builders.hpp"
#   include "../include/rules.hpp"
#   include "../include/grammar.hpp"
#   include "../include/items.hpp"
#   include "../include/parser_tables_builder.hpp"
namespace parser_generator{
    struct Named_rule final{
        std::string               concept_name_;
        std::vector<Named_symbol> body_;
    };

    struct Grammar_info final{
        Names_info              names_info_;
        std::vector<Named_rule> rules_;
    };

    class Grammar_builder final{
    public:
        Grammar_builder()                                        = default;
        Grammar_builder(const Grammar_builder& rhs)              = default;
        Grammar_builder& operator = (const Grammar_builder& rhs) = default;
        Grammar_builder(Grammar_builder&& rhs)                   = default;
        Grammar_builder& operator = (Grammar_builder&& rhs)      = default;

        ~Grammar_builder()                                       = default;

        explicit Grammar_builder(const Grammar_info& grammar_info) :
            symbols_builder_{grammar_info.names_info_}
        {
            init(grammar_info);
        }

        std::string to_string(const Symbol& symbol) const;
        std::string to_string(const std::set<Symbol>& symbols) const;
        std::string to_string(const std::vector<std::set<Symbol>>& sets, const std::string& prefix = "") const;
        std::string to_string(const Rule& rule) const;
        std::string to_string(const Grammar& grammar) const;
        std::string to_string(const slr1::Item& item, const Grammar& grammar) const;
        std::string to_string(const slr1::Items_collection& items, const Grammar& grammar) const;
        std::string to_string(const slr1::Items_map& items_map, const Grammar& grammar) const;
        std::string to_string(slr1::Action action) const;
        std::string to_string(const slr1::Parser_table& parser_table, const Grammar& grammar) const;

        Grammar operator()() const;

        std::size_t get_number_of_nonterminals() const
        {
            return symbols_builder_.get_number_of_nonterminals();
        }

        std::size_t get_number_of_terminals() const
        {
            return symbols_builder_.get_number_of_terminals();
        }

    private:
        Symbols_builder symbols_builder_;
        rules_t         rules_;
        std::uint16_t   target_code_;

        void init(const Grammar_info& grammar_info);
        std::string to_string(const slr1::Table_index& index) const;
    };

};
#endif