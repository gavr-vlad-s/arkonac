/*
    File:    symbol_types.hpp
    Created: 18 April 2020 at 09:08 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SYMBOL_TYPES_H
#define SYMBOL_TYPES_H
#   include <cstdint>
namespace parser_generator{
    enum class Symbol_kind : std::uint16_t{
        Terminal, Non_terminal, Epsilon
    };

    struct Symbol final{
        Symbol_kind kind_;
        uint16_t    code_;
    };

    bool operator <  (Symbol lhs,  Symbol rhs);

    bool operator == (Symbol lhs,  Symbol rhs);

    bool operator != (Symbol lhs,  Symbol rhs);
};
#endif
