/*
    File:    closure_builder.cpp
    Created: 01 May 2020 at 21:37 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/closure_builder.hpp"
#include "../include/rule_to_item.hpp"

namespace parser_generator {
    namespace slr1 {
        void Closure_builder::init(const Grammar& grammar)
        {
            converted_grammar_ = std::vector<std::list<std::size_t>>(number_of_non_terminals_);

            std::size_t idx = 0;
            for (const auto &rule : grammar.rules_) {
                std::uint16_t concept = rule.concept_;
                converted_grammar_[concept].push_back(idx);
                idx++;
            }
        }

        Items_collection Closure_builder::operator()(const Items_collection& collection)
        {
            Items_collection old_collection = collection;
            Items_collection new_collection = build_next_collection(old_collection);

            while(old_collection != new_collection){
                old_collection = new_collection;
                new_collection = build_next_collection(old_collection);
            }
            return new_collection;
        }

        Items_collection Closure_builder::build_next_collection(const Items_collection& old_collection)
        {
            Items_collection result = old_collection;
            for(const auto& item : old_collection){
                const std::size_t idx          = item.idx_of_rule_;
                const auto&       rule         = grammar_.rules_[idx];
                const auto&       body         = rule.body_;

                if(body.empty()){
                    continue;
                }

                std::size_t       body_length  = body.size();
                std::size_t       dot_location = item.dot_location_;

                if(dot_location >= body_length){
                    continue;
                }

                Symbol symbol = body[dot_location];

                if(symbol.kind_ == Symbol_kind::Non_terminal){
                    std::int16_t code = symbol.code_;
                    for(const auto& r : converted_grammar_[code]){
                        result.insert(rule_to_item(r, grammar_));
                    }
                }
            }
            return result;
        }
    };
};