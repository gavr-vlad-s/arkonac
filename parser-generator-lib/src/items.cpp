/*
    File:    items.cpp
    Created: 01 May 2020 at 21:10 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <tuple>
#include "../include/items.hpp"

namespace parser_generator{
    namespace slr1{
        bool operator <  (const Item& lhs, const Item& rhs)
        {
            return std::tie(lhs.idx_of_rule_, lhs.dot_location_) <
                   std::tie(rhs.idx_of_rule_, rhs.dot_location_);
        }

        bool operator == (const Item& lhs, const Item& rhs)
        {
            return std::tie(lhs.idx_of_rule_, lhs.dot_location_) ==
                   std::tie(rhs.idx_of_rule_, rhs.dot_location_);
        }

        bool operator != (const Item& lhs, const Item& rhs)
        {
            return std::tie(lhs.idx_of_rule_, lhs.dot_location_) !=
                   std::tie(rhs.idx_of_rule_, rhs.dot_location_);
        }
    };
};