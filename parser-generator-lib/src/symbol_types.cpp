/*
    File:    symbol_types.cpp
    Created: 21 April 2020 at 19:47 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/symbol_types.hpp"

namespace parser_generator{
    bool operator < (Symbol lhs,  Symbol rhs)
    {
        if(lhs.kind_ != rhs.kind_){
            return lhs.kind_ < rhs.kind_;
        }

        if(lhs.kind_ == Symbol_kind::Epsilon){
            return false;
        }

        return lhs.code_ < rhs.code_;
    }

    bool operator == (Symbol lhs, Symbol rhs)
    {
        if(lhs.kind_ != rhs.kind_){
            return false;
        }

        if(lhs.kind_ == Symbol_kind::Epsilon){
            return true;
        }

        return lhs.code_ == rhs.code_;
    }

    bool operator != (Symbol lhs, Symbol rhs)
    {
        return !(lhs == rhs);
    }
};