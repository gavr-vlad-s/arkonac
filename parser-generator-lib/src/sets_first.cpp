/*
    File:    sets_first.cpp
    Created: 21 April 2020 at 20:25 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/sets_first.hpp"
namespace parser_generator{
    static const Symbol eps{Symbol_kind::Epsilon, 0};

    sets_first_t Sets_first::operator()() const{
        sets_first_t old_sets{grammar_.num_of_nonterminals_};

        auto new_sets = build_next_sets(old_sets);
        while(old_sets != new_sets){
            old_sets = new_sets;
            new_sets = build_next_sets(old_sets);
        }

        return new_sets;
    }

    sets_first_t Sets_first::build_next_sets(const sets_first_t& current_sets) const{
        sets_first_t result = current_sets;

        for(const auto& rule: grammar_.rules_){
            std::uint16_t concept = rule.concept_;
            auto&         body    = rule.body_;

            if(body.empty()){
                continue;
            }

            auto        old_set  = result[concept];
            std::size_t i        = 0;
            std::size_t rule_len = body.size();

            for(const auto& s: body){
                if(s.kind_ != Symbol_kind::Non_terminal){
                    old_set.insert(s);
                    break;
                }

                auto          first_of_s      = result[s.code_];
                auto          added_terminals = first_of_s;

                added_terminals.erase(eps);
                old_set.insert(added_terminals.begin(), added_terminals.end());
                if(first_of_s.count(eps) == 0){
                    break;
                }
                ++i;
            }

            if(i >= rule_len){
                old_set.insert(eps);
            }

            result[concept] = old_set;
        }

        return result;
    }
};