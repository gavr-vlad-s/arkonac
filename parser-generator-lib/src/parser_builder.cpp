/*
    File:    parser_builder.cpp
    Created: 24 May 2020 at 21:12 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <vector>
#include <list>
#include "../include/parser_builder.hpp"
#include "../include/format.h"
#include "../include/parser_tables_builder.hpp"
#include "../../strings-lib/include/strings_to_columns.hpp"
#include "../../strings-lib/include/join.h"
#include "../../char-conv/include/char_conv.h"

namespace parser_generator{
    namespace slr1{
        void Parser_builder::correct_names()
        {
            if(parser_class_details_.parser_class_name_.empty()){
                parser_class_details_.parser_class_name_      = "Parser";
            }
            lower_case_name_   = to_lower(parser_class_details_.parser_class_name_);
            if(parser_class_details_.parser_namespace_name_.empty()){
                parser_class_details_.parser_namespace_name_  = "parser";
            }
            if(parser_class_details_.scanner_class_name_.empty()){
                parser_class_details_.scanner_class_name_     = "Scanner";
            }
            if(parser_class_details_.scanner_namespace_name_.empty()){
                parser_class_details_.scanner_namespace_name_ = "scanner";
            }
        }


        static const std::string multipop_stack_hpp = R"~(// File: multipop_stack.hpp
// This file was generated automatically.
#ifndef MULTIPOP_STACK_H
#define MULTIPOP_STACK_H
#   include <deque>
#   include <cassert>
#   include <iostream>
namespace stack{
    template<typename T, template<typename Elem,
                                  typename = std::allocator<Elem>> class ContainerT = std::deque>
    class Stack{
    public:
        Stack()          = default;
        virtual ~Stack() = default;

        template<typename ForwardIterator>
        Stack(ForwardIterator first, ForwardIterator last);

        template<typename U, template<typename Elem, typename = std::allocator<Elem>> class ContainerU>
        Stack& operator = (const Stack<U, ContainerU>& rhs);

        template<typename, template<typename, typename> class>
        friend class Stack;

        bool empty() const
        {
            return elems_.empty();
        }

        std::size_t size() const
        {
            return elems_.size();
        }

        void push(const T& e);
        void pop();
        const T& top() const;

        template<typename ForwardIterator>
        void push(ForwardIterator first, ForwardIterator last);

        void npop(std::size_t n);

        void ntop(T* first, std::size_t n);

        void print(std::ostream& stream) const;
    private:
        ContainerT<T> elems_;
    };

    template<typename T, template<typename, typename> class ContainerT>
    void Stack<T, ContainerT>::push(const T &e)
    {
        elems_.push_back(e);
    }

    template<typename T, template<typename, typename> class ContainerT>
    void Stack<T, ContainerT>::pop()
    {
        assert(!empty());
        elems_.pop_back();
    }

    template<typename T, template<typename, typename> class ContainerT>
    const T& Stack<T, ContainerT>::top() const
    {
        assert(!empty());
        return elems_.back();
    }


    template<typename T, template<typename, typename> class ContainerT>
    template<typename ForwardIterator>
    Stack<T, ContainerT>::Stack(ForwardIterator first, ForwardIterator last)
    {

        for(auto it = first; it != last; it++){
            elems_.push_back(*it);
        }
    }

    template<typename T, template<typename, typename> class ContainerT>
    template<typename U, template<typename, typename> class ContainerU>
    Stack<T, ContainerT>& Stack<T, ContainerT>::operator = (const Stack<U, ContainerU>& rhs)
    {
        elems_.clear();
        elems_.insert(elems_.end(), rhs.elems_.begin(), rhs.elems_.end());
        return *this;
    }

    template<typename T, template<typename, typename> class ContainerT>
    template<typename ForwardIterator>
    void Stack<T, ContainerT>::push(ForwardIterator first, ForwardIterator last)
    {
        for(auto it = first; it != last; it++){
            elems_.push_back(*it);
        }
    }

    template<typename T, template<typename, typename> class ContainerT>
    void Stack<T, ContainerT>::npop(std::size_t n)
    {
        assert(size() >= n);
        for(std::size_t i = 0; i < n; ++i){
            pop();
        }
    }

    template<typename T, template<typename, typename> class ContainerT>
    void Stack<T, ContainerT>::ntop(T *first, std::size_t n)
    {
        assert(size() >= n);
        std::size_t num_of_elems = size();
        for(std::size_t i = 0; i < n; ++i){
            first[i] = elems_[num_of_elems - n + i];
        }
    }

    template<typename T, template<typename, typename> class ContainerT>
    void Stack<T, ContainerT>::print(std::ostream &stream) const
    {
        for(const auto& e : elems_){
            stream << e << "\n";
        }
    }
};
#endif)~";

        static const std::string action_hpp_content_fmt = R"~(// File: action.hpp
// This file was generated automatically.
#ifndef ACTION_H
#define ACTION_H
#   include <cstdint>
namespace {0}{{
    enum Action_kind{{
        Shift, Reduce, OK, Error
    }};

    struct Action{{
        unsigned      kind_ : 2;
        std::uint16_t idx_  : 14;
    }};
}};
#endif)~";


        static const std::string table_class_header_fmt = R"~(// File: tables.hpp
// This file was generated automatically.
#ifndef TABLES_H
#define TABLES_H
#   include <cstddef>
#   include "../include/action.hpp"
#   include "../include/terminal_enum.hpp"
namespace {0}{{
    namespace slr1_tables{{
        class Tables final{{
        public:
            Tables()                  = default;
            Tables(const Tables& rhs) = default;
            ~Tables()                 = default;

            Action      get_action(std::size_t state, Terminal t) const;
            std::size_t get_goto(std::size_t state, std::size_t non_terminal) const;
            std::size_t get_rule_len(std::size_t rule_idx) const;
            std::size_t get_rule_target(std::size_t rule_idx) const;
        }};
    }};
}};
#endif)~";


        static const std::string table_class_impl_fmt = R"~(// File: tables.cpp
// This file was generated automatically.
#include <utility>
#include <map>
#include <cstddef>
#include <cstdint>
#include "../include/tables.hpp"
#include "../include/nonterminal_enum.hpp"
namespace {0}{{
    namespace slr1_tables{{
        using GOTO_index   = std::pair<std::size_t, std::uint16_t>;

        static const std::map<GOTO_index, std::size_t> gotos = {{
{1}
        }};

        static const std::size_t lengths_of_rules[] = {{
{2}
        }};

        using State_and_terminal = std::pair<std::size_t, Terminal>;

        static const std::map<State_and_terminal, Action> acts = {{
{3}
        }};

        static const Nonterminal targets[] = {{
{4}
        }};

        Action Tables::get_action(std::size_t state, Terminal t) const
        {{
            auto   it     = acts.find({{state, t}});
            Action result = Action{{Action_kind::Error, 0}};
            if(it != acts.end()){{
                result = it->second;
            }}
            return result;
        }}

        std::size_t Tables::get_goto(std::size_t state, std::size_t non_terminal) const
        {{
            std::size_t result = 0;
            auto        it     = gotos.find({{state, non_terminal}});
            if(it != gotos.end()){{
                result = it->second;
            }}
            return result;
        }}

        std::size_t Tables::get_rule_len(std::size_t rule_idx) const
        {{
            return lengths_of_rules[rule_idx];
        }}

        std::size_t Tables::get_rule_target(std::size_t rule_idx) const
        {{
            return static_cast<std::size_t>(targets[rule_idx]);
        }}
    }};
}};)~";


        static const std::string parser_header_fmt = R"~(// File: {0}.hpp
// This file was generated automatically.
#ifndef {1}_H
#define {1}_H
// Preamble:
#   include <memory>
#   include "../include/multipop_stack.hpp"
#   include "../include/terminal_enum.hpp"
{2}
{3}
#endif)~";

        static const std::string parser_interface_fmt = R"~(namespace {0}{{
    using scanner_ptr_t = std::shared_ptr<{1}::{2}>;
{3}
}};)~";

        static const std::string parser_class_fmt = R"~(
    class {0}{{
    public:
{1}
    private:
{2}
    }};)~";

        static const std::string terminal_hpp_fmt = R"~(// File: terminal_enum.hpp
// This file was generated automatically.
#ifndef TERMINAL_ENUM_H
#define TERMINAL_ENUM_H
#   include <cstdint>
namespace {0}{{
    enum class Terminal : std::uint16_t{{
{1}
    }};
}};
#endif)~";

        static const std::string nonterminal_hpp_fmt = R"~(// File: nonterminal_enum.hpp
// This file was generated automatically.
#ifndef NONTERMINAL_ENUM_H
#define NONTERMINAL_ENUM_H
#   include <cstdint>
namespace {0}{{
    enum class Nonterminal : std::uint16_t{{
{1}
    }};
}};
#endif)~";

        static const std::string public_interface_fmt = R"~(        {0}() = default;
        {0}(const {0}& rhs) = default;
        ~{0}() = default;
        {0}(const scanner_ptr_t& scanner{1}) :
            scanner_{{scanner}}{2}
            {{{3}}}
        {4} operator ()();)~";

        static const std::string private_interface_fmt = R"~(        scanner_ptr_t scanner_;
{0}

        struct Stack_elem{{
            std::size_t state_;
            {1} shift_result_;
        }};

        stack::Stack<Stack_elem> parser_stack_;
        Stack_elem               current_rule_body_[{2}];
        std::size_t              current_rule_length_;
        std::size_t              current_rule_target_;

        {5} token_;
        Terminal terminal_;

        {3};
        static Reduce_function reductions_[];

        {1} reduction(std::size_t rule_number);

{4})~";

        static const std::string parser_implementation_fmt = R"~(// File: {0}.cpp
// This file was generated automatically.
// Preamble:
#include <cstdio>
#include "../include/{0}.hpp"
#include "../include/tables.hpp"
#include "../include/diagnostic.hpp"
{2}
namespace {1}{{
// Reduction functions block:
{3}
// Implementation of operator():
{4}
}};)~";

        static const std::string reduction_block_fmt = R"~(    {0}::Reduce_function {0}::reductions_[] = {{
{1}
    }};
{2}

    Shift_result {0}::reduction(std::size_t rule_number){{
        return (this->*reductions_[rule_number - 1])();
    }})~";

        static constexpr std::size_t num_of_columns_for_actions          = 1;
        static constexpr std::size_t num_of_columns_for_gotos            = 5;
        static constexpr std::size_t num_of_columns_for_rules_lengths    = 10;
        static constexpr std::size_t num_of_columns_for_targets          = 3;
        static constexpr std::size_t num_of_columns_for_terminal_enum    = 4;
        static constexpr std::size_t num_of_columns_for_nonterminal_enum = 4;
        static constexpr std::size_t indent_unit                         = 4;
        static constexpr std::size_t num_of_columns_for_reductions       = 2;

        static const char            elem_separator                      = ',';

        static const std::string     symbol_kinds[] = {
            "Terminal", "Non_terminal", "Epsilon"
        };

        static const std::string     action_kinds[] = {
            "Shift", "Reduce", "OK", "Error"
        };

        static const std::string     symbol_fmt = "Symbol{{Symbol_kind::{0}, {1}}}";
        static const std::string     action_fmt = "Action{{Action_kind::{0}, {1}}}";


        static std::string action_to_str(Action action)
        {
            std::string result;
            result = fmt::format(action_fmt,
                                 action_kinds[action.kind_],
                                 std::to_string(action.idx_));
            return result;
        }


        static std::string goto_index_to_str(GOTO_index g_idx)
        {
            std::string result = fmt::format("{{{0}, {1}}}",
                                             std::to_string(g_idx.first),
                                             std::to_string(g_idx.second));
            return result;
        }

        using goto_pair_t = std::pair<GOTO_index, std::size_t>;
        static std::string goto_pair_to_str(const goto_pair_t& p)
        {
            std::string result = fmt::format("{{{0}, {1}}}",
                                             goto_index_to_str(p.first),
                                             std::to_string(p.second));
            return result;
        }

        std::string Parser_builder::gotos_to_str(const gotos_t& gotos) const
        {
            std::string              result;
            std::vector<std::string> gotos_strs;
            for(const auto& g : gotos){
                gotos_strs.push_back(goto_pair_to_str(g));
            }
            Format format{indent_unit * 3, num_of_columns_for_gotos, 1};
            result = strings_to_columns(gotos_strs, format, elem_separator);
            return result;
        }

        static std::string build_content_of_array_of_length_of_rules(const Grammar& g)
        {
            std::string              result;
            std::vector<std::string> strs;
            for(const auto& rule : g.rules_){
                auto& body = rule.body_;
                strs.push_back(std::to_string(body.size()));
            }
            Format format{indent_unit * 3, num_of_columns_for_rules_lengths, 1};
            result = strings_to_columns(strs, format, elem_separator);
            return result;
        }

        std::string Parser_builder::build_content_of_array_of_targets() const
        {
            std::string              result;
            std::vector<std::string> strs;
            for(const auto& rule : grammar_.rules_){
                std::uint16_t concept = rule.concept_;
                strs.push_back("Nonterminal::" + names_of_nonterminals_[concept]);
            }
            Format format{indent_unit * 3, num_of_columns_for_targets, 1};
            result = strings_to_columns(strs, format, elem_separator);
            return result;
        }

        static std::size_t maximal_rule_length(const Grammar& g)
        {
            std::size_t result = 0;
            for(const auto& rule : g.rules_){
                auto&       body = rule.body_;
                std::size_t len  = body.size();
                if(len > result){
                    result = len;
                }
            }
            return result;
        }

        static std::set<Action_index> get_clashes(const Parser_table& table)
        {
            std::set<Action_index> result;
            for(const auto& p : table.actions_){
                const auto& s = p.second;
                const auto  i = p.first;
                if(s.size() != 1){
                    result.insert(i);
                }
            }
            return result;
        }

        std::string Parser_builder::error_info_to_string(Error_info& einfo)
        {
            std::string result;
            std::string clashes_str;
            auto        tables_str   = grammar_builder_.to_string(einfo.tables_, grammar_);
            auto&       clashes      = einfo.clashes_;
            auto&       action_table = einfo.tables_.actions_;

            auto        action_lambda = [this](Action action) -> std::string{
                return grammar_builder_.to_string(action);
            };

            for(const auto& clash : clashes){
                auto actions          = action_table[clash];
                std::string clash_str = "Clash for [" + std::to_string(clash.first) + ", " +
                                        grammar_builder_.to_string(clash.second) + "is {"  +
                                        join(action_lambda,
                                             actions.begin(),
                                             actions.end(),
                                             std::string{", "}) +
                                        "}\n";
                clashes_str += clash_str;
            }
            result = clashes_str + "\n\n" + tables_str;
            return result;
        }

        std::string Parser_builder::build_public_parser_class_interface() const
        {
            std::string result;
            auto&       parser_class_name       = parser_class_details_.parser_class_name_;
            auto&       ctor_body               = parser_class_details_.ctor_body_;
            auto&       parsing_result_type     = parser_class_details_.parsing_result_type_;
            auto        additional_initializers = parser_class_details_.initializers_for_ctor_;
            if(!additional_initializers.empty()){
                additional_initializers = "," + additional_initializers;
            }
            auto        additional_args         = parser_class_details_.additional_ctor_args_;
            if(!additional_args.empty()){
                additional_args = "," + additional_args;
            }
            result = fmt::format(public_interface_fmt,
                                 parser_class_name,
                                 additional_args,
                                 additional_initializers,
                                 ctor_body,
                                 parsing_result_type);
            return result;
        }

        std::string Parser_builder::build_private_parser_class_interface() const
        {
            auto&       additional_members = parser_class_details_.additional_parser_members_;
            auto&       shift_result_type  = parser_class_details_.shift_function_result_type_;
            auto&       parser_class_name  = parser_class_details_.parser_class_name_;
            auto&       token_type         = parser_class_details_.token_type_;
            std::string typedef_str        = "typedef " + shift_result_type + " (" +
                                             parser_class_name + "::*Reduce_function)()";
            std::size_t max_len            = maximal_rule_length(grammar_);
            std::string protos             = build_reduction_functions_protos();
            std::string result             = fmt::format(private_interface_fmt,
                                                         additional_members,
                                                         shift_result_type,
                                                         std::to_string(max_len),
                                                         typedef_str,
                                                         protos,
                                                         token_type);
            return result;
        }

        std::string Parser_builder::build_reduction_functions_protos() const
        {
            std::string            result;
            std::size_t            num_of_rules       = grammar_info_.rules_.size();
            std::list<std::string> protos;
            auto&                  shift_result_type  = parser_class_details_.shift_function_result_type_;
            for(std::size_t i = 1; i <= num_of_rules; ++i){
                std::string proto =  std::string(indent_unit * 2, ' ') +
                                     fmt::format("{0} reduce{1}",
                                                 shift_result_type,
                                                 std::to_string(i))    +
                                     "();";
                protos.push_back(proto);
            }
            result = join(protos.begin(), protos.end(), std::string{"\n"});
            return result;
        }


        std::string Parser_builder::build_reduction_function_array() const
        {
            std::string result;
            std::vector<std::string> func_ptrs;
            std::size_t              num_of_rules      = grammar_info_.rules_.size();
            auto&                    parser_class_name = parser_class_details_.parser_class_name_;
            for(std::size_t i = 1; i <= num_of_rules; ++i){
                std::string func_ptr = "&" + parser_class_name + "::reduce" + std::to_string(i);
                func_ptrs.push_back(func_ptr);
            }
            Format f{indent_unit * 2, num_of_columns_for_reductions, 1};
            result = strings_to_columns(func_ptrs, f, elem_separator);
            return result;
        }

        static const std::string reduction_func_fmt = R"~(
    {0}
    {{
        {1} result;
        {2}
        return result;
    }})~";


        std::string Parser_builder::build_reduction_functions() const
        {
            std::string result;
            auto&                  actions           = parser_class_details_.reduce_actions_;
            auto&                  parser_class_name = parser_class_details_.parser_class_name_;
            auto&                  shift_result_type = parser_class_details_.shift_function_result_type_;
            std::size_t            num_of_actions    = actions.size();
            std::size_t            num_of_rules      = grammar_info_.rules_.size();
            std::list<std::string> funcs;
            for(std::size_t i = 1; i <= num_of_rules; ++i){
                std::string func_body;
                if(i - 1 < num_of_actions){
                    func_body = actions[i - 1];
                }
                std::string func_header = fmt::format("{0} {1}::reduce{2}",
                                                      shift_result_type,
                                                      parser_class_name,
                                                      std::to_string(i)) + "()";
                std::string func = fmt::format(reduction_func_fmt,
                                               func_header,
                                               shift_result_type,
                                               func_body);
                funcs.push_back(func);
            }
            result = join(funcs.begin(), funcs.end(), std::string{"\n"});
            return result;
        }

        std::string Parser_builder::build_reduction_functions_block() const
        {
            auto&       parser_class_name = parser_class_details_.parser_class_name_;
            std::string reduction_array   = build_reduction_function_array();
            std::string reductions_impl   = build_reduction_functions();
            std::string result            = fmt::format(reduction_block_fmt,
                                                        parser_class_name,
                                                        reduction_array,
                                                        reductions_impl);
            return result;
        }

        static const std::string call_operator_impl_fmt = R"~(
    {1} {0}::operator ()(){{
        {1} result;
        Stack_elem initial_elem_;
        initial_elem_.state_ = 0;
        parser_stack_.push(initial_elem_);

        slr1_tables::Tables parsing_tables;
        Diagnostic          diagnostic;

        for( ; ; ){{
            token_                    = scanner_->current_lexeme();
            terminal_                 = {3};
            std::size_t current_state = parser_stack_.top().state_;
            auto        action        = parsing_tables.get_action(current_state, terminal_);
{2}
        }}
        return result;
    }})~";

        static const std::string operator_call_body_fmt = R"~(
            switch(action.kind_){{
                case Shift:
                    {{
                        Stack_elem elem;
                        elem.state_        = action.idx_;
                        elem.shift_result_ = {0};
                        parser_stack_.push(elem);
                    }}
                    break;
                case Reduce:
                    {{
                        current_rule_length_ = parsing_tables.get_rule_len(action.idx_);
                        current_rule_target_ = parsing_tables.get_rule_target(action.idx_);

                        parser_stack_.ntop(current_rule_body_, current_rule_length_);

                        Stack_elem elem;
                        elem.shift_result_   = reduction(action.idx_);

                        parser_stack_.npop(current_rule_length_);

                        Stack_elem top_elem  = parser_stack_.top();
                        elem.state_          = parsing_tables.get_goto(top_elem.state_, current_rule_target_);
                        parser_stack_.push(elem);
                    }}
                    break;
                case OK:
{1}
                    return result;
                    break;
                case Error:
                    {{
                        auto msg = diagnostic.get_diagnostic_msg(current_state, terminal_);
                        printf("Ошибка в строке %zu, позиции %zu: %s\n",
                               token_.range_.begin_pos_.line_no_,
                               token_.range_.begin_pos_.line_pos_,
                               msg.c_str());
                        {2}
                        return result;
                    }}
                    break;
            }})~";


        std::string Parser_builder::build_call_operator() const
        {
            auto&       parser_class_name   = parser_class_details_.parser_class_name_;
            auto&       parsing_result_type = parser_class_details_.parsing_result_type_;
            auto&       conversion_func     = parser_class_details_.token_to_terminal_conversion_function_name_;
            auto&       shift_function_name = parser_class_details_.shift_function_name_;
            std::string token_conversion    = conversion_func + "(token_)";
            auto&       postprocessing      = parser_class_details_.postprocessing_;
            auto&       user_error_handling = parser_class_details_.user_error_handling_;
            std::string operator_call_body  = fmt::format(operator_call_body_fmt,
                                                          shift_function_name + "(token_)",
                                                          postprocessing,
                                                          user_error_handling);
            std::string result              = fmt::format(call_operator_impl_fmt,
                                                          parser_class_name,
                                                          parsing_result_type,
                                                          operator_call_body,
                                                          token_conversion);
            return result;
        }

        std::string Parser_builder::build_parser_implementation() const
        {
            auto&       parser_namespace_name   = parser_class_details_.parser_namespace_name_;
            auto&       parser_impl_preamble    = parser_class_details_.parser_impl_preamble_;
            std::string reduction_block         = build_reduction_functions_block();
            std::string call_operator_impl      = build_call_operator();
            std::string result                  = fmt::format(parser_implementation_fmt,
                                                              lower_case_name_,
                                                              parser_namespace_name,
                                                              parser_impl_preamble,
                                                              reduction_block,
                                                              call_operator_impl);
            return result;
        }

        std::string Parser_builder::build_parser_class_interface() const
        {
            std::string result;
            auto  public_interface  = build_public_parser_class_interface();
            auto  private_interface = build_private_parser_class_interface();
            auto& parser_class_name = parser_class_details_.parser_class_name_;
            result                  = fmt::format(parser_class_fmt,
                                                  parser_class_name,
                                                  public_interface,
                                                  private_interface);
            return result;
        }

        std::string Parser_builder::build_parser_interface() const
        {
            std::string result;
            std::string parser_class_interface  = build_parser_class_interface();
            auto&       parser_namespace_name   = parser_class_details_.parser_namespace_name_;
            auto&       scanner_class_name      = parser_class_details_.scanner_class_name_;
            auto&       scanner_namespace_name  = parser_class_details_.scanner_namespace_name_;
            result                              = fmt::format(parser_interface_fmt,
                                                              parser_namespace_name,
                                                              scanner_namespace_name,
                                                              scanner_class_name,
                                                              parser_class_interface);
            return result;
        }

        void Parser_builder::build_names_of_terminals()
        {
            names_of_terminals_.clear();
            names_of_terminals_.push_back("End_of_text");
            auto& grammar_terminals = grammar_info_.names_info_.terminals_mames_;
            names_of_terminals_.insert(names_of_terminals_.end(),
                                       grammar_terminals.begin(),
                                       grammar_terminals.end());
        }

        void Parser_builder::build_names_of_nonterminals()
        {
            names_of_nonterminals_.clear();
            names_of_nonterminals_.push_back("fictive");
            auto& grammar_nonterminals = grammar_info_.names_info_.nonterminals_mames_;
            names_of_nonterminals_.insert(names_of_nonterminals_.end(),
                                          grammar_nonterminals.begin(),
                                          grammar_nonterminals.end());
        }

        std::string Parser_builder::build_terminal_hpp() const
        {
            Format f{indent_unit * 2, num_of_columns_for_terminal_enum, 1};

            std::string enum_class_content    = strings_to_columns(names_of_terminals_, f, elem_separator);
            auto&       parser_namespace_name = parser_class_details_.parser_namespace_name_;
            std::string result                = fmt::format(terminal_hpp_fmt, parser_namespace_name, enum_class_content);
            return result;
        }

        std::string Parser_builder::build_nonterminal_hpp() const
        {
            Format f{indent_unit * 2, num_of_columns_for_nonterminal_enum, 1};

            std::string enum_class_content    = strings_to_columns(names_of_nonterminals_, f, elem_separator);
            auto&       parser_namespace_name = parser_class_details_.parser_namespace_name_;
            std::string result                = fmt::format(nonterminal_hpp_fmt, parser_namespace_name, enum_class_content);
            return result;
        }

        std::string Parser_builder::build_parser_header_str() const
        {
            std::string result;
            auto& parser_class_name = parser_class_details_.parser_class_name_;
            auto  upper_case_name   = to_upper(parser_class_name);
            auto  parser_interface  = build_parser_interface();
            auto& preamble          = parser_class_details_.parser_header_preamble_;
            result                  = fmt::format(parser_header_fmt,
                                                  lower_case_name_,
                                                  upper_case_name,
                                                  preamble,
                                                  parser_interface);
            return result;
        }


        std::string Parser_builder::acts_pair_to_str(const action_pair_t& p) const
        {
            auto        index        = p.first;
            auto        act          = p.second;
            std::string index_as_str = fmt::format("{{{0}, Terminal::{1}}}",
                                                   std::to_string(index.first),
                                                   names_of_terminals_[index.second.code_]);
            std::string result       = fmt::format("{{{0}, {1}}}",
                                                   index_as_str,
                                                   action_to_str(act));
            return result;
        }

        std::string Parser_builder::build_acts(const actions_t& actions) const
        {
            std::string result;
            std::vector<std::string> actions_strs;
            for(const auto& a : actions){
                action_pair_t p{a.first, *(a.second.begin())};
                actions_strs.push_back(acts_pair_to_str(p));
            }
            Format format{indent_unit * 3, num_of_columns_for_actions, 1};
            result = strings_to_columns(actions_strs, format, elem_separator);
            return result;
        }

        Parser_builder::bitscales_t Parser_builder::build_missing_action_indices(const Parser_table& t) const
        {

            std::size_t                 num_of_states = t.item_collections_.size();
            Parser_builder::bitscales_t result        = Parser_builder::bitscales_t(num_of_states);
            std::size_t                 bitset_size   = grammar_builder_.get_number_of_terminals();
            for(auto& b : result){
                b = bitscale::bitset{bitset_size};
            }

            auto actions_table = t.actions_;
            for(const auto& p : actions_table){
                auto        index  = p.first;
                Symbol      symbol = index.second;
                std::size_t state  = index.first;
                auto&       b      = result[state];
                b[symbol.code_]    = true;
            }
            return result;
        }

        Parser_builder::messages_t Parser_builder::build_messages(const bitscales_t& bits) const
        {
            Parser_builder::messages_t result           = Parser_builder::messages_t(bits.size());
            std::size_t                num_of_terminals = grammar_builder_.get_number_of_terminals();
            std::size_t                i                = 0;
            for(const auto& b : bits){
                std::list<std::string> message_parts;
                for(std::size_t j = 1; j < num_of_terminals; ++j){
                    if(!b[j]){
                        continue;
                    }
                    auto& name = names_of_terminals_[j];
                    auto  it   = parser_class_details_.terminal_to_msg_.find(name);
                    if(it != parser_class_details_.terminal_to_msg_.end()){
                        message_parts.push_back(it->second);
                    }
                }
                result[i] = "ожидается что-либо из следующего: " +
                            join(message_parts.begin(), message_parts.end(), std::string{", "});
                ++i;
            }
            return result;
        }


        static const std::string diagnostic_messages_header_fmt = R"~(// File: diagnostic.hpp
// This file was generated automatically.
#ifndef DIAGNOSTIC_H
#define DIAGNOSTIC_H
#   include <cstddef>
#   include <string>
#   include "../include/terminal_enum.hpp"
namespace {0}{{
    class Diagnostic final{{
    public:
        Diagnostic()                      = default;
        Diagnostic(const Diagnostic& rhs) = default;
        ~Diagnostic()                     = default;

        std::string get_diagnostic_msg(std::size_t state, Terminal t) const;
    }};
}};
#endif)~";

        static const std::string diagnostic_messages_impl_fmt0 = R"~(// File: diagnostic.cpp
// This file was generated automatically.
#include "../include/diagnostic.hpp"
namespace {0}{{
    std::string Diagnostic::get_diagnostic_msg(std::size_t, Terminal) const
    {{
        return "Синтаксическая ошибка.";
    }}
}};)~";

        static const std::string diagnostic_messages_impl_fmt1 = R"~(// File: diagnostic.cpp
// This file was generated automatically.
#include "../include/diagnostic.hpp"
namespace {0}{{
    static const std::string messages_info[] = {{
{1}
    }};

    std::string Diagnostic::get_diagnostic_msg(std::size_t state, Terminal t) const
    {{
        auto&       msg_info = messages_info[state];
        std::string result   = "Неожиданный конец исходного файла.";
        if(t == Terminal::End_of_text){{
            return result;
        }}
        if(msg_info.empty()){{
            return "Синтаксическая ошибка.";
        }}
        return msg_info;
    }}
}};)~";

        std::string Parser_builder::build_diagnostic_messages_header() const
        {
            std::string result = fmt::format(diagnostic_messages_header_fmt,
                                             parser_class_details_.parser_namespace_name_);
            return result;
        }

        std::string Parser_builder::build_diagnostic_messages_impl(const Parser_table& t) const
        {
            auto        bitscales             = build_missing_action_indices(t);
            auto        messages              = build_messages(bitscales);
            std::string messages_array_as_str = join([](const std::string& s) -> std::string{
                                                        return std::string(indent_unit * 2, ' ') + "\"R~(" + s + ")~\"";
                                                     },
                                                     messages.begin(),
                                                     messages.end(),
                                                     std::string{",\n"});
            auto&       parser_namespace_name = parser_class_details_.parser_namespace_name_;
            std::string result                = fmt::format(diagnostic_messages_impl_fmt1,
                                                            parser_namespace_name,
                                                            messages_array_as_str);
            return result;
        }

        Parser_builder_result Parser_builder::operator ()()
        {
            Generated_texts result;
            correct_names();
            build_names_of_terminals();
            build_names_of_nonterminals();

            result.include_["action.hpp"]              = fmt::format(action_hpp_content_fmt,
                                                                     parser_class_details_.parser_namespace_name_);
            result.include_["tables.hpp"]              = fmt::format(table_class_header_fmt,
                                                                     parser_class_details_.parser_namespace_name_);
            grammar_                                   = grammar_builder_();

            Parser_tables_builder tables_builder{grammar_};

            auto actions_and_gotos                     = tables_builder.build_actions_and_gotos();
            auto clashes                               = get_clashes(actions_and_gotos);
            if(!clashes.empty()){
                Error_info einfo;
                einfo.tables_  = actions_and_gotos;
                einfo.clashes_ = clashes;
                return einfo;
            }

            auto gotos_as_str                          = gotos_to_str(actions_and_gotos.gotos_);
            auto rules_lengths_as_str                  = build_content_of_array_of_length_of_rules(grammar_);
            auto targets_as_str                        = build_content_of_array_of_targets();
            auto acts_str                              = build_acts(actions_and_gotos.actions_);
            auto tables_cpp_content                    = fmt::format(table_class_impl_fmt,
                                                                     parser_class_details_.parser_namespace_name_,
                                                                     gotos_as_str,
                                                                     rules_lengths_as_str,
                                                                     acts_str,
                                                                     targets_as_str);
            result.src_["tables.cpp"]                  = tables_cpp_content;

            result.include_[lower_case_name_ + ".hpp"] = build_parser_header_str();

            result.include_["terminal_enum.hpp"]       = build_terminal_hpp();
            result.include_["nonterminal_enum.hpp"]    = build_nonterminal_hpp();

            result.src_[lower_case_name_ + ".cpp"]     = build_parser_implementation();

            for(const auto& p : parser_class_details_.additional_includes_){
                result.include_[p.first] = p.second;
            }

            for(const auto& p : parser_class_details_.additional_srcs_){
                result.src_[p.first] = p.second;
            }

            result.include_["multipop_stack.hpp"]      = multipop_stack_hpp;

            result.include_["diagnostic.hpp"]          = build_diagnostic_messages_header();

            if(!parser_class_details_.terminal_to_msg_.empty()){
                result.src_["diagnostic.cpp"] = build_diagnostic_messages_impl(actions_and_gotos);
            }else{
                result.src_["diagnostic.cpp"] = fmt::format(diagnostic_messages_impl_fmt0,
                                                            parser_class_details_.parser_namespace_name_);
            }
            return result;
        }
    };
};