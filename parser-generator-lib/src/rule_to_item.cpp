/*
    File:    rule_to_item.cpp
    Created: 01 May 2020 at 21:19 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/rule_to_item.hpp"

namespace parser_generator{
    namespace slr1{
        Item rule_to_item(std::size_t rule_idx, const Grammar& grammar)
        {
            Item result{rule_idx, 0};

            const auto& rule   = grammar.rules_[rule_idx];
            const auto& body   = rule.body_;

            if(body.empty()){
                return result;
            }

            const auto& symbol = body[0];

            if(symbol.kind_ != Symbol_kind::Epsilon){
                return result;
            }

            result.dot_location_ = 1;

            return result;
        }
    };
};