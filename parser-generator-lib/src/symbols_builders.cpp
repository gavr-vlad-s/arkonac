/*
    File:    symbols_builders.cpp
    Created: 23 May 2020 at 14:03 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <stdexcept>
#include "../include/symbols_builders.hpp"
#include "../../strings-lib/include/join.h"

namespace parser_generator{
    Symbols_builder::Symbols_builder(const Names_info& names_info){
        terminal_names_.emplace_back("⊥");
        terminal_names_.insert(terminal_names_.end(),
                               names_info.terminals_mames_.begin(),
                               names_info.terminals_mames_.end());

        std::uint16_t idx = 0;
        for(const auto& name : terminal_names_){
            terminal_name_to_code_[name] = idx++;
        }

        nonterminal_names_.emplace_back(names_info.target_name_ + "\'");
        nonterminal_names_.insert(nonterminal_names_.end(),
                                  names_info.nonterminals_mames_.begin(),
                                  names_info.nonterminals_mames_.end());

        idx = 0;
        for(const auto& name : nonterminal_names_){
            nonterminal_name_to_code_[name] = idx++;
        }
    }

    std::size_t Symbols_builder::get_number_of_nonterminals() const{
        return nonterminal_names_.size();
    }

    std::size_t Symbols_builder::get_number_of_terminals() const{
        return terminal_names_.size();
    }

    Symbol Symbols_builder::build_symbol(const Named_symbol& symbol) const{
        Symbol result = {Symbol_kind::Epsilon, 0};
        switch(symbol.kind_){
            case Kind::Non_terminal:
                {
                    result = {Symbol_kind::Non_terminal, 0};
                    auto it = nonterminal_name_to_code_.find(symbol.name_);
                    if(it != nonterminal_name_to_code_.end()){
                        result.code_ = it->second;
                    }else{
                        throw std::logic_error("Undefined name of nonterminal: " + symbol.name_);
                    }
                }
                break;
            case Kind::Terminal:
                {
                    result = {Symbol_kind::Terminal, 0};
                    auto it = terminal_name_to_code_.find(symbol.name_);
                    if(it != terminal_name_to_code_.end()){
                        result.code_ = it->second;
                    }else{
                        throw std::logic_error("Undefined name of terminal: " + symbol.name_);
                    }
                }
                break;
            case Kind::Epsilon:
                ;
        }
        return result;
    }

    std::string Symbols_builder::to_string(const Symbol& symbol) const {
        std::string   result;
        std::uint16_t code   = symbol.code_;
        switch(symbol.kind_){
            case Symbol_kind::Terminal:
                {
                    if(code < terminal_names_.size()){
                        result = "Terminal " + terminal_names_[code];
                    }else{

                        throw std::out_of_range("Code " + std::to_string(code) + " of terminal is too large.");
                    }

                }
                break;
            case Symbol_kind::Non_terminal:
                {
                    if(code < nonterminal_names_.size()){
                        result = "NonTerminal " + nonterminal_names_[code];
                    }else{

                        throw std::out_of_range("Code " + std::to_string(code) + " of nonterminal is too large.");
                    }

                }
                break;
            case Symbol_kind::Epsilon:
                result = "Epsilon";
                break;
        }
        return result;
    }

    std::string Symbols_builder::to_string(const std::set<Symbol> &symbols) const{
        std::string result;
        auto body_str = join([this](Symbol s) -> std::string{return to_string(s);},
                             symbols.begin(),
                             symbols.end(),
                             std::string{", "});
        result        = "{" + body_str + "}";
        return result;
    }
};
