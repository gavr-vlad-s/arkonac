/*
    File:    parser_tables_builder.cpp
    Created: 23 May 2020 at 14:20 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/parser_tables_builder.hpp"
#include "../include/sets_first.hpp"
#include "../include/sets_follows.hpp"

namespace parser_generator{
    namespace slr1{

        Items_map Parser_tables_builder::build_item_map()
        {
            Items_map result;

            std::map<Items_collection, std::size_t> item_to_idx_map;
            std::map<Table_index, std::size_t>      table;

            std::vector<Items_collection>           marked_states;
            std::vector<Items_collection>           unmarked_states;
            std::size_t                             current_nom_of_state = 0;

            auto                                    initial_item         = Item{0, 0};
            auto                                    initial_collection   = Items_collection{initial_item};
            auto                                    initial_closure      = closure_builder_(initial_collection);

            item_to_idx_map[initial_closure] = current_nom_of_state;

            current_nom_of_state++;
            unmarked_states.push_back(initial_closure);

            while(!unmarked_states.empty()){
                auto t                = unmarked_states.back();
                marked_states.push_back(t);
                unmarked_states.pop_back();
                auto transitions      = get_transitions(t);
                for(const auto& transition : transitions){
                    auto s     = transition.first;
                    auto items = transition.second;
                    auto u     = closure_builder_(move(items, s));
                    if(!u.empty()){
                        auto        it    = item_to_idx_map.find(u);
                        std::size_t t_idx = item_to_idx_map[t];
                        if(it == item_to_idx_map.end()){
                            unmarked_states.push_back(u);
                            item_to_idx_map[u] = current_nom_of_state;
                            table[{t_idx, s}]  = current_nom_of_state;
                            current_nom_of_state++;
                        }else{
                            std::size_t u_idx  = it->second;
                            table[{t_idx, s}]  = u_idx;
                        }
                    }
                }
            }

            std::size_t num_of_states = item_to_idx_map.size();
            result.item_map_          = table;
            result.item_collections_  = std::vector<Items_collection>(num_of_states);
            for(const auto& c : item_to_idx_map){
                result.item_collections_[c.second] = c.first;
            }

            return result;
        }

        Parser_tables_builder::transitions_t Parser_tables_builder::get_transitions(const collection_t& collection) const
        {
            transitions_t result;
            for(const auto& item : collection){
                std::size_t idx_of_rule  = item.idx_of_rule_;
                std::size_t dot_location = item.dot_location_;
                auto&       rule         = grammar_.rules_[idx_of_rule];
                auto&       body         = rule.body_;
                std::size_t body_length  = body.size();

                if(body.empty() || (dot_location >= body_length)){
                    continue;
                }

                Symbol symbol = body[dot_location];
                if(symbol.kind_ != Symbol_kind::Epsilon){
                    result[symbol].insert(item);
                }
            }
            return result;
        }

        collection_t Parser_tables_builder::move(const collection_t& collection, const Symbol& s) const
        {
            collection_t result;
            for(const auto& item : collection){
                std::size_t idx_of_rule  = item.idx_of_rule_;
                std::size_t dot_location = item.dot_location_;
                auto&       rule         = grammar_.rules_[idx_of_rule];
                auto&       body         = rule.body_;
                std::size_t body_length  = body.size();

                if(body.empty() || (dot_location >= body_length)){
                    continue;
                }

                Symbol symbol = body[dot_location];
                if(symbol == s){
                    result.insert(Item{item.idx_of_rule_, item.dot_location_ + 1});
                }
            }
            return result;
        }

        bool operator < (Action lhs, Action rhs)
        {
            if(lhs.kind_ != rhs.kind_){
                return lhs.kind_ < rhs.kind_;
            }

            return lhs.idx_ < rhs.idx_;
        }

        Parser_table Parser_tables_builder::build_actions_and_gotos()
        {
            Parser_table result;
            auto item_map = build_item_map();

            Symbol end_of_text{Symbol_kind::Terminal, 0};

            result.item_collections_ = item_map.item_collections_;
            for(const auto& e : item_map.item_map_){
                const auto& index      = e.first;
                std::size_t from_state = index.first;
                const auto& symbol     = index.second;
                std::size_t to_state   = e.second;

                // Creating records with action Shift.
                switch(symbol.kind_){
                    case Symbol_kind::Non_terminal:
                        result.gotos_[{from_state, symbol.code_}] = to_state;
                        break;
                    case Symbol_kind::Terminal:
                        result.actions_[index].insert(Action{Action_kind::Shift,
                                                             static_cast<std::uint16_t>(to_state & 0x3FFF)});
                        break;
                    case Symbol_kind::Epsilon:
                        break;
                }
            }

            // Creating records with action Reduce.
            Sets_first  firsts_builder{grammar_};
            auto        firsts         = firsts_builder();
            Sets_follow follows_builder{grammar_, firsts};
            auto        follows        = follows_builder();
            std::size_t collection_idx = 0;

            for(const auto& collection : item_map.item_collections_){
                for(const auto& item : collection){
                    const std::size_t idx_of_rule  = item.idx_of_rule_;
                    const auto&       rule         = grammar_.rules_[idx_of_rule];
                    const auto&       body         = rule.body_;

                    if(body.empty()){
                        continue;
                    }

                    std::size_t       body_length  = body.size();
                    std::size_t       dot_location = item.dot_location_;

                    if(dot_location < body_length){
                        continue;
                    }

                    std::uint16_t     concept      = rule.concept_;
                    if(concept == 0){
                        result.actions_[{collection_idx, end_of_text}].insert(Action{Action_kind::OK, 0});
                    }else{
                        const auto& set_follow   = follows[concept];
                        for(const auto& s : set_follow){
                            Action inserted_action = Action{Action_kind::Reduce,
                                                            static_cast<std::uint16_t>(idx_of_rule & 0x3FFF)};
                            result.actions_[{collection_idx, s}].insert(inserted_action);
                        }
                    }
                }
                collection_idx++;
            }

            return result;
        }
    };
};