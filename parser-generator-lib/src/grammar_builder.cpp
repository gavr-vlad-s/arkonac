/*
    File:    grammar_builder.cpp
    Created: 23 May 2020 at 13:48 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <list>
#include "../include/grammar_builder.hpp"
#include "../../strings-lib/include/join.h"

namespace parser_generator{
    void Grammar_builder::init(const Grammar_info& grammar_info)
    {
        auto&  named_rules = grammar_info.rules_;
        auto&  target_name = grammar_info.names_info_.target_name_;
        Symbol target      = symbols_builder_.build_symbol(Named_symbol{Kind::Non_terminal, target_name});

        target_code_       = target.code_;

        rules_.push_back(Rule{0, {target}});

        for(const auto& named_rule : named_rules){
            auto& concept_name = named_rule.concept_name_;
            auto& body         = named_rule.body_;
            Rule  rule;

            rule.concept_ = symbols_builder_.build_symbol(Named_symbol{Kind::Non_terminal, concept_name}).code_;
            for(const auto& named_symbol : body){
                Symbol symbol {Symbol_kind::Epsilon, 0};
                switch(named_symbol.kind_){
                    case Kind::Terminal:
                        symbol = symbols_builder_.build_symbol({Kind::Terminal, named_symbol.name_});
                        break;
                    case Kind::Non_terminal:
                        symbol = symbols_builder_.build_symbol({Kind::Non_terminal, named_symbol.name_});
                        break;
                    case Kind::Epsilon:
                        ;
                }
                rule.body_.push_back(symbol);
            }
            rules_.push_back(rule);
        }
    }

    Grammar Grammar_builder::operator()() const
    {
        Grammar grammar;
        grammar.rules_               = rules_;
        grammar.num_of_nonterminals_ = symbols_builder_.get_number_of_nonterminals();
        grammar.num_of_terminals_    = symbols_builder_.get_number_of_terminals();
        grammar.target_              = target_code_;
        return grammar;
    }

    std::string Grammar_builder::to_string(const Grammar& grammar) const
    {
        std::string result;
        auto target_str = symbols_builder_.to_string(Symbol{Symbol_kind::Non_terminal, grammar.target_});
        auto rules_str  = join([this](const Rule& rule) -> auto {return to_string(rule);},
                               grammar.rules_.begin(),
                               grammar.rules_.end(),
                               std::string{"\n"});
        result = "Target non-terminal: "   + target_str                                    +
                 "\nRules: \n"             + rules_str                                     +
                 "\nTotal nonterminals: "  + std::to_string(grammar.num_of_nonterminals_)  +
                 "\nTotal terminals:    "  + std::to_string(grammar.num_of_terminals_)     +
                 "\n\n";
        return result;
    }

    std::string Grammar_builder::to_string(const Rule& rule) const
    {
        std::string concept_str = symbols_builder_.to_string(Symbol{Symbol_kind::Non_terminal, rule.concept_}) +
                                  " --> ";
        auto        body_str    = join([this](Symbol symbol) -> auto {return symbols_builder_.to_string(symbol);},
                                       rule.body_.begin(),
                                       rule.body_.end(),
                                       std::string{" "});
        return concept_str + "[" + body_str + "]";
    }

    std::string Grammar_builder::to_string(const Symbol& symbol) const
    {
        return symbols_builder_.to_string(symbol);
    }

    std::string Grammar_builder::to_string(const std::set<Symbol>& symbols) const
    {
        return symbols_builder_.to_string(symbols);
    }

    std::string Grammar_builder::to_string(const std::vector<std::set<Symbol>>& sets, const std::string& prefix) const
    {
        std::string            result;
        std::list<std::string> firsts;
        std::uint16_t          idx    = 0;
        for(const auto& set : sets){
            std::string str = prefix                                                             + "("    +
                              symbols_builder_.to_string(Symbol{Symbol_kind::Non_terminal, idx}) + ") = " +
                              symbols_builder_.to_string(set);
            firsts.push_back(str);
            ++idx;
        }
        result = join(firsts.begin(), firsts.end(), std::string{"\n"});
        return result;
    }

    std::string Grammar_builder::to_string(const slr1::Item& item, const Grammar& grammar) const
    {
        std::string result;

        std::size_t   idx_of_rule  = item.idx_of_rule_;
        const auto&   rule         = grammar.rules_[idx_of_rule];
        const auto&   body         = rule.body_;
        std::uint16_t concept      = rule.concept_;
        std::size_t   dot_location = item.dot_location_;
        auto          it_begin     = body.begin();
        auto          it_end       = body.end();

        std::string   before_dot   = join([this](Symbol symbol) -> auto {return to_string(symbol);},
                                          it_begin,
                                          it_begin + dot_location,
                                          std::string{" "});

        std::string   after_dot    = join([this](Symbol symbol) -> auto {return to_string(symbol);},
                                          it_begin + dot_location,
                                          it_end,
                                          std::string{" "});

        std::string   str_for_body;
        if(!before_dot.empty()){
            str_for_body = before_dot + " ●";
        }else{
            str_for_body = "●";
        }

        if(!after_dot.empty()){
            str_for_body += " " + after_dot;
        }

        Symbol concept_symbol {Symbol_kind::Non_terminal, concept};
        result = "[" + symbols_builder_.to_string(concept_symbol) + " --> " + str_for_body + "]";

        return result;
    }

    std::string Grammar_builder::to_string(const slr1::Items_collection& items, const Grammar& grammar) const
    {
        std::string result;
        auto body_str = join([this, &grammar](const auto& item) -> std::string{return this->to_string(item, grammar);},
                             items.begin(),
                             items.end(),
                             std::string{", "});
        result = "{" + body_str + "}";
        return result;
    }

    std::string Grammar_builder::to_string(const slr1::Table_index& index) const
    {
        std::string result;
        result = "I_{" + std::to_string(index.first) + "}, " + symbols_builder_.to_string(index.second);
        return result;
    }

    std::string Grammar_builder::to_string(const slr1::Items_map& items_map, const Grammar& grammar) const
    {
        std::string result;

        auto        pair_to_str = [this](const std::pair<slr1::Table_index, std::size_t>& p) -> std::string{
            std::string str;
            str = "GOTO(" + to_string(p.first) + ") = I_{" + std::to_string(p.second) + "}";
            return str;
        };

        const auto& table        = items_map.item_map_;
        std::string item_map_str = join(pair_to_str, table.begin(), table.end(), std::string{"\n"});
        const auto& collections  = items_map.item_collections_;
        std::size_t idx          = 0;

        std::list<std::string> collections_strs;

        for(const auto& collection : collections){
            auto collection_str = "I_{" + std::to_string(idx) + "} = " + to_string(collection, grammar);
            collections_strs.push_back(collection_str);
            idx++;
        }

        result = item_map_str + "\n\n" + join(collections_strs.begin(), collections_strs.end(), std::string{"\n"});
        return result;
    }

    static const std::string action_kinds[] = {
        "Shift ", "Reduce ", "OK "
    };

    std::string Grammar_builder::to_string(slr1::Action action) const
    {
        std::string result;
        result = action_kinds[action.kind_] + std::to_string(action.idx_);
        return result;
    }

    std::string Grammar_builder::to_string(const slr1::Parser_table& parser_table, const Grammar& grammar) const
    {
        std::string result;

        using actions_t = std::set<slr1::Action>;

        auto actions_set_to_str  = [this](const actions_t& set){
            std::string str;
            std::string body_str = join([this](slr1::Action action) -> std::string{return this->to_string(action);},
                                        set.begin(),
                                        set.end(),
                                        std::string{", "});
            str = "{" + body_str + "}";
            return str;
        };

        auto act_pair_to_str     = [this, &actions_set_to_str](const std::pair<slr1::Action_index, actions_t>& p){
            std::string str;
            str = "ACTION[" + to_string(p.first) + "] = " + actions_set_to_str(p.second);
            return str;
        };

        auto actions_to_str      = [&act_pair_to_str](const std::map<slr1::Action_index, actions_t>& actions){
            std::string str;
            str = join(act_pair_to_str, actions.begin(), actions.end(), std::string{"\n"});
            return str;
        };

        auto goto_index_to_str   = [this](const slr1::GOTO_index& g_idx){
            Symbol symbol{Symbol_kind::Non_terminal, g_idx.second};
            return std::to_string(g_idx.first) + ", " + symbols_builder_.to_string(symbol);
        };

        auto goto_pair_to_str    = [&goto_index_to_str](const std::pair<slr1::GOTO_index, std::size_t>& p){
            std::string str;
            str = "GOTO[" + goto_index_to_str(p.first) + "] = " + std::to_string(p.second);
            return str;
        };

        auto gotos_to_str        = [&goto_pair_to_str](const std::map<slr1::GOTO_index, std::size_t>& gotos){
            std::string str;
            str = join(goto_pair_to_str, gotos.begin(), gotos.end(), std::string{"\n"});
            return str;
        };

        auto items_collection_to_str = [this, &grammar](const std::vector<slr1::Items_collection>& collections){
            std::string str;
            std::size_t idx  = 0;

            std::list<std::string> collections_strs;

            for(const auto& collection : collections){
                auto collection_str = "I_{" + std::to_string(idx) + "} = " + to_string(collection, grammar);
                collections_strs.push_back(collection_str);
                idx++;
            }

            str = join(collections_strs.begin(), collections_strs.end(), std::string{"\n"});
            return str;
        };

        result = actions_to_str(parser_table.actions_)                   + "\n\n" +
                 gotos_to_str(parser_table.gotos_)                       + "\n\n" +
                 items_collection_to_str(parser_table.item_collections_);

        return result;
    }
};