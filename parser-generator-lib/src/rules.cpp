/*
    File:    rules.cpp
    Created: 01 May 2020 at 21:00 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/rules.hpp"

namespace parser_generator{
    bool operator <  (const Rule& lhs, const Rule& rhs)
    {
        if(lhs.concept_ != rhs.concept_){
            return lhs.concept_ < rhs.concept_;
        }

        return lhs.body_ < rhs.body_;
    }

    bool operator == (const Rule& lhs, const Rule& rhs)
    {
        if(lhs.concept_ != rhs.concept_){
            return false;
        }

        return lhs.body_ == rhs.body_;
    }

    bool operator != (const Rule& lhs, const Rule& rhs)
    {
        return !(lhs == rhs);
    }
};