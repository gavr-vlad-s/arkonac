/*
    File:    sets_follows.cpp
    Created: 26 April 2020 at 16:19 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/sets_follows.hpp"
namespace parser_generator{
    static const Symbol eps{Symbol_kind::Epsilon, 0};


    follow_sets_t Sets_follow::operator ()()
    {
        follow_sets_t old_sets{number_of_sets_};

        build_converted_rules();

        old_sets[grammar_.target_] = {Symbol{Symbol_kind::Terminal, 0}};

        auto new_sets = build_next_sets(old_sets);
        while(old_sets != new_sets){
            old_sets = new_sets;
            new_sets = build_next_sets(old_sets);
        }

        return new_sets;
    }

    follow_sets_t Sets_follow::build_next_sets(const follow_sets_t &current_sets) const
    {
        follow_sets_t result = current_sets;
        for(const auto& rule : converted_rules_){
            auto        concept                = rule.rule_.concept_;
            auto&       body                   = rule.rule_.body_;
            auto&       firsts_for_suffixes    = rule.firsts_for_suffixes_;
            auto&       nonterminals_positions = rule.nonterminals_positions_;

            std::size_t body_length            = body.size();

            for(std::size_t idx : nonterminals_positions){
                std::uint16_t non_terminal = body[idx - 1].code_;

                auto          new_follows  = result[non_terminal];

                if(idx == body_length){
                    auto inserted_follows = result[concept];
                    new_follows.insert(inserted_follows.begin(), inserted_follows.end());
                }else{
                    auto& firsts_for_rest_of_body = firsts_for_suffixes[idx];
                    if(firsts_for_rest_of_body.count(eps) == 0){
                        new_follows.insert(firsts_for_rest_of_body.begin(), firsts_for_rest_of_body.end());
                    }else{
                        for(const auto s : firsts_for_rest_of_body){
                            if(s != eps){
                                new_follows.insert(s);
                            }
                        }
                        auto inserted_follows = result[concept];
                        new_follows.insert(inserted_follows.begin(), inserted_follows.end());
                    }
                }

                result[non_terminal] = new_follows;
            }
        }
        return result;
    }

    void Sets_follow::build_converted_rules()
    {
        for(const auto& rule : grammar_.rules_){
            auto& body = rule.body_;
            if(body.empty()){
                continue;
            }

            Converted_rule converted_rule;

            std::size_t body_length             = body.size();
            converted_rule.rule_                = rule;
            converted_rule.firsts_for_suffixes_ = std::vector<std::set<Symbol>>(body_length);

            std::set<Symbol>      current_first;
            std::set<std::size_t> non_terminal_positions;

            for(std::size_t idx = body_length; idx >= 1 ; --idx){
                auto symbol = body[idx - 1];
                switch(symbol.kind_){
                    case Symbol_kind::Terminal:
                        current_first = {symbol};
                        break;
                    case Symbol_kind::Epsilon:
                        current_first.insert(symbol);
                        break;
                    case Symbol_kind::Non_terminal:
                    {
                        auto& firsts_for_nonterminal = firsts_[symbol.code_];
                        if(firsts_for_nonterminal.count(eps) == 0){
                            current_first = firsts_for_nonterminal;
                        }else{
                            current_first.insert(firsts_for_nonterminal.begin(), firsts_for_nonterminal.end());
                        }
                        converted_rule.nonterminals_positions_.insert(idx);
                    }
                        break;
                }

                converted_rule.firsts_for_suffixes_[idx - 1] = current_first;
            }

            converted_rules_.push_back(converted_rule);
        }
    }
};