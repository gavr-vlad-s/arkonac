/*
    File:    test_lexeme_to_string.h
    Created: 29 January 2020 at 07:22 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef TEST_LEXEME_TO_STRING_H
#define TEST_LEXEME_TO_STRING_H
namespace snd_lvl_testing_scanner{
    void test_lexeme_to_string();
};
#endif