/*
    File:    usage.h
    Created: 04 January 2020 at 13:15 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef USAGE_H
#define USAGE_H
void usage(const char* program_name);
#endif