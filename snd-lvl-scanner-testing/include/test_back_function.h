/*
    File:    test_back_function.h
    Created: 06 April 2020 at 07:33 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef TEST_BACK_FUNCTION_H
#define TEST_BACK_FUNCTION_H
namespace snd_lvl_testing_scanner{
    void test_back_function();
};
#endif