/*
    File:    snd_lvl_test_func.h
    Created: 07 January 2020 at 09:52 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SND_LVL_TESTING_FUNC_H
#define SND_LVL_TESTING_FUNC_H
#   include <memory>
#   include "../../snd-lvl-scanner/include/arkona-scanner-snd-lvl.h"
namespace snd_lvl_testing_scanner{
    void test_func(const std::shared_ptr<arkona_scanner_snd_lvl::Scanner>& arkonasc);
};
#endif