/*
    File:    test_back_function.cpp
    Created: 06 April 2020 at 07:45 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <vector>
#include <string>
#include <iterator>
#include <memory>
#include <cstddef>
#include <cstdio>
#include "../include/test_back_function.h"
#include "../include/lexeme_fab.h"
#include "../../snd-lvl-scanner/include/arkona-scanner-snd-lvl.h"
#include "../../other/include/testing.hpp"
#include "../../tries/include/errors_and_tries.h"
#include "../../tries/include/char_trie.h"
#include "../../tries/include/trie_for_vector.h"
#include "../../scanner/include/location.h"

namespace snd_lvl_testing_scanner{
    static const char32_t* test_text = UR"~(категория аддитивный_моноид<:T : тип:>
{
    конст нуль: T;
    операция + (x: T, y: T) : T;
    операция += (x: ссылка T, y: T) : ссылка T ;
    /*
       Данная категория определяет моноид по сложению, называемый ещё
       аддитивным моноидом.

       Напомним, что моноидом называется алгебраическая
       структура G с определённой на ней операцией ◊ : G x G → G,
       которая обладает следующими свойствами:
       1) для всех a, b, c справедливо соотношение
          (a ◊ b) ◊ c = a ◊ (b ◊ c)
       2) найдётся элемент e из G, такой, что
          a ◊ e = e ◊ a
          для всех элементов a из G.

       Первое свойство называется ассоциативностью операции, а элемент e
       из второго свойства --- нейтральным элементом.

       Аддитивным же моноидом называется моноид, в котором операция обозначается
       знаком +, а нейтральный элемент обозначается как 0.
    */
}

категория мультипликативный_моноид<:T : тип:>
{
    конст единица: T; операция * (x: T, y: T) : T;
    операция *= (x: ссылка T, y: T) : ссылка T ;
    /*
       Данная категория определяет моноид по умножению, называемый ещё
       мультипликативным моноидом.

       Напомним, что моноидом называется алгебраическая
       структура G с определённой на ней операцией ◊ : G x G → G,
       которая обладает следующими свойствами:
       1) для всех a, b, c справедливо соотношение
          (a ◊ b) ◊ c = a ◊ (b ◊ c)
       2) найдётся элемент e из G, такой, что
          a ◊ e = e ◊ a
          для всех элементов a из G.

       Первое свойство называется ассоциативностью операции, а элемент e
       из второго свойства --- нейтральным элементом.

       Мультипликативным же моноидом называется моноид, в котором операция обозначается
       знаком *, а нейтральный элемент обозначается как 1.

            /*
                Данная категория определяет моноид по сложению.
                Напомним, что моноидом называется алгебраическая
                структура G с определённой на ней операцией ◊ : G x G → G,
                которая обладает следующими свойствами:
                1) для всех a, b, c ∊ G справедливо соотношение
                    (a ◊ b) ◊ c = a ◊ (b ◊ c)
                2) найдётся элемент e ∊ G, такой, что
                    a ◊ e = e ◊ a
                    для всех элементов a из G.
                    /* А здесь ещё комментарий. */

                Первое свойство называется ассоциативностью операции, а элемент e
                из второго свойства --- нейтральным элементом.

                Аддитивным же моноидом называется моноид, в котором операция обозначается
                знаком +, а нейтральный элемент обозначается как 0.
            */

    */
}

мета функция фибоначчи<:n <:> T:> : T -> аддитивный_моноид<:T:>,
                                         мультипликативный_моноид<:T:>,
                                         упорядочение<:T:>
{
    мета если n < мультипликативный_моноид<:T:>::единица то
    {
        мета возврат аддитивный_моноид<:T:>::нуль
    }

    мета перем результат, текущее, предыдущее : T
    мета перем i : T
    предыдущее = аддитивный_моноид<:T:>::нуль;
    текущее = мультипликативный_моноид<:T:>::единица;

    мета для i = аддитивный_моноид<:T:>::нуль, n, мультипликативный_моноид<:T:>::единица
    {
        результат = текущее + предыдущее;
        предыдущее = текущее;
        текущее = результат
    };
    мета возврат результат
}

функция это_цифра(символ : симв) : лог
{
    возврат '0' <= символ <= '9' ||
            'A' <= символ <= 'F' ||
            'a' <= символ <= 'f'
}

функция приветствие(имя : конст ссылка строка16) : само
{
    возврат u16"Привет, " + имя + u16'!'
}

функция цифру_в_число(s: симв) : цел
{
    перем результат : цел = код(s) - код('0')
    возврат (результат < 10) ? результат : ((код(s) & 0b1101'1111) - 0x37)
}   ?.
)~";

    enum class Action{
        Get_current, Get_previous
    };

    using Token                 = arkona_scanner_snd_lvl::Arkona_token_snd_lvl;
    using Module_parts_trie_ptr = std::shared_ptr<Trie_for_vector<std::size_t>>;
    using Scanner               = arkona_scanner_snd_lvl::Scanner;
    using Scanner_ptr           = std::shared_ptr<Scanner>;
    using Token_generator       = testing::Generator<Token>;


    class Scanner_as_generator_with_actions : public Token_generator{
    public:
        Scanner_as_generator_with_actions()                                         = default;
        Scanner_as_generator_with_actions(const Scanner_as_generator_with_actions&) = default;
        virtual ~Scanner_as_generator_with_actions()                                = default;

        Scanner_as_generator_with_actions(const Scanner_ptr&         scanner,
                                          const std::vector<Action>& actions):
            scanner_{scanner}, actions_{actions}
        {
            current_idx_ = 0;
        }

        Token yield()                     override;
        bool  is_finished(const Token& t) override;
    private:
        Scanner_ptr                scanner_;
        const std::vector<Action>& actions_;
        std::size_t                current_idx_;
    };

    Token Scanner_as_generator_with_actions::yield()
    {
        if(actions_[current_idx_] == Action::Get_previous){
            scanner_->back();
        }
        current_idx_++;
        return scanner_->current_lexeme();
    }

    bool Scanner_as_generator_with_actions::is_finished(const Token& t)
    {
        return t.lexeme_.code_.kind_ == Lexem_kind::Nothing;
    }


    static const std::vector<Action> actions = {
        Action::Get_current,  Action::Get_current,  Action::Get_previous,
        Action::Get_current,  Action::Get_previous, Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_previous, // 9

        Action::Get_previous, Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 18

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 27

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 36

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 45

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_previous, Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 54

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 63

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 72

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_previous,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 81

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 90

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 99

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 108

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 117

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 126

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 135

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 144

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 153

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 162

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 171

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 180

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 189

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 198

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 207

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 216

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_previous, // 225

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 234

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 243

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 252

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_previous, Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 261

        Action::Get_previous, Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 270

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 279

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 288

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_previous,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 297

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_previous, // 306

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_previous, Action::Get_current,
    };

    static const std::vector<Token> tokens_for_text = {
            {{{1,  1  }, {1,  9  }}, keyword_lexeme(Keyword_kind::Kw_kategorija)              }, // Keyword Kw_kategorija
            {{{1,  11 }, {1,  27 }}, id_lexeme(17)                                            }, // Id аддитивный_моноид
            {{{1,  11 }, {1,  27 }}, id_lexeme(17)                                            }, // Id аддитивный_моноид

            {{{1,  28 }, {1,  29 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{1,  28 }, {1,  29 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{1,  30 }, {1,  30 }}, id_lexeme(18)                                            }, // Id T

            {{{1,  32 }, {1,  32 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{1,  34 }, {1,  36 }}, keyword_lexeme(Keyword_kind::Kw_tip)                     }, // Keyword Kw_tip
            {{{1,  34 }, {1,  36 }}, keyword_lexeme(Keyword_kind::Kw_tip)                     }, // Keyword Kw_tip
// 9
            {{{1,  34 }, {1,  36 }}, keyword_lexeme(Keyword_kind::Kw_tip)                     }, // Keyword Kw_tip
            {{{1,  37 }, {1,  38 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br
            {{{2,  1  }, {2,  1  }}, delim_lexeme(Delimiter_kind::Fig_br_opened)              }, // Delimiter Fig_br_opened

            {{{3,  5  }, {3,  9  }}, keyword_lexeme(Keyword_kind::Kw_konst)                   }, // Keyword Kw_konst
            {{{3,  11 }, {3,  14 }}, id_lexeme(22)                                            }, // Id нуль
            {{{3,  15 }, {3,  15 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon

            {{{3,  17 }, {3,  17 }}, id_lexeme(18)                                            }, // Id T
            {{{3,  18 }, {3,  18 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{4,  5  }, {4,  12 }}, keyword_lexeme(Keyword_kind::Kw_operaciya)               }, // Keyword Kw_operaciya
// 18
            {{{4,  14 }, {4,  14 }}, delim_lexeme(Delimiter_kind::Plus)                       }, // Delimiter Plus
            {{{4,  16 }, {4,  16 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened
            {{{4,  17 }, {4,  17 }}, id_lexeme(23)                                            }, // Id x

            {{{4,  18 }, {4,  18 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{4,  20 }, {4,  20 }}, id_lexeme(18)                                            }, // Id T
            {{{4,  21 }, {4,  21 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma

            {{{4,  23 }, {4,  23 }}, id_lexeme(24)                                            }, // Id y
            {{{4,  24 }, {4,  24 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{4,  26 }, {4,  26 }}, id_lexeme(18)                                            }, // Id T
// 27
            {{{4,  27 }, {4,  27 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed
            {{{4,  29 }, {4,  29 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{4,  31 }, {4,  31 }}, id_lexeme(18)                                            }, // Id T

            {{{4,  32 }, {4,  32 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{5,  5  }, {5,  12 }}, keyword_lexeme(Keyword_kind::Kw_operaciya)               }, // Keyword Kw_operaciya
            {{{5,  14 }, {5,  15 }}, delim_lexeme(Delimiter_kind::Plus_assign)                }, // Delimiter Plus_assign

            {{{5,  17 }, {5,  17 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened
            {{{5,  18 }, {5,  18 }}, id_lexeme(23)                                            }, // Id x
            {{{5,  19 }, {5,  19 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
// 36
            {{{5,  21 }, {5,  26 }}, keyword_lexeme(Keyword_kind::Kw_ssylka)                  }, // Keyword Kw_ssylka
            {{{5,  28 }, {5,  28 }}, id_lexeme(18)                                            }, // Id x
            {{{5,  29 }, {5,  29 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma

            {{{5,  31 }, {5,  31 }}, id_lexeme(24)                                            }, // Id y
            {{{5,  32 }, {5,  32 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{5,  34 }, {5,  34 }}, id_lexeme(18)                                            }, // Id T

            {{{5,  35 }, {5,  35 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed
            {{{5,  37 }, {5,  37 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{5,  39 }, {5,  44 }}, keyword_lexeme(Keyword_kind::Kw_ssylka)                  }, // Keyword Kw_ssylka
// 45
            {{{5,  46 }, {5,  46 }}, id_lexeme(18)                                            }, // Id T
            {{{5,  48 }, {5,  48 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{25, 1  }, {25, 1  }}, delim_lexeme(Delimiter_kind::Fig_br_closed)              }, // Delimiter Fig_br_closed

            {{{27, 1  }, {27, 9  }}, keyword_lexeme(Keyword_kind::Kw_kategorija)              }, // Keyword Kw_kategorija
            {{{27, 1  }, {27, 9  }}, keyword_lexeme(Keyword_kind::Kw_kategorija)              }, // Keyword Kw_kategorija
            {{{27, 11 }, {27, 34 }}, id_lexeme(48)                                            }, // Id мультипликативный_моноид

            {{{27, 35 }, {27, 36 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{27, 37 }, {27, 37 }}, id_lexeme(18)                                            }, // Id T
            {{{27, 39 }, {27, 39 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
// 54
            {{{27, 41 }, {27, 43 }}, keyword_lexeme(Keyword_kind::Kw_tip)                     }, // Keyword Kw_tip
            {{{27, 44 }, {27, 45 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br
            {{{28, 1  }, {28, 1  }}, delim_lexeme(Delimiter_kind::Fig_br_opened)              }, // Delimiter Fig_br_opened

            {{{29, 5  }, {29, 9  }}, keyword_lexeme(Keyword_kind::Kw_konst)                   }, // Keyword Kw_konst
            {{{29, 11 }, {29, 17 }}, id_lexeme(55)                                            }, // Id нуль
            {{{29, 18 }, {29, 18 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon

            {{{29, 20 }, {29, 20 }}, id_lexeme(18)                                            }, // Id T
            {{{29, 21 }, {29, 21 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{29, 23 }, {29, 30 }}, keyword_lexeme(Keyword_kind::Kw_operaciya)               }, // Keyword Kw_operaciya
// 63
            {{{29, 32 }, {29, 32 }}, delim_lexeme(Delimiter_kind::Mul)                        }, // Delimiter Mul
            {{{29, 34 }, {29, 34 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened
            {{{29, 35 }, {29, 35 }}, id_lexeme(23)                                            }, // Id x

            {{{29, 36 }, {29, 36 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{29, 38 }, {29, 38 }}, id_lexeme(18)                                            }, // Id T
            {{{29, 39 }, {29, 39 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma

            {{{29, 41 }, {29, 41 }}, id_lexeme(24)                                            }, // Id y
            {{{29, 42 }, {29, 42 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{29, 44 }, {29, 44 }}, id_lexeme(18)                                            }, // Id T
// 72
            {{{29, 45 }, {29, 45 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed
            {{{29, 47 }, {29, 47 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{29, 49 }, {29, 49 }}, id_lexeme(18)                                            }, // Id T

            {{{29, 50 }, {29, 50 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{30, 5  }, {30, 12 }}, keyword_lexeme(Keyword_kind::Kw_operaciya)               }, // Keyword Kw_operaciya
            {{{30, 5  }, {30, 12 }}, keyword_lexeme(Keyword_kind::Kw_operaciya)               }, // Keyword Kw_operaciya

            {{{30, 14 }, {30, 15 }}, delim_lexeme(Delimiter_kind::Mul_assign)                 }, // Delimiter Mul_assign
            {{{30, 17 }, {30, 17 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened
            {{{30, 18 }, {30, 18 }}, id_lexeme(23)                                            }, // Id x
// 81
            {{{30, 19 }, {30, 19 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{30, 21 }, {30, 26 }}, keyword_lexeme(Keyword_kind::Kw_ssylka)                  }, // Keyword Kw_ssylka
            {{{30, 28 }, {30, 28 }}, id_lexeme(18)                                            }, // Id x

            {{{30, 29 }, {30, 29 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma
            {{{30, 31 }, {30, 31 }}, id_lexeme(24)                                            }, // Id y
            {{{30, 32 }, {30, 32 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon

            {{{30, 34 }, {30, 34 }}, id_lexeme(18)                                            }, // Id T
            {{{30, 35 }, {30, 35 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed
            {{{30, 37 }, {30, 37 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
// 90
            {{{30, 39 }, {30, 44 }}, keyword_lexeme(Keyword_kind::Kw_ssylka)                  }, // Keyword Kw_ssylka
            {{{30, 46 }, {30, 46 }}, id_lexeme(18)                                            }, // Id T
            {{{30, 48 }, {30, 48 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon

            {{{70, 1  }, {70, 1  }}, delim_lexeme(Delimiter_kind::Fig_br_closed)              }, // Delimiter Fig_br_closed
            {{{72, 1  }, {72, 4  }}, keyword_lexeme(Keyword_kind::Kw_meta)                    }, // Keyword Kw_meta
            {{{72, 6  }, {72, 12 }}, keyword_lexeme(Keyword_kind::Kw_funktsiya)               }, // Keyword Kw_funktsiya

            {{{72, 14 }, {72, 22 }}, id_lexeme(64)                                            }, // Id фибоначчи
            {{{72, 23 }, {72, 24 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{72, 25 }, {72, 25 }}, id_lexeme(65)                                            }, // Id n
// 99
            {{{72, 27 }, {72, 29 }}, delim_lexeme(Delimiter_kind::Deduce_arg_type)            }, // Delimiter Deduce_arg_type
            {{{72, 31 }, {72, 31 }}, id_lexeme(18)                                            }, // Id T
            {{{72, 32 }, {72, 33 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br

            {{{72, 35 }, {72, 35 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{72, 37 }, {72, 37 }}, id_lexeme(18)                                            }, // Id T
            {{{72, 39 }, {72, 40 }}, delim_lexeme(Delimiter_kind::Right_arrow)                }, // Delimiter Right_arrow

            {{{72, 42 }, {72, 58 }}, id_lexeme(17)                                            }, // Id аддитивный_моноид
            {{{72, 59 }, {72, 60 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{72, 61 }, {72, 61 }}, id_lexeme(18)                                            }, // Id T
// 108
            {{{72, 62 }, {72, 63 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br
            {{{72, 64 }, {72, 64 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma
            {{{73, 42 }, {73, 65 }}, id_lexeme(48)                                            }, // Id мультипликативный_моноид

            {{{73, 66 }, {73, 67 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{73, 68 }, {73, 68 }}, id_lexeme(18)                                            }, // Id T
            {{{73, 69 }, {73, 70 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br

            {{{73, 71 }, {73, 71 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma
            {{{74, 42 }, {74, 53 }}, id_lexeme(77)                                            }, // Id упорядочение
            {{{74, 54 }, {74, 55 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
// 117
            {{{74, 56 }, {74, 56 }}, id_lexeme(18)                                            }, // Id T
            {{{74, 57 }, {74, 58 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br
            {{{75, 1  }, {75, 1  }}, delim_lexeme(Delimiter_kind::Fig_br_opened)              }, // Delimiter Fig_br_opened

            {{{76, 5  }, {76, 8  }}, keyword_lexeme(Keyword_kind::Kw_meta)                    }, // Keyword Kw_meta
            {{{76, 10 }, {76, 13 }}, keyword_lexeme(Keyword_kind::Kw_esli)                    }, // Keyword Kw_esli
            {{{76, 15 }, {76, 15 }}, id_lexeme(65)                                            }, // Id n

            {{{76, 17 }, {76, 17 }}, delim_lexeme(Delimiter_kind::LT)                         }, // Delimiter LT
            {{{76, 19 }, {76, 42 }}, id_lexeme(48)                                            }, // Id мультипликативный_моноид
            {{{76, 43 }, {76, 44 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
// 126
            {{{76, 45 }, {76, 45 }}, id_lexeme(18)                                            }, // Id T
            {{{76, 46 }, {76, 47 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br
            {{{76, 48 }, {76, 49 }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol

            {{{76, 50 }, {76, 56 }}, id_lexeme(55)                                            }, // Id нуль
            {{{76, 58 }, {76, 59 }}, keyword_lexeme(Keyword_kind::Kw_to)                      }, // Keyword Kw_to
            {{{77, 5  }, {77, 5  }}, delim_lexeme(Delimiter_kind::Fig_br_opened)              }, // Delimiter Fig_br_opened

            {{{78, 9  }, {78, 12 }}, keyword_lexeme(Keyword_kind::Kw_meta)                    }, // Keyword Kw_meta
            {{{78, 14 }, {78, 20 }}, keyword_lexeme(Keyword_kind::Kw_vozvrat)                 }, // Keyword Kw_vozvrat
            {{{78, 22 }, {78, 38 }}, id_lexeme(17)                                            }, // Id аддитивный_моноид
// 135
            {{{78, 39 }, {78, 40 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{78, 41 }, {78, 41 }}, id_lexeme(18)                                            }, // Id T
            {{{78, 42 }, {78, 43 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br

            {{{78, 44 }, {78, 45 }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
            {{{78, 46 }, {78, 49 }}, id_lexeme(22)                                            }, // Id нуль
            {{{79, 5  }, {79, 5  }}, delim_lexeme(Delimiter_kind::Fig_br_closed)              }, // Delimiter Fig_br_closed

            {{{81, 5  }, {81, 8  }}, keyword_lexeme(Keyword_kind::Kw_meta)                    }, // Keyword Kw_meta
            {{{81, 10 }, {81, 14 }}, keyword_lexeme(Keyword_kind::Kw_perem)                   }, // Keyword Kw_perem
            {{{81, 16 }, {81, 24 }}, id_lexeme(86)                                            }, // Id результат
// 144
            {{{81, 25 }, {81, 25 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma
            {{{81, 27 }, {81, 33 }}, id_lexeme(93)                                            }, // Id текущее
            {{{81, 34 }, {81, 34 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma

            {{{81, 36 }, {81, 45 }}, id_lexeme(103)                                           }, // Id предыдущее
            {{{81, 47 }, {81, 47 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{81, 49 }, {81, 49 }}, id_lexeme(18)                                            }, // Id T

            {{{82, 5  }, {82, 8  }}, keyword_lexeme(Keyword_kind::Kw_meta)                    }, // Keyword Kw_meta
            {{{82, 10 }, {82, 14 }}, keyword_lexeme(Keyword_kind::Kw_perem)                   }, // Keyword Kw_perem
            {{{82, 16 }, {82, 16 }}, id_lexeme(104)                                           }, // Id i
// 153
            {{{82, 18 }, {82, 18 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{82, 20 }, {82, 20 }}, id_lexeme(18)                                            }, // Id T
            {{{83, 5  }, {83, 14 }}, id_lexeme(103)                                           }, // Id предыдущее

            {{{83, 16 }, {83, 16 }}, delim_lexeme(Delimiter_kind::Assign)                     }, // Delimiter Assign
            {{{83, 18 }, {83, 34 }}, id_lexeme(17)                                            }, // Id аддитивный_моноид
            {{{83, 35 }, {83, 36 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br

            {{{83, 37 }, {83, 37 }}, id_lexeme(18)                                            }, // Id T
            {{{83, 38 }, {83, 39 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br
            {{{83, 40 }, {83, 41 }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
// 162
            {{{83, 42 }, {83, 45 }}, id_lexeme(22)                                            }, // Id нуль
            {{{83, 46 }, {83, 46 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{84, 5  }, {84, 11 }}, id_lexeme(93)                                            }, // Id текущее

            {{{84, 13 }, {84, 13 }}, delim_lexeme(Delimiter_kind::Assign)                     }, // Delimiter Assign
            {{{84, 15 }, {84, 38 }}, id_lexeme(48)                                            }, // Id мультипликативный_моноид
            {{{84, 39 }, {84, 40 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br

            {{{84, 41 }, {84, 41 }}, id_lexeme(18)                                            }, // Id T
            {{{84, 42 }, {84, 43 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br
            {{{84, 44 }, {84, 45 }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
// 171
            {{{84, 46 }, {84, 52 }}, id_lexeme(55)                                            }, // Id нуль
            {{{84, 53 }, {84, 53 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{86, 5  }, {86, 8  }}, keyword_lexeme(Keyword_kind::Kw_meta)                    }, // Keyword Kw_meta

            {{{86, 10 }, {86, 12 }}, keyword_lexeme(Keyword_kind::Kw_dlya)                    }, // Keyword Kw_dlya
            {{{86, 14 }, {86, 14 }}, id_lexeme(104)                                           }, // Id i
            {{{86, 16 }, {86, 16 }}, delim_lexeme(Delimiter_kind::Assign)                     }, // Delimiter Assign

            {{{86, 18 }, {86, 34 }}, id_lexeme(17)                                            }, // Id аддитивный_моноид
            {{{86, 35 }, {86, 36 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{86, 37 }, {86, 37 }}, id_lexeme(18)                                            }, // Id T
// 180
            {{{86, 38 }, {86, 39 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br
            {{{86, 40 }, {86, 41 }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
            {{{86, 42 }, {86, 45 }}, id_lexeme(22)                                            }, // Id нуль

            {{{86, 46 }, {86, 46 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma
            {{{86, 48 }, {86, 48 }}, id_lexeme(65)                                            }, // Id n
            {{{86, 49 }, {86, 49 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma

            {{{86, 51 }, {86, 74 }}, id_lexeme(48)                                            }, // Id мультипликативный_моноид
            {{{86, 75 }, {86, 76 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{86, 77 }, {86, 77 }}, id_lexeme(18)                                            }, // Id T
// 189
            {{{86, 78 }, {86, 79 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br
            {{{86, 80 }, {86, 81 }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
            {{{86, 82 }, {86, 88 }}, id_lexeme(55)                                            }, // Id нуль

            {{{87, 5  }, {87, 5  }}, delim_lexeme(Delimiter_kind::Fig_br_opened)              }, // Delimiter Fig_br_opened
            {{{88, 9  }, {88, 17 }}, id_lexeme(86)                                            }, // Id результат
            {{{88, 19 }, {88, 19 }}, delim_lexeme(Delimiter_kind::Assign)                     }, // Delimiter Assign

            {{{88, 21 }, {88, 27 }}, id_lexeme(93)                                            }, // Id текущее
            {{{88, 29 }, {88, 29 }}, delim_lexeme(Delimiter_kind::Plus)                       }, // Delimiter Plus
            {{{88, 31 }, {88, 40 }}, id_lexeme(103)                                           }, // Id предыдущее
// 198
            {{{88, 41 }, {88, 41 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{89, 9  }, {89, 18 }}, id_lexeme(103)                                           }, // Id предыдущее
            {{{89, 20 }, {89, 20 }}, delim_lexeme(Delimiter_kind::Assign)                     }, // Delimiter Assign

            {{{89, 22 }, {89, 28 }}, id_lexeme(93)                                            }, // Id текущее
            {{{89, 29 }, {89, 29 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{90, 9  }, {90, 15 }}, id_lexeme(93)                                            }, // Id текущее

            {{{90, 17 }, {90, 17 }}, delim_lexeme(Delimiter_kind::Assign)                     }, // Delimiter Assign
            {{{90, 19 }, {90, 27 }}, id_lexeme(86)                                            }, // Id результат
            {{{91, 5  }, {91, 5  }}, delim_lexeme(Delimiter_kind::Fig_br_closed)              }, // Delimiter Fig_br_closed
// 207
            {{{91, 6  }, {91, 6  }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{92, 5  }, {92, 8  }}, keyword_lexeme(Keyword_kind::Kw_meta)                    }, // Keyword Kw_meta
            {{{92, 10 }, {92, 16 }}, keyword_lexeme(Keyword_kind::Kw_vozvrat)                 }, // Keyword Kw_vozvrat

            {{{92, 18 }, {92, 26 }}, id_lexeme(86)                                            }, // Id результат
            {{{93, 1  }, {93, 1  }}, delim_lexeme(Delimiter_kind::Fig_br_closed)              }, // Delimiter Fig_br_closed
            {{{95, 1  }, {95, 7  }}, keyword_lexeme(Keyword_kind::Kw_funktsiya)               }, // Keyword Kw_funktsiya

            {{{95, 9  }, {95, 17 }}, id_lexeme(113)                                           }, // Id это_цифра
            {{{95, 18 }, {95, 18 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened
            {{{95, 19 }, {95, 24 }}, id_lexeme(119)                                           }, // Id символ
// 216
            {{{95, 26 }, {95, 26 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{95, 28 }, {95, 31 }}, keyword_lexeme(Keyword_kind::Kw_simv)                    }, // Keyword Kw_simv
            {{{95, 32 }, {95, 32 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed

            {{{95, 34 }, {95, 34 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{95, 36 }, {95, 38 }}, keyword_lexeme(Keyword_kind::Kw_log)                     }, // Keyword Kw_log
            {{{96, 1  }, {96, 1  }}, delim_lexeme(Delimiter_kind::Fig_br_opened)              }, // Delimiter Fig_br_opened

            {{{97, 5  }, {97, 11 }}, keyword_lexeme(Keyword_kind::Kw_vozvrat)                 }, // Keyword Kw_vozvrat
            {{{97, 13 }, {97, 15 }}, char_lexeme(U'0')                                        }, // Char [Char32] '0'
            {{{97, 13 }, {97, 15 }}, char_lexeme(U'0')                                        }, // Char [Char32] '0'
// 225
            {{{97, 17 }, {97, 18 }}, delim_lexeme(Delimiter_kind::LEQ)                        }, // Delimiter LEQ
            {{{97, 20 }, {97, 25 }}, id_lexeme(119)                                           }, // Id символ
            {{{97, 27 }, {97, 28 }}, delim_lexeme(Delimiter_kind::LEQ)                        }, // Delimiter LEQ

            {{{97, 30 }, {97, 32 }}, char_lexeme(U'9')                                        }, // Char [Char32] '9'
            {{{97, 34 }, {97, 35 }}, delim_lexeme(Delimiter_kind::Logical_or)                 }, // Delimiter Logical_or
            {{{98, 13 }, {98, 15 }}, char_lexeme(U'A')                                        }, // Char [Char32] 'A'

            {{{98, 17 }, {98, 18 }}, delim_lexeme(Delimiter_kind::LEQ)                        }, // Delimiter LEQ
            {{{98, 20 }, {98, 25 }}, id_lexeme(119)                                           }, // Id символ
            {{{98, 27 }, {98, 28 }}, delim_lexeme(Delimiter_kind::LEQ)                        }, // Delimiter LEQ
// 234
            {{{98, 30 }, {98, 32 }}, char_lexeme(U'F')                                        }, // Char [Char32] 'F'
            {{{98, 34 }, {98, 35 }}, delim_lexeme(Delimiter_kind::Logical_or)                 }, // Delimiter Logical_or
            {{{99, 13 }, {99, 15 }}, char_lexeme(U'a')                                        }, // Char [Char32] 'a'

            {{{99, 17 }, {99, 18 }}, delim_lexeme(Delimiter_kind::LEQ)                        }, // Delimiter LEQ
            {{{99, 20 }, {99, 25 }}, id_lexeme(119)                                           }, // Id символ
            {{{99, 27 }, {99, 28 }}, delim_lexeme(Delimiter_kind::LEQ)                        }, // Delimiter LEQ

            {{{99, 30 }, {99, 32 }}, char_lexeme(U'f')                                        }, // Char [Char32] 'f'
            {{{100,1  }, {100,1  }}, delim_lexeme(Delimiter_kind::Fig_br_closed)              }, // Delimiter Fig_br_closed
            {{{102,1  }, {102,7  }}, keyword_lexeme(Keyword_kind::Kw_funktsiya)               }, // Keyword Kw_funktsiya
// 243
            {{{102,9  }, {102,19 }}, id_lexeme(128)                                           }, // Id приветствие
            {{{102,20 }, {102,20 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened
            {{{102,21 }, {102,23 }}, id_lexeme(131)                                           }, // Id имя


            {{{102,25 }, {102,25 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{102,27 }, {102,31 }}, keyword_lexeme(Keyword_kind::Kw_konst)                   }, // Keyword Kw_konst
            {{{102,33 }, {102,38 }}, keyword_lexeme(Keyword_kind::Kw_ssylka)                  }, // Keyword Kw_ssylka


            {{{102,40 }, {102,47 }}, keyword_lexeme(Keyword_kind::Kw_stroka16)                }, // Keyword Kw_stroka16
            {{{102,48 }, {102,48 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed
            {{{102,50 }, {102,50 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
// 252
            {{{102,52 }, {102,55 }}, keyword_lexeme(Keyword_kind::Kw_samo)                    }, // Keyword Kw_samo
            {{{103,1  }, {103,1  }}, delim_lexeme(Delimiter_kind::Fig_br_opened)              }, // Delimiter Fig_br_opened
            {{{104,5  }, {104,11 }}, keyword_lexeme(Keyword_kind::Kw_vozvrat)                 }, // Keyword Kw_vozvrat

            {{{104,13 }, {104,25 }}, string_lexeme(8, String_kind::String16)                  }, // String [String16] "Привет, "
            {{{104,13 }, {104,25 }}, string_lexeme(8, String_kind::String16)                  }, // String [String16] "Привет, "
            {{{104,27 }, {104,27 }}, delim_lexeme(Delimiter_kind::Plus)                       }, // Delimiter Plus

            {{{104,29 }, {104,31 }}, id_lexeme(131)                                           }, // Id имя
            {{{104,33 }, {104,33 }}, delim_lexeme(Delimiter_kind::Plus)                       }, // Delimiter Plus
            {{{104,35 }, {104,40 }}, char_lexeme(U'!', Char_kind::Char16)                     }, // Char [Char16] '!'
// 261
            {{{104,35 }, {104,40 }}, char_lexeme(U'!', Char_kind::Char16)                     }, // Char [Char16] '!'
            {{{105,1  }, {105,1  }}, delim_lexeme(Delimiter_kind::Fig_br_closed)              }, // Delimiter Fig_br_closed
            {{{107,1  }, {107,7  }}, keyword_lexeme(Keyword_kind::Kw_funktsiya)               }, // Keyword Kw_funktsiya

            {{{107,9  }, {107,21 }}, id_lexeme(147)                                           }, // Id цифру_в_число
            {{{107,22 }, {107,22 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened
            {{{107,23 }, {107,23 }}, id_lexeme(148)                                           }, // Id s


            {{{107,24 }, {107,24 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{107,26 }, {107,29 }}, keyword_lexeme(Keyword_kind::Kw_simv)                    }, // Keyword Kw_simv
            {{{107,30 }, {107,30 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed
// 270
            {{{107,32 }, {107,32 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{107,34 }, {107,36 }}, keyword_lexeme(Keyword_kind::Kw_tsel)                    }, // Keyword Kw_tsel
            {{{108,1  }, {108,1  }}, delim_lexeme(Delimiter_kind::Fig_br_opened)              }, // Delimiter Fig_br_opened

            {{{109,5  }, {109,9  }}, keyword_lexeme(Keyword_kind::Kw_perem)                   }, // Keyword Kw_perem
            {{{109,11 }, {109,19 }}, id_lexeme(86)                                            }, // Id результат
            {{{109,21 }, {109,21 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon

            {{{109,23 }, {109,25 }}, keyword_lexeme(Keyword_kind::Kw_tsel)                    }, // Keyword Kw_tsel
            {{{109,27 }, {109,27 }}, delim_lexeme(Delimiter_kind::Assign)                     }, // Delimiter Assign
            {{{109,29 }, {109,31 }}, id_lexeme(151)                                           }, // Id код
// 279
            {{{109,32 }, {109,32 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened
            {{{109,33 }, {109,33 }}, id_lexeme(148)                                           }, // Id s
            {{{109,34 }, {109,34 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed

            {{{109,36 }, {109,36 }}, delim_lexeme(Delimiter_kind::Minus)                      }, // Delimiter Minus
            {{{109,38 }, {109,40 }}, id_lexeme(151)                                           }, // Id код
            {{{109,41 }, {109,41 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened

            {{{109,42 }, {109,44 }}, char_lexeme(U'0')                                        }, // Char [Char32] '0'
            {{{109,45 }, {109,45 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed
            {{{110,5  }, {110,11 }}, keyword_lexeme(Keyword_kind::Kw_vozvrat)                 }, // Keyword Kw_vozvrat
// 288
            {{{110,13 }, {110,13 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened
            {{{110,14 }, {110,22 }}, id_lexeme(86)                                            }, // Id результат
            {{{110,24 }, {110,24 }}, delim_lexeme(Delimiter_kind::LT)                         }, // Delimiter LT

            {{{110,26 }, {110,27 }}, integer_lexeme(10)                                       }, // Integer 10
            {{{110,28 }, {110,28 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed
            {{{110,28 }, {110,28 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed

            {{{110,30 }, {110,30 }}, delim_lexeme(Delimiter_kind::Cond_op)                    }, // Delimiter Cond_op
            {{{110,32 }, {110,40 }}, id_lexeme(86)                                            }, // Id результат
            {{{110,42 }, {110,42 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
// 297
            {{{110,44 }, {110,44 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened
            {{{110,45 }, {110,45 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened
            {{{110,46 }, {110,48 }}, id_lexeme(151)                                           }, // Id код

            {{{110,49 }, {110,49 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened
            {{{110,50 }, {110,50 }}, id_lexeme(148)                                           }, // Id s
            {{{110,51 }, {110,51 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed

            {{{110,53 }, {110,53 }}, delim_lexeme(Delimiter_kind::Bitwise_and)                }, // Delimiter Bitwise_and
            {{{110,55 }, {110,65 }}, integer_lexeme(0b1101'1111)                              }, // Integer 0b1101'1111
            {{{110,55 }, {110,65 }}, integer_lexeme(0b1101'1111)                              }, // Integer 0b1101'1111
// 306
            {{{110,66 }, {110,66 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed
            {{{110,68 }, {110,68 }}, delim_lexeme(Delimiter_kind::Minus)                      }, // Delimiter Minus
            {{{110,70 }, {110,73 }}, integer_lexeme(0x37)                                     }, // Integer 0x37

            {{{110,74 }, {110,74 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed
            {{{111,1  }, {111,1  }}, delim_lexeme(Delimiter_kind::Fig_br_closed)              }, // Delimiter Fig_br_closed
            {{{111,5  }, {111,6  }}, delim_lexeme(Delimiter_kind::Cond_op_full)               }, // Delimiter Cond_op_full

            {{{111,5  }, {111,6  }}, delim_lexeme(Delimiter_kind::Cond_op_full)               }, // Delimiter Cond_op_full
            {{{112,1  }, {112,1  }}, nothing_lexeme()                                         }, // Nothing
    };

    static const Token nothing = {
        {{112,1  }, {112,1  }}, nothing_lexeme()
    };

    void test_back_function()
    {
        Errors_and_tries  et;
        et.ec_                      = std::make_shared<Error_count>();
        et.wc_                      = std::make_shared<Warning_count>();
        et.ids_trie_                = std::make_shared<Char_trie>();
        et.strs_trie_               = std::make_shared<Char_trie>();

        auto module_name_parts_trie = std::make_shared<Trie_for_vector<std::size_t>>();
        auto p                      = const_cast<char32_t*>(test_text);
        auto loc                    = std::make_shared<ascaner::Location>(p);
        auto scanner                = std::make_shared<Scanner>(loc, et, module_name_parts_trie);

//         puts("Testing of the method back().");
//         for(const auto action : actions){
//             if(action == Action::Get_previous){
//                 scanner->back();
//             }
//             auto token     = scanner->current_lexeme();
//             auto token_str = scanner->token_to_string(token);
//             puts(token_str.c_str());
//         }
//         puts("End of testing of the method back().\n\n");

        Scanner_as_generator_with_actions gen{scanner, actions};
        puts("Testing of the method back().");
        testing::test(gen,                         [](auto t){return t;},
                      std::begin(tokens_for_text), std::end(tokens_for_text));

        puts("Additional tests of the method back().");
        scanner->back();
        auto token                         = scanner->current_lexeme();
        bool first_additional_test_passed  = token == nothing;
        printf("First additional test %s.\n",
               first_additional_test_passed ? "passed" : "failed");

        scanner->back();
        token                              = scanner->current_lexeme();
        bool second_additional_test_passed = token == nothing;
        printf("Second additional test %s.\n\n\n",
               second_additional_test_passed ? "passed" : "failed");
    }
};