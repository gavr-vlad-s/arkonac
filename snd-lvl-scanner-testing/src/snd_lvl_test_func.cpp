/*
    File:    snd_lvl_test_func.cpp
    Created: 07 January 2020 at 10:44 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstdio>
#include "../include/snd_lvl_test_func.h"
namespace snd_lvl_testing_scanner{
    void test_func(const std::shared_ptr<arkona_scanner_snd_lvl::Scanner>& arkonasc)
    {
        arkona_scanner_snd_lvl::Arkona_token_snd_lvl token;
        arkona_scanner_snd_lvl::Lexeme_info          lexeme;
        arkona_scanner_snd_lvl::Lexem_code           code;
        arkona_scanner_snd_lvl::Lexem_kind           kind;
        do{
            code = (lexeme = (token = arkonasc->current_lexeme()).lexeme_).code_;
            kind = code.kind_;

            auto token_str = arkonasc->token_to_string(token);
            puts(token_str.c_str());
        }while(kind != arkona_scanner_snd_lvl::Lexem_kind::Nothing);
    }
};