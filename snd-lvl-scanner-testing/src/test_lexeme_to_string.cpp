/*
    File:    test_lexeme_to_string.cpp
    Created: 29 January 2020 at 07:28 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <string>
#include <utility>
#include <cstdio>
#include <memory>
#include <vector>
#include <iterator>
#include "../include/test_lexeme_to_string.h"
#include "../include/lexeme_fab.h"
#include "../../other/include/testing.hpp"
#include "../../snd-lvl-scanner/include/snd_level_lexeme.h"
#include "../../snd-lvl-scanner/include/lexeme_to_str.h"
#include "../../tries/include/char_trie.h"
#include "../../char-conv/include/char_conv.h"
#include "../../tries/include/trie_for_vector.h"
#include "../../strings-lib/include/join.h"


namespace snd_lvl_testing_scanner{
    using Pair_for_test = std::pair<arkona_scanner_snd_lvl::Lexeme_info,
                                    std::string>;

    //! Tests for special lexemes.
    static const Pair_for_test special_lexemes_tests[] = {
        {unknown_lexeme(), "UnknownLexem "}, {nothing_lexeme(), "Nothing "}
    };

    //! Tests for keywords.
    static const Pair_for_test keywords_tests[] = {
        {keyword_lexeme(Keyword_kind::Kw_bolshoe),       "Keyword Kw_bolshoe"      },
        {keyword_lexeme(Keyword_kind::Kw_bezzn),         "Keyword Kw_bezzn"        },
        {keyword_lexeme(Keyword_kind::Kw_bezzn64),       "Keyword Kw_bezzn64"      },
        {keyword_lexeme(Keyword_kind::Kw_bezzn8),        "Keyword Kw_bezzn8"       },
        {keyword_lexeme(Keyword_kind::Kw_bolshe),        "Keyword Kw_bolshe"       },
        {keyword_lexeme(Keyword_kind::Kw_bezzn16),       "Keyword Kw_bezzn16"      },
        {keyword_lexeme(Keyword_kind::Kw_bezzn128),      "Keyword Kw_bezzn128"     },
        {keyword_lexeme(Keyword_kind::Kw_bezzn32),       "Keyword Kw_bezzn32"      },
        {keyword_lexeme(Keyword_kind::Kw_dlya),          "Keyword Kw_dlya"         },
        {keyword_lexeme(Keyword_kind::Kw_v),             "Keyword Kw_v"            },
        {keyword_lexeme(Keyword_kind::Kw_vozvrat),       "Keyword Kw_vozvrat"      },
        {keyword_lexeme(Keyword_kind::Kw_vechno),        "Keyword Kw_vechno"       },
        {keyword_lexeme(Keyword_kind::Kw_golovnaya),     "Keyword Kw_golovnaya"    },
        {keyword_lexeme(Keyword_kind::Kw_veshch),        "Keyword Kw_veshch"       },
        {keyword_lexeme(Keyword_kind::Kw_vydeli),        "Keyword Kw_vydeli"       },
        {keyword_lexeme(Keyword_kind::Kw_veshch80),      "Keyword Kw_veshch80"     },
        {keyword_lexeme(Keyword_kind::Kw_veshch32),      "Keyword Kw_veshch32"     },
        {keyword_lexeme(Keyword_kind::Kw_vyjdi),         "Keyword Kw_vyjdi"        },
        {keyword_lexeme(Keyword_kind::Kw_veshch64),      "Keyword Kw_veshch64"     },
        {keyword_lexeme(Keyword_kind::Kw_vyberi),        "Keyword Kw_vyberi"       },
        {keyword_lexeme(Keyword_kind::Kw_veshch128),     "Keyword Kw_veshch128"    },
        {keyword_lexeme(Keyword_kind::Kw_konst),         "Keyword Kw_konst"        },
        {keyword_lexeme(Keyword_kind::Kw_esli),          "Keyword Kw_esli"         },
        {keyword_lexeme(Keyword_kind::Kw_kvat),          "Keyword Kw_kvat"         },
        {keyword_lexeme(Keyword_kind::Kw_kompl128),      "Keyword Kw_kompl128"     },
        {keyword_lexeme(Keyword_kind::Kw_iz),            "Keyword Kw_iz"           },
        {keyword_lexeme(Keyword_kind::Kw_kvat32),        "Keyword Kw_kvat32"       },
        {keyword_lexeme(Keyword_kind::Kw_kompl80),       "Keyword Kw_kompl80"      },
        {keyword_lexeme(Keyword_kind::Kw_inache),        "Keyword Kw_inache"       },
        {keyword_lexeme(Keyword_kind::Kw_kvat64),        "Keyword Kw_kvat64"       },
        {keyword_lexeme(Keyword_kind::Kw_kompl64),       "Keyword Kw_kompl64"      },
        {keyword_lexeme(Keyword_kind::Kw_ines),          "Keyword Kw_ines"         },
        {keyword_lexeme(Keyword_kind::Kw_kompl32),       "Keyword Kw_kompl32"      },
        {keyword_lexeme(Keyword_kind::Kw_ispolzuet),     "Keyword Kw_ispolzuet"    },
        {keyword_lexeme(Keyword_kind::Kw_kompl),         "Keyword Kw_kompl"        },
        {keyword_lexeme(Keyword_kind::Kw_istina),        "Keyword Kw_istina"       },
        {keyword_lexeme(Keyword_kind::Kw_kvat128),       "Keyword Kw_kvat128"      },
        {keyword_lexeme(Keyword_kind::Kw_kak),           "Keyword Kw_kak"          },
        {keyword_lexeme(Keyword_kind::Kw_kvat80),        "Keyword Kw_kvat80"       },
        {keyword_lexeme(Keyword_kind::Kw_kategorija),    "Keyword Kw_kategorija"   },
        {keyword_lexeme(Keyword_kind::Kw_lyambda),       "Keyword Kw_lyambda"      },
        {keyword_lexeme(Keyword_kind::Kw_realizuj),      "Keyword Kw_realizuj"     },
        {keyword_lexeme(Keyword_kind::Kw_element),       "Keyword Kw_element"      },
        {keyword_lexeme(Keyword_kind::Kw_lozh),          "Keyword Kw_lozh"         },
        {keyword_lexeme(Keyword_kind::Kw_shkalu),        "Keyword Kw_shkalu"       },
        {keyword_lexeme(Keyword_kind::Kw_modul),         "Keyword Kw_modul"        },
        {keyword_lexeme(Keyword_kind::Kw_pusto),         "Keyword Kw_pusto"        },
        {keyword_lexeme(Keyword_kind::Kw_realizacija),   "Keyword Kw_realizacija"  },
        {keyword_lexeme(Keyword_kind::Kw_malenkoe),      "Keyword Kw_malenkoe"     },
        {keyword_lexeme(Keyword_kind::Kw_perem),         "Keyword Kw_perem"        },
        {keyword_lexeme(Keyword_kind::Kw_stroka),        "Keyword Kw_stroka"       },
        {keyword_lexeme(Keyword_kind::Kw_perech_mnozh),  "Keyword Kw_perech_mnozh" },
        {keyword_lexeme(Keyword_kind::Kw_stroka8),       "Keyword Kw_stroka8"      },
        {keyword_lexeme(Keyword_kind::Kw_log),           "Keyword Kw_log"          },
        {keyword_lexeme(Keyword_kind::Kw_funktsiya),     "Keyword Kw_funktsiya"    },
        {keyword_lexeme(Keyword_kind::Kw_ekviv),         "Keyword Kw_ekviv"        },
        {keyword_lexeme(Keyword_kind::Kw_pokuda),        "Keyword Kw_pokuda"       },
        {keyword_lexeme(Keyword_kind::Kw_eksport),       "Keyword Kw_eksport"      },
        {keyword_lexeme(Keyword_kind::Kw_paket),         "Keyword Kw_paket"        },
        {keyword_lexeme(Keyword_kind::Kw_nichto),        "Keyword Kw_nichto"       },
        {keyword_lexeme(Keyword_kind::Kw_pauk),          "Keyword Kw_pauk"         },
        {keyword_lexeme(Keyword_kind::Kw_perechislenie), "Keyword Kw_perechislenie"},
        {keyword_lexeme(Keyword_kind::Kw_simv),          "Keyword Kw_simv"         },
        {keyword_lexeme(Keyword_kind::Kw_shkala),        "Keyword Kw_shkala"       },
        {keyword_lexeme(Keyword_kind::Kw_postfiksnaya),  "Keyword Kw_postfiksnaya" },
        {keyword_lexeme(Keyword_kind::Kw_stroka32),      "Keyword Kw_stroka32"     },
        {keyword_lexeme(Keyword_kind::Kw_chistaya),      "Keyword Kw_chistaya"     },
        {keyword_lexeme(Keyword_kind::Kw_osvobodi),      "Keyword Kw_osvobodi"     },
        {keyword_lexeme(Keyword_kind::Kw_tsel128),       "Keyword Kw_tsel128"      },
        {keyword_lexeme(Keyword_kind::Kw_struktura),     "Keyword Kw_struktura"    },
        {keyword_lexeme(Keyword_kind::Kw_psevdonim),     "Keyword Kw_psevdonim"    },
        {keyword_lexeme(Keyword_kind::Kw_simv8),         "Keyword Kw_simv8"        },
        {keyword_lexeme(Keyword_kind::Kw_razbor),        "Keyword Kw_razbor"       },
        {keyword_lexeme(Keyword_kind::Kw_prefiksnaya),   "Keyword Kw_prefiksnaya"  },
        {keyword_lexeme(Keyword_kind::Kw_tsel32),        "Keyword Kw_tsel32"       },
        {keyword_lexeme(Keyword_kind::Kw_povtoryaj),     "Keyword Kw_povtoryaj"    },
        {keyword_lexeme(Keyword_kind::Kw_operaciya),     "Keyword Kw_operaciya"    },
        {keyword_lexeme(Keyword_kind::Kw_ssylka),        "Keyword Kw_ssylka"       },
        {keyword_lexeme(Keyword_kind::Kw_tsel16),        "Keyword Kw_tsel16"       },
        {keyword_lexeme(Keyword_kind::Kw_preobrazuj),    "Keyword Kw_preobrazuj"   },
        {keyword_lexeme(Keyword_kind::Kw_tsel8),         "Keyword Kw_tsel8"        },
        {keyword_lexeme(Keyword_kind::Kw_rassmatrivaj),  "Keyword Kw_rassmatrivaj" },
        {keyword_lexeme(Keyword_kind::Kw_log16),         "Keyword Kw_log16"        },
        {keyword_lexeme(Keyword_kind::Kw_tsel64),        "Keyword Kw_tsel64"       },
        {keyword_lexeme(Keyword_kind::Kw_samo),          "Keyword Kw_samo"         },
        {keyword_lexeme(Keyword_kind::Kw_tsel),          "Keyword Kw_tsel"         },
        {keyword_lexeme(Keyword_kind::Kw_simv32),        "Keyword Kw_simv32"       },
        {keyword_lexeme(Keyword_kind::Kw_to),            "Keyword Kw_to"           },
        {keyword_lexeme(Keyword_kind::Kw_massiv),        "Keyword Kw_massiv"       },
        {keyword_lexeme(Keyword_kind::Kw_log8),          "Keyword Kw_log8"         },
        {keyword_lexeme(Keyword_kind::Kw_poka),          "Keyword Kw_poka"         },
        {keyword_lexeme(Keyword_kind::Kw_tip),           "Keyword Kw_tip"          },
        {keyword_lexeme(Keyword_kind::Kw_log32),         "Keyword Kw_log32"        },
        {keyword_lexeme(Keyword_kind::Kw_stroka16),      "Keyword Kw_stroka16"     },
        {keyword_lexeme(Keyword_kind::Kw_meta),          "Keyword Kw_meta"         },
        {keyword_lexeme(Keyword_kind::Kw_simv16),        "Keyword Kw_simv16"       },
        {keyword_lexeme(Keyword_kind::Kw_log64),         "Keyword Kw_log64"        },
        {keyword_lexeme(Keyword_kind::Kw_sravni_s),      "Keyword Kw_sravni_s"     },
        {keyword_lexeme(Keyword_kind::Kw_poryadok16),    "Keyword Kw_poryadok16"   },
        {keyword_lexeme(Keyword_kind::Kw_poryadok),      "Keyword Kw_poryadok"     },
        {keyword_lexeme(Keyword_kind::Kw_menshe),        "Keyword Kw_menshe"       },
        {keyword_lexeme(Keyword_kind::Kw_poryadok64),    "Keyword Kw_poryadok64"   },
        {keyword_lexeme(Keyword_kind::Kw_poryadok32),    "Keyword Kw_poryadok32"   },
        {keyword_lexeme(Keyword_kind::Kw_ravno),         "Keyword Kw_ravno"        },
        {keyword_lexeme(Keyword_kind::Kw_poryadok8),     "Keyword Kw_poryadok8"    }
    };

    struct Ident_components{
        std::vector<std::u32string> prefix_;
        std::u32string              ident_;
    };

    static const Ident_components identifiers[] = {
        {{}, U"лунатик65"},
        {{}, U"_form"},
        {{U"math"}, U"besselj"},
        {{U"геометрия", U"фигуры"}, U"треугольник"},
    };

    using Char_trie_ptr         = std::shared_ptr<Char_trie>;
    using Module_parts_trie_ptr = std::shared_ptr<Trie_for_vector<std::size_t>>;

    static const std::u32string scope_resolution = U"::";

    static const Pair_for_test create_test_for_identifier(const Ident_components&      id,
                                                          const Char_trie_ptr&         ids_trie,
                                                          const Module_parts_trie_ptr& module_name_parts_trie)
    {
        Pair_for_test            result;
        Lexeme                   li;
        std::vector<std::size_t> indeces_of_parts_of_prefix;

        li.code_.kind_    = Lexem_kind::Id;
        li.code_.subkind_ = 0;

        for(const auto& p : id.prefix_){
            std::size_t idx = ids_trie->insert(p);
            indeces_of_parts_of_prefix.push_back(idx);
        }
        std::size_t prefix_idx = module_name_parts_trie->insert_vector(indeces_of_parts_of_prefix);

        li.id_info_.qualifying_prefix_ = prefix_idx;
        li.id_info_.id_idx_            = ids_trie->insert(id.ident_);
        result.first                   = li;
        std::u32string id_str          = join2(join(std::begin(id.prefix_),
                                                    std::end(id.prefix_),
                                                    scope_resolution),
                                               id.ident_,
                                               scope_resolution);
        result.second                  = u32string_to_utf8(U"Id " + id_str);
        return result;
    }


    static std::vector<Pair_for_test> tests_for_identifiers;

    static void create_identifiers_tests(const Char_trie_ptr&         ids_trie,
                                         const Module_parts_trie_ptr& module_name_parts_trie)
    {
        for(const auto& id : identifiers){
            tests_for_identifiers.push_back(create_test_for_identifier(id, ids_trie, module_name_parts_trie));
        }
    }

    void test_lexeme_to_string()
    {
        auto ids_trie               = std::make_shared<Char_trie>();
        auto strs_trie              = std::make_shared<Char_trie>();
        auto module_name_parts_trie = std::make_shared<Trie_for_vector<std::size_t>>();
        auto to_string_lambda = [&ids_trie, &strs_trie, &module_name_parts_trie]
            (const arkona_scanner_snd_lvl::Lexeme_info& li)
        {
            return arkona_scanner_snd_lvl::to_string(li,
                                                     ids_trie,
                                                     strs_trie,
                                                     module_name_parts_trie);
        };
        puts("Testing of converting of a lexeme into the string...\n\n");

        puts("Running tests for special lexemes (UnknownLexem and None)...");
        testing::test(std::begin(special_lexemes_tests),
                      std::end(special_lexemes_tests),
                      to_string_lambda);

        puts("Running tests for keywords...");
        testing::test(std::begin(keywords_tests),
                      std::end(keywords_tests),
                      to_string_lambda);

        create_identifiers_tests(ids_trie, module_name_parts_trie);
        puts("Running tests for identifiers...");
        testing::test(std::begin(tests_for_identifiers),
                      std::end(tests_for_identifiers),
                      to_string_lambda);
    }
};
