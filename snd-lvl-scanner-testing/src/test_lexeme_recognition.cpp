/*
    File:    test_lexeme_recognition.cpp
    Created: 03 February 2020 at 07:16 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <vector>
#include <string>
#include <iterator>
#include <memory>
#include <cstddef>
#include <cstdio>
#include "../include/test_lexeme_recognition.h"
#include "../include/lexeme_fab.h"
#include "../../snd-lvl-scanner/include/arkona-scanner-snd-lvl.h"
#include "../../other/include/testing.hpp"
#include "../../tries/include/errors_and_tries.h"
#include "../../tries/include/char_trie.h"
#include "../../tries/include/trie_for_vector.h"
#include "../../scanner/include/location.h"
#include "../../numbers/include/digit_to_int.h"

namespace snd_lvl_testing_scanner{
    using Token                 = arkona_scanner_snd_lvl::Arkona_token_snd_lvl;
    using Module_parts_trie_ptr = std::shared_ptr<Trie_for_vector<std::size_t>>;
    using Scanner               = arkona_scanner_snd_lvl::Scanner;
    using Scanner_ptr           = std::shared_ptr<Scanner>;

    static const char32_t* test_texts[] = {
        // Text #0:
        UR"~(
           0123'45689
                         302'231'454'903'657'293'676'544         158'456'325'028'528'675'187'087'900'672



       1'329'227'995'784'915'872'903'807'060'280'344'576
                  340'282'366'920'938'463'463'374'607'431'768'211'455

                  340282366920938463463374607431768211455



 0xffff'ffff'ffffff'ffffffffffffff'ffff

                0x1238912345                   0o123'755'5551'232'4561'2345       0o123755555123245612345



        0xdeadbeafde123456789abcde

0b1111111110000111

                           0b1111111110000111101010001110001111000001111100000001111111110111111010101000001011110011110100001111110111111011010100101010101

                           0b10101)~",

        // Text #1:
        UR"~(
          3.1415926535897932384626


            45678.912'34'567'8923'45E-12           3.45678e+35


        5141.592653e+8   6.141592653148e+8f


            3.141592653e-78q  9141.59265356e-78q  2.141592653e+11d


      71415926537.182818e-8d     6.141592653148e+8f


        4'5.678912'34'567'8923'45E-1'2                  45760.912'34'567'8923'45E-223i
                45760.912'34'567'8923'45E-223j    45760.912'34'567'8923'45E-223k


          0.618281828459045e-4x    0.618281828459045e+4x    0.618281828459045e+4xj
                6.141592653148e+8fk 4'5.678912'34'567'8923'45E-1'2qj 0.6)~",

        // Text #2:
        UR"~(''''

        'Ϣ'   'ϣ'            'Ϥ' 'ϥ'

    'Ϧ'  'ϧ'   'Ϩ'     'ϩ'    'Ϫ'    'ϫ'


            'Ϭ'  'ϭ'  'Ϯ'    'ϯ'   'ϰ'  'ϱ'   'ϲ'   'ϳ'   'ϴ'   'ϵ'  '϶'


                'Ϸ'   'ϸ'  'Ϲ'  'Ϻ'  'ϻ'  'ϼ'  'Ͻ'  'Ͼ'  'Ͽ'  'Ѐ'  'Ё'     'Ђ'   'Ѓ'   'Є'   'Ѕ'

  'І'   'Ї'   'Ј'  'Љ'   'Њ'   'Ћ'   'Ќ'  'Ѝ'  'Ў'  'Џ'  'А'  'Б'  'В'  'Г'   'Д'    'Е'     'Ж'     'З'    'И'

            'Й'   'К'       'Л'    'М'       'Н'


                       'О'      'џ'   'Ѡ'          'ѡ'     'Ѣ'           'ѣ'

          'Ѥ'               'ѥ'            'Ѧ'           'ѧ'      'Ѩ'          'ѩ'

      'Ѫ'         'ѫ'                'Ѭ'             'ѭ'       'Ѯ'      'ѯ'

            'Ѱ'        'ѱ'    'Ѳ'        'ѳ'    'Ѵ'       'ѵ'       'Ѷ'   '"')~",

        // Text #3:
        UR"~(
    б              бег

            бор
                 бе
        бо

белый     безмолвный                  безмятежный
        бурый
                болото

безликий            беззнаковый
        беззн1024           больной_кот

            большеголовый

                    беззн16             беззн8
    беззн32     беззн64         беззн128

            большое

                        беззн
        больше

  __лунатики_прилетели _негр_литературный



веский_довод

                в
        выйди         вон        выбери

                выбери_меня         выдели_цветом

    возврат        выдели

            вещественных_чисел_поле

                                        вещ    вечно

        вечный_кипр   вещ80       вещ128     вещ32     вещ64    вещ256

    веский_довод

                в
        выйди         вон        выбери

                выбери_меня         выдели_цветом

    возврат        выдели


главный головная       грач

            вещественных_чисел_поле

            для     длительный         деловая_колбаса

                                        вещ    вечно

                         если          есть          ел_ем_и_буду_есть

        вечный_кипр   вещ80       вещ128     вещ32     вещ64    вещ256


из    инес иначе


            конст           как            компл

    кват       компл64            кват32          компл80

        кват64            компл128         кват80             компл32

                кват128

  категория

                комплексное_значение     кватернион          колонист

        исторический        использует

                исконный       истина           используемый     истинный

            иностранный



    логарифм       лог         ложь         лживый     логический

        лягушка       лямбда        лог64     линейный

            лог8              ля           лог32          лог16



        маляр       маленькое        массив

                мандрагора      маска


    мета             мечта             метафора

            меньше          меньшинство            модуль



        магический

                    ничто     нечто     начальный        новый




            операция       оператор         освободи       крылатый


 паук   опёнок  поучайте_лучше_ваших_паучат         пакет

            пакетный_менеджер                 повторяй        повторение

  пока        позволить          показывать     покуда          порядок
    порядковое_числительное  порядок64   порядок8   порядок16     порядок32

        постфиксная      преобразуй
                префиксная

            псевдоним                          пусто         пустота   вакуум


    равенство    равно     разность

        разбор    рассматривай   рассказ

реализуй            реализация      концепция_концепции_категории


        сослать        ссылочный_тип    ссылка

                самовар    само        структура


    строка32       строка8               строка           строка16

                сравнение     сравни_с              символьные_данные

        симв         симв32              симв8

                симв16



то       тип  функция          трогательный     типовой

    целостный             целый  цел

    феррит         флот     фумигатор


            цел128                         цел8
                    цел16      цел64             цел32   честно      чистая

    шкала      шкалу                        область_целостности


            элементарный      элемент           экспортный_товар        экспорт
                эспериментальный        эквив       эквипотенциальный
)~",
        // Text #4:
        UR"~(;      !
                             !=
    !&&
                          !||         #    ->
        !&&.        !||.
              ##               %
            !&&=            !||=
                       %.
                                    !&&.=        -=

          --<
                    !||.=

  %=                %.=

      --                        -


          &&.=                 &         (:     (
                  &&                &=
   &&.       &&=          )


:) : :: :=
    :>    :}     :]

   /*   /=  /\=  /\   /

,   ]
        */  **   *
                        **.   *=        **=
                **.=             +                     ++


      +=              ++<

               .          ..                      .|.


                 ==         =

  <                    <-           <:
           <<         <=                               <|


      <!>   <&>     <+>     <&&>          <+>=

         <:>               <<=            <?>   <??>

  \      [:     \=        [
@    ^^      ^=             ^^=    ^

     |            |#|       |=
           ||   ||.     ||=        ||.=

  {..} {                >  >=   >>               >>=


         ~             ~|         ~&=           ~&  ~|=

    })~",

        // Text #5:
        UR"~(    буся ::киса

        пыжик::файловый::кот12_

    a1::b2::c3::сравни_с   гена::сравни_с

        цел


        у1::цеце5::+

            айболит

                бармаглот  :: кот_бегемот

        беззн)~",

        // Text #6:
        UR"~(    'f' $65

"Bittida en morgon innan solen upprann
Innan foglarna började sjunga
Bergatroliet friade till fager ungersven
Hon hade en falskeliger tunga "

            "Тебе, девка, житье у меня будет лёгкое, --- не столько работать, сколько отдыхать будешь!
Утром станешь, ну, как подобат, --- до свету. Избу вымоешь, двор уберешь, коров подоишь, на поскотину выпустишь, в хлеву приберешься и --- спи-отдыхай!
Завтрак состряпаешь, самовар согреешь, нас с матушкой завтраком накормишь --- спи-отдыхай!
В поле поработашь, али в огороде пополешь, коли зимой --- за дровами али за сеном съездишь и --- спи-отдыхай!
Обед сваришь, пирогов напечешь: мы с матушкой обедать сядем, а ты --- спи-отдыхай!
После обеда посуду вымоешь, избу приберешь и --- спи-отдыхай!
Коли время подходяче --- в лес по ягоду, по грибы сходишь, али матушка в город спосылат, дак сбегашь. До городу --- рукой подать, и восьми верст не будет, а потом --- спи-отдыхай!
Из городу прибежишь, самовар поставишь. Мы с матушкой чай станем пить, а ты --- спи-отды­хай!
Вечером коров встретишь, подоишь, попоишь, корм задашь и --- спи-отдыхай!
Ужену сваришь, мы с матушкой съедим, а ты --- спи-отдыхай!
Воды наносишь, дров наколешь --- это к завтрему, и --- спи-отдыхай!
Постели наладишь, нас с матушкой спать повалишь. А ты, девка, день-деньской проспишь-про­отдыхаешь --- во что ночь-то будешь спать?
Ночью попрядёшь, поткешь, повышивашь, пошьешь и опять --- спи-отдыхай!
Ну, под утро белье постирать, которо надо --- поштопашь да зашьешь и --- спи-отдыхай!
Да ведь, девка, не даром. Деньги платить буду. Кажной год по рублю! Сама подумай. Сто годов --- сто рублев. Богатейкой станешь! "


"""У попа была собака"""

        $123
          $0x0401    $0x41

            $0o103

               $0b01000010

          $123

$1025
      $10'25
            $0x0'46'C
                $0x4d'e   $0b0001'1010'0010

                    $0o1746  $0o1'74'5


      u8   u32 $67  u16    +
            u8'З' $0x11d   u16'ĝ' u32$0x11d u16"Привет, Чебурашка!")~",

        // Text #7:
        UR"~(-- /*  +=  ** */ (:)~",
    };


    static bool is_digit(char32_t c)
    {
        bool result;
        result = ((U'0' <= c) && (c <= U'9')) ||
                 ((U'A' <= c) && (c <= U'F')) ||
                 ((U'a' <= c) && (c <= U'f'));
        return result;
    }

    static unsigned __int128 str_to_int(const char32_t* str, unsigned base = 10)
    {
        unsigned __int128 result = 0;
        char32_t          c;
        while((c = *str++)){
            if(is_digit(c)){
                result = result * base + digit_to_int(c);
            }
        }
        return result;
    }

    static const unsigned __int128 int_vals[] = {
        str_to_int(UR"~(0123'45689)~"),
        str_to_int(UR"~(302'231'454'903'657'293'676'544)~"),
        str_to_int(UR"~(158'456'325'028'528'675'187'087'900'672)~"),
        str_to_int(UR"~(1'329'227'995'784'915'872'903'807'060'280'344'576)~"),
        str_to_int(UR"~(340'282'366'920'938'463'463'374'607'431'768'211'455)~"),
        str_to_int(UR"~(340282366920938463463374607431768211455)~"),
        str_to_int(UR"~(0xffff'ffff'ffffff'ffffffffffffff'ffff)~", 16),
        str_to_int(UR"~(0x1238912345)~", 16),
        str_to_int(UR"~(0o123'755'5551'232'4561'2345)~", 8),
        str_to_int(UR"~(0o123755555123245612345)~", 8),
        str_to_int(UR"~(0xdeadbeafde123456789abcde)~", 16),
        str_to_int(UR"~(1111111110000111)~", 2),
        str_to_int(UR"~(1111111110000111101010001110001111000001111100000001111111110111111010101000001011110011110100001111110111111011010100101010101)~", 2),
        str_to_int(UR"~(10101)~", 2),
    };

    static const quat::quat_t<__float128> quat_vals[] = {
        {0.0q, 0.0q, 45760.91234567892345E-223q, 0.0q                      },
        {0.0q, 0.0q, 0.0q,                       45760.91234567892345E-223q},
        {0.0q, 0.0q, 0.618281828459045e+4q,      0.0q                      },
        {0.0q, 0.0q, 0.0q,                       6.141592653148e+8q        },
        {0.0q, 0.0q, 45.67891234567892345E-12q,  0.0q                      },
    };

    static const std::vector<Token> tokens_for_texts[] = {
        {
            // References for text #0:
            {{{2,  12 }, {2,  21 }}, integer_lexeme(int_vals[0])                              }, // Integer 0123'45689
            {{{3,  26 }, {3,  56 }}, integer_lexeme(int_vals[1])                              }, // Integer 302'231'454'903'657'293'676'544
            {{{3,  66 }, {3,  104}}, integer_lexeme(int_vals[2])                              }, // Integer 158'456'325'028'528'675'187'087'900'672
            {{{7,  8  }, {7,  56 }}, integer_lexeme(int_vals[3])                              }, // Integer 1'329'227'995'784'915'872'903'807'060'280'344'576
            {{{8,  19 }, {8,  69 }}, integer_lexeme(int_vals[4])                              }, // Integer 340'282'366'920'938'463'463'374'607'431'768'211'455
            {{{10, 19 }, {10, 57 }}, integer_lexeme(int_vals[5])                              }, // Integer 340282366920938463463374607431768211455
            {{{14, 2  }, {14, 39 }}, integer_lexeme(int_vals[6])                              }, // Integer 0xffff'ffff'ffffff'ffffffffffffff'ffff
            {{{16, 17 }, {16, 28 }}, integer_lexeme(int_vals[7])                              }, // Integer 0x1238912345
            {{{16, 48 }, {16, 75 }}, integer_lexeme(int_vals[8])                              }, // Integer 0o123'755'5551'232'4561'2345
            {{{16, 83 }, {16, 105}}, integer_lexeme(int_vals[9])                              }, // Integer 0o123755555123245612345
            {{{20, 9  }, {20, 34 }}, integer_lexeme(int_vals[10])                             }, // Integer 0xdeadbeafde123456789abcde
            {{{22, 1  }, {22, 18 }}, integer_lexeme(int_vals[11])                             }, // Integer 0b1111111110000111
            {{{24, 28 }, {24, 156}}, integer_lexeme(int_vals[12])                             }, // Integer 0b1111111110000111101010001110001111000001111100000001111111110111111010101000001011110011110100001111110111111011010100101010101
            {{{26, 28 }, {26, 34 }}, integer_lexeme(int_vals[13])                             }, // Integer 0b10101
            {{{26, 35 }, {26, 35 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #1:
            {{{2,  11 }, {2,  34 }}, float_lexeme(3.1415926535897932384626q)                  }, // Float 3.1415926535897932384626
            {{{5,  13 }, {5,  40 }}, float_lexeme(45678.91234567892345E-12q)                  }, // Float 45678.91234567892345E-12
            {{{5,  52 }, {5,  62 }}, float_lexeme(3.45678e+35q)                               }, // Float 3.45678e+35
            {{{8,  9  }, {8,  22 }}, float_lexeme(5141.592653e+8q)                            }, // Float 5141.592653e+8
            {{{8,  26 }, {8,  43 }}, float_lexeme(6.141592653148e+8q, Float_kind::Float32)    }, // Float 6.141592653148e+8
            {{{11, 13 }, {11, 28 }}, float_lexeme(3.141592653e-78q, Float_kind::Float128)     }, // Float 3.141592653e-78
            {{{11, 31 }, {11, 48 }}, float_lexeme(9141.59265356e-78q, Float_kind::Float128)   }, // Float 9141.59265356e-78
            {{{11, 51 }, {11, 66 }}, float_lexeme(2.141592653e+11q, Float_kind::Float64)      }, // Float 2.141592653e+11
            {{{14, 7  }, {14, 28 }}, float_lexeme(71415926537.182818e-8q)                     }, // Float 71415926537.182818e-8
            {{{14, 34 }, {14, 51 }}, float_lexeme(6.141592653148e+8q, Float_kind::Float32)    }, // Float 6.141592653148e+8
            {{{17, 9  }, {17, 38 }}, float_lexeme(4'5.678912'34'567'8923'45E-1'2q)            }, // Float 45.67891234567892345E-12
            {{{17, 57 }, {17, 86 }}, complex_lexeme({0.0q, 45760.91234567892345E-223q})       }, // Complex 45760.912'34'567'8923'45E-223i
            {{{18, 17 }, {18, 46 }}, quat_lexeme(quat_vals[0])                                }, // Quat 45760.912'34'567'8923'45E-223j
            {{{18, 51 }, {18, 80 }}, quat_lexeme(quat_vals[1])                                }, // Quat 45760.912'34'567'8923'45E-223k
            {{{21, 11 }, {21, 31 }}, float_lexeme(0.618281828459045e-4q, Float_kind::Float80) }, // Float 0.618281828459045e-4
            {{{21, 36 }, {21, 56 }}, float_lexeme(0.618281828459045e+4q, Float_kind::Float80) }, // Float 0.618281828459045e+4
            {{{21, 61 }, {21, 82 }}, quat_lexeme(quat_vals[2], Quat_kind::Quat80)             }, // Quat 0.618281828459045e+4j
            {{{22, 17 }, {22, 35 }}, quat_lexeme(quat_vals[3], Quat_kind::Quat32)             }, // Quat 6.141592653148e+8k
            {{{22, 37 }, {22, 68 }}, quat_lexeme(quat_vals[4], Quat_kind::Quat128)            }, // Quat 45.67891234567892345E-12j
            {{{22, 70 }, {22, 72 }}, float_lexeme(0.6q)                                       }, // Float 0.6
            {{{22, 73 }, {22, 73 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #2:
            {{{1,  1  }, {1,  4  }}, char_lexeme(U'\'')                                       }, // Char \'
            {{{3,  9  }, {3,  11 }}, char_lexeme(U'Ϣ')                                        }, // Char 'Ϣ'
            {{{3,  15 }, {3,  17 }}, char_lexeme(U'ϣ')                                        }, // Char 'ϣ'
            {{{3,  30 }, {3,  32 }}, char_lexeme(U'Ϥ')                                        }, // Char 'Ϥ'
            {{{3,  34 }, {3,  36 }}, char_lexeme(U'ϥ')                                        }, // Char 'ϥ'
            {{{5,  5  }, {5,  7  }}, char_lexeme(U'Ϧ')                                        }, // Char 'Ϧ'
            {{{5,  10 }, {5,  12 }}, char_lexeme(U'ϧ')                                        }, // Char 'ϧ'
            {{{5,  16 }, {5,  18 }}, char_lexeme(U'Ϩ')                                        }, // Char 'Ϩ'
            {{{5,  24 }, {5,  26 }}, char_lexeme(U'ϩ')                                        }, // Char 'ϩ'
            {{{5,  31 }, {5,  33 }}, char_lexeme(U'Ϫ')                                        }, // Char 'Ϫ'
            {{{5,  38 }, {5,  40 }}, char_lexeme(U'ϫ')                                        }, // Char 'ϫ'
            {{{8,  13 }, {8,  15 }}, char_lexeme(U'Ϭ')                                        }, // Char 'Ϭ'
            {{{8,  18 }, {8,  20 }}, char_lexeme(U'ϭ')                                        }, // Char 'ϭ'
            {{{8,  23 }, {8,  25 }}, char_lexeme(U'Ϯ')                                        }, // Char 'Ϯ'
            {{{8,  30 }, {8,  32 }}, char_lexeme(U'ϯ')                                        }, // Char 'ϯ'
            {{{8,  36 }, {8,  38 }}, char_lexeme(U'ϰ')                                        }, // Char 'ϰ'
            {{{8,  41 }, {8,  43 }}, char_lexeme(U'ϱ')                                        }, // Char 'ϱ'
            {{{8,  47 }, {8,  49 }}, char_lexeme(U'ϲ')                                        }, // Char 'ϲ'
            {{{8,  53 }, {8,  55 }}, char_lexeme(U'ϳ')                                        }, // Char 'ϳ'
            {{{8,  59 }, {8,  61 }}, char_lexeme(U'ϴ')                                        }, // Char 'ϴ'
            {{{8,  65 }, {8,  67 }}, char_lexeme(U'ϵ')                                        }, // Char 'ϵ'
            {{{8,  70 }, {8,  72 }}, char_lexeme(U'϶')                                        }, // Char '϶'
            {{{11, 17 }, {11, 19 }}, char_lexeme(U'Ϸ')                                        }, // Char 'Ϸ'
            {{{11, 23 }, {11, 25 }}, char_lexeme(U'ϸ')                                        }, // Char 'ϸ'
            {{{11, 28 }, {11, 30 }}, char_lexeme(U'Ϲ')                                        }, // Char 'Ϲ'
            {{{11, 33 }, {11, 35 }}, char_lexeme(U'Ϻ')                                        }, // Char 'Ϻ'
            {{{11, 38 }, {11, 40 }}, char_lexeme(U'ϻ')                                        }, // Char 'ϻ'
            {{{11, 43 }, {11, 45 }}, char_lexeme(U'ϼ')                                        }, // Char 'ϼ'
            {{{11, 48 }, {11, 50 }}, char_lexeme(U'Ͻ')                                        }, // Char 'Ͻ'
            {{{11, 53 }, {11, 55 }}, char_lexeme(U'Ͼ')                                        }, // Char 'Ͼ'
            {{{11, 58 }, {11, 60 }}, char_lexeme(U'Ͽ')                                        }, // Char 'Ͽ'
            {{{11, 63 }, {11, 65 }}, char_lexeme(U'Ѐ')                                        }, // Char 'Ѐ'
            {{{11, 68 }, {11, 70 }}, char_lexeme(U'Ё')                                        }, // Char 'Ё'
            {{{11, 76 }, {11, 78 }}, char_lexeme(U'Ђ')                                        }, // Char 'Ђ'
            {{{11, 82 }, {11, 84 }}, char_lexeme(U'Ѓ')                                        }, // Char 'Ѓ'
            {{{11, 88 }, {11, 90 }}, char_lexeme(U'Є')                                        }, // Char 'Є'
            {{{11, 94 }, {11, 96 }}, char_lexeme(U'Ѕ')                                        }, // Char 'Ѕ'
            {{{13, 3  }, {13, 5  }}, char_lexeme(U'І')                                        }, // Char 'І'
            {{{13, 9  }, {13, 11 }}, char_lexeme(U'Ї')                                        }, // Char 'Ї'
            {{{13, 15 }, {13, 17 }}, char_lexeme(U'Ј')                                        }, // Char 'Ј'
            {{{13, 20 }, {13, 22 }}, char_lexeme(U'Љ')                                        }, // Char 'Љ'
            {{{13, 26 }, {13, 28 }}, char_lexeme(U'Њ')                                        }, // Char 'Њ'
            {{{13, 32 }, {13, 34 }}, char_lexeme(U'Ћ')                                        }, // Char 'Ћ'
            {{{13, 38 }, {13, 40 }}, char_lexeme(U'Ќ')                                        }, // Char 'Ќ'
            {{{13, 43 }, {13, 45 }}, char_lexeme(U'Ѝ')                                        }, // Char 'Ѝ'
            {{{13, 48 }, {13, 50 }}, char_lexeme(U'Ў')                                        }, // Char 'Ў'
            {{{13, 53 }, {13, 55 }}, char_lexeme(U'Џ')                                        }, // Char 'Џ'
            {{{13, 58 }, {13, 60 }}, char_lexeme(U'А')                                        }, // Char 'А'
            {{{13, 63 }, {13, 65 }}, char_lexeme(U'Б')                                        }, // Char 'Б'
            {{{13, 68 }, {13, 70 }}, char_lexeme(U'В')                                        }, // Char 'В'
            {{{13, 73 }, {13, 75 }}, char_lexeme(U'Г')                                        }, // Char 'Г'
            {{{13, 79 }, {13, 81 }}, char_lexeme(U'Д')                                        }, // Char 'Д'
            {{{13, 86 }, {13, 88 }}, char_lexeme(U'Е')                                        }, // Char 'Е'
            {{{13, 94 }, {13, 96 }}, char_lexeme(U'Ж')                                        }, // Char 'Ж'
            {{{13, 102}, {13, 104}}, char_lexeme(U'З')                                        }, // Char 'З'
            {{{13, 109}, {13, 111}}, char_lexeme(U'И')                                        }, // Char 'И'
            {{{15, 13 }, {15, 15 }}, char_lexeme(U'Й')                                        }, // Char 'Й'
            {{{15, 19 }, {15, 21 }}, char_lexeme(U'К')                                        }, // Char 'К'
            {{{15, 29 }, {15, 31 }}, char_lexeme(U'Л')                                        }, // Char 'Л'
            {{{15, 36 }, {15, 38 }}, char_lexeme(U'М')                                        }, // Char 'М'
            {{{15, 46 }, {15, 48 }}, char_lexeme(U'Н')                                        }, // Char 'Н'
            {{{18, 24 }, {18, 26 }}, char_lexeme(U'О')                                        }, // Char 'О'
            {{{18, 33 }, {18, 35 }}, char_lexeme(U'џ')                                        }, // Char 'џ'
            {{{18, 39 }, {18, 41 }}, char_lexeme(U'Ѡ')                                        }, // Char 'Ѡ'
            {{{18, 52 }, {18, 54 }}, char_lexeme(U'ѡ')                                        }, // Char 'ѡ'
            {{{18, 60 }, {18, 62 }}, char_lexeme(U'Ѣ')                                        }, // Char 'Ѣ'
            {{{18, 74 }, {18, 76 }}, char_lexeme(U'ѣ')                                        }, // Char 'ѣ'
            {{{20, 11 }, {20, 13 }}, char_lexeme(U'Ѥ')                                        }, // Char 'Ѥ'
            {{{20, 29 }, {20, 31 }}, char_lexeme(U'ѥ')                                        }, // Char 'ѥ'
            {{{20, 44 }, {20, 46 }}, char_lexeme(U'Ѧ')                                        }, // Char 'Ѧ'
            {{{20, 58 }, {20, 60 }}, char_lexeme(U'ѧ')                                        }, // Char 'ѧ'
            {{{20, 67 }, {20, 69 }}, char_lexeme(U'Ѩ')                                        }, // Char 'Ѩ'
            {{{20, 80 }, {20, 82 }}, char_lexeme(U'ѩ')                                        }, // Char 'ѩ'
            {{{22, 7  }, {22, 9  }}, char_lexeme(U'Ѫ')                                        }, // Char 'Ѫ'
            {{{22, 19 }, {22, 21 }}, char_lexeme(U'ѫ')                                        }, // Char 'ѫ'
            {{{22, 38 }, {22, 40 }}, char_lexeme(U'Ѭ')                                        }, // Char 'Ѭ'
            {{{22, 54 }, {22, 56 }}, char_lexeme(U'ѭ')                                        }, // Char 'ѭ'
            {{{22, 64 }, {22, 66 }}, char_lexeme(U'Ѯ')                                        }, // Char 'Ѯ'
            {{{22, 73 }, {22, 75 }}, char_lexeme(U'ѯ')                                        }, // Char 'ѯ'
            {{{24, 13 }, {24, 15 }}, char_lexeme(U'Ѱ')                                        }, // Char 'Ѱ'
            {{{24, 24 }, {24, 26 }}, char_lexeme(U'ѱ')                                        }, // Char 'ѱ'
            {{{24, 31 }, {24, 33 }}, char_lexeme(U'Ѳ')                                        }, // Char 'Ѳ'
            {{{24, 42 }, {24, 44 }}, char_lexeme(U'ѳ')                                        }, // Char 'ѳ'
            {{{24, 49 }, {24, 51 }}, char_lexeme(U'Ѵ')                                        }, // Char 'Ѵ'
            {{{24, 59 }, {24, 61 }}, char_lexeme(U'ѵ')                                        }, // Char 'ѵ'
            {{{24, 69 }, {24, 71 }}, char_lexeme(U'Ѷ')                                        }, // Char 'Ѷ'
            {{{24, 75 }, {24, 77 }}, char_lexeme(U'"')                                        }, // Char '"'
            {{{24, 78 }, {24, 78 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
           // References for text #3:
            {{{2,  5  }, {2,  5  }}, id_lexeme(1)                                             }, // Id б
            {{{2,  20 }, {2,  22 }}, id_lexeme(3)                                             }, // Id бег
            {{{4,  13 }, {4,  15 }}, id_lexeme(5)                                             }, // Id бор
            {{{5,  18 }, {5,  19 }}, id_lexeme(2)                                             }, // Id бе
            {{{6,  9  }, {6,  10 }}, id_lexeme(4)                                             }, // Id бо
            {{{8,  1  }, {8,  5  }}, id_lexeme(8)                                             }, // Id белый
            {{{8,  11 }, {8,  20 }}, id_lexeme(16)                                            }, // Id безмолвный
            {{{8,  39 }, {8,  49 }}, id_lexeme(23)                                            }, // Id безмятежный
            {{{9,  9  }, {9,  13 }}, id_lexeme(27)                                            }, // Id бурый
            {{{10, 17 }, {10, 22 }}, id_lexeme(31)                                            }, // Id болото
            {{{12, 1  }, {12, 8  }}, id_lexeme(36)                                            }, // Id безликий
            {{{12, 21 }, {12, 31 }}, id_lexeme(44)                                            }, // Id беззнаковый
            {{{13, 9  }, {13, 17 }}, id_lexeme(48)                                            }, // Id беззн1024
            {{{13, 29 }, {13, 39 }}, id_lexeme(56)                                            }, // Id больной_кот
            {{{15, 13 }, {15, 25 }}, id_lexeme(65)                                            }, // Id большеголовый
            {{{17, 21 }, {17, 27 }}, keyword_lexeme(Keyword_kind::Kw_bezzn16)                 }, // Keyword Kw_bezzn16
            {{{17, 41 }, {17, 46 }}, keyword_lexeme(Keyword_kind::Kw_bezzn8)                  }, // Keyword Kw_bezzn8
            {{{18, 5  }, {18, 11 }}, keyword_lexeme(Keyword_kind::Kw_bezzn32)                 }, // Keyword Kw_bezzn32
            {{{18, 17 }, {18, 23 }}, keyword_lexeme(Keyword_kind::Kw_bezzn64)                 }, // Keyword Kw_bezzn64
            {{{18, 33 }, {18, 40 }}, keyword_lexeme(Keyword_kind::Kw_bezzn128)                }, // Keyword Kw_bezzn128
            {{{20, 13 }, {20, 19 }}, keyword_lexeme(Keyword_kind::Kw_bolshoe)                 }, // Keyword Kw_bolshoe
            {{{22, 25 }, {22, 29 }}, keyword_lexeme(Keyword_kind::Kw_bezzn)                   }, // Keyword Kw_bezzn
            {{{23, 9  }, {23, 14 }}, keyword_lexeme(Keyword_kind::Kw_bolshe)                  }, // Keyword Kw_bolshe
            {{{25, 3  }, {25, 22 }}, id_lexeme(85)                                            }, // Id __лунатики_прилетели
            {{{25, 24 }, {25, 41 }}, id_lexeme(102)                                           }, // Id _негр_литературный
            {{{29, 1  }, {29, 12 }}, id_lexeme(114)                                           }, // Id веский_довод
            {{{31, 17 }, {31, 17 }}, keyword_lexeme(Keyword_kind::Kw_v)                       }, // Keyword Kw_v
            {{{32, 9  }, {32, 13 }}, keyword_lexeme(Keyword_kind::Kw_vyjdi)                   }, // Keyword Kw_vyjdi
            {{{32, 23 }, {32, 25 }}, id_lexeme(116)                                           }, // Id вон
            {{{32, 34 }, {32, 39 }}, keyword_lexeme(Keyword_kind::Kw_vyberi)                  }, // Keyword Kw_vyberi
            {{{34, 17 }, {34, 27 }}, id_lexeme(126)                                           }, // Id выбери_меня
            {{{34, 37 }, {34, 49 }}, id_lexeme(137)                                           }, // Id выдели_цветом
            {{{36, 5  }, {36, 11 }}, keyword_lexeme(Keyword_kind::Kw_vozvrat)                 }, // Keyword Kw_vozvrat
            {{{36, 20 }, {36, 25 }}, keyword_lexeme(Keyword_kind::Kw_vydeli)                  }, // Keyword Kw_vydeli
            {{{38, 13 }, {38, 35 }}, id_lexeme(158)                                           }, // Id вещественных_чисел_поле
            {{{40, 41 }, {40, 43 }}, keyword_lexeme(Keyword_kind::Kw_veshch)                  }, // Keyword Kw_veshch
            {{{40, 48 }, {40, 52 }}, keyword_lexeme(Keyword_kind::Kw_vechno)                  }, // Keyword Kw_vechno
            {{{42, 9  }, {42, 19 }}, id_lexeme(167)                                           }, // Id вечный_кипр
            {{{42, 23 }, {42, 27 }}, keyword_lexeme(Keyword_kind::Kw_veshch80)                }, // Keyword Kw_veshch80
            {{{42, 35 }, {42, 40 }}, keyword_lexeme(Keyword_kind::Kw_veshch128)               }, // Keyword Kw_veshch128
            {{{42, 46 }, {42, 50 }}, keyword_lexeme(Keyword_kind::Kw_veshch32)                }, // Keyword Kw_veshch32
            {{{42, 56 }, {42, 60 }}, keyword_lexeme(Keyword_kind::Kw_veshch64)                }, // Keyword Kw_veshch64
            {{{42, 65 }, {42, 70 }}, id_lexeme(170)                                           }, // Id вещ256
            {{{44, 5  }, {44, 16 }}, id_lexeme(114)                                           }, // Id веский_довод
            {{{46, 17 }, {46, 17 }}, keyword_lexeme(Keyword_kind::Kw_v)                       }, // Keyword Kw_v
            {{{47, 9  }, {47, 13 }}, keyword_lexeme(Keyword_kind::Kw_vyjdi)                   }, // Keyword Kw_vyjdi
            {{{47, 23 }, {47, 25 }}, id_lexeme(116)                                           }, // Id вон
            {{{47, 34 }, {47, 39 }}, keyword_lexeme(Keyword_kind::Kw_vyberi)                  }, // Keyword Kw_vyberi
            {{{49, 17 }, {49, 27 }}, id_lexeme(126)                                           }, // Id выбери_меня
            {{{49, 37 }, {49, 49 }}, id_lexeme(137)                                           }, // Id выдели_цветом
            {{{51, 5  }, {51, 11 }}, keyword_lexeme(Keyword_kind::Kw_vozvrat)                 }, // Keyword Kw_vozvrat
            {{{51, 20 }, {51, 25 }}, keyword_lexeme(Keyword_kind::Kw_vydeli)                  }, // Keyword Kw_vydeli
            {{{54, 1  }, {54, 7  }}, id_lexeme(177)                                           }, // Id главный
            {{{54, 9  }, {54, 16 }}, keyword_lexeme(Keyword_kind::Kw_golovnaya)               }, // Keyword Kw_golovnaya
            {{{54, 24 }, {54, 27 }}, id_lexeme(180)                                           }, // Id грач
            {{{56, 13 }, {56, 35 }}, id_lexeme(158)                                           }, // Id вещественных_чисел_поле
            {{{58, 13 }, {58, 15 }}, keyword_lexeme(Keyword_kind::Kw_dlya)                    }, // Keyword Kw_dlya
            {{{58, 21 }, {58, 30 }}, id_lexeme(190)                                           }, // Id длительный
            {{{58, 40 }, {58, 54 }}, id_lexeme(204)                                           }, // Id деловая_колбаса
            {{{60, 41 }, {60, 43 }}, keyword_lexeme(Keyword_kind::Kw_veshch)                  }, // Keyword Kw_veshch
            {{{60, 48 }, {60, 52 }}, keyword_lexeme(Keyword_kind::Kw_vechno)                  }, // Keyword Kw_vechno
            {{{62, 26 }, {62, 29 }}, keyword_lexeme(Keyword_kind::Kw_esli)                    }, // Keyword Kw_esli
            {{{62, 40 }, {62, 43 }}, id_lexeme(208)                                           }, // Id есть
            {{{62, 54 }, {62, 70 }}, id_lexeme(224)                                           }, // Id ел_ем_и_буду_есть
            {{{64, 9  }, {64, 19 }}, id_lexeme(167)                                           }, // Id вечный_кипр
            {{{64, 23 }, {64, 27 }}, keyword_lexeme(Keyword_kind::Kw_veshch80)                }, // Keyword Kw_veshch80
            {{{64, 35 }, {64, 40 }}, keyword_lexeme(Keyword_kind::Kw_veshch128)               }, // Keyword Kw_veshch128
            {{{64, 46 }, {64, 50 }}, keyword_lexeme(Keyword_kind::Kw_veshch32)                }, // Keyword Kw_veshch32
            {{{64, 56 }, {64, 60 }}, keyword_lexeme(Keyword_kind::Kw_veshch64)                }, // Keyword Kw_veshch64
            {{{64, 65 }, {64, 70 }}, id_lexeme(170)                                           }, // Id вещ256
            {{{67, 1  }, {67, 2  }}, keyword_lexeme(Keyword_kind::Kw_iz)                      }, // Keyword Kw_iz
            {{{67, 7  }, {67, 10 }}, keyword_lexeme(Keyword_kind::Kw_ines)                    }, // Keyword Kw_ines
            {{{67, 12 }, {67, 16 }}, keyword_lexeme(Keyword_kind::Kw_inache)                  }, // Keyword Kw_inache
            {{{70, 13 }, {70, 17 }}, keyword_lexeme(Keyword_kind::Kw_konst)                   }, // Keyword Kw_konst
            {{{70, 29 }, {70, 31 }}, keyword_lexeme(Keyword_kind::Kw_kak)                     }, // Keyword Kw_kak
            {{{70, 44 }, {70, 48 }}, keyword_lexeme(Keyword_kind::Kw_kompl)                   }, // Keyword Kw_kompl
            {{{72, 5  }, {72, 8  }}, keyword_lexeme(Keyword_kind::Kw_kvat)                    }, // Keyword Kw_kvat
            {{{72, 16 }, {72, 22 }}, keyword_lexeme(Keyword_kind::Kw_kompl64)                 }, // Keyword Kw_kompl64
            {{{72, 35 }, {72, 40 }}, keyword_lexeme(Keyword_kind::Kw_kvat32)                  }, // Keyword Kw_kvat32
            {{{72, 51 }, {72, 57 }}, keyword_lexeme(Keyword_kind::Kw_kompl80)                 }, // Keyword Kw_kompl80
            {{{74, 9  }, {74, 14 }}, keyword_lexeme(Keyword_kind::Kw_kvat64)                  }, // Keyword Kw_kvat64
            {{{74, 27 }, {74, 34 }}, keyword_lexeme(Keyword_kind::Kw_kompl128)                }, // Keyword Kw_kompl128
            {{{74, 44 }, {74, 49 }}, keyword_lexeme(Keyword_kind::Kw_kvat80)                  }, // Keyword Kw_kvat80
            {{{74, 63 }, {74, 69 }}, keyword_lexeme(Keyword_kind::Kw_kompl32)                 }, // Keyword Kw_kompl32
            {{{76, 17 }, {76, 23 }}, keyword_lexeme(Keyword_kind::Kw_kvat128)                 }, // Keyword Kw_kvat128
            {{{78, 3  }, {78, 11 }}, keyword_lexeme(Keyword_kind::Kw_kategorija)              }, // Keyword Kw_kategorija
            {{{80, 17 }, {80, 36 }}, id_lexeme(244)                                           }, // Id комплексное_значение
            {{{80, 42 }, {80, 51 }}, id_lexeme(253)                                           }, // Id кватернион
            {{{80, 62 }, {80, 69 }}, id_lexeme(259)                                           }, // Id колонист
            {{{82, 9  }, {82, 20 }}, id_lexeme(271)                                           }, // Id исторический
            {{{82, 29 }, {82, 38 }}, keyword_lexeme(Keyword_kind::Kw_ispolzuet)               }, // Keyword Kw_ispolzuet
            {{{84, 17 }, {84, 24 }}, id_lexeme(277)                                           }, // Id исконный
            {{{84, 32 }, {84, 37 }}, keyword_lexeme(Keyword_kind::Kw_istina)                  }, // Keyword Kw_istina
            {{{84, 49 }, {84, 60 }}, id_lexeme(287)                                           }, // Id используемый
            {{{84, 66 }, {84, 73 }}, id_lexeme(292)                                           }, // Id истинный
            {{{86, 13 }, {86, 23 }}, id_lexeme(302)                                           }, // Id иностранный
            {{{90, 5  }, {90, 12 }}, id_lexeme(310)                                           }, // Id логарифм
            {{{90, 20 }, {90, 22 }}, keyword_lexeme(Keyword_kind::Kw_log)                     }, // Keyword Kw_log
            {{{90, 32 }, {90, 35 }}, keyword_lexeme(Keyword_kind::Kw_lozh)                    }, // Keyword Kw_lozh
            {{{90, 45 }, {90, 50 }}, id_lexeme(315)                                           }, // Id лживый
            {{{90, 56 }, {90, 65 }}, id_lexeme(322)                                           }, // Id логический
            {{{92, 9  }, {92, 15 }}, id_lexeme(328)                                           }, // Id лягушка
            {{{92, 23 }, {92, 28 }}, keyword_lexeme(Keyword_kind::Kw_lyambda)                 }, // Keyword Kw_lyambda
            {{{92, 37 }, {92, 41 }}, keyword_lexeme(Keyword_kind::Kw_log64)                   }, // Keyword Kw_log64
            {{{92, 47 }, {92, 54 }}, id_lexeme(335)                                           }, // Id линейный
            {{{94, 13 }, {94, 16 }}, keyword_lexeme(Keyword_kind::Kw_log8)                    }, // Keyword Kw_log8
            {{{94, 31 }, {94, 32 }}, id_lexeme(323)                                           }, // Id ля
            {{{94, 44 }, {94, 48 }}, keyword_lexeme(Keyword_kind::Kw_log32)                   }, // Keyword Kw_log32
            {{{94, 59 }, {94, 63 }}, keyword_lexeme(Keyword_kind::Kw_log16)                   }, // Keyword Kw_log16
            {{{98, 9  }, {98, 13 }}, id_lexeme(340)                                           }, // Id маляр
            {{{98, 21 }, {98, 29 }}, keyword_lexeme(Keyword_kind::Kw_malenkoe)                }, // Keyword Kw_malenkoe
            {{{98, 38 }, {98, 43 }}, keyword_lexeme(Keyword_kind::Kw_massiv)                  }, // Keyword Kw_massiv
            {{{100,17 }, {100,26 }}, id_lexeme(348)                                           }, // Id мандрагора
            {{{100,33 }, {100,37 }}, id_lexeme(351)                                           }, // Id маска
            {{{103,5  }, {103,8  }}, keyword_lexeme(Keyword_kind::Kw_meta)                    }, // Keyword Kw_meta
            {{{103,22 }, {103,26 }}, id_lexeme(355)                                           }, // Id мечта
            {{{103,40 }, {103,47 }}, id_lexeme(361)                                           }, // Id метафора
            {{{105,13 }, {105,18 }}, keyword_lexeme(Keyword_kind::Kw_menshe)                  }, // Keyword Kw_menshe
            {{{105,29 }, {105,39 }}, id_lexeme(370)                                           }, // Id меньшинство
            {{{105,52 }, {105,57 }}, keyword_lexeme(Keyword_kind::Kw_modul)                   }, // Keyword Kw_modul
            {{{109,9  }, {109,18 }}, id_lexeme(378)                                           }, // Id магический
            {{{111,21 }, {111,25 }}, keyword_lexeme(Keyword_kind::Kw_nichto)                  }, // Keyword Kw_nichto
            {{{111,31 }, {111,35 }}, id_lexeme(383)                                           }, // Id нечто
            {{{111,41 }, {111,49 }}, id_lexeme(391)                                           }, // Id начальный
            {{{111,58 }, {111,62 }}, id_lexeme(395)                                           }, // Id новый
            {{{116,13 }, {116,20 }}, keyword_lexeme(Keyword_kind::Kw_operaciya)               }, // Keyword Kw_operaciya
            {{{116,28 }, {116,35 }}, id_lexeme(403)                                           }, // Id оператор
            {{{116,45 }, {116,52 }}, keyword_lexeme(Keyword_kind::Kw_osvobodi)                }, // Keyword Kw_osvobodi
            {{{116,60 }, {116,67 }}, id_lexeme(410)                                           }, // Id крылатый
            {{{119,2  }, {119,5  }}, keyword_lexeme(Keyword_kind::Kw_pauk)                    }, // Keyword Kw_pauk
            {{{119,9  }, {119,14 }}, id_lexeme(414)                                           }, // Id опёнок
            {{{119,17 }, {119,43 }}, id_lexeme(441)                                           }, // Id поучайте_лучше_ваших_паучат
            {{{119,53 }, {119,57 }}, keyword_lexeme(Keyword_kind::Kw_paket)                   }, // Keyword Kw_paket
            {{{121,13 }, {121,29 }}, id_lexeme(457)                                           }, // Id пакетный_менеджер
            {{{121,47 }, {121,54 }}, keyword_lexeme(Keyword_kind::Kw_povtoryaj)               }, // Keyword Kw_povtoryaj
            {{{121,63 }, {121,72 }}, id_lexeme(465)                                           }, // Id повторение
            {{{123,3  }, {123,6  }}, keyword_lexeme(Keyword_kind::Kw_poka)                    }, // Keyword Kw_poka
            {{{123,15 }, {123,23 }}, id_lexeme(472)                                           }, // Id позволить
            {{{123,34 }, {123,43 }}, id_lexeme(480)                                           }, // Id показывать
            {{{123,49 }, {123,54 }}, keyword_lexeme(Keyword_kind::Kw_pokuda)                  }, // Keyword Kw_pokuda
            {{{123,65 }, {123,71 }}, keyword_lexeme(Keyword_kind::Kw_poryadok)                }, // Keyword Kw_poryadok
            {{{124,5  }, {124,27 }}, id_lexeme(501)                                           }, // Id порядковое_числительное
            {{{124,30 }, {124,38 }}, keyword_lexeme(Keyword_kind::Kw_poryadok64)              }, // Keyword Kw_poryadok64
            {{{124,42 }, {124,49 }}, keyword_lexeme(Keyword_kind::Kw_poryadok8)               }, // Keyword Kw_poryadok8
            {{{124,53 }, {124,61 }}, keyword_lexeme(Keyword_kind::Kw_poryadok16)              }, // Keyword Kw_poryadok16
            {{{124,67 }, {124,75 }}, keyword_lexeme(Keyword_kind::Kw_poryadok32)              }, // Keyword Kw_poryadok32
            {{{126,9  }, {126,19 }}, keyword_lexeme(Keyword_kind::Kw_postfiksnaya)            }, // Keyword Kw_postfiksnaya
            {{{126,26 }, {126,35 }}, keyword_lexeme(Keyword_kind::Kw_preobrazuj)              }, // Keyword Kw_preobrazuj
            {{{127,17 }, {127,26 }}, keyword_lexeme(Keyword_kind::Kw_prefiksnaya)             }, // Keyword Kw_prefiksnaya
            {{{129,13 }, {129,21 }}, keyword_lexeme(Keyword_kind::Kw_psevdonim)               }, // Keyword Kw_psevdonim
            {{{129,48 }, {129,52 }}, keyword_lexeme(Keyword_kind::Kw_pusto)                   }, // Keyword Kw_pusto
            {{{129,62 }, {129,68 }}, id_lexeme(507)                                           }, // Id пустота
            {{{129,72 }, {129,77 }}, id_lexeme(512)                                           }, // Id вакуум
            {{{132,5  }, {132,13 }}, id_lexeme(521)                                           }, // Id равенство
            {{{132,18 }, {132,22 }}, keyword_lexeme(Keyword_kind::Kw_ravno)                   }, // Keyword Kw_ravno
            {{{132,28 }, {132,35 }}, id_lexeme(527)                                           }, // Id разность
            {{{134,9  }, {134,14 }}, keyword_lexeme(Keyword_kind::Kw_razbor)                  }, // Keyword Kw_razbor
            {{{134,19 }, {134,30 }}, keyword_lexeme(Keyword_kind::Kw_rassmatrivaj)            }, // Keyword Kw_rassmatrivaj
            {{{134,34 }, {134,40 }}, id_lexeme(532)                                           }, // Id рассказ
            {{{136,1  }, {136,8  }}, keyword_lexeme(Keyword_kind::Kw_realizuj)                }, // Keyword Kw_realizuj
            {{{136,21 }, {136,30 }}, keyword_lexeme(Keyword_kind::Kw_realizacija)             }, // Keyword Kw_realizacija
            {{{136,37 }, {136,65 }}, id_lexeme(559)                                           }, // Id концепция_концепции_категории
            {{{139,9  }, {139,15 }}, id_lexeme(566)                                           }, // Id сослать
            {{{139,24 }, {139,36 }}, id_lexeme(578)                                           }, // Id ссылочный_тип
            {{{139,41 }, {139,46 }}, keyword_lexeme(Keyword_kind::Kw_ssylka)                  }, // Keyword Kw_ssylka
            {{{141,17 }, {141,23 }}, id_lexeme(584)                                           }, // Id самовар
            {{{141,28 }, {141,31 }}, keyword_lexeme(Keyword_kind::Kw_samo)                    }, // Keyword Kw_samo
            {{{141,40 }, {141,48 }}, keyword_lexeme(Keyword_kind::Kw_struktura)               }, // Keyword Kw_struktura
            {{{144,5  }, {144,12 }}, keyword_lexeme(Keyword_kind::Kw_stroka32)                }, // Keyword Kw_stroka32
            {{{144,20 }, {144,26 }}, keyword_lexeme(Keyword_kind::Kw_stroka8)                 }, // Keyword Kw_stroka8
            {{{144,42 }, {144,47 }}, keyword_lexeme(Keyword_kind::Kw_stroka)                  }, // Keyword Kw_stroka
            {{{144,59 }, {144,66 }}, keyword_lexeme(Keyword_kind::Kw_stroka16)                }, // Keyword Kw_stroka16
            {{{146,17 }, {146,25 }}, id_lexeme(592)                                           }, // Id сравнение
            {{{146,31 }, {146,38 }}, delim_lexeme(Delimiter_kind::Compare_with)               }, // Delimiter Compare_with
            {{{146,53 }, {146,69 }}, id_lexeme(608)                                           }, // Id символьные_данные
            {{{148,9  }, {148,12 }}, keyword_lexeme(Keyword_kind::Kw_simv)                    }, // Keyword Kw_simv
            {{{148,22 }, {148,27 }}, keyword_lexeme(Keyword_kind::Kw_simv32)                  }, // Keyword Kw_simv32
            {{{148,42 }, {148,46 }}, keyword_lexeme(Keyword_kind::Kw_simv8)                   }, // Keyword Kw_simv8
            {{{150,17 }, {150,22 }}, keyword_lexeme(Keyword_kind::Kw_simv16)                  }, // Keyword Kw_simv16
            {{{154,1  }, {154,2  }}, keyword_lexeme(Keyword_kind::Kw_to)                      }, // Keyword Kw_to
            {{{154,10 }, {154,12 }}, keyword_lexeme(Keyword_kind::Kw_tip)                     }, // Keyword Kw_tip
            {{{154,15 }, {154,21 }}, keyword_lexeme(Keyword_kind::Kw_funktsiya)               }, // Keyword Kw_funktsiya
            {{{154,32 }, {154,43 }}, id_lexeme(620)                                           }, // Id трогательный
            {{{154,49 }, {154,55 }}, id_lexeme(626)                                           }, // Id типовой
            {{{156,5  }, {156,13 }}, id_lexeme(635)                                           }, // Id целостный
            {{{156,27 }, {156,31 }}, id_lexeme(637)                                           }, // Id целый
            {{{156,34 }, {156,36 }}, keyword_lexeme(Keyword_kind::Kw_tsel)                    }, // Keyword Kw_tsel
            {{{158,5  }, {158,10 }}, id_lexeme(643)                                           }, // Id феррит
            {{{158,20 }, {158,23 }}, id_lexeme(646)                                           }, // Id флот
            {{{158,29 }, {158,37 }}, id_lexeme(654)                                           }, // Id фумигатор
            {{{161,13 }, {161,18 }}, keyword_lexeme(Keyword_kind::Kw_tsel128)                 }, // Keyword Kw_tsel128
            {{{161,44 }, {161,47 }}, keyword_lexeme(Keyword_kind::Kw_tsel8)                   }, // Keyword Kw_tsel8
            {{{162,21 }, {162,25 }}, keyword_lexeme(Keyword_kind::Kw_tsel16)                  }, // Keyword Kw_tsel16
            {{{162,32 }, {162,36 }}, keyword_lexeme(Keyword_kind::Kw_tsel64)                  }, // Keyword Kw_tsel64
            {{{162,50 }, {162,54 }}, keyword_lexeme(Keyword_kind::Kw_tsel32)                  }, // Keyword Kw_tsel32
            {{{162,58 }, {162,63 }}, id_lexeme(660)                                           }, // Id честно
            {{{162,70 }, {162,75 }}, keyword_lexeme(Keyword_kind::Kw_chistaya)                }, // Keyword Kw_chistaya
            {{{164,5  }, {164,9  }}, keyword_lexeme(Keyword_kind::Kw_shkala)                  }, // Keyword Kw_shkala
            {{{164,16 }, {164,20 }}, keyword_lexeme(Keyword_kind::Kw_shkalu)                  }, // Keyword Kw_shkalu
            {{{164,45 }, {164,63 }}, id_lexeme(678)                                           }, // Id область_целостности
            {{{167,13 }, {167,24 }}, id_lexeme(690)                                           }, // Id элементарный
            {{{167,31 }, {167,37 }}, keyword_lexeme(Keyword_kind::Kw_element)                 }, // Keyword Kw_element
            {{{167,49 }, {167,64 }}, id_lexeme(705)                                           }, // Id экспортный_товар
            {{{167,73 }, {167,79 }}, keyword_lexeme(Keyword_kind::Kw_eksport)                 }, // Keyword Kw_eksport
            {{{168,17 }, {168,32 }}, id_lexeme(720)                                           }, // Id эспериментальный
            {{{168,41 }, {168,45 }}, keyword_lexeme(Keyword_kind::Kw_ekviv)                   }, // Keyword Kw_ekviv
            {{{168,53 }, {168,69 }}, id_lexeme(735)                                           }, // Id эквипотенциальный
            {{{169,1  }, {169,1  }}, nothing_lexeme()                                         }, // Nothing
        },

        {
           // References for text #4:
            {{{1, 1  }, {1, 1  }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{1, 8  }, {1, 8  }}, delim_lexeme(Delimiter_kind::Logical_not)                }, // Delimiter Logical_not
            {{{2, 30 }, {2, 31 }}, delim_lexeme(Delimiter_kind::NEQ)                        }, // Delimiter NEQ
            {{{3, 5  }, {3, 7  }}, delim_lexeme(Delimiter_kind::Logical_and_not)            }, // Delimiter Logical_and_not
            {{{4, 27 }, {4, 29 }}, delim_lexeme(Delimiter_kind::Logical_or_not)             }, // Delimiter Logical_or_not
            {{{4, 39 }, {4, 39 }}, delim_lexeme(Delimiter_kind::Sharp)                      }, // Delimiter Sharp
            {{{4, 44 }, {4, 45 }}, delim_lexeme(Delimiter_kind::Right_arrow)                }, // Delimiter Right_arrow
            {{{5, 9  }, {5, 12 }}, delim_lexeme(Delimiter_kind::Logical_and_not_full)       }, // Delimiter Logical_and_not_full
            {{{5, 21 }, {5, 24 }}, delim_lexeme(Delimiter_kind::Logical_or_not_full)        }, // Delimiter Logical_or_not_full
            {{{6, 15 }, {6, 16 }}, delim_lexeme(Delimiter_kind::Data_size)                  }, // Delimiter Data_size
            {{{6, 32 }, {6, 32 }}, delim_lexeme(Delimiter_kind::Remainder)                  }, // Delimiter Remainder
            {{{7, 13 }, {7, 16 }}, delim_lexeme(Delimiter_kind::Logical_and_not_assign)     }, // Delimiter Logical_and_not_assign
            {{{7, 29 }, {7, 32 }}, delim_lexeme(Delimiter_kind::Logical_or_not_assign)      }, // Delimiter Logical_or_not_assign
            {{{8, 24 }, {8, 25 }}, delim_lexeme(Delimiter_kind::Float_remainder)            }, // Delimiter Float_remainder
            {{{9, 37 }, {9, 41 }}, delim_lexeme(Delimiter_kind::Logical_and_not_full_assign)}, // Delimiter Logical_and_not_full_assign
            {{{9, 50 }, {9, 51 }}, delim_lexeme(Delimiter_kind::Minus_assign)               }, // Delimiter Minus_assign
            {{{11,11 }, {11,13 }}, delim_lexeme(Delimiter_kind::Wrap_dec)                   }, // Delimiter Wrap_dec
            {{{12,21 }, {12,25 }}, delim_lexeme(Delimiter_kind::Logical_or_not_full_assign) }, // Delimiter Logical_or_not_full_assign
            {{{14,3  }, {14,4  }}, delim_lexeme(Delimiter_kind::Remainder_assign)           }, // Delimiter Remainder_assign
            {{{14,21 }, {14,23 }}, delim_lexeme(Delimiter_kind::Float_remainder_assign)     }, // Delimiter Float_remainder_assign
            {{{16,7  }, {16,8  }}, delim_lexeme(Delimiter_kind::Dec)                        }, // Delimiter Dec
            {{{16,33 }, {16,33 }}, delim_lexeme(Delimiter_kind::Minus)                      }, // Delimiter Minus
            {{{19,11 }, {19,14 }}, delim_lexeme(Delimiter_kind::Logical_and_full_assign)    }, // Delimiter Logical_and_full_assign
            {{{19,32 }, {19,32 }}, delim_lexeme(Delimiter_kind::Bitwise_and)                }, // Delimiter Bitwise_and
            {{{19,42 }, {19,43 }}, delim_lexeme(Delimiter_kind::Tuple_begin)                }, // Delimiter Tuple_begin
            {{{19,49 }, {19,49 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened
            {{{20,19 }, {20,20 }}, delim_lexeme(Delimiter_kind::Logical_and)                }, // Delimiter Logical_and
            {{{20,37 }, {20,38 }}, delim_lexeme(Delimiter_kind::Bitwise_and_assign)         }, // Delimiter Bitwise_and_assign
            {{{21,4  }, {21,6  }}, delim_lexeme(Delimiter_kind::Logical_and_full)           }, // Delimiter Logical_and_full
            {{{21,14 }, {21,16 }}, delim_lexeme(Delimiter_kind::Logical_and_assign)         }, // Delimiter Logical_and_assign
            {{{21,27 }, {21,27 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed
            {{{24,1  }, {24,2  }}, delim_lexeme(Delimiter_kind::Tuple_end)                  }, // Delimiter Tuple_end
            {{{24,4  }, {24,4  }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{24,6  }, {24,7  }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
            {{{24,9  }, {24,10 }}, delim_lexeme(Delimiter_kind::Copy)                       }, // Delimiter Copy
            {{{25,5  }, {25,6  }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br
            {{{25,11 }, {25,12 }}, delim_lexeme(Delimiter_kind::Colon_fig_br_closed)        }, // Delimiter Colon_fig_br_closed
            {{{25,18 }, {25,19 }}, delim_lexeme(Delimiter_kind::Colon_sq_br_closed)         }, // Delimiter Colon_sq_br_closed
            {{{30,13 }, {30,14 }}, delim_lexeme(Delimiter_kind::Power)                      }, // Delimiter Power
            {{{30,18 }, {30,18 }}, delim_lexeme(Delimiter_kind::Mul)                        }, // Delimiter Mul
            {{{31,25 }, {31,27 }}, delim_lexeme(Delimiter_kind::Float_power)                }, // Delimiter Float_power
            {{{31,31 }, {31,32 }}, delim_lexeme(Delimiter_kind::Mul_assign)                 }, // Delimiter Mul_assign
            {{{31,41 }, {31,43 }}, delim_lexeme(Delimiter_kind::Power_assign)               }, // Delimiter Power_assign
            {{{32,17 }, {32,20 }}, delim_lexeme(Delimiter_kind::Float_power_assign)         }, // Delimiter Float_power_assign
            {{{32,34 }, {32,34 }}, delim_lexeme(Delimiter_kind::Plus)                       }, // Delimiter Plus
            {{{32,56 }, {32,57 }}, delim_lexeme(Delimiter_kind::Inc)                        }, // Delimiter Inc
            {{{35,7  }, {35,8  }}, delim_lexeme(Delimiter_kind::Plus_assign)                }, // Delimiter Plus_assign
            {{{35,23 }, {35,25 }}, delim_lexeme(Delimiter_kind::Wrap_inc)                   }, // Delimiter Wrap_inc
            {{{37,16 }, {37,16 }}, delim_lexeme(Delimiter_kind::Point)                      }, // Delimiter Point
            {{{37,27 }, {37,28 }}, delim_lexeme(Delimiter_kind::Range)                      }, // Delimiter Range
            {{{37,51 }, {37,53 }}, delim_lexeme(Delimiter_kind::Algebraic_sep)              }, // Delimiter Algebraic_sep
            {{{40,18 }, {40,19 }}, delim_lexeme(Delimiter_kind::EQ)                         }, // Delimiter EQ
            {{{40,29 }, {40,29 }}, delim_lexeme(Delimiter_kind::Assign)                     }, // Delimiter Assign
            {{{42,3  }, {42,3  }}, delim_lexeme(Delimiter_kind::LT)                         }, // Delimiter LT
            {{{42,24 }, {42,25 }}, delim_lexeme(Delimiter_kind::Left_arrow)                 }, // Delimiter Left_arrow
            {{{42,37 }, {42,38 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{43,12 }, {43,13 }}, delim_lexeme(Delimiter_kind::Left_shift)                 }, // Delimiter Left_shift
            {{{43,23 }, {43,24 }}, delim_lexeme(Delimiter_kind::LEQ)                        }, // Delimiter LEQ
            {{{43,56 }, {43,57 }}, delim_lexeme(Delimiter_kind::Cycle_name_prefix)          }, // Delimiter Cycle_name_prefix
            {{{46,7  }, {46,9  }}, delim_lexeme(Delimiter_kind::Non_specific)               }, // Delimiter Non_specific
            {{{46,13 }, {46,15 }}, delim_lexeme(Delimiter_kind::Address)                    }, // Delimiter Address
            {{{46,21 }, {46,23 }}, delim_lexeme(Delimiter_kind::Meta_plus)                  }, // Delimiter Meta_plus
            {{{46,29 }, {46,32 }}, delim_lexeme(Delimiter_kind::Data_address)               }, // Delimiter Data_address
            {{{46,43 }, {46,46 }}, delim_lexeme(Delimiter_kind::Meta_plus_assign)           }, // Delimiter Meta_plus_assign
            {{{48,10 }, {48,12 }}, delim_lexeme(Delimiter_kind::Deduce_arg_type)            }, // Delimiter Deduce_arg_type
            {{{48,28 }, {48,30 }}, delim_lexeme(Delimiter_kind::Left_shift_assign)          }, // Delimiter Left_shift_assign
            {{{48,43 }, {48,45 }}, delim_lexeme(Delimiter_kind::ElemType)                   }, // Delimiter ElemType
            {{{48,49 }, {48,52 }}, delim_lexeme(Delimiter_kind::ExprType)                   }, // Delimiter ExprType
            {{{50,3  }, {50,3  }}, delim_lexeme(Delimiter_kind::Set_difference)             }, // Delimiter Set_difference
            {{{50,10 }, {50,11 }}, delim_lexeme(Delimiter_kind::Colon_sq_br_opened)         }, // Delimiter Colon_sq_br_opened
            {{{50,17 }, {50,18 }}, delim_lexeme(Delimiter_kind::Set_difference_assign)      }, // Delimiter Set_difference_assign
            {{{50,27 }, {50,27 }}, delim_lexeme(Delimiter_kind::Sq_br_opened)               }, // Delimiter Sq_br_opened
            {{{51,1  }, {51,1  }}, delim_lexeme(Delimiter_kind::At)                         }, // Delimiter At
            {{{51,6  }, {51,7  }}, delim_lexeme(Delimiter_kind::Logical_xor)                }, // Delimiter Logical_xor
            {{{51,14 }, {51,15 }}, delim_lexeme(Delimiter_kind::Bitwise_xor_assign)         }, // Delimiter Bitwise_xor_assign
            {{{51,29 }, {51,31 }}, delim_lexeme(Delimiter_kind::Logical_xor_assign)         }, // Delimiter Logical_xor_assign
            {{{51,36 }, {51,36 }}, delim_lexeme(Delimiter_kind::Bitwise_xor)                }, // Delimiter Bitwise_xor
            {{{53,6  }, {53,6  }}, delim_lexeme(Delimiter_kind::Bitwise_or)                 }, // Delimiter Bitwise_or
            {{{53,19 }, {53,21 }}, delim_lexeme(Delimiter_kind::Card)                       }, // Delimiter Card
            {{{53,29 }, {53,30 }}, delim_lexeme(Delimiter_kind::Bitwise_or_assign)          }, // Delimiter Bitwise_or_assign
            {{{54,12 }, {54,13 }}, delim_lexeme(Delimiter_kind::Logical_or)                 }, // Delimiter Logical_or
            {{{54,17 }, {54,19 }}, delim_lexeme(Delimiter_kind::Logical_or_full)            }, // Delimiter Logical_or_full
            {{{54,25 }, {54,27 }}, delim_lexeme(Delimiter_kind::Logical_or_assign)          }, // Delimiter Logical_or_assign
            {{{54,36 }, {54,39 }}, delim_lexeme(Delimiter_kind::Logical_or_full_assign)     }, // Delimiter Logical_or_full_assign
            {{{56,3  }, {56,6  }}, delim_lexeme(Delimiter_kind::Pattern)                    }, // Delimiter Pattern
            {{{56,8  }, {56,8  }}, delim_lexeme(Delimiter_kind::Fig_br_opened)              }, // Delimiter Fig_br_opened
            {{{56,25 }, {56,25 }}, delim_lexeme(Delimiter_kind::GT)                         }, // Delimiter GT
            {{{56,28 }, {56,29 }}, delim_lexeme(Delimiter_kind::GEQ)                        }, // Delimiter GEQ
            {{{56,33 }, {56,34 }}, delim_lexeme(Delimiter_kind::Right_shift)                }, // Delimiter Right_shift
            {{{56,50 }, {56,52 }}, delim_lexeme(Delimiter_kind::Right_shift_assign)         }, // Delimiter Right_shift_assign
            {{{59,10 }, {59,10 }}, delim_lexeme(Delimiter_kind::Bitwise_not)                }, // Delimiter Bitwise_not
            {{{59,24 }, {59,25 }}, delim_lexeme(Delimiter_kind::Bitwise_or_not)             }, // Delimiter Bitwise_or_not
            {{{59,35 }, {59,37 }}, delim_lexeme(Delimiter_kind::Bitwise_and_not_assign)     }, // Delimiter Bitwise_and_not_assign
            {{{59,49 }, {59,50 }}, delim_lexeme(Delimiter_kind::Bitwise_and_not)            }, // Delimiter Bitwise_and_not
            {{{59,53 }, {59,55 }}, delim_lexeme(Delimiter_kind::Bitwise_or_not_assign)      }, // Delimiter Bitwise_or_not_assign
            {{{61,5  }, {61,5  }}, delim_lexeme(Delimiter_kind::Fig_br_closed)              }, // Delimiter Fig_br_closed
            {{{61,6  }, {61,6  }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #5:
            {{{1, 5  }, {1, 8  }}, id_lexeme(737)                                           }, // Id буся
            {{{1, 10 }, {1, 11 }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
            {{{1, 12 }, {1, 15 }}, id_lexeme(740)                                           }, // Id киса
            {{{3, 9  }, {3, 13 }}, id_lexeme(744)                                           }, // Id пыжик
            {{{3, 14 }, {3, 15 }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
            {{{3, 16 }, {3, 23 }}, id_lexeme(751)                                           }, // Id файловый
            {{{3, 24 }, {3, 25 }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
            {{{3, 26 }, {3, 31 }}, id_lexeme(755)                                           }, // Id кот12_
            {{{5, 5  }, {5, 6  }}, id_lexeme(757)                                           }, // Id a1
            {{{5, 7  }, {5, 8  }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
            {{{5, 9  }, {5, 10 }}, id_lexeme(759)                                           }, // Id b2
            {{{5, 11 }, {5, 12 }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
            {{{5, 13 }, {5, 14 }}, id_lexeme(761)                                           }, // Id c3
            {{{5, 15 }, {5, 16 }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
            {{{5, 17 }, {5, 24 }}, delim_lexeme(Delimiter_kind::Compare_with)               }, // Delimiter Compare_with
            {{{5, 28 }, {5, 31 }}, id_lexeme(764)                                           }, // Id гена
            {{{5, 32 }, {5, 33 }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
            {{{5, 34 }, {5, 41 }}, delim_lexeme(Delimiter_kind::Compare_with)               }, // Delimiter Compare_with
            {{{7, 9  }, {7, 11 }}, keyword_lexeme(Keyword_kind::Kw_tsel)                    }, // Keyword Kw_tsel
            {{{10,9  }, {10,10 }}, id_lexeme(766)                                           }, // Id у1
            {{{10,11 }, {10,12 }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
            {{{10,13 }, {10,17 }}, id_lexeme(769)                                           }, // Id цеце5
            {{{10,18 }, {10,19 }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
            {{{10,20 }, {10,20 }}, delim_lexeme(Delimiter_kind::Plus)                       }, // Delimiter Plus
            {{{12,13 }, {12,19 }}, id_lexeme(776)                                           }, // Id айболит
            {{{14,17 }, {14,25 }}, id_lexeme(784)                                           }, // Id бармаглот
            {{{14,28 }, {14,29 }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
            {{{14,31 }, {14,41 }}, id_lexeme(792)                                           }, // Id кот_бегемот
            {{{16,9  }, {16,13 }}, keyword_lexeme(Keyword_kind::Kw_bezzn)                   }, // Keyword Kw_bezzn
            {{{16,14 }, {16,14 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #6:
#if defined(_WIN32) || defined(_WIN64)
#   define HERR_MANNELIG 142
#   define POPE_AND_MAIDEN_LEN 1494
#   define POPE_AND_MAIDEN (HERR_MANNELIG + POPE_AND_MAIDEN_LEN)
#   define POPE_AND_DOG_LEN 20
#   define POPE_AND_DOG ((POPE_AND_MAIDEN) + POPE_AND_DOG_LEN)
#   define HELLO_CHEBURASHKA_LEN 18
#   define HELLO_CHEBURASHKA ((POPE_AND_DOG) + HELLO_CHEBURASHKA_LEN)
#else
#   define HERR_MANNELIG 139
#   define POPE_AND_MAIDEN_LEN (1494 - 14)
#   define POPE_AND_MAIDEN (HERR_MANNELIG + POPE_AND_MAIDEN_LEN)
#   define POPE_AND_DOG_LEN 20
#   define POPE_AND_DOG ((POPE_AND_MAIDEN) + POPE_AND_DOG_LEN)
#   define HELLO_CHEBURASHKA_LEN 18
#   define HELLO_CHEBURASHKA ((POPE_AND_DOG) + HELLO_CHEBURASHKA_LEN)
#endif
            {{{1, 5  }, {1, 7  }}, char_lexeme(U'f')                                        }, // Char [Chr32] 'f'
            {{{1, 9  }, {1, 11 }}, char_lexeme(U'A')                                        }, // Char [Chr32] 'A'
            {{{3, 1  }, {6, 31 }}, string_lexeme(HERR_MANNELIG)                             }, // String "Bettida..."
            {{{8, 13 }, {22,129}}, string_lexeme(POPE_AND_MAIDEN)                           }, // String "Тебе, девка, житье..."
            {{{25,1  }, {25,24 }}, string_lexeme(POPE_AND_DOG)                              }, // String """У попа была собака..."
            {{{27,9  }, {27,12 }}, char_lexeme(U'{')                                        }, // Char [Char32] '{'
            {{{28,11 }, {28,17 }}, char_lexeme(U'Ё')                                        }, // Char [Char32] 'Ё'
            {{{28,22 }, {28,26 }}, char_lexeme(U'A')                                        }, // Char [Char32] 'A' (Latin)
            {{{30,13 }, {30,18 }}, char_lexeme(U'C')                                        }, // Char [Char32] 'C' (Latin)
            {{{32,16 }, {32,26 }}, char_lexeme(U'B')                                        }, // Char [Char32] 'B' (Latin)
            {{{34,11 }, {34,14 }}, char_lexeme(U'{')                                        }, // Char [Char32] '{'
            {{{36,1  }, {36,5  }}, char_lexeme(U'Ё')                                        }, // Char [Char32] 'Ё'
            {{{37,7  }, {37,12 }}, char_lexeme(U'Ё')                                        }, // Char [Char32] 'Ё'
            {{{38,13 }, {38,21 }}, char_lexeme(U'Ѭ')                                        }, // Char [Char32] 'Ѭ'
            {{{39,17 }, {39,23 }}, char_lexeme(U'Ӟ')                                        }, // Char [Char32] 'Ӟ'
            {{{39,27 }, {39,43 }}, char_lexeme(U'Ƣ')                                        }, // Char [Char32] 'Ƣ'
            {{{41,21 }, {41,27 }}, char_lexeme(U'Ϧ')                                        }, // Char [Char32] 'Ϧ'
            {{{41,30 }, {41,38 }}, char_lexeme(U'ϥ')                                        }, // Char [Char32] 'ϥ'
            {{{44,7  }, {44,8  }}, id_lexeme(794)                                           }, // Id u8
            {{{44,12 }, {44,14 }}, id_lexeme(796)                                           }, // Id u32
            {{{44,16 }, {44,18 }}, char_lexeme(U'C')                                        }, // Char [Char32] 'C' (Latin)
            {{{44,21 }, {44,23 }}, id_lexeme(798)                                           }, // Id u16
            {{{44,28 }, {44,28 }}, delim_lexeme(Delimiter_kind::Plus)                       }, // Delimiter Plus
            {{{45,13 }, {45,17 }}, char_lexeme(U'З', Char_kind::Char8)                      }, // Char [Char8] 'З'
            {{{45,19 }, {45,24 }}, char_lexeme(U'ĝ')                                        }, // Char [Char32] 'ĝ'
            {{{45,28 }, {45,33 }}, char_lexeme(U'ĝ', Char_kind::Char16)                     }, // Char [Char16] 'ĝ'
            {{{45,35 }, {45,43 }}, char_lexeme(U'ĝ', Char_kind::Char32)                     }, // Char [Char32] 'ĝ'
            {{{45,45 }, {45,67 }}, string_lexeme(HELLO_CHEBURASHKA, String_kind::String16)  }, // String [String16] "Привет, Чебурашка!"
            {{{45,68 }, {45,68 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #7:
            {{{1, 1  }, {1, 2  }}, delim_lexeme(Delimiter_kind::Dec)                        }, // Delimiter Dec
            {{{1, 18 }, {1, 19 }}, delim_lexeme(Delimiter_kind::Tuple_begin)                }, // Delimiter Tuple_begin
            {{{1, 20 }, {1, 20 }}, nothing_lexeme()                                         }, // Nothing
        },
    };


    class Scanner_as_generator : public testing::Generator<Token>{
    public:
        Scanner_as_generator()                            = default;
        Scanner_as_generator(const Scanner_as_generator&) = default;
        virtual ~Scanner_as_generator()                   = default;

        Scanner_as_generator(const ascaner::Location_ptr& location,
                             const Errors_and_tries&      et,
                             const Module_parts_trie_ptr& module_parts_trie) :
            scanner_{std::make_shared<Scanner>(location, et, module_parts_trie)}
        {  }

        Token yield() override;
        bool          is_finished(const Token& t) override;
    private:
        Scanner_ptr   scanner_;
    };

    Token Scanner_as_generator::yield()
    {
        return scanner_->current_lexeme();
    }

    bool Scanner_as_generator::is_finished(const Token& t)
    {
        return t.lexeme_.code_.kind_ == Lexem_kind::Nothing;
    }

    void test_lexeme_recognition()
    {
        Errors_and_tries  et;
        et.ec_                      = std::make_shared<Error_count>();
        et.wc_                      = std::make_shared<Warning_count>();
        et.ids_trie_                = std::make_shared<Char_trie>();
        et.strs_trie_               = std::make_shared<Char_trie>();

        auto module_name_parts_trie = std::make_shared<Trie_for_vector<std::size_t>>();

        std::size_t i = 0;
        for(auto test_text: test_texts){
            auto                 p   = const_cast<char32_t*>(test_text);
            auto                 loc = std::make_shared<ascaner::Location>(p);
            Scanner_as_generator gen{loc, et, module_name_parts_trie};
            printf("Testing for text #%zu\n", i);
            testing::test(gen,                             [](auto t){return t;},
                          std::begin(tokens_for_texts[i]), std::end(tokens_for_texts[i]));
            ++i;
        }
    }
};