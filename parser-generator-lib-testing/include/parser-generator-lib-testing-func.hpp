/*
    File:    parser-generator-lib-testing-func.hpp
    Created: 22 April 2020 at 19:41 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef PARSER_GENERATOR_LIB_TESTING_FUNC_H
#define PARSER_GENERATOR_LIB_TESTING_FUNC_H
namespace parser_gen_lib_testing{
    void testing_func();
};
#endif