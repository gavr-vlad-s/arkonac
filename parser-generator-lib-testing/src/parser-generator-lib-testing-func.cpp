/*
    File:    parser-generator-lib-testing-func.cpp
    Created: 22 April 2020 at 19:46 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <iostream>
#include <vector>
#include <string>
#include "../include/parser-generator-lib-testing-func.hpp"
#include "../../parser-generator-lib/include/rules.hpp"
#include "../../parser-generator-lib/include/symbol_types.hpp"
#include "../../parser-generator-lib/include/symbols_builders.hpp"
#include "../../parser-generator-lib/include/grammar.hpp"
#include "../../parser-generator-lib/include/grammar_builder.hpp"
#include "../../parser-generator-lib/include/sets_first.hpp"
#include "../../parser-generator-lib/include/sets_follows.hpp"
#include "../../parser-generator-lib/include/items.hpp"
#include "../../parser-generator-lib/include/closure_builder.hpp"
#include "../../parser-generator-lib/include/parser_tables_builder.hpp"
#include "../../parser-generator-lib/include/parser_builder.hpp"

namespace parser_gen_lib_testing{
    using parser_generator::Names_info;
    using parser_generator::Grammar_info;
    using parser_generator::Named_symbol;
    using parser_generator::Kind;
    using parser_generator::Named_rule;
    using parser_generator::Grammar_builder;
    using parser_generator::Grammar;
    using parser_generator::Sets_first;
    using parser_generator::Sets_follow;
    using parser_generator::slr1::Item;
    using parser_generator::slr1::Closure_builder;
    using parser_generator::slr1::Items_collection;
    using parser_generator::slr1::Parser_tables_builder;
    using parser_generator::slr1::Parser_builder;
    using parser_generator::slr1::Parser_info;
    using parser_generator::slr1::Generated_texts;
    using parser_generator::slr1::Error_info;
    using parser_generator::slr1::Parser_class_details;



static const std::string parser_header_preamble = R"~(#   include "../../snd-lvl-scanner/include/arkona-scanner-snd-lvl.h"
#   include "../include/ast_node.hpp"
#   include "../../tries/include/errors_and_tries.h"
#   include "../../tries/include/error_count.h"
#   include "../../tries/include/char_trie.h"
#   include "../include/shift_result.hpp")~";

    static const std::vector<Grammar_info> grammar_info = {
        Grammar_info{
            // Grammar #0:
            /*
             * S -> mQlr | mQlMr
             * M -> uQ | McQ
             * Q -> i | isQ
             */
            Names_info{
                {"i", "m", "l", "r", "c", "u", "s"},
                {"S", "M", "Q"},
                "S"
            },
            {
                Named_rule{
                    "S", {
                        Named_symbol{Kind::Terminal, "m"}, Named_symbol{Kind::Non_terminal, "Q"},
                        Named_symbol{Kind::Terminal, "l"}, Named_symbol{Kind::Terminal,     "r"},
                    }
                },
                Named_rule{
                    "S", {
                        Named_symbol{Kind::Terminal, "m"}, Named_symbol{Kind::Non_terminal, "Q"},
                        Named_symbol{Kind::Terminal, "l"}, Named_symbol{Kind::Non_terminal, "M"},
                        Named_symbol{Kind::Terminal, "r"},
                    }
                },
                Named_rule{
                    "M", {
                        Named_symbol{Kind::Terminal, "u"}, Named_symbol{Kind::Non_terminal, "Q"},
                    }
                },
                Named_rule{
                    "M", {
                        Named_symbol{Kind::Non_terminal, "M"}, Named_symbol{Kind::Terminal, "c"},
                        Named_symbol{Kind::Non_terminal, "Q"},
                    }
                },
                Named_rule{
                    "Q", {
                        Named_symbol{Kind::Terminal, "i"},
                    }
                },
                Named_rule{
                    "Q", {
                        Named_symbol{Kind::Terminal, "i"},     Named_symbol{Kind::Terminal, "s"},
                        Named_symbol{Kind::Non_terminal, "Q"},
                    }
                },

            }
        },
        Grammar_info{
            // Grammar #1:
            /*
             * S -> S + T | S - T | T
             * T -> T * E | T / E | E
             * E -> F ** E | F
             * F -> +G | -G | G
             * G -> (S) | a
             */
            Names_info{
                {"Plus", "Minus", "Mul", "Div", "Power", "LP", "RP", "a"},
                {"S", "T", "E", "F", "G"},
                "S"
            },
            {
                Named_rule{
                    "S", {
                        Named_symbol{Kind::Non_terminal, "S"}, Named_symbol{Kind::Terminal, "Plus"},
                        Named_symbol{Kind::Non_terminal, "T"},
                    }
                },
                Named_rule{
                    "S", {
                        Named_symbol{Kind::Non_terminal, "S"}, Named_symbol{Kind::Terminal, "Minus"},
                        Named_symbol{Kind::Non_terminal, "T"},
                    }
                },
                Named_rule{
                    "S", {
                        Named_symbol{Kind::Non_terminal, "T"},
                    }
                },
                Named_rule{
                    "T", {
                        Named_symbol{Kind::Non_terminal, "T"}, Named_symbol{Kind::Terminal, "Mul"},
                        Named_symbol{Kind::Non_terminal, "E"},
                    }
                },
                Named_rule{
                    "T", {
                        Named_symbol{Kind::Non_terminal, "T"}, Named_symbol{Kind::Terminal, "Div"},
                        Named_symbol{Kind::Non_terminal, "E"},
                    }
                },
                Named_rule{
                    "T", {
                        Named_symbol{Kind::Non_terminal, "E"},
                    }
                },
                Named_rule{
                    "E", {
                        Named_symbol{Kind::Non_terminal, "F"}, Named_symbol{Kind::Terminal, "Power"},
                        Named_symbol{Kind::Non_terminal, "E"},
                    }
                },
                Named_rule{
                    "E", {
                        Named_symbol{Kind::Non_terminal, "F"},
                    }
                },
                Named_rule{
                    "F", {
                        Named_symbol{Kind::Terminal, "Plus"}, Named_symbol{Kind::Non_terminal, "G"},
                    }
                },
                Named_rule{
                    "F", {
                        Named_symbol{Kind::Terminal, "Minus"}, Named_symbol{Kind::Non_terminal, "G"},
                    }
                },
                Named_rule{
                    "F", {
                        Named_symbol{Kind::Non_terminal, "G"},
                    }
                },
                Named_rule{
                    "G", {
                        Named_symbol{Kind::Terminal, "LP"}, Named_symbol{Kind::Non_terminal, "S"},
                        Named_symbol{Kind::Terminal, "RP"},
                    }
                },
                Named_rule{
                    "G", {
                        Named_symbol{Kind::Terminal, "a"},
                    }
                },
            }
        },
        Grammar_info{
            // Grammar #2:
            /*
             * S -> T S1
             * S1 -> +T S1 | -T S1 | ε
             * T -> E T1
             * T1 -> *E T1 | /E T1 | ε
             * E -> (S) | a | b
             */
            Names_info{
                {"Plus", "Minus", "Mul", "Div", "LP", "RP", "a", "b"},
                {"S", "S1", "T", "T1", "E"},
                "S"
            },
            {
                Named_rule{
                    "S", {
                        Named_symbol{Kind::Non_terminal, "T"}, Named_symbol{Kind::Non_terminal, "S1"},
                    }
                },
                Named_rule{
                    "S1", {
                        Named_symbol{Kind::Terminal, "Plus"},   Named_symbol{Kind::Non_terminal, "T"},
                        Named_symbol{Kind::Non_terminal, "S1"},
                    }
                },
                Named_rule{
                    "S1", {
                        Named_symbol{Kind::Terminal, "Minus"},   Named_symbol{Kind::Non_terminal, "T"},
                        Named_symbol{Kind::Non_terminal, "S1"},
                    }
                },
                Named_rule{
                    "S1", {
                        Named_symbol{Kind::Epsilon, ""},
                    }
                },
                Named_rule{
                    "T", {
                        Named_symbol{Kind::Non_terminal, "E"}, Named_symbol{Kind::Non_terminal, "T1"},
                    }
                },
                Named_rule{
                    "T1", {
                        Named_symbol{Kind::Terminal, "Mul"},   Named_symbol{Kind::Non_terminal, "E"},
                        Named_symbol{Kind::Non_terminal, "T1"},
                    }
                },
                Named_rule{
                    "T1", {
                        Named_symbol{Kind::Terminal, "Div"},   Named_symbol{Kind::Non_terminal, "E"},
                        Named_symbol{Kind::Non_terminal, "T1"},
                    }
                },
                Named_rule{
                    "T1", {
                        Named_symbol{Kind::Epsilon, ""},
                    }
                },
                Named_rule{
                    "E", {
                        Named_symbol{Kind::Terminal, "LP"}, Named_symbol{Kind::Non_terminal, "S"},
                        Named_symbol{Kind::Terminal, "RP"},
                    }
                },
                Named_rule{
                    "E", {
                        Named_symbol{Kind::Terminal, "a"},
                    }
                },
                Named_rule{
                    "E", {
                        Named_symbol{Kind::Terminal, "b"},
                    }
                },
            }
        },

        Grammar_info{
            // Grammar #3:
            /*
             * arkona_module -> module_name fig_br_opened used_modules fig_br_closed |
             *                  module_name fig_br_opened used_modules fig_br_closed
             * used_modules  -> used_module | used_modules semicolon used_module
             * used_module   -> uses id | used_module scope_resolution id
             * module_name   -> module id | module_name scope_resolution id
             */
            Names_info{
                {
                    "module",        "uses", "scope_resolution", "comma", "fig_br_opened",
                    "fig_br_closed", "id",   "semicolon"
                },

                {"arkona_module", "used_modules", "used_module", "module_body", "module_name"},

                "arkona_module"
            },
            {
                Named_rule{
                    "arkona_module", {
                        Named_symbol{Kind::Non_terminal, "module_name"     },
                        Named_symbol{Kind::Terminal,     "fig_br_opened"   },
                        Named_symbol{Kind::Non_terminal, "used_modules"    },
                        Named_symbol{Kind::Terminal,     "fig_br_closed"   },
                    }
                },
                Named_rule{
                    "arkona_module", {
                        Named_symbol{Kind::Non_terminal, "module_name"     },
                        Named_symbol{Kind::Terminal,     "fig_br_opened"   },
                        Named_symbol{Kind::Terminal,     "fig_br_closed"   },
                    }
                },
                Named_rule{
                    "used_modules", {
                        Named_symbol{Kind::Non_terminal, "used_module"     },
                    }
                },
                Named_rule{
                    "used_modules", {
                        Named_symbol{Kind::Non_terminal, "used_modules"    },
                        Named_symbol{Kind::Terminal,     "semicolon"       },
                        Named_symbol{Kind::Non_terminal, "used_module"     },
                    }
                },
                Named_rule{
                    "used_module", {
                        Named_symbol{Kind::Terminal,     "uses"            },
                        Named_symbol{Kind::Terminal,     "id"              },
                    }
                },
                Named_rule{
                    "used_module", {
                        Named_symbol{Kind::Non_terminal, "used_module"     },
                        Named_symbol{Kind::Terminal,     "scope_resolution"},
                        Named_symbol{Kind::Terminal,     "id"              },
                    }
                },
                Named_rule{
                    "module_name", {
                        Named_symbol{Kind::Terminal,     "module"          },
                        Named_symbol{Kind::Terminal,     "id"              },
                    }
                },
                Named_rule{
                    "module_name", {
                        Named_symbol{Kind::Non_terminal, "module_name"     },
                        Named_symbol{Kind::Terminal,     "scope_resolution"},
                        Named_symbol{Kind::Terminal,     "id"              },
                    }
                },

            }
        },
    };



    static const std::vector<std::vector<Item>> items_for_grammars = {
        // Items for grammar #0
        {},
        // Items for grammar #1
        {
            {1,  0}, // [S -> ● S + T]
            {2,  1}, // [S -> S ● - T]
            {4,  2}, // [T -> T * ● E]
        },
        // Items for grammar #2
        {
            {7,  3}, // [T1 -> / E T1 ●]
            {4,  0}, // [S1 -> ●]
        },
        // Items for grammar #3
        {},
    };

    static const Parser_class_details parser_details[] = {
        /*
         * S -> mQlr | mQlMr
         * M -> uQ | McQ
         * Q -> i | isQ
         */
        Parser_class_details{
            .parser_class_name_                          = "parser",
            .parser_namespace_name_                      = "parser",
            .scanner_class_name_                         = "Scanner",
            .scanner_namespace_name_                     = "scanner",
            .parser_header_preamble_                     = R"~(#   include "../include/node.hpp")~",
            .parser_impl_preamble_                       = "",
            .token_to_terminal_conversion_function_name_ = "convert_token_to_terminal",
            .parsing_result_type_                        = "Node_ptr",
            .shift_function_name_                        = "token_to_node",
            .shift_function_result_type_                 = "Node_ptr",
            .reduce_actions_                             = {},
            .additional_ctor_args_                       = "",
            .ctor_body_                                  = "",
            .additional_parser_members_                  = "",
            .initializers_for_ctor_                      = "",
            .additional_includes_                        = {},
            .additional_srcs_                            = {},
            .terminal_to_msg_                            = {},
            .token_type_                                 = "Token",
            .postprocessing_                             = "",
            .user_error_handling_                        = ""
        },
       /*
        * S -> S + T | S - T | T
        * T -> T * E | T / E | E
        * E -> F ** E | F
        * F -> +G | -G | G
        * G -> (S) | a
        */
        Parser_class_details{
            .parser_class_name_                          = "eparser",
            .parser_namespace_name_                      = "expression",
            .scanner_class_name_                         = "Scanner",
            .scanner_namespace_name_                     = "scanner",
            .parser_header_preamble_                     = R"~(#   include "../include/command.hpp")~",
            .parser_impl_preamble_                       = "",
            .token_to_terminal_conversion_function_name_ = "convert_token_to_terminal",
            .parsing_result_type_                        = "std::vector<Command>",
            .shift_function_name_                        = "token_to_IR",
            .shift_function_result_type_                 = "Command",
            .reduce_actions_                             = {},
            .additional_ctor_args_                       = "",
            .ctor_body_                                  = "",
            .additional_parser_members_                  = "    std::vector<Command> buffer_;",
            .initializers_for_ctor_                      = " buffer_{}",
            .additional_includes_                        = {
                {"command.hpp", R"~(// File: command.hpp
#ifndef COMMAND_H
#define COMMAND_H
#   include <vector>
enum class Command_kind{
    Value, ADD, SUB, MUL, DIV, POWER, CHS
};

struct Command{
    Command_kind kind_;
    int          value_;
};
#endif
)~"}
            },
            .additional_srcs_                            = {},
            .terminal_to_msg_                            = {},
            .token_type_                                 = "Token",
            .postprocessing_                             = "",
            .user_error_handling_                        = ""
        },
        /*
         * S -> T S1
         * S1 -> +T S1 | -T S1 | ε
         * T -> E T1
         * T1 -> *E T1 | /E T1 | ε
         * E -> (S) | a | b
         */
        Parser_class_details{
            .parser_class_name_                          = "eparser",
            .parser_namespace_name_                      = "expression",
            .scanner_class_name_                         = "Scanner",
            .scanner_namespace_name_                     = "scanner",
            .parser_header_preamble_                     = R"~(#   include "../include/command.hpp")~",
            .parser_impl_preamble_                       = "",
            .token_to_terminal_conversion_function_name_ = "convert_token_to_terminal",
            .parsing_result_type_                        = "std::vector<Command>",
            .shift_function_name_                        = "token_to_IR",
            .shift_function_result_type_                 = "Command",
            .reduce_actions_                             = {},
            .additional_ctor_args_                       = "",
            .ctor_body_                                  = "",
            .additional_parser_members_                  = "    std::vector<Command> buffer_;",
            .initializers_for_ctor_                      = " buffer_{}",
            .additional_includes_                        = {
                {"command.hpp", R"~(// File: command.hpp
#ifndef COMMAND_H
#define COMMAND_H
#   include <vector>
enum class Command_kind{
    Value, ADD, SUB, MUL, DIV, POWER, CHS
};

struct Command{
    Command_kind kind_;
    int          value_;
};
#endif
)~"}
            },
            .additional_srcs_                            = {},
            .terminal_to_msg_                            = {},
            .token_type_                                 = "Token",
            .postprocessing_                             = "",
            .user_error_handling_                        = ""
        },
        // Grammar #3:
        /*
         * arkona_module -> module_name fig_br_opened used_modules fig_br_closed |
         *                  module_name fig_br_opened used_modules fig_br_closed
         * used_modules  -> used_module | used_modules semicolon used_module
         * used_module   -> uses id | used_module scope_resolution id
         * module_name   -> module id | module_name scope_resolution id
         */
        Parser_class_details{
            .parser_class_name_                          = "Parser",
            .parser_namespace_name_                      = "arkona_parser",
            .scanner_class_name_                         = "Scanner",
            .scanner_namespace_name_                     = "arkona_scanner_snd_lvl",
            .parser_header_preamble_                     = parser_header_preamble,
            .parser_impl_preamble_                       = "",
            .token_to_terminal_conversion_function_name_ = "token_to_terminal",
            .parsing_result_type_                        = "arkona_parser::ast::Node_ptr",
            .shift_function_name_                        = "shift_function",
            .shift_function_result_type_                 = "Shift_result",
            .reduce_actions_                             = {},
            .additional_ctor_args_                       = " const Errors_and_tries& et",
            .ctor_body_                                  = "",
            .additional_parser_members_                  = R"~(        /* a pointer to a class that counts the number of errors: */
        std::shared_ptr<Error_count>                  en_;
        /* a pointer to a class that counts the number of warnings: */
        std::shared_ptr<Warning_count>                wn_;
        /* a pointer to the prefix tree for identifiers: */
        std::shared_ptr<Char_trie>                    ids_;
        /* a pointer to the prefix tree for string literals: */
        std::shared_ptr<Char_trie>                    strs_;)~",
            .initializers_for_ctor_                      = " en_{et.ec_}, wn_{et.wc_}, ids_{et.ids_trie_}, strs_{et.strs_trie_}",
            .additional_includes_                        = {
        {"token.hpp", R"~(// File: token.hpp
#ifndef ATOKEN_H
#define ATOKEN_H
#   include "../../snd-lvl-scanner/include/arkona-scanner-snd-lvl.h"
namespace arkona_parser{
    using Token = arkona_scanner_snd_lvl::Arkona_token_snd_lvl;
};
#endif
)~"},
        {"ast_node.hpp", R"~(/*
    File:    ast_node.hpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef AST_NODE_H
#define AST_NODE_H
#   include <memory>
#   include <list>
#   include <cstddef>
#   include "../include/token.hpp"
namespace arkona_parser{
    namespace ast{
        class Module;
        class Module_body;
        class Module_name;
        class Used_module;
        class Used_modules;

        class Visitor{
        public:
            Visitor()                   = default;
            Visitor(const Visitor& rhs) = default;
            virtual ~Visitor()          = default;

            virtual void visit(Module&       ref) = 0;
            virtual void visit(Module_name&  ref) = 0;
            virtual void visit(Module_body&  ref) = 0;
            virtual void visit(Used_module&  ref) = 0;
            virtual void visit(Used_modules& ref) = 0;
        };

        class Node{
        public:
            Node()                = default;
            Node(const Node& rhs) = default;
            virtual ~Node()       = default;

            virtual void accept(Visitor& v) = 0;
        };

        using Node_ptr = std::shared_ptr<Node>;

        class Module : public Node{
        public:
            Module()                  = default;
            Module(const Module& rhs) = default;
            virtual ~Module()         = default;

            std::shared_ptr<Module_name>  name_;
            std::shared_ptr<Module_body>  body_;
            std::shared_ptr<Used_modules> used_modules_;

            void accept(Visitor& v) override
            {
                v.visit(*this);
            }
        };

        class Module_name : public Node{
        public:
            Module_name()                       = default;
            Module_name(const Module_name& rhs) = default;
            virtual ~Module_name()              = default;

            std::list<Token> name_components_;

            void accept(Visitor& v) override
            {
                v.visit(*this);
            }
        };

        class Used_module : public Node{
        public:
            Used_module()                       = default;
            Used_module(const Used_module& rhs) = default;
            virtual ~Used_module()              = default;

            std::list<Token> name_components_;

            void accept(Visitor& v) override
            {
                v.visit(*this);
            }
        };


        class Used_modules : public Node{
        public:
            Used_modules()                        = default;
            Used_modules(const Used_modules& rhs) = default;
            virtual ~Used_modules()               = default;

            std::list<std::shared_ptr<Used_module>> name_components_;

            void accept(Visitor& v) override
            {
                v.visit(*this);
            }
        };

        class AST{
        public:
            AST()               = default;
            AST(const AST& rhs) = default;
            virtual ~AST()      = default;

            explicit AST(const Node_ptr& root) : root_{root} {}

            void traverse(Visitor& v) const
            {
                if(root_){
                    root_->accept(v);
                }
            }

            void traverse(Visitor& v)
            {
                if(root_){
                    root_->accept(v);
                }
            }
        private:
            Node_ptr root_;
        };
    };
};
#endif)~"
        },
        {
            "token_to_terminal.hpp", R"~(/*
    File:    token_to_terminal.hpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef TOKEN_TO_TERMINAL_H
#define TOKEN_TO_TERMINAL_H
#   include "../include/terminal_enum.hpp"
#   include "../include/token.hpp"
namespace arkona_parser{
    Terminal token_to_terminal(const Token& token);
};
#endif)~"
        },
        {
            "symbol_table.hpp", R"~(/*
    File:    symbol_table.hpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H
#   include <vector>
#   include <memory>
#   include <set>
#   include <cstddef>
#   include "../../tries/include/trie_for_vector.h"
namespace arkona_parser{
    using Module_parts_trie_ptr = std::shared_ptr<Trie_for_vector<std::size_t>>;

    class Symbol_table{
    public:
        Symbol_table()                                     = default;
        virtual ~Symbol_table()                            = default;
        Symbol_table(const Symbol_table& rhs)              = default;
        Symbol_table& operator = (const Symbol_table& rhs) = default;

        explicit Symbol_table(const Module_parts_trie_ptr& module_parts_trie) :
            indeces_of_inserted_names_{},
            module_parts_trie_{module_parts_trie} {}

        bool        has_module_name(const std::vector<std::size_t>& parts);
        std::size_t insert_module_name(const std::vector<std::size_t>& parts);
    private:
        std::set<std::size_t> indeces_of_inserted_names_;
        Module_parts_trie_ptr module_parts_trie_;
    };
};
#endif)~"
        },

        {"shift_result.hpp", R"~(/*
    File:    shift_result.hpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef SHIFT_RESULT_H
#define SHIFT_RESULT_H
#   include "../include/token.hpp"
#   include "../include/ast_node.hpp"
namespace arkona_parser{
    struct Shift_result{
        Token         token_;
        ast::Node_ptr pnode_;
    };
};
#endif)~"},

        {"shift_function.hpp", R"~(/*
    File:    shift_function.hpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef SHIFT_FUNCTION_H
#define SHIFT_FUNCTION_H
#   include "../include/shift_result.hpp"
#   include "../include/token.hpp"
namespace arkona_parser{
    Shift_result shift_function(const Token& token);
};
#endif)~"},
    },
        .additional_srcs_                            = {
        {
            "token_to_terminal.cpp", R"~(/*
    File:    token_to_terminal.cpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#include <map>
#include "../include/token_to_terminal.hpp"
namespace arkona_parser{
    using Lexem_code     = arkona_scanner_snd_lvl::Lexem_code;
    using Lexem_kind     = arkona_scanner_snd_lvl::Lexem_kind;
    using Keyword_kind   = arkona_scanner_snd_lvl::Keyword_kind;
    using Delimiter_kind = arkona_scanner_snd_lvl::Delimiter_kind;

    static const std::map<Keyword_kind, Terminal> keyword_to_terminal_mapping = {
        {Keyword_kind::Kw_modul,     Terminal::module},
        {Keyword_kind::Kw_ispolzuet, Terminal::uses  },
    };

    static const std::map<Delimiter_kind, Terminal> delimiter_to_terminal_mapping = {
        {Delimiter_kind::Scope_resol,   Terminal::scope_resolution},
        {Delimiter_kind::Comma,         Terminal::comma           },
        {Delimiter_kind::Fig_br_opened, Terminal::fig_br_opened   },
        {Delimiter_kind::Fig_br_closed, Terminal::fig_br_closed   },
        {Delimiter_kind::Semicolon,     Terminal::semicolon       },
    };

    static Terminal convert_keyword_to_terminal(Keyword_kind kw_kind)
    {
        auto     it     = keyword_to_terminal_mapping.find(kw_kind);
        Terminal result = Terminal::End_of_text;
        if(it != keyword_to_terminal_mapping.end()){
            result = it->second;
        }
        return result;
    }

    static Terminal convert_delimiter_to_terminal(Delimiter_kind del_kind)
    {
        auto     it     = delimiter_to_terminal_mapping.find(del_kind);
        Terminal result = Terminal::End_of_text;
        if(it != delimiter_to_terminal_mapping.end()){
            result = it->second;
        }
        return result;
    }

    Terminal token_to_terminal(const Token& token)
    {
        auto       code = token.lexeme_.code_;
        Lexem_kind kind = code.kind_;
        switch(kind){
            case Lexem_kind::Nothing: case Lexem_kind::UnknownLexem:
                return Terminal::End_of_text;
                break;
            case Lexem_kind::Keyword:
                return convert_keyword_to_terminal(static_cast<Keyword_kind>(code.subkind_));
                break;
            case Lexem_kind::Id:
                return Terminal::id;
                break;
            case Lexem_kind::Delimiter:
                return convert_delimiter_to_terminal(static_cast<Delimiter_kind>(code.subkind_));
                break;
            case Lexem_kind::Char:  case Lexem_kind::String:  case Lexem_kind::Integer:
            case Lexem_kind::Float: case Lexem_kind::Complex: case Lexem_kind::Quat:
                return Terminal::End_of_text;
                break;
        }
        return Terminal::End_of_text;
    }
})~"
        },

        {
            "symbol_table.cpp", R"~(/*
    File:    symbol_table.cpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#include "../include/symbol_table.hpp"
namespace arkona_parser{
    std::size_t Symbol_table::insert_module_name(const std::vector<std::size_t>& parts)
    {
        std::size_t result = module_parts_trie_->insert_vector(parts);
        indeces_of_inserted_names_.insert(result);
        return result;
    }

    bool Symbol_table::has_module_name(const std::vector<std::size_t>& parts)
    {
        std::size_t idx = module_parts_trie_->insert_vector(parts);
        return indeces_of_inserted_names_.count(idx) != 0;
    }
};)~"
        },

        {"shift_function.cpp", R"~(/*
    File:    shift_function.cpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#include "../include/shift_function.hpp"
namespace arkona_parser{
    Shift_result shift_function(const Token& token)
    {
        Shift_result result;
        result.token_ = token;
        result.pnode_ = nullptr;
        return result;
    }
};)~"
        },
    },
            .terminal_to_msg_                            = {
        {"module",           "ключевое слово 'модуль'"                   },
        {"uses",             "ключевое слово 'использует'"               },
        {"scope_resolution", "оператор разрешения области видимости '::'"},
        {"comma",            "запятая"                                   },
        {"fig_br_opened",    "открывающая фигурная скобка"               },
        {"fig_br_closed",    "закрывающая фигурная скобка"               },
        {"id",               "идентификатор"                             },
        {"semicolon",        "точка с запятой"                           },
    },
            .token_type_                                 = "Token",
            .postprocessing_                             = "",
            .user_error_handling_                        = ""
        },
    };

    struct Content_processing : public boost::static_visitor<void>{
    public:
        Content_processing(Parser_builder& builder, std::size_t idx) :
            builder_{builder}, idx_{idx} {}

        void operator ()(Generated_texts& texts)
        {
            std::cout << "Includes for the grammar #" << idx_ << "\n";
            for(const auto& p : texts.include_){
                const auto& name    = p.first;
                const auto& content = p.second;
                std::cout << "File ../include/" << name << ":\n"
                          << content            << "\n\n";
            }

            std::cout << "Srcs for the grammar #" << idx_ << "\n";
            for(const auto& p : texts.src_){
                const auto& name    = p.first;
                const auto& content = p.second;
                std::cout << "File ../src/" << name << ":\n"
                          << content        << "\n\n";
            }
        }

        void operator ()(Error_info& einfo)
        {
            auto error_msg   = builder_.error_info_to_string(einfo);
            std::cout << "There are clashes in SLR(1) tables" << "\n"
                      << error_msg                            << "\n";
        }
    private:
        Parser_builder& builder_;
        std::size_t     idx_;
    };


    void testing_func()
    {
        std::size_t idx = 0;
        for(const auto& g_info : grammar_info){
            std::cout << "*******************************************************************************************\n";
            std::cout << "Grammar #" << idx << "\n";

            Grammar_builder grammar_builder{g_info};
            Grammar         grammar     = grammar_builder();
            std::string     grammar_str = grammar_builder.to_string(grammar);

            std::cout << grammar_str << "\n\n";

            std::cout << "Terminals and their codes:\n";
            std::size_t counter = 1;
            for(const auto& t : g_info.names_info_.terminals_mames_){
                std::cout << t << ": " << counter << "  ";
                counter++;
            }

            std::cout << "\nNon-terminals and their codes:\n";
            counter = 1;
            for(const auto& t : g_info.names_info_.nonterminals_mames_){
                std::cout << t << ": " << counter << "  ";
                counter++;
            }

            Sets_first      firsts_builder{grammar};
            auto            firsts      = firsts_builder();

            std::cout << "\n\nSets FIRST for grammar #" << idx << "\n";
            std::string     firsts_str  = grammar_builder.to_string(firsts, "FIRST");
            std::cout << firsts_str << "\n\n";

            Sets_follow     follow_builder{grammar, firsts};
            auto            follows = follow_builder();
            std::cout << "Sets FOLLOW for grammar #" << idx << "\n";
            std::string     follows_str = grammar_builder.to_string(follows, "FOLLOW");
            std::cout << follows_str << "\n\n";

            Closure_builder closure_builder{grammar};
            std::size_t item_number = 0;
            for(const auto& item : items_for_grammars[idx]){
                std::cout << "Calculation of closure for the item #" << item_number << "\n";
                Items_collection collection {item};

                auto closure               = closure_builder(collection);
                std::string closure_str    = grammar_builder.to_string(closure, grammar);
                std::string collection_str = grammar_builder.to_string(collection, grammar);

                std::cout << "closure(" << collection_str << ") = " << closure_str << "\n\n";
                ++item_number;
            }

            Parser_tables_builder parser_tables_builder{grammar};
            auto items_map     = parser_tables_builder.build_item_map();
            auto items_map_str = grammar_builder.to_string(items_map, grammar);

            std::cout << "Items collections and GOTOs for the grammar #" << idx << "\n";
            std::cout << items_map_str << "\n\n";

            auto parser_tables     = parser_tables_builder.build_actions_and_gotos();
            auto parser_tables_str = grammar_builder.to_string(parser_tables, grammar);

            std::cout << "SLR(1) analysis tables for the grammar #" << idx << "\n";
            std::cout << parser_tables_str << "\n\n";

            Parser_info    parser_info;
            parser_info.class_details_         = parser_details[idx];
            parser_info.grammar_info_          = g_info;
            Parser_builder parser_builder{parser_info};

            auto parser_files = parser_builder();
            Content_processing visitor{parser_builder, idx};
            boost::apply_visitor(visitor, parser_files);
            idx++;
        }
    }
};