/*
    File:    parser-generator-lib-testing.cpp
    Created: 22 April 2020 at 19:39 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/parser-generator-lib-testing-func.hpp"
int main()
{
    parser_gen_lib_testing::testing_func();
    return 0;
}