/*
    File:    position.cpp
    Created: 06 November 2019 at 07:05 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/position.h"
#include <cstdio>
namespace ascaner{
    bool operator==(const Position& lhs, const Position& rhs)
    {
        return (lhs.line_no_ == rhs.line_no_) && (lhs.line_pos_ == rhs.line_pos_);
    }

    bool operator==(const Position_range& lhs, const Position_range& rhs)
    {
        bool t = (lhs.begin_pos_ == rhs.begin_pos_) && (lhs.end_pos_ == rhs.end_pos_);
        if(!t){
            puts("Positions of two tokens are not equal.");
            printf("First position is  %zu:%zu--%zu:%zu\n",
                   lhs.begin_pos_.line_no_, lhs.begin_pos_.line_pos_,
                   lhs.end_pos_.line_no_,   lhs.end_pos_.line_pos_);
            printf("Second position is %zu:%zu--%zu:%zu\n",
                   rhs.begin_pos_.line_no_, rhs.begin_pos_.line_pos_,
                   rhs.end_pos_.line_no_,   rhs.end_pos_.line_pos_);
        }
        return (lhs.begin_pos_ == rhs.begin_pos_) && (lhs.end_pos_ == rhs.end_pos_);
    }
};