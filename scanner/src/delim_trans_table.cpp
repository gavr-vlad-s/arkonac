/*
    File:    delim_trans_table.cpp
    Created: 27 October 2019 at 16:15 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstdint>
#include "../include/delim_trans_table.h"
#include "../include/get_init_state.h"
#include "../include/state_for_char.h"
#include "../include/mysize.h"
#include "../include/trans-elem.h"
#include "../include/search_char.h"

namespace arkona_scanner{
    // "U'!', U'#', U'%', U'&', U'(', U')', U'*', U'+', U',', U'-', U'.', U'/', U':', U';', U'<', U'=', U'>', U'?', U'@', U'[', U'\\', U']', U'^', U'{', U'|', U'}', U'~'"
    static const State_for_char init_table[] = {
        {0,   U'!' }, {12,  U'#'}, {14,  U'%'}, {18,  U'&'},
        {24,  U'(' }, {26,  U')'}, {27,  U'*'}, {34,  U'+'},
        {38,  U',' }, {39,  U'-'}, {44,  U'.'}, {48,  U'/'},
        {53,  U':' }, {60,  U';'}, {61,  U'<'}, {82,  U'='},
        {84,  U'>' }, {117, U'?'}, {88,  U'@'}, {89,  U'['},
        {91,  U'\\'}, {93,  U']'}, {94,  U'^'}, {98,  U'{'},
        {103, U'|' }, {111, U'}'}, {112, U'~'}
    };

    static constexpr std::size_t init_table_size = size(init_table);

    enum Offsets : uint16_t{
        Empty = 0,
        Str_exclamation_amp_plus_minus_colon_lt_eq_qm_vert = 1,
        Str_sharp = 11,
        Str_sharp_eq_vert = 13,
        Str_amp = 17,
        Str_amp_eq = 19,
        Str_amp_eq_vert = 22,
        Str_amp_gt = 26,
        Str_amp_vert = 29,
        Str_rbc_colon_eq_gt_sqbrc_figbrc = 32,
        Str_ast_slash_eq = 39,
        Str_ast_eq_bslash = 43,
        Str_plus_eq = 47,
        Str_minus_eq_gt = 50,
        Str_point = 54,
        Str_point_colon = 56,
        Str_point_eq = 59,
        Str_point_vert = 62,
        Str_colon = 65,
        Str_lt = 67,
        Str_eq = 69,
        Str_eq_gt = 71,
        Str_gt = 74,
        Str_gt_qm = 76,
        Str_hat_eq = 79,
        Str_vert = 82,
        Str_figbrc = 84
    };


    static const char32_t trans_strs[] = {
        U'\0',
        U'!', U'&', U'+', U'-', U':', U'<', U'=', U'?', U'|', U'\0',
        U'#', U'\0',
        U'#', U'=', U'|', U'\0',
        U'&', U'\0',
        U'&', U'=', U'\0',
        U'&', U'=', U'|', U'\0',
        U'&', U'>', U'\0',
        U'&', U'|', U'\0',
        U')', U':', U'=', U'>', U']', U'}', U'\0',
        U'*', U'/', U'=', U'\0',
        U'*', U'=', U'\\', U'\0',
        U'+', U'=', U'\0',
        U'-', U'=', U'>', U'\0',
        U'.', U'\0',
        U'.', U':', U'\0',
        U'.', U'=', U'\0',
        U'.', U'|', U'\0',
        U':', U'\0',
        U'<', U'\0',
        U'=', U'\0',
        U'=', U'>', U'\0',
        U'>', U'\0',
        U'>', U'?', U'\0',
        U'^', U'=', U'\0',
        U'|', U'\0',
        U'}', U'\0'
    };


    static const trans_table::Trans_elem trans_table[] = {
        {Str_amp_eq_vert,                                    {Lexem_kind::Delimiter,                 Delimiter_kind::Logical_not},   1}, // 0:   !
        {Str_amp,                                            {Lexem_kind::Delimiter,      Delimiter_kind::Maybe_logical_and_not },   4}, // 1:   !&
        {Empty,                                              {Lexem_kind::Delimiter,                         Delimiter_kind::NEQ},   0}, // 2:   !=
        {Str_vert,                                           {Lexem_kind::Delimiter,        Delimiter_kind::Maybe_logical_or_not},   5}, // 3:   !|
        {Str_point_eq,                                       {Lexem_kind::Delimiter,             Delimiter_kind::Logical_and_not},   6}, // 4:   !&&
        {Str_point_eq,                                       {Lexem_kind::Delimiter,              Delimiter_kind::Logical_or_not},   8}, // 5:   !||
        {Str_eq,                                             {Lexem_kind::Delimiter,        Delimiter_kind::Logical_and_not_full},  10}, // 6:   !&&.
        {Empty,                                              {Lexem_kind::Delimiter,      Delimiter_kind::Logical_and_not_assign},   0}, // 7:   !&&=
        {Str_eq,                                             {Lexem_kind::Delimiter,         Delimiter_kind::Logical_or_not_full},  11}, // 8:   !||.
        {Empty,                                              {Lexem_kind::Delimiter,       Delimiter_kind::Logical_or_not_assign},   0}, // 9:   !||=
        {Empty,                                              {Lexem_kind::Delimiter, Delimiter_kind::Logical_and_not_full_assign},   0}, // 10:  !&&.=
        {Empty,                                              {Lexem_kind::Delimiter,  Delimiter_kind::Logical_or_not_full_assign},   0}, // 11:  !||.

        {Str_sharp,                                          {Lexem_kind::Delimiter,                       Delimiter_kind::Sharp},  13}, // 12:  #
        {Empty,                                              {Lexem_kind::Delimiter,                   Delimiter_kind::Data_size},   0}, // 13:  ##

        {Str_point_eq,                                       {Lexem_kind::Delimiter,                   Delimiter_kind::Remainder},  15}, // 14:  %
        {Str_eq,                                             {Lexem_kind::Delimiter,             Delimiter_kind::Float_remainder},  17}, // 15:  %.
        {Empty,                                              {Lexem_kind::Delimiter,            Delimiter_kind::Remainder_assign},   0}, // 16:  %=
        {Empty,                                              {Lexem_kind::Delimiter,      Delimiter_kind::Float_remainder_assign},   0}, // 17:  %.=

        {Str_amp_eq,                                         {Lexem_kind::Delimiter,                 Delimiter_kind::Bitwise_and},  19}, // 18:  &
        {Str_point_eq,                                       {Lexem_kind::Delimiter,                 Delimiter_kind::Logical_and},  21}, // 19:  &&
        {Empty,                                              {Lexem_kind::Delimiter,          Delimiter_kind::Bitwise_and_assign},   0}, // 20:  &=
        {Str_eq,                                             {Lexem_kind::Delimiter,            Delimiter_kind::Logical_and_full},  23}, // 21:  &&.
        {Empty,                                              {Lexem_kind::Delimiter,          Delimiter_kind::Logical_and_assign},   0}, // 22:  &&=
        {Empty,                                              {Lexem_kind::Delimiter,     Delimiter_kind::Logical_and_full_assign},   0}, // 23:  &&.=

        {Str_colon,                                          {Lexem_kind::Delimiter,             Delimiter_kind::Round_br_opened},  25}, // 24:  (
        {Empty,                                              {Lexem_kind::Delimiter,                 Delimiter_kind::Tuple_begin},   0}, // 25:  (:

        {Empty,                                              {Lexem_kind::Delimiter,             Delimiter_kind::Round_br_closed},   0}, // 26:  )

        {Str_ast_slash_eq,                                   {Lexem_kind::Delimiter,                         Delimiter_kind::Mul},  28}, // 27:  *
        {Str_point_eq,                                       {Lexem_kind::Delimiter,                       Delimiter_kind::Power},  31}, // 28:  **
        {Empty,                                              {Lexem_kind::Delimiter,                 Delimiter_kind::Comment_end},   0}, // 29:  */
        {Empty,                                              {Lexem_kind::Delimiter,                  Delimiter_kind::Mul_assign},   0}, // 30:  *=
        {Str_eq,                                             {Lexem_kind::Delimiter,                 Delimiter_kind::Float_power},  33}, // 31:  **.
        {Empty,                                              {Lexem_kind::Delimiter,                Delimiter_kind::Power_assign},   0}, // 32:  **=
        {Empty,                                              {Lexem_kind::Delimiter,          Delimiter_kind::Float_power_assign},   0}, // 33:  **.=

        {Str_plus_eq,                                        {Lexem_kind::Delimiter,                        Delimiter_kind::Plus},  35}, // 34:  +
        {Str_lt,                                             {Lexem_kind::Delimiter,                         Delimiter_kind::Inc},  37}, // 35:  ++
        {Empty,                                              {Lexem_kind::Delimiter,                 Delimiter_kind::Plus_assign},   0}, // 36:  +=
        {Empty,                                              {Lexem_kind::Delimiter,                    Delimiter_kind::Wrap_inc},   0}, // 37:  ++<

        {Empty,                                              {Lexem_kind::Delimiter,                       Delimiter_kind::Comma},   0}, // 38:  ,

        {Str_minus_eq_gt,                                    {Lexem_kind::Delimiter,                       Delimiter_kind::Minus},  40}, // 39:  -
        {Str_lt,                                             {Lexem_kind::Delimiter,                         Delimiter_kind::Dec},  43}, // 40:  --
        {Empty,                                              {Lexem_kind::Delimiter,                Delimiter_kind::Minus_assign},   0}, // 41:  -=
        {Empty,                                              {Lexem_kind::Delimiter,                 Delimiter_kind::Right_arrow},   0}, // 42:  ->
        {Empty,                                              {Lexem_kind::Delimiter,                    Delimiter_kind::Wrap_dec},   0}, // 43:  --<

        {Str_point_vert,                                     {Lexem_kind::Delimiter,                       Delimiter_kind::Point},  45}, // 44:  .
        {Empty,                                              {Lexem_kind::Delimiter,                       Delimiter_kind::Range},   0}, // 45:  ..
        {Str_point,                                          {Lexem_kind::Delimiter,         Delimiter_kind::Maybe_algebraic_sep},  47}, // 46:  .|
        {Empty,                                              {Lexem_kind::Delimiter,               Delimiter_kind::Algebraic_sep},   0}, // 47:  .|.

        {Str_ast_eq_bslash,                                  {Lexem_kind::Delimiter,                         Delimiter_kind::Div},  49}, // 48:  /
        {Empty,                                              {Lexem_kind::Delimiter,               Delimiter_kind::Comment_begin},   0}, // 49:  /*
        {Empty,                                              {Lexem_kind::Delimiter,                  Delimiter_kind::Div_assign},   0}, // 50:  /=
        {Str_eq,                                             {Lexem_kind::Delimiter,        Delimiter_kind::Symmetric_difference},  52}, /* 51:  /\ */
        {Empty,                                              {Lexem_kind::Delimiter, Delimiter_kind::Symmetric_difference_assign},   0}, /* 52:  /\= */

        {Str_rbc_colon_eq_gt_sqbrc_figbrc,                   {Lexem_kind::Delimiter,                       Delimiter_kind::Colon},  54}, // 53:  :
        {Empty,                                              {Lexem_kind::Delimiter,                   Delimiter_kind::Tuple_end},   0}, // 54:  :)
        {Empty,                                              {Lexem_kind::Delimiter,                 Delimiter_kind::Scope_resol},   0}, // 55:  ::
        {Empty,                                              {Lexem_kind::Delimiter,                        Delimiter_kind::Copy},   0}, // 56:  :=
        {Empty,                                              {Lexem_kind::Delimiter,              Delimiter_kind::Meta_closed_br},   0}, // 57:  :>
        {Empty,                                              {Lexem_kind::Delimiter,          Delimiter_kind::Colon_sq_br_closed},   0}, // 58:  :]
        {Empty,                                              {Lexem_kind::Delimiter,         Delimiter_kind::Colon_fig_br_closed},   0}, // 59:  :}

        {Empty,                                              {Lexem_kind::Delimiter,                   Delimiter_kind::Semicolon},   0}, // 60:  ;

        {Str_exclamation_amp_plus_minus_colon_lt_eq_qm_vert, {Lexem_kind::Delimiter,                          Delimiter_kind::LT},  62}, // 61:  <
        {Str_gt,                                             {Lexem_kind::Delimiter,          Delimiter_kind::Maybe_non_specific},  71}, // 62:  <!
        {Str_amp_gt,                                         {Lexem_kind::Delimiter,               Delimiter_kind::Maybe_address},  72}, // 63:  <&
        {Str_gt,                                             {Lexem_kind::Delimiter,             Delimiter_kind::Maybe_meta_plus},  74}, // 64:  <+
        {Empty,                                              {Lexem_kind::Delimiter,                  Delimiter_kind::Left_arrow},   0}, // 65:  <-
        {Str_gt,                                             {Lexem_kind::Delimiter,                Delimiter_kind::Meta_open_br},  75}, // 66:  <:
        {Str_eq,                                             {Lexem_kind::Delimiter,                  Delimiter_kind::Left_shift},  76}, // 67:  <<
        {Empty,                                              {Lexem_kind::Delimiter,                         Delimiter_kind::LEQ},   0}, // 68:  <=
        {Str_gt_qm,                                          {Lexem_kind::Delimiter,              Delimiter_kind::Maybe_ElemType},  77}, // 69:  <?
        {Empty,                                              {Lexem_kind::Delimiter,           Delimiter_kind::Cycle_name_prefix},   0}, // 70:  <|
        {Empty,                                              {Lexem_kind::Delimiter,                Delimiter_kind::Non_specific},   0}, // 71:  <!>
        {Str_gt,                                             {Lexem_kind::Delimiter,          Delimiter_kind::Maybe_data_address},  79}, // 72:  <&&
        {Empty,                                              {Lexem_kind::Delimiter,                     Delimiter_kind::Address},   0}, // 73:  <&>
        {Str_eq,                                             {Lexem_kind::Delimiter,                   Delimiter_kind::Meta_plus},  80}, // 74:  <+>
        {Empty,                                              {Lexem_kind::Delimiter,            Delimiter_kind:: Deduce_arg_type},   0}, // 75:  <:>
        {Empty,                                              {Lexem_kind::Delimiter,           Delimiter_kind::Left_shift_assign},   0}, // 76:  <<=
        {Empty,                                              {Lexem_kind::Delimiter,                    Delimiter_kind::ElemType},   0}, // 77:  <?>
        {Str_gt,                                             {Lexem_kind::Delimiter,             Delimiter_kind::Maybe_expr_type},  81}, // 78:  <??
        {Empty,                                              {Lexem_kind::Delimiter,                Delimiter_kind::Data_address},   0}, // 79:  <&&>
        {Empty,                                              {Lexem_kind::Delimiter,            Delimiter_kind::Meta_plus_assign},   0}, // 80:  <+>=
        {Empty,                                              {Lexem_kind::Delimiter,                    Delimiter_kind::ExprType},   0}, // 81:  <??>

        {Str_eq,                                             {Lexem_kind::Delimiter,                      Delimiter_kind::Assign},  83}, // 82:  =
        {Empty,                                              {Lexem_kind::Delimiter,                          Delimiter_kind::EQ},   0}, // 83:  ==

        {Str_eq_gt,                                          {Lexem_kind::Delimiter,                          Delimiter_kind::GT},  85}, // 84:  >
        {Empty,                                              {Lexem_kind::Delimiter,                         Delimiter_kind::GEQ},   0}, // 85:  >=
        {Str_eq,                                             {Lexem_kind::Delimiter,                 Delimiter_kind::Right_shift},  87}, // 86:  >>
        {Empty,                                              {Lexem_kind::Delimiter,          Delimiter_kind::Right_shift_assign},   0}, // 87:  >>=

        {Empty,                                              {Lexem_kind::Delimiter,                          Delimiter_kind::At},   0}, // 88:  @

        {Str_colon,                                          {Lexem_kind::Delimiter,                Delimiter_kind::Sq_br_opened},  90}, // 89:  [
        {Empty,                                              {Lexem_kind::Delimiter,          Delimiter_kind::Colon_sq_br_opened},   0}, // 90:  [:

        {Str_eq,                                             {Lexem_kind::Delimiter,              Delimiter_kind::Set_difference},  92}, /* 91:  \ */
        {Empty,                                              {Lexem_kind::Delimiter,       Delimiter_kind::Set_difference_assign},   0}, // 92:  \=

        {Empty,                                              {Lexem_kind::Delimiter,                Delimiter_kind::Sq_br_closed},   0}, // 93:  ]

        {Str_hat_eq,                                         {Lexem_kind::Delimiter,                 Delimiter_kind::Bitwise_xor},  95}, // 94:  ^
        {Str_eq,                                             {Lexem_kind::Delimiter,                 Delimiter_kind::Logical_xor},  97}, // 95:  ^^
        {Empty,                                              {Lexem_kind::Delimiter,          Delimiter_kind::Bitwise_xor_assign},   0}, // 96:  ^=
        {Empty,                                              {Lexem_kind::Delimiter,          Delimiter_kind::Logical_xor_assign},   0}, // 97:  ^^=

        {Str_point_colon,                                    {Lexem_kind::Delimiter,               Delimiter_kind::Fig_br_opened},  99}, // 98:  {
        {Str_point,                                          {Lexem_kind::Delimiter,               Delimiter_kind::Maybe_pattern}, 101}, // 99: {.
        {Empty,                                              {Lexem_kind::Delimiter,         Delimiter_kind::Colon_fig_br_opened},   0}, // 100: {:
        {Str_figbrc,                                         {Lexem_kind::Delimiter,               Delimiter_kind::Maybe_pattern}, 102}, // 101: {..
        {Empty,                                              {Lexem_kind::Delimiter,                     Delimiter_kind::Pattern},   0}, // 102: {..}

        {Str_sharp_eq_vert,                                  {Lexem_kind::Delimiter,                  Delimiter_kind::Bitwise_or}, 104}, // 103: |
        {Str_vert,                                           {Lexem_kind::Delimiter,                  Delimiter_kind::Maybe_card}, 107}, // 104: |#
        {Empty,                                              {Lexem_kind::Delimiter,           Delimiter_kind::Bitwise_or_assign},   0}, // 105: |=
        {Str_point_eq,                                       {Lexem_kind::Delimiter,                  Delimiter_kind::Logical_or}, 108}, // 106: ||
        {Empty,                                              {Lexem_kind::Delimiter,                        Delimiter_kind::Card},   0}, // 107: |#|
        {Str_eq,                                             {Lexem_kind::Delimiter,             Delimiter_kind::Logical_or_full}, 110}, // 108: ||.
        {Empty,                                              {Lexem_kind::Delimiter,           Delimiter_kind::Logical_or_assign},   0}, // 109: ||=
        {Empty,                                              {Lexem_kind::Delimiter,      Delimiter_kind::Logical_or_full_assign},   0}, // 110: ||.=

        {Empty,                                              {Lexem_kind::Delimiter,               Delimiter_kind::Fig_br_closed},   0}, // 111: }

        {Str_amp_vert,                                       {Lexem_kind::Delimiter,                 Delimiter_kind::Bitwise_not}, 113}, // 112: ~
        {Str_eq,                                             {Lexem_kind::Delimiter,             Delimiter_kind::Bitwise_and_not}, 115}, // 113: ~&
        {Str_eq,                                             {Lexem_kind::Delimiter,              Delimiter_kind::Bitwise_or_not}, 116}, // 114: ~|
        {Empty,                                              {Lexem_kind::Delimiter,      Delimiter_kind::Bitwise_and_not_assign},   0}, // 115: ~&=
        {Empty,                                              {Lexem_kind::Delimiter,       Delimiter_kind::Bitwise_or_not_assign}, 116}, // 116: ~|=

        {Str_point,                                          {Lexem_kind::Delimiter,                     Delimiter_kind::Cond_op}, 118}, // 117: ?
        {Empty,                                              {Lexem_kind::Delimiter,                Delimiter_kind::Cond_op_full},   0}, // 118: ?.

    };


    int Delim_trans_table::init_state(char32_t c) const
    {
        return get_init_state(c, init_table, init_table_size);
    }

    int Delim_trans_table::next_state(int state, char32_t c) const
    {
        int             result           = Delim_trans_table::there_is_no_transition;
        auto            elem             = trans_table[state];
        const char32_t* transition_chars = trans_strs + elem.offset_of_first_transition_char_;
        int             y                = search_char(c, transition_chars);
        if(y != THERE_IS_NO_CHAR){
            result = elem.first_state_ + y;
        }
        return result;
    }

    Lexeme_info Delim_trans_table::lexeme_for_state(int state) const
    {
        Lexeme_info result;
        result.code_     = trans_table[state].code_;
        return result;
    }
};