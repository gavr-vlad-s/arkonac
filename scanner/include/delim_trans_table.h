/*
    File:    delim_trans_table.h
    Created: 27 October 2019 at 14:17 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef DELIM_TRANS_TABLE_H
#define DELIM_TRANS_TABLE_H
#   include "../include/lexeme.h"
namespace arkona_scanner{
    class Delim_trans_table{
    public:
        static constexpr int there_is_no_transition = -1;

        int init_state(char32_t c) const;
        int next_state(int state, char32_t c) const;
        Lexeme_info lexeme_for_state(int state) const;
    };
};
#endif