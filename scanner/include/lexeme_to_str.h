/*
    File:    lexeme_to_str.h
    Created: 11 August 2019 at 20:26 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef LEXEME_TO_STR_H
#define LEXEME_TO_STR_H
#   include <string>
#   include <memory>
#   include "../include/lexeme.h"
#   include "../../tries/include/char_trie.h"
namespace arkona_scanner{
    std::string to_string(const Lexeme_info& li,
                          const std::shared_ptr<Char_trie>& ids_trie,
                          const std::shared_ptr<Char_trie>& strs_trie);
};
#endif