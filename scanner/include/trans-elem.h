/*
    File:    trans-elem.h
    Created: 19 October 2019 at 07:15 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef TRANS_ELEM_H
#define TRANS_ELEM_H
#   include <cstdint>
#   include "../include/lexeme.h"
namespace trans_table{
/**
 * Element of transition table.
 */
    struct Trans_elem{
        uint16_t                   offset_of_first_transition_char_;
        arkona_scanner::Lexem_code code_;
        uint16_t                   first_state_;
    };
};
#endif