/*
    File:    category.h
    Created: 02 July 2019 at 20:05 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef CATEGORY_H
#define CATEGORY_H
namespace arkona_scanner{
    enum class Category : uint32_t{
        Spaces,       Other,            Id_begin,
        Id_body,      Keyword_begin,    Delimiter_begin,
        Double_quote, Letters_Xx,       Letters_Bb,
        Letters_Oo,   Single_quote,     Dollar,
        Hex_digit,    Oct_digit,        Bin_digit,
        Dec_digit,    Zero,             Letters_Ee,
        Plus_minus,   Precision_letter, Digits_1_to_9,
        Point,        Quat_suffix
    };
};
#endif