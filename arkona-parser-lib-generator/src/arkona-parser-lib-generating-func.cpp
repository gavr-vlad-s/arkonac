/*
    File:    arkona-parser-lib-generating-func.cpp
    Created: 31 May 2020 at 09:31 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/arkona-parser-lib-generating-func.hpp"
#include "../../parser-generator-lib/include/parser_builder.hpp"

using parser_generator::Names_info;
using parser_generator::Grammar_info;
using parser_generator::Named_symbol;
using parser_generator::Kind;
using parser_generator::Named_rule;
using parser_generator::Grammar_builder;
using parser_generator::Grammar;
using parser_generator::slr1::Parser_builder;
using parser_generator::slr1::Parser_info;
using parser_generator::slr1::Parser_class_details;

static const Grammar_info grammar_info {
    Names_info{
        {
            "module",        "uses", "scope_resolution", "comma", "fig_br_opened",
            "fig_br_closed", "id",   "semicolon"
        },

        {"arkona_module", "qualified_id", "block", "imports"},

        "arkona_module"
    },
    {
        Named_rule{
            "arkona_module", {
                Named_symbol{Kind::Terminal,     "module"          },
                Named_symbol{Kind::Non_terminal, "qualified_id"    },
                Named_symbol{Kind::Non_terminal, "block"           },
            }
        },
        Named_rule{
            "arkona_module", {
                Named_symbol{Kind::Terminal,     "module"          },
                Named_symbol{Kind::Non_terminal, "qualified_id"    },
                Named_symbol{Kind::Non_terminal, "imports"         },
                Named_symbol{Kind::Non_terminal, "block"           },
            }
        },
        Named_rule{
            "qualified_id", {
                Named_symbol{Kind::Terminal,     "id"              },
            }
        },
        Named_rule{
            "qualified_id", {
                Named_symbol{Kind::Non_terminal, "qualified_id"    },
                Named_symbol{Kind::Terminal,     "scope_resolution"},
                Named_symbol{Kind::Terminal,     "id"              },
            }
        },
        Named_rule{
            "imports", {
                Named_symbol{Kind::Terminal,     "uses"            },
                Named_symbol{Kind::Non_terminal, "qualified_id"    },
            }
        },
        Named_rule{
            "imports", {
                Named_symbol{Kind::Non_terminal, "imports"         },
                Named_symbol{Kind::Terminal,     "semicolon"       },
                Named_symbol{Kind::Terminal,     "uses"            },
                Named_symbol{Kind::Non_terminal, "qualified_id"    },
            }
        },
        Named_rule{
            "block", {
                Named_symbol{Kind::Terminal,     "fig_br_opened"   },
                Named_symbol{Kind::Terminal,     "fig_br_closed"   },
            }
        },

    }
};


using visitor_result_t = boost::variant<Texts, std::string>;

using Error_info = parser_generator::slr1::Error_info;


struct Content_processing : public boost::static_visitor<visitor_result_t>{
public:
    Content_processing(Parser_builder& builder) : builder_{builder} {}

    visitor_result_t operator ()(Texts& texts)
    {
        return texts;
    }

    visitor_result_t operator ()(Error_info& einfo)
    {
        return builder_.error_info_to_string(einfo);
    }
private:
    Parser_builder& builder_;
};


static const std::string parser_header_preamble = R"~(#   include "../../snd-lvl-scanner/include/arkona-scanner-snd-lvl.h"
#   include "../include/ast_node.hpp"
#   include "../../tries/include/errors_and_tries.h"
#   include "../../tries/include/error_count.h"
#   include "../../tries/include/char_trie.h"
#   include "../include/shift_result.hpp"
#   include "../include/symbol_table.hpp")~";

static const std::string additional_parser_members = R"~(        /* a pointer to a class that counts the number of errors: */
        std::shared_ptr<Error_count>                  en_;
        /* a pointer to a class that counts the number of warnings: */
        std::shared_ptr<Warning_count>                wn_;
        /* a pointer to the prefix tree for identifiers: */
        std::shared_ptr<Char_trie>                    ids_;
        /* a pointer to the prefix tree for string literals: */
        std::shared_ptr<Char_trie>                    strs_;
        /* a pointer to the symbol table */
        std::shared_ptr<Symbol_table>                 symbol_table_;)~";

static const std::string initializers_for_ctor = " en_{et.ec_}, wn_{et.wc_}, ids_{et.ids_trie_}, strs_{et.strs_trie_}, symbol_table_{symbol_table}";

static const std::string token_hpp = R"~(/*
    File:    token.hpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ATOKEN_H
#define ATOKEN_H
#   include "../../snd-lvl-scanner/include/arkona-scanner-snd-lvl.h"
namespace arkona_parser{
    using Token = arkona_scanner_snd_lvl::Arkona_token_snd_lvl;
};
#endif)~";

static const std::string ast_node_hpp = R"~(/*
    File:    ast_node.hpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef AST_NODE_H
#define AST_NODE_H
#   include <memory>
#   include <list>
#   include <cstddef>
#   include "../include/token.hpp"
namespace arkona_parser{
    namespace ast{
        struct Module;
        struct Qualified_id;
        struct Imports;
        struct Block;

        class Visitor{
        public:
            Visitor()                   = default;
            Visitor(const Visitor& rhs) = default;
            virtual ~Visitor()          = default;

            virtual void visit(Module&       ref) = 0;
            virtual void visit(Qualified_id& ref) = 0;
            virtual void visit(Imports&      ref) = 0;
            virtual void visit(Block&        ref) = 0;
        };

        class Node{
        public:
            Node()                = default;
            Node(const Node& rhs) = default;
            virtual ~Node()       = default;

            virtual void accept(Visitor& v) = 0;
        };

        using Node_ptr = std::shared_ptr<Node>;

        template<typename Derived>
        class Visitable : public Node{
        public:
            using Node::Node;
            void accept(Visitor& v) override
            {
                v.visit(*static_cast<Derived*>(this));
            }
        };

        struct Module : public Visitable<Module>{
        public:
            using Visitable<Module>::Visitable;

            Module()                  = default;
            Module(const Module& rhs) = default;
            virtual ~Module()         = default;

            Module(const std::shared_ptr<Qualified_id>& name,
                   const std::shared_ptr<Block>&        body,
                   const std::shared_ptr<Imports>&      used_modules) :
                name_{name}, body_{body}, used_modules_{used_modules}
            {
            }

            std::shared_ptr<Qualified_id> name_;
            std::shared_ptr<Block>        body_;
            std::shared_ptr<Imports>      used_modules_;
        };

        struct Qualified_id : public Visitable<Qualified_id>{
        public:
            using Visitable<Qualified_id>::Visitable;

            Qualified_id()                        = default;
            Qualified_id(const Qualified_id& rhs) = default;
            virtual ~Qualified_id()               = default;

            Qualified_id(const std::list<Token>& name_components) :
                name_components_{name_components}
            {
            }

            std::list<Token> name_components_;
        };

        struct Imports : public Visitable<Imports>{
        public:
            using Visitable<Imports>::Visitable;

            Imports()                   = default;
            Imports(const Imports& rhs) = default;
            virtual ~Imports()          = default;

            Imports(const std::list<std::shared_ptr<Qualified_id>>& imports) :
                imports_{imports}
            {
            }

            std::list<std::shared_ptr<Qualified_id>> imports_;
        };

        struct Block : public Visitable<Block>{
        public:
            using Visitable<Block>::Visitable;

            Block()                 = default;
            Block(const Block& rhs) = default;
            virtual ~Block()        = default;
        };

        class AST{
        public:
            AST()               = default;
            AST(const AST& rhs) = default;
            virtual ~AST()      = default;

            explicit AST(const Node_ptr& root) : root_{root} {}

            void traverse(Visitor& v) const
            {
                if(root_){
                    root_->accept(v);
                }
            }

            void traverse(Visitor& v)
            {
                if(root_){
                    root_->accept(v);
                }
            }
        private:
            Node_ptr root_;
        };
    };
};
#endif)~";

static const std::string token_to_terminal_hpp = R"~(/*
    File:    token_to_terminal.hpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef TOKEN_TO_TERMINAL_H
#define TOKEN_TO_TERMINAL_H
#   include "../include/terminal_enum.hpp"
#   include "../include/token.hpp"
namespace arkona_parser{
    Terminal token_to_terminal(const Token& token);
};
#endif)~";

static const std::string symbol_table_hpp = R"~(/*
    File:    symbol_table.hpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H
#   include <vector>
#   include <memory>
#   include <set>
#   include <cstddef>
#   include <string>
#   include "../../tries/include/trie_for_vector.h"
#   include "../../tries/include/char_trie.h"
namespace arkona_parser{
    using Module_parts_trie_ptr = std::shared_ptr<Trie_for_vector<std::size_t>>;

    class Symbol_table{
    public:
        Symbol_table()                                     = default;
        virtual ~Symbol_table()                            = default;
        Symbol_table(const Symbol_table& rhs)              = default;
        Symbol_table& operator = (const Symbol_table& rhs) = default;

        explicit Symbol_table(const Module_parts_trie_ptr& module_parts_trie,
                              const std::shared_ptr<Char_trie>& ids_trie) :
            indeces_of_inserted_names_{},
            module_parts_trie_{module_parts_trie},
            ids_trie_{ids_trie}
        {
        }

        bool        has_module_name(const std::vector<std::size_t>& parts);
        std::size_t insert_module_name(const std::vector<std::size_t>& parts);

        std::string get_string(const std::vector<std::size_t>& parts);
    private:
        std::set<std::size_t>      indeces_of_inserted_names_;
        Module_parts_trie_ptr      module_parts_trie_;
        std::shared_ptr<Char_trie> ids_trie_;
    };
};
#endif)~";

static const std::string shift_result_hpp = R"~(/*
    File:    shift_result.hpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SHIFT_RESULT_H
#define SHIFT_RESULT_H
#   include "../include/token.hpp"
#   include "../include/ast_node.hpp"
namespace arkona_parser{
    struct Shift_result{
        Token         token_;
        ast::Node_ptr pnode_;
    };
};
#endif)~";

static const std::string shift_function_hpp = R"~(/*
    File:    shift_function.hpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SHIFT_FUNCTION_H
#define SHIFT_FUNCTION_H
#   include "../include/shift_result.hpp"
#   include "../include/token.hpp"
namespace arkona_parser{
    Shift_result shift_function(const Token& token);
};
#endif)~";


static const std::string token_to_terminal_cpp = R"~(/*
    File:    token_to_terminal.cpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <map>
#include "../include/token_to_terminal.hpp"
namespace arkona_parser{
    using Lexem_code     = arkona_scanner_snd_lvl::Lexem_code;
    using Lexem_kind     = arkona_scanner_snd_lvl::Lexem_kind;
    using Keyword_kind   = arkona_scanner_snd_lvl::Keyword_kind;
    using Delimiter_kind = arkona_scanner_snd_lvl::Delimiter_kind;

    static const std::map<Keyword_kind, Terminal> keyword_to_terminal_mapping = {
        {Keyword_kind::Kw_modul,     Terminal::module},
        {Keyword_kind::Kw_ispolzuet, Terminal::uses  },
    };

    static const std::map<Delimiter_kind, Terminal> delimiter_to_terminal_mapping = {
        {Delimiter_kind::Scope_resol,   Terminal::scope_resolution},
        {Delimiter_kind::Comma,         Terminal::comma           },
        {Delimiter_kind::Fig_br_opened, Terminal::fig_br_opened   },
        {Delimiter_kind::Fig_br_closed, Terminal::fig_br_closed   },
        {Delimiter_kind::Semicolon,     Terminal::semicolon       },
    };

    static Terminal convert_keyword_to_terminal(Keyword_kind kw_kind)
    {
        auto     it     = keyword_to_terminal_mapping.find(kw_kind);
        Terminal result = Terminal::End_of_text;
        if(it != keyword_to_terminal_mapping.end()){
            result = it->second;
        }
        return result;
    }

    static Terminal convert_delimiter_to_terminal(Delimiter_kind del_kind)
    {
        auto     it     = delimiter_to_terminal_mapping.find(del_kind);
        Terminal result = Terminal::End_of_text;
        if(it != delimiter_to_terminal_mapping.end()){
            result = it->second;
        }
        return result;
    }

    Terminal token_to_terminal(const Token& token)
    {
        auto       code = token.lexeme_.code_;
        Lexem_kind kind = code.kind_;
        switch(kind){
            case Lexem_kind::Nothing: case Lexem_kind::UnknownLexem:
                return Terminal::End_of_text;
                break;
            case Lexem_kind::Keyword:
                return convert_keyword_to_terminal(static_cast<Keyword_kind>(code.subkind_));
                break;
            case Lexem_kind::Id:
                return Terminal::id;
                break;
            case Lexem_kind::Delimiter:
                return convert_delimiter_to_terminal(static_cast<Delimiter_kind>(code.subkind_));
                break;
            case Lexem_kind::Char:  case Lexem_kind::String:  case Lexem_kind::Integer:
            case Lexem_kind::Float: case Lexem_kind::Complex: case Lexem_kind::Quat:
                return Terminal::End_of_text;
                break;
        }
        return Terminal::End_of_text;
    }
})~";

static const std::string symbol_table_cpp = R"~(/*
    File:    symbol_table.cpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/symbol_table.hpp"
#include "../../strings-lib/include/join.h"
#include "../../char-conv/include/char_conv.h"

namespace arkona_parser{
    std::size_t Symbol_table::insert_module_name(const std::vector<std::size_t>& parts)
    {
        std::size_t result = module_parts_trie_->insert_vector(parts);
        indeces_of_inserted_names_.insert(result);
        return result;
    }

    bool Symbol_table::has_module_name(const std::vector<std::size_t>& parts)
    {
        std::size_t idx = module_parts_trie_->insert_vector(parts);
        return indeces_of_inserted_names_.count(idx) != 0;
    }

    std::string Symbol_table::get_string(const std::vector<std::size_t>& parts)
    {
        std::string result;
        if(!has_module_name(parts)){
            return result;
        }

        std::vector<std::string> components;
        for(size_t part : parts){
            auto component_as_u32string = ids_trie_->get_string(part);
            auto component              = u32string_to_utf8(component_as_u32string);
            components.push_back(component);
        }

        result = join(components.begin(), components.end(), std::string{"::"});

        return result;
    }
};)~";

static const std::string shift_function_cpp = R"~(/*
    File:    shift_function.cpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/shift_function.hpp"
namespace arkona_parser{
    Shift_result shift_function(const Token& token)
    {
        Shift_result result;
        result.token_ = token;
        result.pnode_ = nullptr;
        return result;
    }
};)~";


static const std::string parser_impl_preamble = R"~(#include "../include/token_to_terminal.hpp"
#include "../include/shift_function.hpp"

void add_module_name(const std::shared_ptr<arkona_parser::Symbol_table>&      symbol_table,
                     const std::shared_ptr<arkona_parser::ast::Qualified_id>& pqual_id,
                     const std::shared_ptr<Error_count>&                      ec)
{
    std::vector<size_t> parts;
    for(const auto& token : pqual_id->name_components_){
        parts.push_back(token.lexeme_.id_info_.id_idx_);
    }

    bool has_qual_id = symbol_table->has_module_name(parts);

    if(has_qual_id){
        auto str = symbol_table->get_string(parts);
        printf("Квалифицированный идентификатор %s, являющийся именем модуля, уже определён.\n",
               str.c_str());
        ec->increment_number_of_errors();
    }else{
        symbol_table->insert_module_name(parts);
    }
})~";


static const std::string additional_ctor_args = " const Errors_and_tries& et, const std::shared_ptr<Symbol_table>& symbol_table";

static const std::vector<std::string> reduce_actions = {
    // Actions for the rule "arkona_module -> модуль qualified_id block":
R"~(
    auto pqual_id   = std::dynamic_pointer_cast<ast::Qualified_id>(current_rule_body_[1].shift_result_.pnode_);
    auto pblck      = std::dynamic_pointer_cast<ast::Block>(current_rule_body_[2].shift_result_.pnode_);

    if(!pqual_id || !pblck){
        return result;
    }

    add_module_name(symbol_table_, pqual_id, en_);

    result.token_ = current_rule_body_[1].shift_result_.token_;
    result.pnode_ = std::make_shared<ast::Module>(pqual_id, pblck, nullptr);
)~",
    // Actions for the rule "arkona_module -> модуль qualified_id imports block":
R"~(
    auto pqual_id   = std::dynamic_pointer_cast<ast::Qualified_id>(current_rule_body_[1].shift_result_.pnode_);
    auto pimports   = std::dynamic_pointer_cast<ast::Imports>(current_rule_body_[2].shift_result_.pnode_);
    auto pblck      = std::dynamic_pointer_cast<ast::Block>(current_rule_body_[3].shift_result_.pnode_);

    if(!pqual_id || !pblck || !pimports){
        return result;
    }

    add_module_name(symbol_table_, pqual_id, en_);

    result.token_ = current_rule_body_[1].shift_result_.token_;
    result.pnode_ = std::make_shared<ast::Module>(pqual_id, pblck, pimports);
)~",
    // Actions for the rule "qualified_id -> ид":
R"~(
        std::list<Token> components;
        auto id_token = current_rule_body_[0].shift_result_.token_;
        components.push_back(id_token);

        result.token_ = id_token;
        result.pnode_ = std::make_shared<ast::Qualified_id>(components);
)~",
    // Actions for the rule "qualified_id -> qualified_id :: ид":
R"~(
        auto pqual_id   = std::dynamic_pointer_cast<ast::Qualified_id>(current_rule_body_[0].shift_result_.pnode_);
        if(!pqual_id){
            return result;
        }

        auto components = pqual_id->name_components_;
        auto id_token   = current_rule_body_[2].shift_result_.token_;
        components.push_back(id_token);

        result.token_ = id_token;
        result.pnode_ = std::make_shared<ast::Qualified_id>(components);
)~",
    // Actions for the rule "imports -> использует qualified_id":
R"~(
        auto pqual_id   = std::dynamic_pointer_cast<ast::Qualified_id>(current_rule_body_[1].shift_result_.pnode_);
        if(!pqual_id){
            return result;
        }

        add_module_name(symbol_table_, pqual_id, en_);

        std::list<std::shared_ptr<arkona_parser::ast::Qualified_id>> imports;
        imports.push_back(pqual_id);

        result.token_ = current_rule_body_[1].shift_result_.token_;
        result.pnode_ = std::make_shared<ast::Imports>(imports);
)~",
    // Actions for the rule "imports -> imports ; использует qualified_id":
R"~(
        auto pqual_id   = std::dynamic_pointer_cast<ast::Qualified_id>(current_rule_body_[2].shift_result_.pnode_);
        if(!pqual_id){
            return result;
        }

        auto pimports   = std::dynamic_pointer_cast<ast::Imports>(current_rule_body_[0].shift_result_.pnode_);
        if(!pimports){
            return result;
        }

        add_module_name(symbol_table_, pqual_id, en_);

        auto imports = pimports->imports_;

        imports.push_back(pqual_id);

        result.token_ = current_rule_body_[2].shift_result_.token_;
        result.pnode_ = std::make_shared<ast::Imports>(imports);
)~",
    // Actions for the rule "block -> { }":
R"~(
        auto blck     = std::make_shared<ast::Block>();
        result.pnode_ = blck;
        result.token_ = current_rule_body_[0].shift_result_.token_;
)~",
};

static const Parser_class_details arkona_parser_details = Parser_class_details{
    .parser_class_name_                          = "Parser",
    .parser_namespace_name_                      = "arkona_parser",
    .scanner_class_name_                         = "Scanner",
    .scanner_namespace_name_                     = "arkona_scanner_snd_lvl",
    .parser_header_preamble_                     = parser_header_preamble,
    .parser_impl_preamble_                       = parser_impl_preamble,
    .token_to_terminal_conversion_function_name_ = "token_to_terminal",
    .parsing_result_type_                        = "Shift_result",
    .shift_function_name_                        = "shift_function",
    .shift_function_result_type_                 = "Shift_result",
    .reduce_actions_                             = reduce_actions,
    .additional_ctor_args_                       = additional_ctor_args,
    .ctor_body_                                  = "",
    .additional_parser_members_                  = additional_parser_members,
    .initializers_for_ctor_                      = initializers_for_ctor,
    .additional_includes_                        = {
        {"token.hpp",             token_hpp            },
        {"ast_node.hpp",          ast_node_hpp         },
        {"token_to_terminal.hpp", token_to_terminal_hpp},
        {"symbol_table.hpp",      symbol_table_hpp     },
        {"shift_result.hpp",      shift_result_hpp     },
        {"shift_function.hpp",    shift_function_hpp   },
    },
    .additional_srcs_                            = {
        {"token_to_terminal.cpp", token_to_terminal_cpp},
        {"symbol_table.cpp",      symbol_table_cpp     },
        {"shift_function.cpp",    shift_function_cpp   },
    },
    .terminal_to_msg_                            = {
        {"module",           "ключевое слово 'модуль'"                   },
        {"uses",             "ключевое слово 'использует'"               },
        {"scope_resolution", "оператор разрешения области видимости '::'"},
        {"comma",            "запятая"                                   },
        {"fig_br_opened",    "открывающая фигурная скобка"               },
        {"fig_br_closed",    "закрывающая фигурная скобка"               },
        {"id",               "идентификатор"                             },
        {"semicolon",        "точка с запятой"                           },
    },
    .token_type_                                 = "Token",
    .postprocessing_                             = "                    result = parser_stack_.top().shift_result_;",
    .user_error_handling_                        = "en_->increment_number_of_errors();"
};

boost::variant<Texts, std::string> build_parser_content()
{
    boost::variant<Texts, std::string> result;

    Parser_info         parser_info;

    parser_info.class_details_ = arkona_parser_details;
    parser_info.grammar_info_  = grammar_info;

    Parser_builder parser_builder{parser_info};

    auto content               = parser_builder();

    Content_processing visitor{parser_builder};

    result                     = boost::apply_visitor(visitor, content);

    return result;
}