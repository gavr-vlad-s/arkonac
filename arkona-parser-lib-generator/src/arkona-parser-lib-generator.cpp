/*
    File:    arkona-parser-lib-generator.cpp
    Created: 31 May 2020 at 09:34 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <string>
#include <cstdio>
#include <map>
#include "../include/arkona-parser-lib-generating-func.hpp"

static const char* usage_message = R"~(arkona-parser-lib-generator
Copyright (c) Gavrilov V.S., 2020
Generating of parsing library for Arkona programming language.
Version 1.0.0.

This program is free sofwtware, and it is licensed under the GPLv3 license.
There is NO warranty, not even MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Usage: arkona-parser-lib-generator [-h|--help|-v|--version|-d directory|--output-dir directory]
Options:
    -h, --help    Print this message
    -v, --version Print message about the version of this program.
    -d directory  Defines output directory for generated parser files.
    --output-dir  Defines output directory for generated parser files.
)~";


static const char* version_message = R"~(arkona-parser-lib-generator
Copyright (c) Gavrilov V.S., 2020
Generating of parsing library for Arkona programming language.
Version 1.0.0.)~";


static const std::map<std::string, std::string> messages_map = {
    {"-h", usage_message  }, {"--help",       usage_message  },
    {"-v", version_message}, {"--version",    version_message},
    {"-d", std::string{}  }, {"--output-dir", std::string{}  }
};

static std::string parse_args(int argc, char* argv[])
{
    std::string result;
    if(argc == 1){
        puts(usage_message);
        return result;
    }
    auto it = messages_map.find(argv[1]);
    if(it == messages_map.end()){
        printf("Unsopported command line option %s\n", argv[1]);
        return result;
    }
    auto message = it->second;
    if(!message.empty()){
        puts(message.c_str());
        return result;
    }
    if(argc != 3){
        puts("Unspecified output directory.");
        return result;
    }
    result = argv[2];
    return result;
}

using parser_generator::slr1::Generated_texts;
using parser_generator::slr1::Error_info;


static int process_parser_content(const boost::variant<Texts, std::string>& content,
                                  const std::string&                        output_dir)
{
    switch(content.which()){
        case 0:
            {
                auto texts       = boost::get<Generated_texts>(content);
                auto include_dir = output_dir + "/include/";
                auto src_dir     = output_dir + "/src/";
                for(const auto& p : texts.include_){
                    const auto& name  = include_dir + p.first;
                    const auto& text  = p.second;
                    FILE*       pfile = fopen(name.c_str(), "w");
                    if(pfile){
                        fputs(text.c_str(), pfile);
                        fclose(pfile);
                    }else{
                        printf("Could not create file %s\n", name.c_str());
                        return 1;
                    }
                }
                for(const auto& p : texts.src_){
                    const auto& name = src_dir + p.first;
                    const auto& text = p.second;
                    FILE*       pfile = fopen(name.c_str(), "w");
                    if(pfile){
                        fputs(text.c_str(), pfile);
                        fclose(pfile);
                    }else{
                        printf("Could not create file %s\n", name.c_str());
                        return 1;
                    }
                }
            }
            break;
        case 1:
            {
                auto about_error = boost::get<std::string>(content);
                puts(about_error.c_str());
                return 1;
            }
            break;
        default:
            return 1;
    }
    return 0;
}

int main(int argc, char* argv[])
{
    auto output_dir = parse_args(argc, argv);
    if(output_dir.empty()){
        return 1;
    }
    auto parser_content = build_parser_content();
    return process_parser_content(parser_content, output_dir);
}