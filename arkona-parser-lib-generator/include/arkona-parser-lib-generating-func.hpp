/*
    File:    arkona-parser-lib-generating-func.hpp
    Created: 30 May 2020 at 13:42 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ARKONA_PARSER_LIB_GENERATING_FUNC_H
#define ARKONA_PARSER_LIB_GENERATING_FUNC_H
#   include <boost/variant.hpp>
#   include "../../parser-generator-lib/include/parser_builder.hpp"
using Texts = parser_generator::slr1::Generated_texts;
boost::variant<Texts, std::string> build_parser_content();
#endif