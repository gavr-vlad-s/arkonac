module KeywordsOffsets(
    constructOffsetInfo
)where
import qualified Data.List as L
import qualified Data.Map as M

strs = ["",
        "0",
        "1368",
        "138",
        "2",
        "26",
        "4",
        "6",
        "8",
        "_",
        "_е",
        "а",
        "аво",
        "ае",
        "аео",
        "аеорсу",
        "аиртс",
        "ау",
        "б",
        "бдй",
        "в",
        "взс",
        "вкрс",
        "вс",
        "г",
        "гж",
        "д",
        "е",
        "ео",
        "еоы",
        "ж",
        "з",
        "знс",
        "и",
        "ио",
        "й",
        "к",
        "кл",
        "кт",
        "ку",
        "л",
        "лс",
        "м",
        "мн",
        "мч",
        "н",
        "нт",
        "о",
        "оу",
        "оф",
        "оя",
        "п",
        "пс",
        "пт",
        "р",
        "с",
        "т",
        "у",
        "ф",
        "ц",
        "ч",
        "чщ",
        "ш",
        "ы",
        "ь",
        "я"]


translitMap :: M.Map Char String
translitMap = M.fromList [
    ('а', "cyra"),  ('б', "cyrb"),     ('в', "cyrv"),    ('г', "cyrg"),
    ('д', "cyrd"),  ('е', "cyrje"),    ('ё', "cyryo"),   ('ж', "cyrzh"),
    ('з', "cyrz"),  ('и', "cyri"),     ('й', "cyrj"),    ('к', "cyrk"),
    ('л', "cyrl"),  ('м', "cyrm"),     ('н', "cyrn"),    ('о', "cyro"),
    ('п', "cyrp"),  ('р', "cyrr"),     ('с', "cyrs"),    ('т', "cyrt"),
    ('у', "cyru"),  ('ф', "cyrf"),     ('х', "cyrkh"),   ('ц', "cyrts"),
    ('ч', "cyrch"), ('ш', "cyrsh"),    ('щ', "cyrshch"), ('ъ', "cyrhsign"),
    ('ы', "cyry"),  ('ь', "cyrssign"), ('э', "cyre"),    ('ю', "cyryu"),
    ('я', "cyrya"),

    ('А', "Cyra"),  ('Б', "Cyrb"),     ('В', "Cyrv"),    ('Г', "Cyrg"),
    ('Д', "Cyrd"),  ('Е', "Cyrje"),    ('Ё', "Cyryo"),   ('Ж', "Cyrzh"),
    ('З', "Cyrz"),  ('И', "Cyri"),     ('Й', "Cyrj"),    ('К', "Cyrk"),
    ('Л', "Cyrl"),  ('М', "Cyrm"),     ('Н', "Cyrn"),    ('О', "Cyro"),
    ('П', "Cyrp"),  ('Р', "Cyrr"),     ('С', "Cyrs"),    ('Т', "Cyrt"),
    ('У', "Cyru"),  ('Ф', "Cyrf"),     ('Х', "Cyrkh"),   ('Ц', "Cyrts"),
    ('Ч', "Cyrch"), ('Ш', "Cyrsh"),    ('Щ', "Cyrshch"), ('Ъ', "Cyrhsign"),
    ('Ы', "Cyry"),  ('Ь', "Cyrssign"), ('Э', "Cyre"),    ('Ю', "Cyryu"),
    ('Я', "Cyrya"),

    ('a', "a"),     ('b', "b"),        ('c', "c"),       ('d', "d"),
    ('e', "e"),     ('f', "f"),        ('g', "g"),       ('h', "h"),
    ('i', "i"),     ('j', "j"),        ('k', "k"),       ('l', "l"),
    ('m', "m"),     ('n', "n"),        ('o', "o"),       ('p', "p"),
    ('q', "q"),     ('r', "r"),        ('s', "s"),       ('t', "t"),
    ('u', "u"),     ('v', "v"),        ('w', "w"),       ('x', "x"),
    ('y', "y"),     ('z', "z"),

    ('_', "und"),

    ('A', "A"),     ('B', "B"),        ('C', "C"),       ('D', "D"),
    ('E', "E"),     ('F', "F"),        ('G', "G"),       ('H', "H"),
    ('I', "I"),     ('J', "J"),        ('K', "K"),       ('L', "L"),
    ('M', "M"),     ('N', "N"),        ('O', "O"),       ('P', "P"),
    ('Q', "Q"),     ('R', "R"),        ('S', "S"),       ('T', "T"),
    ('U', "U"),     ('V', "V"),        ('W', "W"),       ('X', "X"),
    ('Y', "Y"),     ('Z', "Z"),

    ('0', "0"),     ('1', "1"),        ('2', "2"),       ('3', "3"),
    ('4', "4"),     ('5', "5"),        ('6', "6"),       ('7', "7"),
    ('8', "8"),     ('9', "9")]



errorMsg :: String
errorMsg = "Expected Latin letter, Russian letter, decimal digit, or _"


translit :: String -> String
translit [] = "Empty"
translit s  = concat ["Str_", L.intercalate "_" . map (\c -> translitChar c) $ s]
    where
        translitChar c = case M.lookup c translitMap of
                              Just s  -> s
                              Nothing -> error errorMsg



data OffsetInfo = OffsetInfo{enumElems :: [String],
                             transStrings :: [String]} deriving(Eq, Ord)


indent = replicate 8 ' '


instance Show OffsetInfo where
    show oi = concat ["    enum class Offsets : uint16_t{\n",
                      showEnumElems oi,
                      "\n    };\n\n",
                      "    static const trans_strs[] = {\n",
                      showTransStrs oi,
                      "\n  };"]
        where
            showEnumElems oi =  L.intercalate ",\n"     .
                                map (\s -> indent ++ s) .
                                enumElems               $ oi
            showTransStrs oi = L.intercalate ",\n" . transStrings $ oi


emptyInfo :: OffsetInfo
emptyInfo = OffsetInfo{enumElems = [], transStrings = []}

transStringAsSequence :: String -> String
transStringAsSequence s = indent ++ transStringAsSequence' s
    where
        transStringAsSequence' = L.intercalate ", " . transStringAsSequence''
        transStringAsSequence'' s = map (\c -> ['U', '\'', c, '\'']) s ++ ["U\'\\0\'"]

constructOffsetInfo :: OffsetInfo
constructOffsetInfo = OffsetInfo{enumElems    = reverse . enumElems    $ auxOffsetInfo,
                                 transStrings = reverse . transStrings $ auxOffsetInfo}
    where
        auxOffsetInfo = fst . L.foldl' (\(oi, n) s -> f s oi n) (emptyInfo, 0) $ strs
        f s oi n = (OffsetInfo{enumElems    = (translit s ++ " = " ++ show n):(enumElems oi),
                               transStrings = (transStringAsSequence s):(transStrings oi)},
                    n + length s + 1)
