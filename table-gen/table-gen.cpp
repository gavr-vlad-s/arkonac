/*
    Файл:    table-gen.cpp
    Создано: 26 February 2019 at 10:56 Moscow time
    Автор:   Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#include <cstdlib>
#include <cstdio>
#include <string>
#include <map>
#include <utility>
#include <sstream>
#include <iomanip>
#include "char_conv.h"
#include "map_as_vector.h"
#include "segment.h"
#include "group_pairs.h"
#include "create_permutation_tree.h"
#include "permutation_tree_to_permutation.h"
#include "permutation.h"
#include "create_permutation.h"
#include "myconcepts.h"
#include "list_to_columns.h"
#include "format.h"

static std::map<char32_t, uint32_t> table;

enum class Category : uint32_t{
    Spaces,          Other,            Id_begin,
    Id_body,         Keyword_begin,    Delimiter_begin,
    Double_quote,    Letters_Xx,       Letters_Bb,
    Letters_Oo,      Single_quote,     Dollar,
    Hex_digit,       Oct_digit,        Bin_digit,
    Dec_digit,       Zero,             Letters_Ee,
    Plus_minus,      Precision_letter, Digits_1_to_9,
    Point,           Quat_suffix
};

struct Category_info{
    std::u32string chars_;
    Category       cat_;
};

static std::u32string spaces_str()
{
    std::u32string s;
    for(char32_t c = 1; c <= U' '; c++){
        s += c;
    }
    return s;
}

static const char32_t* id_begin_chars          =
    U"_ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    U"ЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуф"
    U"хцчшщъыьэюяѐёђѓєѕіїјљњћќўџѠѡѢѣѤѥѦѧѨѩѪѫѬѭѮѯѰѱѲѳѴѵѶѷѸѹѺѻѼѽѾѿҀҁҊҋҌҍҎҏҐґ"
    U"ҒғҔҕҖҗҘҙҚқҜҝҞҟҠҡҢңҤҥҦҧҨҩҪҫҬҭҮүҰұҲҳҴҵҶҷҸҹҺһҼҽҾҿӀӁӂӀӃӄӅӆӇӈӉӊӋӌӍӎӏӐӑӒӓӔӕ"
    U"ӖӗӘәӚӛӜӝӞӟӠӡӢӣӤӥӦӧӨөӪӫӬӭӮӯӰӱӲӳӴӵӶӷӸӹӺӻӼӽӾӿ";
static const char32_t* id_body_chars           =
    U"_ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    U"ЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуф"
    U"хцчшщъыьэюяѐёђѓєѕіїјљњћќўџѠѡѢѣѤѥѦѧѨѩѪѫѬѭѮѯѰѱѲѳѴѵѶѷѸѹѺѻѼѽѾѿҀҁҊҋҌҍҎҏҐґ"
    U"ҒғҔҕҖҗҘҙҚқҜҝҞҟҠҡҢңҤҥҦҧҨҩҪҫҬҭҮүҰұҲҳҴҵҶҷҸҹҺһҼҽҾҿӀӁӂӀӃӄӅӆӇӈӉӊӋӌӍӎӏӐӑӒӓӔӕ"
    U"ӖӗӘәӚӛӜӝӞӟӠӡӢӣӤӥӦӧӨөӪӫӬӭӮӯӰӱӲӳӴӵӶӷӸӹӺӻӼӽӾӿ";
static const char32_t* keyword_begin_chars     =
    U"бвгдезиклмнопрстфцчшэ";
static const char32_t* delim_begin_chars       =
    U"[]{}()!~^@.:;,=#<>+-*/%?|&\\";
static const char32_t* double_quote_chars      =
    U"\"";
static const char32_t* single_quote_chars      =
    U"\'";
static const char32_t* letters_Xx_chars        =
    U"Xx";
static const char32_t* letters_Bb_chars        =
    U"Bb";
static const char32_t* letters_Oo_chars        =
    U"Oo";
static const char32_t* dollar_chars            =
    U"$";
static const char32_t* hex_digits_chars        =
    U"0123456789ABCDEFabcdef";
static const char32_t* oct_digits_chars        =
    U"01234567";
static const char32_t* bin_digits_chars        =
    U"01";
static const char32_t* dec_digits_chars        =
    U"0123456789";
static const char32_t* zero_chars              =
    U"0";
static const char32_t* letters_Ee_chars        =
    U"Ee";
static const char32_t* plus_minus_chars        =
    U"+-";
static const char32_t* precision_letters_chars =
    U"fdxq";
static const char32_t* digits_1_to_9_chars     =
    U"123456789";
static const char32_t* point_chars             =
    U".";
static const char32_t* quat_suffix_chars       =
    U"ijk";

static const Category_info categories[] = {
    {spaces_str(),            Category::Spaces          },
    {id_begin_chars,          Category::Id_begin        },
    {id_body_chars,           Category::Id_body         },
    {keyword_begin_chars,     Category::Keyword_begin   },
    {delim_begin_chars,       Category::Delimiter_begin },
    {double_quote_chars,      Category::Double_quote    },
    {single_quote_chars,      Category::Single_quote    },
    {letters_Xx_chars,        Category::Letters_Xx      },
    {letters_Bb_chars,        Category::Letters_Bb      },
    {letters_Oo_chars,        Category::Letters_Oo      },
    {dollar_chars,            Category::Dollar          },
    {hex_digits_chars,        Category::Hex_digit       },
    {oct_digits_chars,        Category::Oct_digit       },
    {bin_digits_chars,        Category::Bin_digit       },
    {dec_digits_chars,        Category::Dec_digit       },
    {zero_chars,              Category::Zero            },
    {letters_Ee_chars,        Category::Letters_Ee      },
    {plus_minus_chars,        Category::Plus_minus      },
    {precision_letters_chars, Category::Precision_letter},
    {digits_1_to_9_chars,     Category::Digits_1_to_9   },
    {point_chars,             Category::Point           },
    {quat_suffix_chars,       Category::Quat_suffix     }
};

static void insert_char(const char32_t ch, Category category)
{
    auto it = table.find(ch);
    if(it != table.end()){
        table[ch] |= 1U << static_cast<uint32_t>(category);
    }else{
        table[ch] =  1U << static_cast<uint32_t>(category);
    }
}

static void add_category(const char32_t* p, Category category)
{
    while(char32_t ch = *p++){
        insert_char(ch, category);
    }
}

static inline void add_category(const Category_info& ci)
{
    add_category(ci.chars_.c_str(), ci.cat_);
}

void fill_table(){
    for(const auto& ci : categories){
        add_category(ci);
    }
}

template<RandomAccessIterator DestIt, RandomAccessIterator SrcIt, Callable F>
void permutate(DestIt dest_begin, SrcIt src_begin, SrcIt src_end, F f){
    size_t num_of_elems = src_end - src_begin;
    for(size_t i = 0; i < num_of_elems; ++i){
        dest_begin[f(i)] = src_begin[i];
    }
}

template<Integral K, typename V>
SegmentsV<K, V> create_classification_table(const std::map<K, V>& m){
   SegmentsV<K,V> grouped_pairs = group_pairs(map_as_vector(m));
   size_t         n             = grouped_pairs.size();
   auto           result        = SegmentsV<K, V>(n);
   auto           perm          = create_permutation(grouped_pairs.size());
   auto           f             = [&perm](size_t i) -> size_t{return perm[i];};
   permutate(result.begin(), grouped_pairs.begin(), grouped_pairs.end(), f);
   return result;
}


static const std::map<char32_t, std::string> esc_char_strings = {
    {U'\?',   R"~(U'\?')~"  }, {U'\\',   R"~(U'\\')~"  }, {U'\0',   R"~(U'\0')~"  },
    {U'\a',   R"~(U'\a')~"  }, {U'\b',   R"~(U'\b')~"  }, {U'\f',   R"~(U'\f')~"  },
    {U'\n',   R"~(U'\n')~"  }, {U'\r',   R"~(U'\r')~"  }, {U'\t',   R"~(U'\t')~"  },
    {U'\v',   R"~(U'\v')~"  }, {U'\x01', R"~(U'\x01')~"}, {U'\x02', R"~(U'\x02')~"},
    {U'\x03', R"~(U'\x03')~"}, {U'\x04', R"~(U'\x04')~"}, {U'\x05', R"~(U'\x05')~"},
    {U'\x06', R"~(U'\x06')~"}, {U'\x0e', R"~(U'\x0e')~"}, {U'\x0f', R"~(U'\x0f')~"},
    {U'\x10', R"~(U'\x10')~"}, {U'\x11', R"~(U'\x11')~"}, {U'\x12', R"~(U'\x12')~"},
    {U'\x13', R"~(U'\x13')~"}, {U'\x14', R"~(U'\x14')~"}, {U'\x15', R"~(U'\x15')~"},
    {U'\x16', R"~(U'\x16')~"}, {U'\x17', R"~(U'\x17')~"}, {U'\x18', R"~(U'\x18')~"},
    {U'\x19', R"~(U'\x19')~"}, {U'\x1a', R"~(U'\x1a')~"}, {U'\x1b', R"~(U'\x1b')~"},
    {U'\x1c', R"~(U'\x1c')~"}, {U'\x1d', R"~(U'\x1d')~"}, {U'\x1e', R"~(U'\x1e')~"},
    {U'\x1f', R"~(U'\x1f')~"}, {U'\'',   R"~(U'\'')~"  }, {U'\"',   R"~(U'\"')~"  }
};

static std::string show_char32(const char32_t c)
{
    std::string result;
    auto it = esc_char_strings.find(c);
    if(it != esc_char_strings.end()){
        result = it->second;
    }else{
        result = "U\'"  + char32_to_utf8(c) + "\'";
    }
    return result;
}

static const std::string elem_fmt = R"~({{{{{0}, {1}}},  {2:<10}}})~";

static std::string show_table_elem(const Segment_with_value<char32_t, uint32_t>& e)
{
    std::string result;
    auto&       segment = e.bounds;
    result              = fmt::format(elem_fmt,
                                      show_char32(segment.lower_bound),
                                      show_char32(segment.upper_bound),
                                      e.value);
    return result;
}

static const std::string table_fmt =
R"~(static const Segment_with_value<char32_t, uint64_t> categories_table[] = {{
{0}
}};

static constexpr size_t num_of_elems_in_categories_table = {1};

uint64_t get_categories_set(char32_t c)
{{
    auto t = knuth_find(categories_table,
                        categories_table + num_of_elems_in_categories_table,
                        c);
    return t.first ?
           categories_table[t.second].value :
           (1ULL << static_cast<uint64_t>(Category::Other));
}}
)~";

std::string show_table(){
    std::string result;
    auto   t                 = create_classification_table(table);
    size_t num_of_elems      = t.size();

    std::vector<std::string> elems;
    for(const auto& e : t){
        elems.push_back(show_table_elem(e));
    }

    Format f;
    f.indent                 = 4;
    f.number_of_columns      = 3;
    f.spaces_between_columns = 2;

    auto table_body          =  string_list_to_columns(elems, f);
    result                   =  fmt::format(table_fmt, table_body, num_of_elems);
    return result;
}

int main()
{
    fill_table();
    auto table_as_string = show_table();
    puts(table_as_string.c_str());
    return 0;
}