/*
    File:    lexeme_fab.cpp
    Created: 28 January 2020 at 07:27 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/lexeme_fab.h"

namespace recursive_parser_testing{
    const Lexeme unknown_lexeme()
    {
        Lexeme li;
        li.code_.kind_    = Lexem_kind::UnknownLexem;
        li.code_.subkind_ = 0;
        return li;
    }

    const Lexeme nothing_lexeme()
    {
        Lexeme li;
        li.code_.kind_    = Lexem_kind::Nothing;
        li.code_.subkind_ = 0;
        return li;
    }


    //! Function that return keywords.
    const Lexeme keyword_lexeme(Keyword_kind kw)
    {
        Lexeme li;
        li.code_.kind_    = Lexem_kind::Keyword;
        li.code_.subkind_ = kw;
        return li;
    }

    //! Function that return identifier.
    const Lexeme id_lexeme(std::size_t idx, std::size_t prefix)
    {
        Lexeme li;
        li.code_.kind_                 = Lexem_kind::Id;
        li.code_.subkind_              = 0;
        li.id_info_.qualifying_prefix_ = prefix;
        li.id_info_.id_idx_            = idx;
        return li;
    }

    //! Function that return character lexeme.
    const Lexeme char_lexeme(char32_t c, Char_kind char_kind)
    {
        Lexeme li;
        li.code_.kind_    = Lexem_kind::Char;
        li.code_.subkind_ = static_cast<uint8_t>(char_kind);
        li.char_val_      = c;
        return li;
    }

    //! Function that return string lexeme.
    const Lexeme string_lexeme(size_t str_idx, String_kind str_kind)
    {
        Lexeme li;
        li.code_.kind_    = Lexem_kind::String;
        li.code_.subkind_ = static_cast<uint8_t>(str_kind);
        li.str_index_     = str_idx;
        return li;
    }

    //! Function that return integer lexeme.
    const Lexeme integer_lexeme(unsigned __int128 int_val)
    {
        Lexeme li;
        li.code_.kind_    = Lexem_kind::Integer;
        li.code_.subkind_ = 0;
        li.int_val_       = int_val;
        return li;
    }

    //! Function that return float lexeme.
    const Lexeme float_lexeme(__float128 float_val, Float_kind precision)
    {
        Lexeme li;
        li.code_.kind_    = Lexem_kind::Float;
        li.code_.subkind_ = static_cast<uint8_t>(precision);
        li.float_val_     = float_val;
        return li;
    }

    //! Function that return complex lexeme.
    const Lexeme complex_lexeme(__complex128 complex_val, Complex_kind precision)
    {
        Lexeme li;
        li.code_.kind_    = Lexem_kind::Complex;
        li.code_.subkind_ = static_cast<uint8_t>(precision);
        li.complex_val_   = complex_val;
        return li;
    }

    //! Function that return quaternion lexeme.
    const Lexeme quat_lexeme(const quat128& quat_val, Quat_kind precision)
    {
        Lexeme li;
        li.code_.kind_    = Lexem_kind::Quat;
        li.code_.subkind_ = static_cast<uint8_t>(precision);
        li.quat_val_      = quat_val;
        return li;
    }

    //! Function that return delimiter lexeme.
    const Lexeme delim_lexeme(Delimiter_kind delim, std::size_t prefix)
    {
        Lexeme li;
        li.code_.kind_                    = Lexem_kind::Delimiter;
        li.delim_info_.delim_kind_        = delim;
        li.delim_info_.qualifying_prefix_ = prefix;
        return li;
    }
};