/*
    File:    token-classification-testing.cpp
    Created: 06 December 2020 at 17:22 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstdio>
#include <iterator>
#include <utility>
#include "../include/token-classification-testing.hpp"
#include "../../recursive-parser-lib/include/token_to_terminal_set.hpp"
#include "../../recursive-parser-lib/include/terminal_enum.hpp"
#include "../include/lexeme_fab.h"
#include "../../other/include/testing.hpp"

namespace recursive_parser_testing{
    using Pair_for_test     = std::pair<arkona_parser::Token, arkona_parser::Terminal_set>;
    using Terminal_category = arkona_parser::Terminal_category;

    static const Pair_for_test tests[] = {
        {
            {{{17, 21}, {17, 27}}, keyword_lexeme(Keyword_kind::Kw_bezzn16)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{10, 18}, {10, 23}}, keyword_lexeme(Keyword_kind::Kw_bezzn8)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{18, 33}, {18, 40}}, keyword_lexeme(Keyword_kind::Kw_bezzn128)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{144,5  }, {144,12 }}, keyword_lexeme(Keyword_kind::Kw_stroka32)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{18, 5  }, {18, 11 }}, keyword_lexeme(Keyword_kind::Kw_bezzn32)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{18, 17 }, {18, 23 }}, keyword_lexeme(Keyword_kind::Kw_bezzn64)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{42, 35 }, {42, 40 }}, keyword_lexeme(Keyword_kind::Kw_veshch128)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{92, 37 }, {92, 41 }}, keyword_lexeme(Keyword_kind::Kw_log64)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{74, 63 }, {74, 69 }}, keyword_lexeme(Keyword_kind::Kw_kompl32)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{162,32 }, {162,36 }}, keyword_lexeme(Keyword_kind::Kw_tsel64)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{162,21 }, {162,25 }}, keyword_lexeme(Keyword_kind::Kw_tsel16)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{161,44 }, {161,47 }}, keyword_lexeme(Keyword_kind::Kw_tsel8)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{162,50 }, {162,54 }}, keyword_lexeme(Keyword_kind::Kw_tsel32)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{161,13 }, {161,18 }}, keyword_lexeme(Keyword_kind::Kw_tsel128)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{124,30 }, {124,38 }}, keyword_lexeme(Keyword_kind::Kw_poryadok64)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{42, 46 }, {42, 50 }}, keyword_lexeme(Keyword_kind::Kw_veshch32)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{47, 46 }, {47, 50 }}, keyword_lexeme(Keyword_kind::Kw_veshch64)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{710, 36 }, {710, 40 }}, keyword_lexeme(Keyword_kind::Kw_veshch80)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{72, 35 }, {72, 40 }}, keyword_lexeme(Keyword_kind::Kw_kvat32)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{52, 35 }, {52, 40 }}, keyword_lexeme(Keyword_kind::Kw_kvat80)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{12, 35 }, {12, 40 }}, keyword_lexeme(Keyword_kind::Kw_kvat64)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{76, 17 }, {76, 23 }}, keyword_lexeme(Keyword_kind::Kw_kvat128)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{74, 27 }, {74, 34 }}, keyword_lexeme(Keyword_kind::Kw_kompl128)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{72, 16 }, {72, 22 }}, keyword_lexeme(Keyword_kind::Kw_kompl64)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{2, 6 }, {2, 12 }}, keyword_lexeme(Keyword_kind::Kw_kompl80)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{94, 59 }, {94, 63 }}, keyword_lexeme(Keyword_kind::Kw_log16)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{94, 13 }, {94, 16 }}, keyword_lexeme(Keyword_kind::Kw_log8)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{94, 44 }, {94, 48 }}, keyword_lexeme(Keyword_kind::Kw_log32)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{124,53 }, {124,61 }}, keyword_lexeme(Keyword_kind::Kw_poryadok16)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{124,42 }, {124,49 }}, keyword_lexeme(Keyword_kind::Kw_poryadok8)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{124,67 }, {124,75 }}, keyword_lexeme(Keyword_kind::Kw_poryadok32)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{148,22 }, {148,27 }}, keyword_lexeme(Keyword_kind::Kw_simv32)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{148,42 }, {148,46 }}, keyword_lexeme(Keyword_kind::Kw_simv8)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{150,17 }, {150,22 }}, keyword_lexeme(Keyword_kind::Kw_simv16)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{144,59 }, {144,66 }}, keyword_lexeme(Keyword_kind::Kw_stroka16)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{144,20 }, {144,26 }}, keyword_lexeme(Keyword_kind::Kw_stroka8)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{67, 7  }, {67, 10 }}, keyword_lexeme(Keyword_kind::Kw_ines)},
            {Terminal_category::Elif}
        },
        {
            {{{20, 13 }, {20, 19 }}, keyword_lexeme(Keyword_kind::Kw_bolshoe)},
            {Terminal_category::Type_size_modifier, Terminal_category::Block_entry_begin}
        },
        {
            {{{156,34 }, {156,36 }}, keyword_lexeme(Keyword_kind::Kw_tsel)},
            {Terminal_category::Sizeable_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{105,13 }, {105,18 }}, keyword_lexeme(Keyword_kind::Kw_menshe)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{132,18 }, {132,22 }}, keyword_lexeme(Keyword_kind::Kw_ravno)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{111,21 }, {111,25 }}, keyword_lexeme(Keyword_kind::Kw_nichto)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{70, 44 }, {70, 48 }}, keyword_lexeme(Keyword_kind::Kw_kompl)},
            {Terminal_category::Sizeable_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{40, 41 }, {40, 43 }}, keyword_lexeme(Keyword_kind::Kw_veshch)},
            {Terminal_category::Sizeable_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{16,9  }, {16,13 }}, keyword_lexeme(Keyword_kind::Kw_bezzn)},
            {Terminal_category::Sizeable_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{72, 5  }, {72, 8  }}, keyword_lexeme(Keyword_kind::Kw_kvat)},
            {Terminal_category::Sizeable_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{123,65 }, {123,71 }}, keyword_lexeme(Keyword_kind::Kw_poryadok)},
            {Terminal_category::Sizeable_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{90, 20 }, {90, 22 }}, keyword_lexeme(Keyword_kind::Kw_log)},
            {Terminal_category::Sizeable_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{148,9  }, {148,12 }}, keyword_lexeme(Keyword_kind::Kw_simv)},
            {Terminal_category::Sizeable_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{144,42 }, {144,47 }}, keyword_lexeme(Keyword_kind::Kw_stroka)},
            {Terminal_category::Sizeable_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{84, 32 }, {84, 37 }}, keyword_lexeme(Keyword_kind::Kw_istina)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{23, 9  }, {23, 14 }}, keyword_lexeme(Keyword_kind::Kw_bolshe)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        }
    };

    static const Pair_for_test tests1[] = {
        {
            {{{23, 9  }, {23, 12 }}, keyword_lexeme(Keyword_kind::Kw_lozh)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin},
        },
        {
            {{{98, 21 }, {98, 29 }}, keyword_lexeme(Keyword_kind::Kw_malenkoe)},
            {Terminal_category::Type_size_modifier, Terminal_category::Block_entry_begin}
        },
        {
            {{{129,48 }, {129,52 }}, keyword_lexeme(Keyword_kind::Kw_pusto)},
            {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin}
        },
        {
            {{{40, 48 }, {40, 52 }}, keyword_lexeme(Keyword_kind::Kw_vechno)},
            {Terminal_category::Forever, Terminal_category::Block_entry_begin}
        },
        {
            {{{46, 17 }, {46, 17 }}, keyword_lexeme(Keyword_kind::Kw_v)},
            {Terminal_category::In}
        },
        {
            {{{51, 5  }, {51, 11 }}, keyword_lexeme(Keyword_kind::Kw_vozvrat)},
            {Terminal_category::Return, Terminal_category::Block_entry_begin}
        },
        {
            {{{47, 34 }, {47, 39 }}, keyword_lexeme(Keyword_kind::Kw_vyberi)},
            {Terminal_category::Switch, Terminal_category::Block_entry_begin}
        },
        {
            {{{141,40 }, {141,48 }}, keyword_lexeme(Keyword_kind::Kw_struktura)},
            {Terminal_category::Struct, Terminal_category::Block_entry_begin}
        },
        {
            {{{141,40 }, {141,43 }}, keyword_lexeme(Keyword_kind::Kw_esli)},
            {Terminal_category::If, Terminal_category::Block_entry_begin}
        },
        {
            {{{141,40 }, {141,45 }}, keyword_lexeme(Keyword_kind::Kw_vydeli)},
            {Terminal_category::New, Terminal_category::Block_entry_begin}
        },
        {
            {{{116,45 }, {116,52 }}, keyword_lexeme(Keyword_kind::Kw_osvobodi)},
            {Terminal_category::Delete, Terminal_category::Block_entry_begin}
        },
        {
            {{{121,47 }, {121,54 }}, keyword_lexeme(Keyword_kind::Kw_povtoryaj)},
            {Terminal_category::Do, Terminal_category::Block_entry_begin}
        },
        {
            {{{121,47 }, {121,48 }}, keyword_lexeme(Keyword_kind::Kw_to)},
            {Terminal_category::Then}
        },
        {
            {{{67, 12 }, {67, 16 }}, keyword_lexeme(Keyword_kind::Kw_inache)},
            {Terminal_category::Else}
        },
        {
            {{{54, 9  }, {54, 16 }}, keyword_lexeme(Keyword_kind::Kw_golovnaya)},
            {Terminal_category::Main, Terminal_category::Block_entry_begin}
        },
        {
            {{{54, 9  }, {54, 11 }}, keyword_lexeme(Keyword_kind::Kw_dlya)},
            {Terminal_category::For, Terminal_category::Block_entry_begin}
        },
        {
            {{{105,52 }, {105,57 }}, keyword_lexeme(Keyword_kind::Kw_modul)},
            {Terminal_category::Module}
        },
        {
            {{{105,52 }, {105,55 }}, keyword_lexeme(Keyword_kind::Kw_pauk)},
            {Terminal_category::Dijkstra_loop, Terminal_category::Block_entry_begin}
        },
        {
            {{{105,52 }, {105,53 }}, keyword_lexeme(Keyword_kind::Kw_iz)},
            {Terminal_category::From}
        },
        {
            {{{167,73 }, {167,79 }}, keyword_lexeme(Keyword_kind::Kw_eksport)},
            {Terminal_category::Export, Terminal_category::Block_entry_begin}
        },
        {
            {{{134,9  }, {134,14 }}, keyword_lexeme(Keyword_kind::Kw_razbor)},
            {Terminal_category::Match, Terminal_category::Block_entry_begin}
        },
        {
            {{{134,9  }, {134,12 }}, keyword_lexeme(Keyword_kind::Kw_poka)},
            {Terminal_category::While, Terminal_category::Block_entry_begin}
        },
        {
            {{{32, 9  }, {32, 13 }}, keyword_lexeme(Keyword_kind::Kw_vyjdi)},
            {Terminal_category::Exit, Terminal_category::Block_entry_begin}
        },
        {
            {{{32, 9  }, {32, 14 }}, keyword_lexeme(Keyword_kind::Kw_pokuda)},
            {Terminal_category::As_long_as}
        },
        {
            {{{32, 9  }, {32, 11 }}, keyword_lexeme(Keyword_kind::Kw_kak)},
            {Terminal_category::As}
        },
        {
            {{{134,19 }, {134,30 }}, keyword_lexeme(Keyword_kind::Kw_rassmatrivaj)},
            {Terminal_category::Interpret, Terminal_category::Block_entry_begin}
        },
        {
            {{{116,13 }, {116,20 }}, keyword_lexeme(Keyword_kind::Kw_operaciya)},
            {Terminal_category::Operation, Terminal_category::Block_entry_begin}
        },
        {
            {{{116,13 }, {116,24 }}, keyword_lexeme(Keyword_kind::Kw_perechislenie)},
            {Terminal_category::Enum, Terminal_category::Block_entry_begin}
        },
        {
            {{{126,26 }, {126,35 }}, keyword_lexeme(Keyword_kind::Kw_preobrazuj)},
            {Terminal_category::Convert, Terminal_category::Block_entry_begin}
        },
        {
            {{{126,26 }, {126,28 }}, keyword_lexeme(Keyword_kind::Kw_tip)},
            {Terminal_category::Type, Terminal_category::Block_entry_begin}
        },
        {
            {{{98, 38 }, {98, 43 }}, keyword_lexeme(Keyword_kind::Kw_massiv)},
            {Terminal_category::Array, Terminal_category::Block_entry_begin}
        },
        {
            {{{98, 38 }, {98, 41 }}, keyword_lexeme(Keyword_kind::Kw_samo)},
            {Terminal_category::Auto, Terminal_category::Block_entry_begin}
        },
        {
            {{{98, 38 }, {98, 45 }}, keyword_lexeme(Keyword_kind::Kw_sravni_s)},
            {Terminal_category::Compare}
        },
        {
            {{{98, 38 }, {98, 50 }}, keyword_lexeme(Keyword_kind::Kw_perech_mnozh)},
            {Terminal_category::Enum_set, Terminal_category::Block_entry_begin}
        },
        {
            {{{164,5  }, {164,9  }}, keyword_lexeme(Keyword_kind::Kw_shkala)},
            {Terminal_category::Shkala, Terminal_category::Block_entry_begin}
        },
        {
            {{{139,41 }, {139,46 }}, keyword_lexeme(Keyword_kind::Kw_ssylka)},
            {Terminal_category::Reference, Terminal_category::Block_entry_begin}
        },
        {
            {{{154,15 }, {154,21 }}, keyword_lexeme(Keyword_kind::Kw_funktsiya)},
            {Terminal_category::Function, Terminal_category::Block_entry_begin}
        },
        {
            {{{82, 29 }, {82, 38 }}, keyword_lexeme(Keyword_kind::Kw_ispolzuet)},
            {Terminal_category::Uses}
        },
        {
            {{{164,16 }, {164,20 }}, keyword_lexeme(Keyword_kind::Kw_shkalu)},
            {Terminal_category::Shkalu}
        },
        {
            {{{164,16 }, {164,21 }}, keyword_lexeme(Keyword_kind::Kw_lyambda)},
            {Terminal_category::Lambda, Terminal_category::Block_entry_begin}
        },
        {
            {{{167,31 }, {167,37 }}, keyword_lexeme(Keyword_kind::Kw_element)},
            {Terminal_category::Belongs}
        },
        {
            {{{167,31 }, {167,35 }}, keyword_lexeme(Keyword_kind::Kw_konst)},
            {Terminal_category::Const, Terminal_category::Block_entry_begin}
        },
        {
            {{{1670,11 }, {1670,15 }}, keyword_lexeme(Keyword_kind::Kw_perem)},
            {Terminal_category::Var, Terminal_category::Block_entry_begin}
        },
        {
            {{{129,13 }, {129,21 }}, keyword_lexeme(Keyword_kind::Kw_psevdonim)},
            {Terminal_category::Alias, Terminal_category::Block_entry_begin}
        },
        {
            {{{162,70 }, {162,75 }}, keyword_lexeme(Keyword_kind::Kw_chistaya)},
            {Terminal_category::Pure, Terminal_category::Block_entry_begin}
        },
        {
            {{{162,70 }, {162,74 }}, keyword_lexeme(Keyword_kind::Kw_ekviv)},
            {Terminal_category::Type_equivalence}
        },
        {
            {{{78, 3  }, {78, 11 }}, keyword_lexeme(Keyword_kind::Kw_kategorija)},
            {Terminal_category::Type_trait, Terminal_category::Block_entry_begin}
        },
        {
            {{{78, 3  }, {78, 6 }}, keyword_lexeme(Keyword_kind::Kw_meta)},
            {Terminal_category::Meta, Terminal_category::Block_entry_begin}
        },
        {
            {{{126,9  }, {126,19 }}, keyword_lexeme(Keyword_kind::Kw_postfiksnaya)},
            {Terminal_category::Postfix_or_prefix, Terminal_category::Block_entry_begin}
        },
        {
            {{{126,9  }, {126,13 }}, keyword_lexeme(Keyword_kind::Kw_paket)},
            {Terminal_category::Package}
        },
        {
            {{{127,17 }, {127,26 }}, keyword_lexeme(Keyword_kind::Kw_prefiksnaya)},
            {Terminal_category::Postfix_or_prefix, Terminal_category::Block_entry_begin}
        },
        {
            {{{136,21 }, {136,30 }}, keyword_lexeme(Keyword_kind::Kw_realizacija)},
            {Terminal_category::Implementation, Terminal_category::Block_entry_begin}
        },
        {
            {{{136,1  }, {136,8  }}, keyword_lexeme(Keyword_kind::Kw_realizuj)},
            {Terminal_category::Implement, Terminal_category::Block_entry_begin}
        },
    };

    static const Pair_for_test tests2[] = {
        {
            {{{23, 12  }, {23, 12 }}, delim_lexeme(Delimiter_kind::Assign)},
            {Terminal_category::Assignment, Terminal_category::By_definition}
        },
        {
            {{{112, 5  }, {112, 8 }}, delim_lexeme(Delimiter_kind::Meta_plus_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{77, 16  }, {77, 18 }}, delim_lexeme(Delimiter_kind::Bitwise_and_not_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{77, 16  }, {77, 18 }}, delim_lexeme(Delimiter_kind::Logical_and_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{77, 16  }, {77, 17 }}, delim_lexeme(Delimiter_kind::Remainder_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{77, 16  }, {77, 17 }}, delim_lexeme(Delimiter_kind::Bitwise_or_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 19 }}, delim_lexeme(Delimiter_kind::Logical_or_not_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Logical_or_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Copy)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 20 }}, delim_lexeme(Delimiter_kind::Logical_or_not_full_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 19 }}, delim_lexeme(Delimiter_kind::Logical_or_full_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Logical_xor_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 19 }}, delim_lexeme(Delimiter_kind::Logical_and_not_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 19 }}, delim_lexeme(Delimiter_kind::Logical_and_full_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 20 }}, delim_lexeme(Delimiter_kind::Logical_and_not_full_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Set_difference_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 19 }}, delim_lexeme(Delimiter_kind::Float_power_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Power_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Bitwise_xor_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Bitwise_or_not_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Bitwise_and_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Float_remainder_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Symmetric_difference_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Minus_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Left_shift_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Right_shift_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Plus_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Mul_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Div_assign)},
            {Terminal_category::Assignment}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::EQ)},
            {Terminal_category::Relation_op}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::LEQ)},
            {Terminal_category::Relation_op}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::GT)},
            {Terminal_category::Relation_op}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::NEQ)},
            {Terminal_category::Relation_op}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::LT)},
            {Terminal_category::Relation_op}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::GEQ)},
            {Terminal_category::Relation_op}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Cond_op)},
            {Terminal_category::Cond_op}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Cond_op_full)},
            {Terminal_category::Cond_op}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Logical_or)},
            {Terminal_category::Logical_or_like}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Logical_or_full)},
            {Terminal_category::Logical_or_like}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Logical_or_not)},
            {Terminal_category::Logical_or_like}
        },
        {
            {{{13, 16  }, {13, 19 }}, delim_lexeme(Delimiter_kind::Logical_or_not_full)},
            {Terminal_category::Logical_or_like}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Logical_xor)},
            {Terminal_category::Logical_or_like}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Logical_and)},
            {Terminal_category::Logical_and_like}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Logical_and_full)},
            {Terminal_category::Logical_and_like}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Logical_and_not)},
            {Terminal_category::Logical_and_like}
        },
        {
            {{{13, 16  }, {13, 19 }}, delim_lexeme(Delimiter_kind::Logical_and_not_full)},
            {Terminal_category::Logical_and_like}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Logical_not)},
            {Terminal_category::Logical_not, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Power)},
            {Terminal_category::Power_like}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Float_power)},
            {Terminal_category::Power_like}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Bitwise_not)},
            {Terminal_category::Bitwise_not, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Bitwise_and)},
            {Terminal_category::Bitwise_and_like}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Bitwise_and_not)},
            {Terminal_category::Bitwise_and_like}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Left_shift)},
            {Terminal_category::Bitwise_and_like}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Right_shift)},
            {Terminal_category::Bitwise_and_like}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Bitwise_or)},
            {Terminal_category::Bitwise_or_like}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Bitwise_or_not)},
            {Terminal_category::Bitwise_or_like}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Bitwise_xor)},
            {Terminal_category::Bitwise_or_like}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Set_difference)},
            {Terminal_category::Mul_like}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Mul)},
            {Terminal_category::Mul_like}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Div)},
            {Terminal_category::Mul_like}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Float_remainder)},
            {Terminal_category::Mul_like}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Remainder)},
            {Terminal_category::Mul_like}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Symmetric_difference)},
            {Terminal_category::Mul_like}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Algebraic_sep)},
            {Terminal_category::Addition_like}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Meta_plus)},
            {Terminal_category::Addition_like}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Plus)},
            {Terminal_category::Addition_like, Terminal_category::Unary_plus_like,
             Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Minus)},
            {Terminal_category::Addition_like, Terminal_category::Unary_plus_like,
             Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Inc)},
            {Terminal_category::Pre_inc_like, Terminal_category::Post_inc_like}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Wrap_dec)},
            {Terminal_category::Pre_inc_like, Terminal_category::Post_inc_like}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Wrap_inc)},
            {Terminal_category::Pre_inc_like, Terminal_category::Post_inc_like}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Dec)},
            {Terminal_category::Pre_inc_like, Terminal_category::Post_inc_like}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::ElemType)},
            {Terminal_category::Unary_plus_like, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Card)},
            {Terminal_category::Cardinality_like, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Round_br_opened)},
            {Terminal_category::Round_br_opened, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Round_br_closed)},
            {Terminal_category::Round_br_closed}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Sq_br_opened)},
            {Terminal_category::Sq_br_opened}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Sq_br_closed)},
            {Terminal_category::Sq_br_closed}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Fig_br_opened)},
            {Terminal_category::Fig_br_opened}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Fig_br_closed)},
            {Terminal_category::Fig_br_closed}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Tuple_begin)},
            {Terminal_category::Tuple_begin, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Tuple_end)},
            {Terminal_category::Tuple_end}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Meta_open_br)},
            {Terminal_category::Meta_open_br}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)},
            {Terminal_category::Meta_closed_br}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Colon_sq_br_opened)},
            {Terminal_category::Colon_sq_br_opened, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Colon_sq_br_closed)},
            {Terminal_category::Colon_sq_br_closed}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Colon_fig_br_opened)},
            {Terminal_category::Colon_fig_br_opened, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Colon_fig_br_closed)},
            {Terminal_category::Colon_fig_br_closed}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Comma)},
            {Terminal_category::Comma}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Right_arrow)},
            {Terminal_category::Right_arrow}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Point)},
            {Terminal_category::Dot}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Range)},
            {Terminal_category::Range}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Colon)},
            {Terminal_category::Colon}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Scope_resol)},
            {Terminal_category::Scope_resol}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Semicolon)},
            {Terminal_category::Semicolon, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Left_arrow)},
            {Terminal_category::Left_arrow}
        },
        {
            {{{13, 16  }, {13, 19 }}, delim_lexeme(Delimiter_kind::Pattern)},
            {Terminal_category::Pattern}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Cycle_name_prefix)},
            {Terminal_category::Cycle_name_prefix}
        },
        {
            {{{13, 16  }, {13, 17 }}, delim_lexeme(Delimiter_kind::Data_size)},
            {Terminal_category::Sizeof_like, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::Sharp)},
            {Terminal_category::Sizeof_like, Terminal_category::Index_cardinalty,
             Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Address)},
            {Terminal_category::Address_like}
        },
        {
            {{{13, 16  }, {13, 19 }}, delim_lexeme(Delimiter_kind::Data_address)},
            {Terminal_category::Address_like}
        },
        {
            {{{13, 16  }, {13, 19 }}, delim_lexeme(Delimiter_kind::ExprType)},
            {Terminal_category::Expr_type}
        },
        {
            {{{13, 16  }, {13, 16 }}, delim_lexeme(Delimiter_kind::At)},
            {Terminal_category::Deref_op, Terminal_category::Pointer_def}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Non_specific)},
            {Terminal_category::Non_specific}
        },
        {
            {{{13, 16  }, {13, 18 }}, delim_lexeme(Delimiter_kind::Deduce_arg_type)},
            {Terminal_category::Deduce_arg_type}
        },
        {
            {{{13, 16  }, {13, 23 }}, delim_lexeme(Delimiter_kind::Compare_with)},
            {Terminal_category::Compare}
        },
    };


    static const Pair_for_test tests3[] = {
        {
            {{{13, 16  }, {13, 16 }}, nothing_lexeme()},
            {Terminal_category::End_of_text}
        },
        {
            {{{13, 16  }, {13, 87 }}, unknown_lexeme()},
            {Terminal_category::Unrecognized}
        },
        {
            {{{13, 16  }, {13, 87 }}, id_lexeme(716)},
            {Terminal_category::Id, Terminal_category::Block_entry_begin}
        },
    };

    static const quat128 quat_vals[] = {
        {0.0q, 0.0q, 45760.91234567892345E-223q, 0.0q                      },
        {0.0q, 0.0q, 0.0q,                       45760.91234567892345E-223q},
        {0.0q, 0.0q, 0.618281828459045e+4q,      0.0q                      },
        {0.0q, 0.0q, 0.0q,                       6.141592653148e+8q        },
        {0.0q, 0.0q, 45.67891234567892345E-12q,  0.0q                      },
    };

    static const Pair_for_test tests4[] = {
        {
            {{{13, 16  }, {13, 19 }}, integer_lexeme(3578)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {13, 123 }}, string_lexeme(653, String_kind::String8)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {13, 123 }}, string_lexeme(917, String_kind::String16)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {18, 123 }}, string_lexeme(10615, String_kind::String32)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {18, 20 }}, char_lexeme(U'z', Char_kind::Char8)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {18, 21 }}, char_lexeme(U'п', Char_kind::Char16)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {18, 21 }}, char_lexeme(U'ж', Char_kind::Char32)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {18, 20 }}, float_lexeme(3.68q, Float_kind::Float32)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {18, 20 }}, float_lexeme(4.35q, Float_kind::Float64)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {18, 20 }}, float_lexeme(1.37q, Float_kind::Float80)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {18, 20 }}, float_lexeme(5.87q, Float_kind::Float128)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {18, 21 }}, complex_lexeme({0.0q, 3.68q}, Complex_kind::Complex32)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {18, 21 }}, complex_lexeme({0.0q, 3.68q}, Complex_kind::Complex64)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {18, 21 }}, complex_lexeme({0.0q, 3.68q}, Complex_kind::Complex80)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{13, 16  }, {18, 21 }}, complex_lexeme({0.0q, 3.68q}, Complex_kind::Complex128)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{18, 17 }, {18, 46 }}, quat_lexeme(quat_vals[0])},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{18, 51 }, {18, 80 }}, quat_lexeme(quat_vals[1])},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{21, 61 }, {21, 82 }}, quat_lexeme(quat_vals[2], Quat_kind::Quat80)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{22, 17 }, {22, 35 }}, quat_lexeme(quat_vals[3], Quat_kind::Quat32)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
        {
            {{{22, 37 }, {22, 68 }}, quat_lexeme(quat_vals[4], Quat_kind::Quat128)},
            {Terminal_category::Literal, Terminal_category::Block_entry_begin}
        },
    };

    void classification_testing()
    {
        auto converting_lambda = [](const arkona_parser::Token& token){
            return arkona_parser::token_to_terminal_set(token);
        };

        puts("Testing of converting of a token into the set of terminals...\n");
        puts("Keywords. Part 0:");
        testing::test(std::begin(tests), std::end(tests), converting_lambda);
        puts("Keywords. Part 1:");
        testing::test(std::begin(tests1), std::end(tests1), converting_lambda);
        puts("Delimiters. Part 0:");
        testing::test(std::begin(tests2), std::end(tests2), converting_lambda);
        puts("Special lexeme and identifiers. Part 0:");
        testing::test(std::begin(tests3), std::end(tests3), converting_lambda);
        puts("String. character, numeric literals. Part 0:");
        testing::test(std::begin(tests4), std::end(tests4), converting_lambda);
    }
};