/*
    File:    print-ast.cpp
    Created: 12 January 2021 at 19:57 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstdio>
#include <cstddef>
#include "../include/print-ast.hpp"
#include "../../char-conv/include/char_conv.h"
#include "../../strings-lib/include/join.h"
#include "../../numbers/include/int128_to_str.h"
#include "../../numbers/include/float128_to_string.h"
#include "../../char-conv/include/print_char32.h"
#include "../../tries/include/idx_to_string.h"

namespace recursive_parser_testing{
    using Token = arkona_scanner_snd_lvl::Arkona_token_snd_lvl;

    static std::vector<std::size_t> token_sequence2indeces(const std::list<Token>& tokens)
    {
        if(tokens.empty()){
            return std::vector<std::size_t>();
        }

        std::vector<std::size_t> result(tokens.size());
        std::size_t              i = 0;

        for(const auto& token : tokens){
            result[i++] = token.lexeme_.id_info_.id_idx_;
        }

        return result;
    }

    class To_string_visitor : public arkona_parser::ast::Visitor{
    public:
        To_string_visitor()                         = default;
        To_string_visitor(const To_string_visitor&) = default;
        virtual ~To_string_visitor()                = default;

        To_string_visitor(const arkona_parser::ast::AST&                      ast,
                          const std::shared_ptr<arkona_parser::Symbol_table>& symbol_table,
                          const std::shared_ptr<Char_trie>&                   ids_trie,
                          const std::shared_ptr<Char_trie>&                   str_trie)
        : ast_{ast}, symbol_table_{symbol_table}, ids_trie_{ids_trie}, str_trie_{str_trie}
        {
        }

        std::string to_string();

        void visit(arkona_parser::ast::Module&                                ref) override;
        void visit(arkona_parser::ast::Qualified_id&                          ref) override;
        void visit(arkona_parser::ast::Imports&                               ref) override;
        void visit(arkona_parser::ast::Block&                                 ref) override;
        void visit(arkona_parser::ast::If_stmt&                               ref) override;
        void visit(arkona_parser::ast::If_branch&                             ref) override;
        void visit(arkona_parser::ast::Assignment_stmt&                       ref) override;
        void visit(arkona_parser::ast::Ternary_op&                            ref) override;
        void visit(arkona_parser::ast::Not_op&                                ref) override;
        void visit(arkona_parser::ast::Range_op&                              ref) override;
        void visit(arkona_parser::ast::Binary_op&                             ref) override;
        void visit(arkona_parser::ast::Postfix_op&                            ref) override;
        void visit(arkona_parser::ast::Prefix_op&                             ref) override;
        void visit(arkona_parser::ast::Select_stmt&                           ref) override;
        void visit(arkona_parser::ast::Select_branch&                         ref) override;
        void visit(arkona_parser::ast::Meta_if_stmt&                          ref) override;
        void visit(arkona_parser::ast::Meta_if_branch&                        ref) override;
        void visit(arkona_parser::ast::Meta_select_stmt&                      ref) override;
        void visit(arkona_parser::ast::Meta_select_branch&                    ref) override;
        void visit(arkona_parser::ast::While_stmt&                            ref) override;
        void visit(arkona_parser::ast::Meta_while_stmt&                       ref) override;
        void visit(arkona_parser::ast::Do_while_stmt&                         ref) override;
        void visit(arkona_parser::ast::Meta_do_while_stmt&                    ref) override;
        void visit(arkona_parser::ast::Forever_stmt&                          ref) override;
        void visit(arkona_parser::ast::For_with_counter_stmt&                 ref) override;
        void visit(arkona_parser::ast::Meta_for_with_counter_stmt&            ref) override;
        void visit(arkona_parser::ast::Foreach_stmt&                          ref) override;
        void visit(arkona_parser::ast::Meta_foreach_stmt&                     ref) override;
        void visit(arkona_parser::ast::Indexed_foreach_stmt&                  ref) override;
        void visit(arkona_parser::ast::Meta_indexed_foreach_stmt&             ref) override;
        void visit(arkona_parser::ast::Zipped_foreach_stmt&                   ref) override;
        void visit(arkona_parser::ast::Meta_zipped_foreach_stmt&              ref) override;
        void visit(arkona_parser::ast::Return_stmt&                           ref) override;
        void visit(arkona_parser::ast::Meta_return_stmt&                      ref) override;
        void visit(arkona_parser::ast::Exit_stmt&                             ref) override;
        void visit(arkona_parser::ast::Meta_exit_stmt&                        ref) override;
        void visit(arkona_parser::ast::Dijkstra_loop_stmt&                    ref) override;
        void visit(arkona_parser::ast::Dijkstra_branch&                       ref) override;
        void visit(arkona_parser::ast::Meta_dijkstra_loop_stmt&               ref) override;
        void visit(arkona_parser::ast::Meta_dijkstra_branch&                  ref) override;
        void visit(arkona_parser::ast::Match_stmt&                            ref) override;
        void visit(arkona_parser::ast::Match_branch&                          ref) override;
        void visit(arkona_parser::ast::Meta_match_stmt&                       ref) override;
        void visit(arkona_parser::ast::Meta_match_branch&                     ref) override;
        void visit(arkona_parser::ast::Variables_definition&                  ref) override;
        void visit(arkona_parser::ast::Meta_variables_definition&             ref) override;
        void visit(arkona_parser::ast::Constants_definition&                  ref) override;
        void visit(arkona_parser::ast::Constant_definition&                   ref) override;
        void visit(arkona_parser::ast::Meta_constants_definition&             ref) override;
        void visit(arkona_parser::ast::Meta_constant_definition&              ref) override;
        void visit(arkona_parser::ast::Types_definition&                      ref) override;
        void visit(arkona_parser::ast::Type_definition&                       ref) override;
        void visit(arkona_parser::ast::Func_definition&                       ref) override;
        void visit(arkona_parser::ast::Func_header&                           ref) override;
        void visit(arkona_parser::ast::Func_signature&                        ref) override;
        void visit(arkona_parser::ast::Formal_aguments_group&                 ref) override;
        void visit(arkona_parser::ast::Operation_definition&                  ref) override;
        void visit(arkona_parser::ast::Operation_header&                      ref) override;
        void visit(arkona_parser::ast::Meta_func_definition&                  ref) override;
        void visit(arkona_parser::ast::Meta_func_header&                      ref) override;
        void visit(arkona_parser::ast::Meta_func_signature&                   ref) override;
        void visit(arkona_parser::ast::Meta_func_formal_aguments_group&       ref) override;
        void visit(arkona_parser::ast::Alias_definition&                      ref) override;
        void visit(arkona_parser::ast::Scope_resolution&                      ref) override;
        void visit(arkona_parser::ast::Call_expr&                             ref) override;
        void visit(arkona_parser::ast::Indexed_expr&                          ref) override;
        void visit(arkona_parser::ast::Meta_call_expr&                        ref) override;
        void visit(arkona_parser::ast::Field_access_expr&                     ref) override;
        void visit(arkona_parser::ast::Struct_construction_from_field_values& ref) override;
        void visit(arkona_parser::ast::Struct_field_value_expr&               ref) override;
        void visit(arkona_parser::ast::Reinterpret_expr&                      ref) override;
        void visit(arkona_parser::ast::Convert_expr&                          ref) override;
        void visit(arkona_parser::ast::Tuple_expression&                      ref) override;
        void visit(arkona_parser::ast::Set_expression&                        ref) override;
        void visit(arkona_parser::ast::Array_expression&                      ref) override;
        void visit(arkona_parser::ast::BitArray_expression&                   ref) override;
        void visit(arkona_parser::ast::Alloc_expr&                            ref) override;
        void visit(arkona_parser::ast::Free_expr&                             ref) override;
        void visit(arkona_parser::ast::Alloc_array_expr&                      ref) override;
        void visit(arkona_parser::ast::Alloc_bitarray_expr&                   ref) override;
        void visit(arkona_parser::ast::Lambda&                                ref) override;
        void visit(arkona_parser::ast::Literal_expr&                          ref) override;
        void visit(arkona_parser::ast::Fixed_size_base_type&                  ref) override;
        void visit(arkona_parser::ast::Sizeable_base_type&                    ref) override;
        void visit(arkona_parser::ast::Array_type_expr&                       ref) override;
        void visit(arkona_parser::ast::BitArray_type_expr&                    ref) override;
        void visit(arkona_parser::ast::Reference_type_expr&                   ref) override;
        void visit(arkona_parser::ast::Enum_type_expr&                        ref) override;
        void visit(arkona_parser::ast::EnumSet_type_expr&                     ref) override;
        void visit(arkona_parser::ast::Auto_type_expr&                        ref) override;
        void visit(arkona_parser::ast::Type&                                  ref) override;
        void visit(arkona_parser::ast::Func_ptr_type_expr&                    ref) override;
        void visit(arkona_parser::ast::Struct_fields_group&                   ref) override;
        void visit(arkona_parser::ast::Struct_type_expr&                      ref) override;
        void visit(arkona_parser::ast::Used_categories&                       ref) override;
        void visit(arkona_parser::ast::Used_category&                         ref) override;
        void visit(arkona_parser::ast::Category_definition&                   ref) override;
        void visit(arkona_parser::ast::Category_definition_body&              ref) override;
        void visit(arkona_parser::ast::Const_entry&                           ref) override;
        void visit(arkona_parser::ast::Implement_category&                    ref) override;
        void visit(arkona_parser::ast::Category_implementation&               ref) override;
    private:
        arkona_parser::ast::AST                      ast_;
        std::shared_ptr<arkona_parser::Symbol_table> symbol_table_;
        std::shared_ptr<Char_trie>                   ids_trie_;
        std::shared_ptr<Char_trie>                   str_trie_;

        size_t      indent_     = 0;
        std::string str_repres_;
    };

    std::string To_string_visitor::to_string()
    {
        indent_ = 0;
        str_repres_.clear();
        ast_.traverse(*this);
        return str_repres_;
    }

    std::string ast2string(const arkona_parser::ast::AST&                      ast,
                           const std::shared_ptr<arkona_parser::Symbol_table>& symbol_table,
                           const std::shared_ptr<Char_trie>&                   ids_trie,
                           const std::shared_ptr<Char_trie>&                   str_trie)
    {
        std::string       result;
        To_string_visitor v{ast, symbol_table, ids_trie, str_trie};
        result = v.to_string();
        return result;
    }

    void print_ast(const arkona_parser::ast::AST&                      ast,
                   const std::shared_ptr<arkona_parser::Symbol_table>& symbol_table,
                   const std::shared_ptr <Char_trie>&                  ids_trie,
                   const std::shared_ptr<Char_trie>&                   str_trie)
    {
        auto str = ast2string(ast, symbol_table, ids_trie, str_trie);
        puts(str.c_str());
    }

    static constexpr size_t indent_increment = 4;

    void To_string_visitor::visit(arkona_parser::ast::Module& ref)
    {
        str_repres_ = "Module ";

        if(ref.name_){
            ref.name_->accept(*this);
        }else{
            str_repres_ += "{nullptr}";
        }
        str_repres_ += "\n";

        indent_     += indent_increment;

        if(ref.used_modules_){
            ref.used_modules_->accept(*this);
        }else{
            str_repres_ += "Imports {nullptr}";
        }
        str_repres_ += "\n";

        if(ref.body_){
            ref.body_->accept(*this);
        }else{
            str_repres_ += "Body {nullptr}";
        }
        str_repres_ += "\n";

        indent_     -= indent_increment;
        str_repres_ += "\n";
    }

    void To_string_visitor::visit(arkona_parser::ast::Qualified_id& ref)
    {
        const auto indices =  token_sequence2indeces(ref.name_components_);
        const auto str     =  symbol_table_->get_string(indices);
        str_repres_        += str;
    }

    void To_string_visitor::visit(arkona_parser::ast::Imports& ref)
    {
        const auto indent = std::string(indent_, ' ');
        for(const auto& imp : ref.imports_){
            if(imp){
                const auto indices =  token_sequence2indeces(imp->name_components_);
                str_repres_        += indent + "Import {" + symbol_table_->get_string(indices) + "}\n";
            }else{
                str_repres_        += indent + "Import {nullptr}\n";
            }
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Block& ref)
    {
        str_repres_ += "Body{\n";
        for(const auto& entry : ref.entries_){
            if(entry){
                entry->accept(*this);
            }else{
                str_repres_ += std::string(indent_, ' ') + "Body_entry {nullptr}\n";
            }
        }
        str_repres_ += "}";
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_if_stmt& ref)
    {
        auto& non_else_branches = ref.non_else_branches_;

        if(non_else_branches.empty()){
            str_repres_ += std::string(indent_, ' ') + "META_IF (nullptr){nullptr}\n";
            return;
        }

        str_repres_ += std::string(indent_, ' ') + "META_IF (";
        auto first_elem_it = non_else_branches.begin();
        (*first_elem_it)->accept(*this);


        auto snd_elem_it = non_else_branches.begin();
        snd_elem_it++;
        for(auto it = snd_elem_it; it != non_else_branches.end(); ++it){
            str_repres_ += std::string(indent_, ' ') + "META_ELIF (";
            (*it)->accept(*this);
        }

        str_repres_ += std::string(indent_, ' ') + "META_ELSE\n";
        indent_ += indent_increment;
        ref.else_branch_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_if_branch& ref)
    {
        ref.condition_->accept(*this);
        str_repres_ += ")\n";

        indent_ += indent_increment;
        ref.actions_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::If_stmt& ref)
    {
        auto& non_else_branches = ref.non_else_branches_;

        if(non_else_branches.empty()){
            str_repres_ += std::string(indent_, ' ') + "IF (nullptr){nullptr}\n";
            return;
        }

        str_repres_ += std::string(indent_, ' ') + "IF (";
        auto first_elem_it = non_else_branches.begin();
        (*first_elem_it)->accept(*this);


        auto snd_elem_it = non_else_branches.begin();
        snd_elem_it++;
        for(auto it = snd_elem_it; it != non_else_branches.end(); ++it){
            str_repres_ += std::string(indent_, ' ') + "ELIF (";
            (*it)->accept(*this);
        }

        str_repres_ += std::string(indent_, ' ') + "ELSE\n";
        indent_ += indent_increment;
        ref.else_branch_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::If_branch& ref)
    {
        ref.condition_->accept(*this);
        str_repres_ += ")\n";

        indent_ += indent_increment;
        ref.actions_->accept(*this);
        indent_ -= indent_increment;
    }

    static const std::string assignment_name[] = {
        "Non_assignment",          "Assignment",                  "Copy",
        "Logical_or_assign",       "Logical_or_not_full_assign",  "Logical_or_not_assign",
        "Logical_or_full_assign",  "Logical_xor_assign",          "Logical_and_assign",
        "Logical_and_full_assign", "Logical_and_not_assign",      "Logical_and_not_full_assign",
        "Bitwise_or_assign",       "Bitwise_or_not_assign,"       "Bitwise_xor_assign",
        "Bitwise_and_assign",      "Bitwise_and_not_assign",      "Left_shift_assign",
        "Right_shift_assign",      "Plus_assign",                 "Minus_assign",
        "Mul_assign",              "Div_assign",                  "Remainder_assign",
        "Float_remainder_assign",  "Symmetric_difference_assign", "Set_difference_assign",
        "Power_assign",            "Float_power_assign",          "Meta_plus_assign"
    };

    void To_string_visitor::visit(arkona_parser::ast::Assignment_stmt& ref)
    {
        if(ref.kind_ == arkona_parser::ast::Assignment_kind::Non_assignment){
            ref.lhs_->accept(*this);
            return;
        }

        auto indent_str = std::string(indent_, ' ');

        str_repres_ += indent_str + assignment_name[static_cast<unsigned>(ref.kind_)] + "\n";

        indent_ += indent_increment;
        ref.lhs_->accept(*this);
        indent_ -= indent_increment;

        indent_ += indent_increment;
        ref.rhs_->accept(*this);
        indent_ -= indent_increment;
    }

    static const std::string cond_op_name[] = {
        "Not_cond_op", "Complete_calc", "Short_circuit_calc"
    };

    void To_string_visitor::visit(arkona_parser::ast::Ternary_op& ref)
    {
        auto indent_str = std::string(indent_, ' ');

        if(ref.kind_ == arkona_parser::ast::Cond_op_kind::Not_cond_op){
            ref.condition_->accept(*this);
            return;
        }

        str_repres_ += indent_str + cond_op_name[static_cast<unsigned>(ref.kind_)] + "\n";

        indent_ += indent_increment;
        ref.condition_->accept(*this);
        indent_ -= indent_increment;

        indent_ += indent_increment;
        ref.true_branch_->accept(*this);
        indent_ -= indent_increment;

        indent_ += indent_increment;
        ref.false_branch_->accept(*this);
        indent_ -= indent_increment;
    }

    static const std::string not_op_name[] = {
        "Logical ", "Bitwise "
    };

    void To_string_visitor::visit(arkona_parser::ast::Not_op& ref)
    {
        if(ref.order_){
            auto indent_str = std::string(indent_, ' ');
            auto not_name   = not_op_name[static_cast<unsigned>(ref.kind_)];

            str_repres_ += indent_str;


            for(std::size_t i = 0; i < ref.order_; ++i){
                str_repres_ += not_name;
            }
            str_repres_ += "\n";
        }

        indent_ += indent_increment;
        ref.operand_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Range_op& ref)
    {
        if(ref.kind_ == arkona_parser::ast::Range_op_kind::Non_range_op){
            ref.start_->accept(*this);
            return;
        }

        str_repres_ += std::string(indent_, ' ') + "Range\n";

        auto indent_str = std::string(indent_ + indent_increment, ' ');

        str_repres_ += indent_str + "Start\n";
        indent_ += 2 * indent_increment;
        ref.start_->accept(*this);
        indent_ -= 2 * indent_increment;

        str_repres_ += indent_str + "End\n";
        indent_ += 2 * indent_increment;
        ref.end_->accept(*this);
        indent_ -= 2 * indent_increment;

        str_repres_ += indent_str + "Step\n";
        indent_ += 2 * indent_increment;
        ref.step_->accept(*this);
        indent_ -= 2 * indent_increment;
    }

    static const std::string binary_op_name[] = {
        "Non_binary_op",         "Logical_or",          "Logical_or_full",
        "Logical_or_not",        "Logical_or_not_full", "Logical_xor",
        "Logical_and",           "Logical_and_full",    "Logical_and_not",
        "Logical_and_not_full",  "Compare",             "Eq",
        "Neq",                   "Lt",                  "Gt",
        "Leq",                   "Geq",                 "Type_equiv",
        "Bitwise_or",            "Bitwise_or_not",      "Bitwise_xor",
        "Bitwise_and",           "Bitwise_and_not",     "Left_shift",
        "Right_shift",           "Add",                 "Sub",
        "Algebraic_sep",         "Meta_add",            "Mul",
        "Div",                   "Remainder",           "Float_remainder",
        "Symmetric_difference",  "Set_difference",      "Power",
        "Float_power",           "Dimension_size"
    };

    void To_string_visitor::visit(arkona_parser::ast::Binary_op& ref)
    {
        auto indent_str = std::string(indent_, ' ');

        if(ref.kind_ == arkona_parser::ast::Binary_op_kind::Non_binary_op){
            ref.left_operand_->accept(*this);
            return;
        }

        // We need this if statement, because if we use only 'else' branch, then
        // GNU ld (GNU Binutils for Debian) 2.31.1 is segfaulted.
        if(ref.kind_ == arkona_parser::ast::Binary_op_kind::Dimension_size){
            str_repres_ += indent_str + "Dimension_size\n";
        }else{
            str_repres_ += indent_str + binary_op_name[static_cast<unsigned>(ref.kind_)] + "\n";
        }

        indent_ += indent_increment;
        ref.left_operand_->accept(*this);
        indent_ -= indent_increment;

        indent_ += indent_increment;
        ref.right_operand_->accept(*this);
        indent_ -= indent_increment;
    }

    static const std::string postfix_op_name[] = {
        "Non_postfix_op", "Postfix_inc",               "Postfix_inc_with_wrapping",
        "Postfix_dec",    "Postfix_dec_with_wrapping", "Dereference"
    };

    void To_string_visitor::visit(arkona_parser::ast::Postfix_op& ref)
    {
        auto indent_str = std::string(indent_, ' ');

        if(ref.kind_ == arkona_parser::ast::Postfix_op_kind::Non_postfix_op){
            ref.operand_->accept(*this);
            return;
        }

        str_repres_ += indent_str + postfix_op_name[static_cast<unsigned>(ref.kind_)] + "\n";

        indent_ += indent_increment;
        ref.operand_->accept(*this);
        indent_ -= indent_increment;
    }

    static const std::string prefix_op_name[] = {
        "Non_prefix_op", "Prefix_inc",                  "Prefixfix_inc_with_wrapping",
        "Prefix_dec",    "Prefixfix_dec_with_wrapping", "ElemType",
        "Unary_plus",    "Unary_minus",                 "Data_size",
        "Sharp",         "Card",                        "Address",
        "Data_address",  "Pointer_def"
    };

    void To_string_visitor::visit(arkona_parser::ast::Prefix_op& ref)
    {
        auto indent_str = std::string(indent_, ' ');

        if(ref.kind_ == arkona_parser::ast::Prefix_op_kind::Non_prefix_op){
            ref.operand_->accept(*this);
            return;
        }

        str_repres_ += indent_str + prefix_op_name[static_cast<unsigned>(ref.kind_)] + "\n";

        indent_ += indent_increment;
        ref.operand_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Select_branch& ref)
    {
        for(const auto& e : ref.expressions_){
            indent_ += indent_increment;
            str_repres_ += std::string(indent_, ' ') + "CASE (";
            e->accept(*this);
            str_repres_ += ")\n";
            indent_ -= indent_increment;
        }
        indent_ += indent_increment;
        ref.actions_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Select_stmt& ref)
    {
        auto indent_str = std::string(indent_, ' ');

        str_repres_ += indent_str + "SELECT (\n";

        indent_ += indent_increment;
        ref.value_to_select_->accept(*this);
        indent_ -= indent_increment;

        str_repres_ += indent_str + ")\n";

        for(const auto& branch : ref.non_else_branches_){
            indent_ += indent_increment;
            str_repres_ += std::string(indent_, ' ') + "SELECT_BRANCH (\n";
            branch->accept(*this);
            str_repres_ += std::string(indent_, ' ') + ")\n";
            indent_ -= indent_increment;
        }

        indent_ += indent_increment;
        str_repres_ += std::string(indent_, ' ') + "ELSE\n";
        ref.else_branch_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_select_branch& ref)
    {
        for(const auto& e : ref.expressions_){
            indent_ += indent_increment;
            str_repres_ += std::string(indent_, ' ') + "META_CASE (";
            e->accept(*this);
            str_repres_ += ")\n";
            indent_ -= indent_increment;
        }
        indent_ += indent_increment;
        ref.actions_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_select_stmt& ref)
    {
        auto indent_str = std::string(indent_, ' ');

        str_repres_ += indent_str + "META_SELECT (\n";

        indent_ += indent_increment;
        ref.value_to_select_->accept(*this);
        indent_ -= indent_increment;

        str_repres_ += indent_str + ")\n";

        for(const auto& branch : ref.non_else_branches_){
            indent_ += indent_increment;
            str_repres_ += std::string(indent_, ' ') + "META_SELECT_BRANCH (\n";
            branch->accept(*this);
            str_repres_ += std::string(indent_, ' ') + ")\n";
            indent_ -= indent_increment;
        }

        indent_ += indent_increment;
        str_repres_ += std::string(indent_, ' ') + "META_ELSE\n";
        ref.else_branch_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::While_stmt& ref)
    {
        auto indent_str = std::string(indent_, ' ');
        str_repres_ += indent_str;

        if(ref.cycle_label_){
            str_repres_ += "LABEL {" + u32string_to_utf8(ids_trie_->get_string(ref.cycle_label_)) + "}: ";
        }

        str_repres_ += "WHILE (";
        indent_ += indent_increment;
        ref.cycle_condition_->accept(*this);
        indent_ -= indent_increment;
        str_repres_ += indent_str + ")\n";

        indent_ += indent_increment;
        ref.cycle_body_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_while_stmt& ref)
    {
        auto indent_str = std::string(indent_, ' ');
        str_repres_ += indent_str;

        if(ref.cycle_label_){
            str_repres_ += "LABEL {" + u32string_to_utf8(ids_trie_->get_string(ref.cycle_label_)) + "}: ";
        }

        str_repres_ += "META_WHILE (";
        indent_ += indent_increment;
        ref.cycle_condition_->accept(*this);
        indent_ -= indent_increment;
        str_repres_ += indent_str + ")\n";

        indent_ += indent_increment;
        ref.cycle_body_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Do_while_stmt& ref)
    {
        auto indent_str = std::string(indent_, ' ');
        str_repres_ += indent_str;

        if(ref.cycle_label_){
            str_repres_ += "LABEL {" + u32string_to_utf8(ids_trie_->get_string(ref.cycle_label_)) + "}: \n";
        }

        indent_ += indent_increment;
        ref.cycle_body_->accept(*this);
        indent_ -= indent_increment;

        str_repres_ += "DO_WHILE (";
        indent_ += indent_increment;
        ref.cycle_condition_->accept(*this);
        indent_ -= indent_increment;
        str_repres_ += indent_str + ")\n";
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_do_while_stmt& ref)
    {
        auto indent_str = std::string(indent_, ' ');
        str_repres_ += indent_str;

        if(ref.cycle_label_){
            str_repres_ += "LABEL {" + u32string_to_utf8(ids_trie_->get_string(ref.cycle_label_)) + "}: \n";
        }

        indent_ += indent_increment;
        ref.cycle_body_->accept(*this);
        indent_ -= indent_increment;

        str_repres_ += "META_DO_WHILE (";
        indent_ += indent_increment;
        ref.cycle_condition_->accept(*this);
        indent_ -= indent_increment;
        str_repres_ += indent_str + ")\n";
    }

    void To_string_visitor::visit(arkona_parser::ast::Forever_stmt& ref)
    {
        auto indent_str = std::string(indent_, ' ');
        str_repres_ += indent_str;

        if(ref.cycle_label_){
            str_repres_ += "LABEL {" + u32string_to_utf8(ids_trie_->get_string(ref.cycle_label_)) + "}: FOREVER\n";
        }else{
            str_repres_ += "FOREVER\n";
        }

        indent_ += indent_increment;
        ref.cycle_body_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::For_with_counter_stmt& ref)
    {
        str_repres_ += std::string(indent_, ' ');

        if(ref.cycle_label_){
            str_repres_ += "LABEL {" + u32string_to_utf8(ids_trie_->get_string(ref.cycle_label_)) + "}: ";
        }

        str_repres_ += "FOR WITH COUNTER " + u32string_to_utf8(ids_trie_->get_string(ref.loop_var_)) + "\n";

        indent_ += indent_increment;
        str_repres_ += std::string(indent_, ' ') + "START VALUE: (\n";
        ref.start_value_->accept(*this);
        str_repres_ += ")\n";
        indent_ -= indent_increment;

        indent_ += indent_increment;
        str_repres_ += std::string(indent_, ' ') + "END VALUE: (\n";
        ref.end_value_->accept(*this);
        str_repres_ += ")\n";
        indent_ -= indent_increment;

        indent_ += indent_increment;
        str_repres_ += std::string(indent_, ' ') + "STEP VALUE: (\n";
        ref.step_value_->accept(*this);
        str_repres_ += ")\n";
        indent_ -= indent_increment;

        indent_ += indent_increment;
        ref.cycle_body_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_for_with_counter_stmt& ref)
    {
        str_repres_ += std::string(indent_, ' ');

        if(ref.cycle_label_){
            str_repres_ += "LABEL {" + u32string_to_utf8(ids_trie_->get_string(ref.cycle_label_)) + "}: ";
        }

        str_repres_ += "META FOR WITH COUNTER " + u32string_to_utf8(ids_trie_->get_string(ref.loop_var_)) + "\n";

        indent_ += indent_increment;
        str_repres_ += std::string(indent_, ' ') + "START VALUE: (\n";
        ref.start_value_->accept(*this);
        str_repres_ += ")\n";
        indent_ -= indent_increment;

        indent_ += indent_increment;
        str_repres_ += std::string(indent_, ' ') + "END VALUE: (\n";
        ref.end_value_->accept(*this);
        str_repres_ += ")\n";
        indent_ -= indent_increment;

        indent_ += indent_increment;
        str_repres_ += std::string(indent_, ' ') + "STEP VALUE: (\n";
        ref.step_value_->accept(*this);
        str_repres_ += ")\n";
        indent_ -= indent_increment;

        indent_ += indent_increment;
        ref.cycle_body_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Foreach_stmt& ref)
    {
        str_repres_ += std::string(indent_, ' ');

        if(ref.cycle_label_){
            str_repres_ += "LABEL {" + u32string_to_utf8(ids_trie_->get_string(ref.cycle_label_)) + "}: ";
        }

        str_repres_ += "FOR EACH " + u32string_to_utf8(ids_trie_->get_string(ref.loop_var_)) + " OVER COLLECTION\n";

        indent_ += indent_increment;
        ref.collection_->accept(*this);
        indent_ -= indent_increment;

        indent_ += indent_increment;
        ref.cycle_body_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_foreach_stmt& ref)
    {
        str_repres_ += std::string(indent_, ' ');

        if(ref.cycle_label_){
            str_repres_ += "LABEL {" + u32string_to_utf8(ids_trie_->get_string(ref.cycle_label_)) + "}: ";
        }

        str_repres_ += "META FOR EACH " + u32string_to_utf8(ids_trie_->get_string(ref.loop_var_)) + " OVER COLLECTION\n";

        indent_ += indent_increment;
        ref.collection_->accept(*this);
        indent_ -= indent_increment;

        indent_ += indent_increment;
        ref.cycle_body_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Indexed_foreach_stmt& ref)
    {
        str_repres_ += std::string(indent_, ' ');

        if(ref.cycle_label_){
            str_repres_ += "LABEL {" + u32string_to_utf8(ids_trie_->get_string(ref.cycle_label_)) + "}: ";
        }

        str_repres_ += "INDEXED FOR EACH WITH INDEX VARIABLE"                   +
                       u32string_to_utf8(ids_trie_->get_string(ref.index_var_)) +
                       ", VALUE VARIABLE "                                      +
                       u32string_to_utf8(ids_trie_->get_string(ref.value_var_)) +
                       " OVER COLLECTION\n";

        indent_ += indent_increment;
        ref.collection_->accept(*this);
        indent_ -= indent_increment;

        indent_ += indent_increment;
        ref.cycle_body_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_indexed_foreach_stmt& ref)
    {
        str_repres_ += std::string(indent_, ' ');

        if(ref.cycle_label_){
            str_repres_ += "LABEL {" + u32string_to_utf8(ids_trie_->get_string(ref.cycle_label_)) + "}: ";
        }

        str_repres_ += "META INDEXED FOR EACH WITH INDEX VARIABLE"              +
                       u32string_to_utf8(ids_trie_->get_string(ref.index_var_)) +
                       ", VALUE VARIABLE "                                      +
                       u32string_to_utf8(ids_trie_->get_string(ref.value_var_)) +
                       " OVER COLLECTION\n";

        indent_ += indent_increment;
        ref.collection_->accept(*this);
        indent_ -= indent_increment;

        indent_ += indent_increment;
        ref.cycle_body_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Zipped_foreach_stmt& ref)
    {
        str_repres_ += std::string(indent_, ' ');

        if(ref.cycle_label_){
            str_repres_ += "LABEL {" + u32string_to_utf8(ids_trie_->get_string(ref.cycle_label_)) + "}: ";
        }

        auto var_idx_to_string = [&](std::size_t var_idx){
            return u32string_to_utf8(ids_trie_->get_string(var_idx));
        };

        str_repres_ += "ZIPPED FOR EACH WITH VARIABLES "                                                     +
                    join(var_idx_to_string, ref.loop_vars_.begin(), ref.loop_vars_.end(), std::string{", "}) +
                    " OVER COLLECTION\n";

        indent_ += indent_increment;
        ref.collection_->accept(*this);
        indent_ -= indent_increment;

        indent_ += indent_increment;
        ref.cycle_body_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_zipped_foreach_stmt& ref)
    {
        str_repres_ += std::string(indent_, ' ');

        if(ref.cycle_label_){
            str_repres_ += "LABEL {" + u32string_to_utf8(ids_trie_->get_string(ref.cycle_label_)) + "}: ";
        }

        auto var_idx_to_string = [&](std::size_t var_idx){
            return u32string_to_utf8(ids_trie_->get_string(var_idx));
        };

        str_repres_ += "META ZIPPED FOR EACH WITH VARIABLES "                                                +
                    join(var_idx_to_string, ref.loop_vars_.begin(), ref.loop_vars_.end(), std::string{", "}) +
                    " OVER COLLECTION\n";

        indent_ += indent_increment;
        ref.collection_->accept(*this);
        indent_ -= indent_increment;

        indent_ += indent_increment;
        ref.cycle_body_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Return_stmt& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "RETURN (";
        if(ref.returned_value_){
            indent_ += indent_increment;
            ref.returned_value_->accept(*this);
            indent_ -= indent_increment;
        }
        str_repres_ += ")\n";
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_return_stmt& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "META RETURN (";
        if(ref.returned_value_){
            indent_ += indent_increment;
            ref.returned_value_->accept(*this);
            indent_ -= indent_increment;
        }
        str_repres_ += ")\n";
    }

    void To_string_visitor::visit(arkona_parser::ast::Exit_stmt& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "EXIT FROM LOOP";
        if(ref.loop_label_){
            str_repres_ += " WITH LABEL " + u32string_to_utf8(ids_trie_->get_string(ref.loop_label_));
        }
        str_repres_ += "\n";
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_exit_stmt& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "META EXIT FROM LOOP";
        if(ref.loop_label_){
            str_repres_ += " WITH LABEL " + u32string_to_utf8(ids_trie_->get_string(ref.loop_label_));
        }
        str_repres_ += "\n";
    }

    void To_string_visitor::visit(arkona_parser::ast::Dijkstra_loop_stmt& ref)
    {
        str_repres_ += std::string(indent_, ' ');

        if(ref.cycle_label_){
            str_repres_ += "LABEL {" + u32string_to_utf8(ids_trie_->get_string(ref.cycle_label_)) + "}: ";
        }
        str_repres_ += "DIJKSTRA LOOP {\n";

        for(const auto& branch : ref.non_else_branches_){
            indent_ += indent_increment;
            branch->accept(*this);
            indent_ -= indent_increment;
        }

        indent_ += indent_increment;
        str_repres_ += std::string(indent_, ' ') + "ELSE\n";
        ref.else_branch_->accept(*this);
        indent_ -= indent_increment;

        str_repres_ += std::string(indent_, ' ') + "}\n";
    }

    void To_string_visitor::visit(arkona_parser::ast::Dijkstra_branch& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "CONDITION: (\n";

        indent_ += indent_increment;
        ref.condition_->accept(*this);
        indent_ -= indent_increment;

        str_repres_ += std::string(indent_, ' ') + ")\n";

        indent_ += indent_increment;
        ref.actions_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_dijkstra_loop_stmt& ref)
    {
        str_repres_ += std::string(indent_, ' ');

        if(ref.cycle_label_){
            str_repres_ += "LABEL {" + u32string_to_utf8(ids_trie_->get_string(ref.cycle_label_)) + "}: ";
        }
        str_repres_ += "META DIJKSTRA LOOP {\n";

        for(const auto& branch : ref.non_else_branches_){
            indent_ += indent_increment;
            branch->accept(*this);
            indent_ -= indent_increment;
        }

        indent_ += indent_increment;
        str_repres_ += std::string(indent_, ' ') + "META ELSE\n";
        ref.else_branch_->accept(*this);
        indent_ -= indent_increment;

        str_repres_ += std::string(indent_, ' ') + "}\n";
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_dijkstra_branch& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "META CONDITION: (\n";

        indent_ += indent_increment;
        ref.condition_->accept(*this);
        indent_ -= indent_increment;

        str_repres_ += std::string(indent_, ' ') + ")\n";

        indent_ += indent_increment;
        ref.actions_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Match_stmt& ref)
    {
        auto indent_str = std::string(indent_, ' ');

        str_repres_ += indent_str + "MATCH (\n";

        indent_ += indent_increment;
        ref.matched_value_->accept(*this);
        indent_ -= indent_increment;

        str_repres_ += indent_str + ")\n";

        for(const auto& branch : ref.non_else_branches_){
            indent_ += indent_increment;
            str_repres_ += std::string(indent_, ' ') + "MATCH_BRANCH (\n";
            branch->accept(*this);
            str_repres_ += std::string(indent_, ' ') + ")\n";
            indent_ -= indent_increment;
        }

        indent_ += indent_increment;
        str_repres_ += std::string(indent_, ' ') + "MATCH_ELSE\n";
        ref.else_branch_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Match_branch& ref)
    {
        indent_ += indent_increment;
        str_repres_ += std::string(indent_, ' ') + "MATCH_CASE (";
        ref.condition_->accept(*this);
        str_repres_ += ")\n";
        indent_ -= indent_increment;

        indent_ += indent_increment;
        ref.actions_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_match_stmt& ref)
    {
        auto indent_str = std::string(indent_, ' ');

        str_repres_ += indent_str + "META MATCH (\n";

        indent_ += indent_increment;
        ref.matched_value_->accept(*this);
        indent_ -= indent_increment;

        str_repres_ += indent_str + ")\n";

        for(const auto& branch : ref.non_else_branches_){
            indent_ += indent_increment;
            str_repres_ += std::string(indent_, ' ') + "META MATCH_BRANCH (\n";
            branch->accept(*this);
            str_repres_ += std::string(indent_, ' ') + ")\n";
            indent_ -= indent_increment;
        }

        indent_ += indent_increment;
        str_repres_ += std::string(indent_, ' ') + "META MATCH_ELSE\n";
        ref.else_branch_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_match_branch& ref)
    {
        indent_ += indent_increment;
        str_repres_ += std::string(indent_, ' ') + "META MATCH_CASE (";
        ref.condition_->accept(*this);
        str_repres_ += ")\n";
        indent_ -= indent_increment;

        indent_ += indent_increment;
        ref.actions_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Variables_definition& ref)
    {
        auto var_idx_to_string = [&](std::size_t var_idx){
            return u32string_to_utf8(ids_trie_->get_string(var_idx));
        };

        str_repres_ += std::string(indent_, ' ');
        if(ref.is_exported_){
            str_repres_ += "EXPORTED ";
        }

        str_repres_ += "VARIABLES "                                                                             +
                       join(var_idx_to_string, ref.var_names_.begin(), ref.var_names_.end(), std::string{", "}) +
                       " WITH TYPE (\n";

        indent_ += indent_increment;
        ref.vars_type_->accept(*this);
        indent_ -= indent_increment;
        str_repres_ += ")\n";

        if(ref.vars_value_){
            str_repres_ += std::string(indent_, ' ') + "AND WITH VALUE(\n";

            indent_ += indent_increment;
            ref.vars_value_->accept(*this);
            indent_ -= indent_increment;
            str_repres_ += ")\n";
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_variables_definition& ref)
    {
        auto var_idx_to_string = [&](std::size_t var_idx){
            return u32string_to_utf8(ids_trie_->get_string(var_idx));
        };

        str_repres_ += std::string(indent_, ' ');
        if(ref.is_exported_){
            str_repres_ += "EXPORTED ";
        }

        str_repres_ += "VARIABLES "                                                                             +
                       join(var_idx_to_string, ref.var_names_.begin(), ref.var_names_.end(), std::string{", "}) +
                       " WITH TYPE (\n";

        indent_ += indent_increment;
        ref.vars_type_->accept(*this);
        indent_ -= indent_increment;
        str_repres_ += ")\n";

        if(ref.vars_value_){
            str_repres_ += std::string(indent_, ' ') + "AND WITH VALUE(\n";

            indent_ += indent_increment;
            ref.vars_value_->accept(*this);
            indent_ -= indent_increment;
            str_repres_ += ")\n";
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Constant_definition& ref)
    {
        str_repres_ += std::string(indent_, ' ')                                       +
                       "CONSTANT "                                                     +
                       u32string_to_utf8(ids_trie_->get_string(ref.name_of_constant_)) +
                       " WITH TYPE (\n";

        indent_ += indent_increment;
        ref.type_of_constant_->accept(*this);
        indent_ -= indent_increment;
        str_repres_ += ")\n";

        str_repres_ += std::string(indent_, ' ') + "AND VALUE (\n";

        indent_ += indent_increment;
        ref.value_of_constant_->accept(*this);
        indent_ -= indent_increment;
        str_repres_ += ")\n";
    }

    void To_string_visitor::visit(arkona_parser::ast::Constants_definition& ref)
    {
        str_repres_ += std::string(indent_, ' ');
        if(ref.is_exported_){
            str_repres_ += "EXPORTED ";
        }
        str_repres_ += "CONSTANTS\n";

        for(const auto& c : ref.constants_){
            indent_ += indent_increment;
            c->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_constant_definition& ref)
    {
        str_repres_ += std::string(indent_, ' ')                                       +
                       "META CONSTANT "                                                +
                       u32string_to_utf8(ids_trie_->get_string(ref.name_of_constant_)) +
                       " WITH TYPE (\n";

        indent_ += indent_increment;
        ref.type_of_constant_->accept(*this);
        indent_ -= indent_increment;
        str_repres_ += ")\n";

        str_repres_ += std::string(indent_, ' ') + "AND VALUE (\n";

        indent_ += indent_increment;
        ref.value_of_constant_->accept(*this);
        indent_ -= indent_increment;
        str_repres_ += ")\n";
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_constants_definition& ref)
    {
        str_repres_ += std::string(indent_, ' ');
        if(ref.is_exported_){
            str_repres_ += "EXPORTED ";
        }
        str_repres_ += "META CONSTANTS\n";

        for(const auto& c : ref.constants_){
            indent_ += indent_increment;
            c->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Type_definition& ref)
    {
        str_repres_ += std::string(indent_, ' ')                                   +
                       "TYPE "                                                     +
                       u32string_to_utf8(ids_trie_->get_string(ref.name_of_type_)) +
                       " AS (\n";

        indent_ += indent_increment;
        ref.definition_of_type_->accept(*this);
        indent_ -= indent_increment;
        str_repres_ += ")\n";
    }

    void To_string_visitor::visit(arkona_parser::ast::Types_definition& ref)
    {
        str_repres_ += std::string(indent_, ' ');
        if(ref.is_exported_){
            str_repres_ += "EXPORTED ";
        }
        str_repres_ += "TYPES\n";

        for(const auto& t : ref.types_){
            indent_ += indent_increment;
            t->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Func_definition& ref)
    {
        str_repres_ += std::string(indent_, ' ');
        if(ref.is_exported_){
            str_repres_ += "EXPORTED ";
        }

        if(ref.func_body_){
            str_repres_ += "FUNCTION\n";

            indent_ += indent_increment;
            ref.func_header_->accept(*this);
            indent_ -= indent_increment;

            indent_ += indent_increment;
            ref.func_body_->accept(*this);
            indent_ -= indent_increment;
        }else{
            str_repres_ += "FUNCTION PROTOTYPE\n";

            indent_ += indent_increment;
            ref.func_header_->accept(*this);
            indent_ -= indent_increment;
        }
    }

    static const std::string func_kind_name[] = {
        "", "Main ", "Pure "
    };

    void To_string_visitor::visit(arkona_parser::ast::Func_header& ref)
    {
        str_repres_ += std::string(indent_, ' ')                                +
                       func_kind_name[static_cast<unsigned>(ref.kind_)]         +
                       u32string_to_utf8(ids_trie_->get_string(ref.func_name_)) +
                       " WITH ARGUMENTS\n";

        indent_ += indent_increment;
        ref.func_signature_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Func_signature& ref)
    {
        str_repres_ += std::string(indent_, ' ');

        for(const auto& group : ref.arguments_){
            indent_ += indent_increment;
            group->accept(*this);
            indent_ -= indent_increment;
        }

        str_repres_ += std::string(indent_, ' ') + "AND RETURN VALUE TYPE\n";
        indent_ += indent_increment;
        ref.returned_type_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Formal_aguments_group& ref)
    {
        auto var_idx_to_string = [&](std::size_t var_idx){
            return u32string_to_utf8(ids_trie_->get_string(var_idx));
        };

        str_repres_ += std::string(indent_, ' ')                                                                        +
                       "ARGUMENTS GROUP: "                                                                              +
                       join(var_idx_to_string, ref.names_of_args_.begin(), ref.names_of_args_.end(), std::string{", "}) +
                       " WITH TYPE\n";
        indent_ += indent_increment;
        ref.type_of_args_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Operation_definition& ref)
    {
        str_repres_ += std::string(indent_, ' ');
        if(ref.is_exported_){
            str_repres_ += "EXPORTED ";
        }

        if(ref.op_body_){
            str_repres_ += "OPERATION\n";

            indent_ += indent_increment;
            ref.op_header_->accept(*this);
            indent_ -= indent_increment;

            indent_ += indent_increment;
            ref.op_body_->accept(*this);
            indent_ -= indent_increment;
        }else{
            str_repres_ += "OPERATION PROTOTYPE\n";

            indent_ += indent_increment;
            ref.op_header_->accept(*this);
            indent_ -= indent_increment;
        }
    }

    static const std::string operation_name[] = {
        "Prefix_inc",                  "Prefixfix_inc_with_wrapping", "Prefix_dec",
        "Prefixfix_dec_with_wrapping", "ElemType",                    "Unary_plus",
        "Unary_minus",                 "Data_size",                   "Sharp",
        "Card",                        "Address",                     "Data_address",
        "Pointer_def",                 "Postfix_inc",                 "Postfix_inc_with_wrapping",
        "Postfix_dec",                 "Postfix_dec_with_wrapping",   "Dereference",
        "Logical_or",                  "Logical_or_full",             "Logical_or_not",
        "Logical_or_not_full",         "Logical_xor",                 "Logical_and",
        "Logical_and_full",            "Logical_and_not",             "Logical_and_not_full",
        "Compare",                     "Eq",                          "Neq",
        "Lt",                          "Gt",                          "Leq",
        "Geq",                         "Type_equiv",                  "Bitwise_or",
        "Bitwise_or_not",              "Bitwise_xor",                 "Bitwise_and",
        "Bitwise_and_not",             "Left_shift, Right_shift",     "Add",
        "Sub",                         "Mul",                         "Div",
        "Remainder",                   "Float_remainder",             "Symmetric_difference",
        "Set_difference",              "Power",                       "Float_power",
        "Dimension_size",              "Assignment",                  "Copy",
        "Logical_or_assign",           "Logical_or_not_full_assign",  "Logical_or_not_assign",
        "Logical_or_full_assign",      "Logical_xor_assign",          "Logical_and_assign",
        "Logical_and_full_assign",     "Logical_and_not_assign",      "Logical_and_not_full_assign",
        "Bitwise_or_assign",           "Bitwise_or_not_assign",       "Bitwise_xor_assign",
        "Bitwise_and_assign",          "Bitwise_and_not_assign",      "Left_shift_assign",
        "Right_shift_assign",          "Plus_assign",                 "Minus_assign",
        "Mul_assign",                  "Div_assign",                  "Remainder_assign",
        "Float_remainder_assign",      "Symmetric_difference_assign", "Set_difference_assign",
        "Power_assign",                "Float_power_assign",          "Indexing",
        "Call",                        "Field_access"
    };

    static const std::string operation_kind_name[] = {
        "Ordinary", "Pure"
    };

    void To_string_visitor::visit(arkona_parser::ast::Operation_header& ref)
    {
        str_repres_ += std::string(indent_, ' ')                                +
                       operation_kind_name[static_cast<unsigned>(ref.kind_)]    +
                       operation_name[static_cast<unsigned>(ref.op_name_)]      +
                       " WITH ARGUMENTS\n";

        indent_ += indent_increment;
        ref.op_signature_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_func_definition& ref)
    {
        str_repres_ += std::string(indent_, ' ');
        if(ref.is_exported_){
            str_repres_ += "EXPORTED ";
        }

        if(ref.func_body_){
            str_repres_ += "META FUNCTION\n";

            indent_ += indent_increment;
            ref.func_header_->accept(*this);
            indent_ -= indent_increment;

            indent_ += indent_increment;
            ref.func_body_->accept(*this);
            indent_ -= indent_increment;
        }else{
            str_repres_ += "META FUNCTION PROTOTYPE\n";

            indent_ += indent_increment;
            ref.func_header_->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_func_header& ref)
    {
        str_repres_ += std::string(indent_, ' ')                                +
                       u32string_to_utf8(ids_trie_->get_string(ref.func_name_)) +
                       " WITH ARGUMENTS\n";

        indent_ += indent_increment;
        ref.func_signature_->accept(*this);
        indent_ -= indent_increment;

        if(ref.used_categories_){
            str_repres_ += std::string(indent_, ' ') + "AND WITH USED CATEGORIES\n";
            indent_ += indent_increment;
            ref.used_categories_->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_func_signature& ref)
    {
        str_repres_ += std::string(indent_, ' ');

        for(const auto& group : ref.arguments_){
            indent_ += indent_increment;
            group->accept(*this);
            indent_ -= indent_increment;
        }

        str_repres_ += std::string(indent_, ' ') + "AND RETURN VALUE TYPE\n";
        indent_ += indent_increment;
        ref.returned_type_->accept(*this);
        indent_ -= indent_increment;
    }

    static const std::string meta_group_kind_name[] = {
        "IDENTICAL TYPES", "MAYBE VARIOUS TYPES", "NON CLARIFIED TYPES"
    };

    void To_string_visitor::visit(arkona_parser::ast::Meta_func_formal_aguments_group& ref)
    {
        auto var_idx_to_string = [&](std::size_t var_idx){
            return u32string_to_utf8(ids_trie_->get_string(var_idx));
        };

        str_repres_ += std::string(indent_, ' ');
        if(ref.is_package_){
            str_repres_ += "PACKAGE ";
        }

        str_repres_ += std::string(indent_, ' ')                                                                        +
                       "ARGUMENTS GROUP: "                                                                              +
                       join(var_idx_to_string, ref.names_of_args_.begin(), ref.names_of_args_.end(), std::string{", "}) +
                       " WITH "                                                                                         +
                       meta_group_kind_name[static_cast<unsigned>(ref.kind_)]                                           +
                       "\n";
        indent_ += indent_increment;
        ref.type_of_args_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Alias_definition& ref)
    {
        str_repres_ += std::string(indent_, ' ');
        if(ref.is_exported_){
            str_repres_ += "EXPORTED ";
        }
        str_repres_ += "ALIAS "                                            +
                       u32string_to_utf8(ids_trie_->get_string(ref.name_)) +
                       " OF\n";

        indent_ += indent_increment;
        ref.body_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Scope_resolution& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "SCOPE RESOLUTION\n";
        indent_ += indent_increment;
        ref.scope_for_name_->accept(*this);
        indent_ -= indent_increment;
        str_repres_ += std::string(indent_ , ' ')                          +
                       "FOR NAME "                                         +
                       u32string_to_utf8(ids_trie_->get_string(ref.name_)) +
                       "\n";
    }

    void To_string_visitor::visit(arkona_parser::ast::Call_expr& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "CALL\n";
        indent_ += indent_increment;
        ref.caller_->accept(*this);
        indent_ -= indent_increment;
        str_repres_ += std::string(indent_, ' ') + "WITH ARGS\n";
        for(const auto& arg : ref.args_){
            indent_ += indent_increment;
            arg->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Indexed_expr& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "INDEXED EXPRESSION\n";
        indent_ += indent_increment;
        ref.indexed_->accept(*this);
        indent_ -= indent_increment;
        str_repres_ += std::string(indent_, ' ') + "WITH INDICES\n";
        for(const auto& index : ref.indices_){
            indent_ += indent_increment;
            index->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Meta_call_expr& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "META CALL\n";
        indent_ += indent_increment;
        ref.caller_->accept(*this);
        indent_ -= indent_increment;
        str_repres_ += std::string(indent_, ' ') + "WITH ARGS\n";
        for(const auto& arg : ref.args_){
            indent_ += indent_increment;
            arg->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Field_access_expr& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "FIELD ACCESS\n";
        indent_ += indent_increment;
        ref.scope_for_name_->accept(*this);
        indent_ -= indent_increment;
        str_repres_ += std::string(indent_ , ' ')                          +
                       "FOR NAME "                                         +
                       u32string_to_utf8(ids_trie_->get_string(ref.name_)) +
                       "\n";
    }

    void To_string_visitor::visit(arkona_parser::ast::Struct_construction_from_field_values& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "STRUCT CONSTRUCTION EXPRESSION FOR STRUCT\n";
        indent_ += indent_increment;
        ref.structure_->accept(*this);
        indent_ -= indent_increment;
        str_repres_ += std::string(indent_, ' ') + "WITH THE FOLLOWING VALUES OF FIELDS:\n";
        for(const auto& value : ref.values_){
            indent_ += indent_increment;
            value->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Struct_field_value_expr& ref)
    {
        str_repres_ += std::string(indent_, ' ')                                 +
                       "STRUCT FIELD VALUE EXPRESSION FOR FIELD\n"               +
                       std::string(indent_ + indent_increment, ' ')              +
                       u32string_to_utf8(ids_trie_->get_string(ref.field_name_)) +
                       "\n"                                                      +
                       std::string(indent_, ' ')                                 +
                       "AND FIELD VALUE\n";
        indent_ += indent_increment;
        ref.field_value_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Reinterpret_expr& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "REINTERPET EXPRESSION\n";
        indent_ += indent_increment;
        ref.source_expr_->accept(*this);
        indent_ -= indent_increment;

        str_repres_ += std::string(indent_, ' ') + "AS VALUE OF TYPE\n";
        indent_ += indent_increment;
        ref.dest_type_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Convert_expr& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "CONVERT EXPRESSION\n";
        indent_ += indent_increment;
        ref.source_expr_->accept(*this);
        indent_ -= indent_increment;

        str_repres_ += std::string(indent_, ' ') + "TO A VALUE OF THE TYPE\n";
        indent_ += indent_increment;
        ref.dest_type_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Tuple_expression& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "TUPLE\n";
        for(const auto& value : ref.components_){
            indent_ += indent_increment;
            value->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Set_expression& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "SET WITH VALUES\n";
        for(const auto& value : ref.values_){
            indent_ += indent_increment;
            value->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Array_expression& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "ARRAY WITH ELEMENTS\n";
        for(const auto& value : ref.values_){
            indent_ += indent_increment;
            value->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::BitArray_expression& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "BITARRAY WITH INDICES OF NONZERO ELEMENTS\n";
        for(const auto& value : ref.values_){
            indent_ += indent_increment;
            value->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Alloc_expr& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "ALLOCATE \n";
        indent_ += indent_increment;
        ref.var_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Free_expr& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "FREE \n";
        indent_ += indent_increment;
        ref.var_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Alloc_array_expr& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "ALLOCATE ARRAY WITH SIZES OF DIMENSIONS\n";
        for(const auto& dim_size : ref.dim_sizes_){
            indent_ += indent_increment;
            dim_size->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Alloc_bitarray_expr& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "ALLOCATE BITARRAY WITH SIZES OF DIMENSIONS\n";
        for(const auto& dim_size : ref.dim_sizes_){
            indent_ += indent_increment;
            dim_size->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Lambda& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "LAMBDA\n";

        indent_ += indent_increment;
        ref.func_signature_->accept(*this);
        indent_ -= indent_increment;

        indent_ += indent_increment;
        ref.body_->accept(*this);
        indent_ -= indent_increment;
    }

    static const std::string bool_kind_name[] = {
        "Bool8", "Bool16", "Bool32", "Bool64"
    };

    static const std::string ord_kind_name[] = {
        "Ord8 ", "Ord16 ", "Ord32 ", "Ord64 "
    };

    static const std::string ord_value_name[] = {
        "Lower\n", "Equal\n", "Greater\n"
    };

    static const std::string float_kind_name[] = {
        "Float32 ", "Float64 ", "Float80 ", "Float128 "
    };

    static const std::string complex_kind_name[] = {
        "Complex32 ", "Complex64 ", "Complex80 ", "Complex128 "
    };

    static const std::string quaternion_kind_name[] = {
        "Quaternion32", "Quaternion64", "Quaternion80", "Quaternion128"
    };

    void To_string_visitor::visit(arkona_parser::ast::Literal_expr& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "LITERAL ";

        const auto& value = ref.value_;
        switch(value.kind_){
            case arkona_parser::ast::Literal_kind::Boolean:
                str_repres_ += bool_kind_name[static_cast<unsigned>(value.subkind_)] +
                               (value.bool_value_ ? " true\n" : " false\n");
                break;
            case arkona_parser::ast::Literal_kind::Ord:
                str_repres_ += ord_kind_name[static_cast<unsigned>(value.subkind_)] +
                               ord_value_name[static_cast<unsigned>(value.ordering_value_)];
                break;
            case arkona_parser::ast::Literal_kind::Nullptr:
                str_repres_ += "NULLPTR\n";
                break;
            case arkona_parser::ast::Literal_kind::Char:
                switch(static_cast<arkona_parser::ast::Char_kind>(value.subkind_))
                {
                    case arkona_parser::ast::Char_kind::Char8:
                        str_repres_ += "Char8 " + show_char32(value.char_val_) + "\n";
                        break;
                    case arkona_parser::ast::Char_kind::Char16:
                        str_repres_ += "Char16 " + show_char32(value.char_val_) + "\n";
                        break;
                    case arkona_parser::ast::Char_kind::Char32:
                        str_repres_ += "Char32 " + show_char32(value.char_val_) + "\n";
                        break;
                    default:
                        ;
                }
                break;
            case arkona_parser::ast::Literal_kind::String:
                switch(static_cast<arkona_parser::ast::String_kind>(value.subkind_))
                {
                    case arkona_parser::ast::String_kind::String8:
                        str_repres_ += "String8 " + idx_to_string(str_trie_, value.str_index_) + "\n";
                        break;
                    case arkona_parser::ast::String_kind::String16:
                        str_repres_ += "String16 " + idx_to_string(str_trie_, value.str_index_) + "\n";
                        break;
                    case arkona_parser::ast::String_kind::String32:
                        str_repres_ += "String32 " + idx_to_string(str_trie_, value.str_index_) + "\n";
                        break;
                    default:
                        ;
                }
                break;
            case arkona_parser::ast::Literal_kind::Integer:
                str_repres_ += "INTEGER " + ::to_string(value.int_val_) + "\n";
                break;
            case arkona_parser::ast::Literal_kind::Float:
                str_repres_ += float_kind_name[static_cast<unsigned>(value.subkind_)] +
                               ::to_string(value.float_val_)                          +
                               "\n";
                break;
            case arkona_parser::ast::Literal_kind::Complex:
                str_repres_ += complex_kind_name[static_cast<unsigned>(value.subkind_)] +
                               ::to_string(cimagq(value.complex_val_))                  +
                               "i\n";
                break;
            case arkona_parser::ast::Literal_kind::Quaternion:
                str_repres_ += quaternion_kind_name[static_cast<unsigned>(value.subkind_)] +
                               ::to_string(value.quat_val_[0]) +
                               ::to_string(value.quat_val_[1]) + "i" +
                               ::to_string(value.quat_val_[2]) + "j" +
                               ::to_string(value.quat_val_[3]) + "k\n";
                break;
            default:
                ;
        }
    }

    static const std::string fixed_size_base_type_kind_name[] = {
        "unsigned8",  "unsigned16",  "unsigned32",  "unsigned64", "unsigned128",
        "signed8",    "signed16",    "signed32",    "signed64",   "signed128",
        "fp32",       "fp64",        "fp80",        "fp128",      "q32",
        "q64",        "q80",         "q128",        "c32",        "c64",
        "c80",        "c128",        "bool8",       "bool16",     "bool32",
        "bool64",     "ord8",        "ord16",       "ord32",      "ord64",
        "character8", "character16", "character32", "string8",    "string16",
        "string32",   "pusto"

    };

    void To_string_visitor::visit(arkona_parser::ast::Fixed_size_base_type& ref)
    {
         str_repres_ += std::string(indent_, ' ')                                        +
                        "FIXED SIZE TYPE "                                               +
                        fixed_size_base_type_kind_name[static_cast<unsigned>(ref.kind_)] +
                        "\n";
    }

    static const std::string sizeable_base_type_kind_name[] = {
        "unsigned_int\n", "fp\n",      "quat\n",
        "complex\n",      "boolean\n", "ordering\n",
        "character\n",    "string\n",  "signed_int\n"
    };

    static const std::string modifiers_kind_name[] = {
        "short_modifier ", "long_modifier "
    };

    void To_string_visitor::visit(arkona_parser::ast::Sizeable_base_type& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "SIZEABLE BASE TYPE ";
        for(const auto m : ref.modifiers_){
            str_repres_ += modifiers_kind_name[static_cast<unsigned>(m)];
        }
        str_repres_ += sizeable_base_type_kind_name[static_cast<unsigned>(ref.kind_)];
    }

    void To_string_visitor::visit(arkona_parser::ast::Array_type_expr& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "ARRAY TYPE WITH DIMENSIONS\n";
        for(const auto& dim : ref.dims_){
            str_repres_ += std::string(indent_, ' ') + "DIM ";
            indent_ += indent_increment;
            if(dim){
                str_repres_ += "\n";
                dim->accept(*this);
            }else{
                str_repres_ += "?\n";
            }
            indent_ -= indent_increment;
        }
        str_repres_ += std::string(indent_, ' ') + "AND WITH ELEMENT TYPE\n";
        indent_ += indent_increment;
        ref.elem_type_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::BitArray_type_expr& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "BITARRAY TYPE WITH DIMENSIONS\n";
        for(const auto& dim : ref.dims_){
            str_repres_ += std::string(indent_, ' ') + "DIM ";
            indent_ += indent_increment;
            if(dim){
                str_repres_ += "\n";
                dim->accept(*this);
            }else{
                str_repres_ += "?\n";
            }
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Reference_type_expr& ref)
    {

        str_repres_ += std::string(indent_, ' ')                                   +
                       (ref.is_const_ ? "CONST REFERENCE TO\n" : "REFERENCE TO\n");
        indent_ += indent_increment;
        ref.elem_type_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Enum_type_expr& ref)
    {
        auto var_idx_to_string = [&](std::size_t var_idx){
            return u32string_to_utf8(ids_trie_->get_string(var_idx));
        };

        str_repres_ += std::string(indent_, ' ')                                                                  +
                       "ENUM TYPE "                                                                               +
                       u32string_to_utf8(ids_trie_->get_string(ref.enum_name_))                                   +
                       "{"                                                                                        +
                       join(var_idx_to_string, ref.components_.begin(), ref.components_.end(), std::string{", "}) +
                       "}\n";
    }

    void To_string_visitor::visit(arkona_parser::ast::EnumSet_type_expr& ref)
    {

        str_repres_ += std::string(indent_, ' ')    +
                       "ENUM_SET WITH ELEMENT TYPE\n";
        indent_ += indent_increment;
        ref.elem_type_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Auto_type_expr&)
    {

        str_repres_ += std::string(indent_, ' ') + "AUTO_TYPE\n";
    }

    void To_string_visitor::visit(arkona_parser::ast::Type&)
    {

        str_repres_ += std::string(indent_, ' ') + "TYPE_TYPE\n";
    }

    void To_string_visitor::visit(arkona_parser::ast::Func_ptr_type_expr& ref)
    {
        str_repres_ += std::string(indent_, ' ')     +
                       (ref.is_pure_ ? "PURE " : "") +
                       "FUNCTION POINTER TYPE\n";

        indent_ += indent_increment;
        ref.func_signature_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Struct_type_expr& ref)
    {
        str_repres_ += std::string(indent_, ' ')                                 +
                      "STRUCT TYPE "                                             +
                      u32string_to_utf8(ids_trie_->get_string(ref.struct_name_)) +
                      " WITH GROUPS OF FIELDS\n";

        for(const auto& group : ref.groups_){
            indent_ += indent_increment;
            group->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Struct_fields_group& ref)
    {
        auto var_idx_to_string = [&](std::size_t var_idx){
            return u32string_to_utf8(ids_trie_->get_string(var_idx));
        };

        str_repres_ += std::string(indent_, ' ')                                                                            +
                       "FIELDS GROUP: "                                                                                     +
                       join(var_idx_to_string, ref.names_of_fields_.begin(), ref.names_of_fields_.end(), std::string{", "}) +
                       " WITH TYPE\n";
        indent_ += indent_increment;
        ref.type_of_fields_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Used_categories& ref)
    {
        for(const auto& c : ref.categories_){
            indent_ += indent_increment;
            c->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Used_category& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "CATEGORY\n";

        indent_ += indent_increment;
        ref.name_->accept(*this);
        indent_ -= indent_increment;

        str_repres_ += std::string(indent_, ' ') + "WITH ACTUAL ARGS\n";

        for(const auto& arg : ref.args_){
            indent_ += indent_increment;
            arg->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Category_definition& ref)
    {
        str_repres_ += std::string(indent_, ' ')                           +
                       "DEFINITION OF CATEGORY "                           +
                       u32string_to_utf8(ids_trie_->get_string(ref.name_)) +
                       "WITH FORMAL ARGUMENTS\n";

        for(const auto& arg : ref.formal_args_){
            indent_ += indent_increment;
            arg->accept(*this);
            indent_ -= indent_increment;
        }

        if(ref.used_categories_){
            str_repres_ += std::string(indent_, ' ') + "AND WITH USED CATEGORIES\n";
            indent_ += indent_increment;
            ref.used_categories_->accept(*this);
            indent_ -= indent_increment;
        }

        str_repres_ += std::string(indent_, ' ') + "AND BODY\n";

        indent_ += indent_increment;
        ref.body_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Category_definition_body& ref)
    {
        for(const auto& e : ref.entries_){
            indent_ += indent_increment;
            e->accept(*this);
            indent_ -= indent_increment;
        }
    }

    void To_string_visitor::visit(arkona_parser::ast::Const_entry& ref)
    {
        str_repres_ += std::string(indent_, ' ')                           +
                       "CONST ENTRY "                                      +
                       u32string_to_utf8(ids_trie_->get_string(ref.name_)) +
                       "WITH TYPE\n";

        indent_ += indent_increment;
        ref.type_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Implement_category& ref)
    {
        str_repres_ += std::string(indent_, ' ') + "DERIVE CATEGORY\n";

        indent_ += indent_increment;
        ref.name_->accept(*this);
        indent_ -= indent_increment;

        str_repres_ += std::string(indent_, ' ') + "FOR TYPE\n";

        indent_ += indent_increment;
        ref.type_->accept(*this);
        indent_ -= indent_increment;
    }

    void To_string_visitor::visit(arkona_parser::ast::Category_implementation& ref)
    {
//         str_repres_ += std::string(indent_, ' ') + "DERIVE CATEGORY\n";
//
//         indent_ += indent_increment;
//         ref.name_->accept(*this);
//         indent_ -= indent_increment;
//
//         str_repres_ += std::string(indent_, ' ') + "FOR TYPE\n";
//
//         indent_ += indent_increment;
//         ref.type_->accept(*this);
//         indent_ -= indent_increment;
    }
};