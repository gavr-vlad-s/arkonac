/*
    File:    recursive-parser-testing.cpp
    Created: 06 December 2020 at 17:16 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <string>
#include <cstdio>
#include <memory>
#include "../../file_utils/include/get_processed_text.h"
#include "../../scanner/include/location.h"
#include "../../tries/include/errors_and_tries.h"
#include "../../tries/include/error_count.h"
#include "../../tries/include/warning_count.h"
#include "../../tries/include/char_trie.h"
#include "../../tries/include/trie_for_vector.h"
#include "../../snd-lvl-scanner/include/arkona-scanner-snd-lvl.h"
#include "../include/token-classification-testing.hpp"
#include "../../recursive-parser-lib/include/symbol_table.hpp"
#include "../../recursive-parser-lib/include/recursive-parser.hpp"
#include "../include/print-ast.hpp"

enum Exit_codes{
    Success, File_processing_error, Parsing_failed
};

int main(int argc, char* argv[])
{
    recursive_parser_testing::classification_testing();

    if(argc > 1){
        auto              text      = file_utils::get_processed_text(argv[1]);
        if(!text.length()){
            return File_processing_error;
        }

        char32_t*         p         = const_cast<char32_t*>(text.c_str());
        auto              loc       = std::make_shared<ascaner::Location>(p);
        Errors_and_tries  et;
        et.ec_                      = std::make_shared<Error_count>();
        et.wc_                      = std::make_shared<Warning_count>();
        et.ids_trie_                = std::make_shared<Char_trie>();
        et.strs_trie_               = std::make_shared<Char_trie>();

        auto module_name_parts_trie = std::make_shared<Trie_for_vector<std::size_t>>();
        auto snd_lvl_scanner        = std::make_shared<arkona_scanner_snd_lvl::Scanner>(loc, et, module_name_parts_trie);

        auto symbol_table           = std::make_shared<arkona_parser::Symbol_table>(module_name_parts_trie, et.ids_trie_);

        arkona_parser::ParserArgs parser_args;
        parser_args.symbol_table_ = symbol_table;
        parser_args.et_           = et;
        parser_args.scanner_      = snd_lvl_scanner;

        auto parser                 = std::make_shared<arkona_parser::Parser>(parser_args);

        auto compilation_result     = parser->compile();

        et.ec_->print();
        et.wc_->print();

        puts("\nAST:\n");
        recursive_parser_testing::print_ast(compilation_result, symbol_table, et.ids_trie_, et.strs_trie_);

        if(et.ec_->get_number_of_errors()){
            return Parsing_failed;
        }

        return Success;
    }

    return Success;
}