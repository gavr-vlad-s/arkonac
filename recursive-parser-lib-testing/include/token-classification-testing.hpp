/*
    File:    token-classification-testing.hpp
    Created: 06 December 2020 at 17:19 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef TOKEN_CLASSIFICATION_TESTING_H
#define TOKEN_CLASSIFICATION_TESTING_H
namespace recursive_parser_testing{
    void classification_testing();
};
#endif