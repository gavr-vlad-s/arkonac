/*
    File:    print-ast.hpp
    Created: 11 January 2021 at 19:27 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef PRINT_AST_H
#define PRINT_AST_H
#   include <memory>
#   include <string>
#   include "../../recursive-parser-lib/include/ast_node.hpp"
#   include "../../recursive-parser-lib/include/symbol_table.hpp"
#   include "../../tries/include/char_trie.h"
namespace recursive_parser_testing{
    void print_ast(const arkona_parser::ast::AST&                      ast,
                   const std::shared_ptr<arkona_parser::Symbol_table>& symbol_table,
                   const std::shared_ptr<Char_trie>&                   ids_trie,
                   const std::shared_ptr<Char_trie>&                   str_trie);

    std::string ast2string(const arkona_parser::ast::AST&                      ast,
                           const std::shared_ptr<arkona_parser::Symbol_table>& symbol_table,
                           const std::shared_ptr<Char_trie>&                   ids_trie,
                           const std::shared_ptr<Char_trie>&                   str_trie);
};
#endif