/*
    File:    file_contents.cpp
    Author:  Гаврилов Владимир Сергеевич
    Created: 4 February 2016, 13:10
    e-mails: vladimir.s.gavrilov@gmail.com,
             gavrilov.vladimir.s@mail.ru,
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstdio>
#include <memory>
#include <boost/filesystem/operations.hpp>
#include "../include/file_contents.h"
#include "../include/fsize.h"

namespace fs = boost::filesystem;

namespace file_utils{
    class Binary_file{
    public:
        Binary_file() = default;
        Binary_file(const char* name) : fptr(fopen(name, "rb")) {};
        ~Binary_file() {fclose(fptr);};

        FILE* get() const {return fptr;};
    private:
        FILE* fptr = 0;
    };

    Contents get_contents(const char* name)
    {
        Contents result= std::make_pair(Return_code::Normal, std::string{});
        if(!fs::exists(name) || fs::is_directory(name)){
            result.first = Return_code::Impossible_open;
            return result;
        }

        Binary_file f {name};
        FILE* fptr = f.get();
        if(!fptr){
            result.first = Return_code::Impossible_open;
            return result;
        }
        long file_size = fsize(fptr);
        if(!file_size){
            return result;
        }
        auto     text    = std::make_unique<char[]>(file_size + 1);
        char*    q       = text.get();
        size_t   fr      = fread(q, 1, file_size, fptr);
        if(fr < (unsigned long)file_size){
            result.first = Return_code::Read_error;
            return result;
        }
        text[file_size]  = 0;
        result.second    = std::string{text.get()};
        return result;
    }
};



