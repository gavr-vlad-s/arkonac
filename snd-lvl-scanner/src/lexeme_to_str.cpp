/*
    File:    lexeme_to_str.cpp
    Created: 04 January 2020 at 16:15 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/lexeme_to_str.h"
#include "../../numbers/include/float128_to_string.h"
#include "../../numbers/include/int128_to_str.h"
#include "../../tries/include/idx_of_vector_to_string.h"
#include "../../tries/include/idx_to_string.h"
#include "../../strings-lib/include/join.h"
#include "../../char-conv/include/print_char32.h"

namespace arkona_scanner_snd_lvl{
    static const std::string kind_strs[] = {
        "Nothing ",   "UnknownLexem ", "Keyword ", "Id ",
        "Delimiter ", "Char ",         "String ",  "Integer ",
        "Float ",     "Complex ",      "Quat "
    };

    static const std::string keyword_kind_strs[] = {
        "Kw_bezzn",         "Kw_bezzn8",        "Kw_bezzn16",       "Kw_bezzn32",
        "Kw_bezzn64",       "Kw_bezzn128",      "Kw_bolshe",        "Kw_bolshoe",
        "Kw_v",             "Kw_vechno",        "Kw_veshch",        "Kw_veshch32",
        "Kw_veshch64",      "Kw_veshch80",      "Kw_veshch128",     "Kw_vozvrat",
        "Kw_vyberi",        "Kw_vydeli",        "Kw_vyjdi",         "Kw_golovnaya",
        "Kw_dlya",          "Kw_esli",          "Kw_iz",            "Kw_inache",
        "Kw_ines",          "Kw_ispolzuet",     "Kw_istina",        "Kw_kak",
        "Kw_kategorija",    "Kw_kvat",          "Kw_kvat32",        "Kw_kvat64",
        "Kw_kvat80",        "Kw_kvat128",       "Kw_kompl",         "Kw_kompl32",
        "Kw_kompl64",       "Kw_kompl80",       "Kw_kompl128",      "Kw_konst",
        "Kw_log",           "Kw_log8",          "Kw_log16",         "Kw_log32",
        "Kw_log64",         "Kw_lozh",          "Kw_lyambda",       "Kw_malenkoe",
        "Kw_massiv",        "Kw_menshe",        "Kw_meta",          "Kw_modul",
        "Kw_nichto",        "Kw_operaciya",     "Kw_osvobodi",      "Kw_paket",
        "Kw_pauk",          "Kw_perem",         "Kw_perech_mnozh",  "Kw_perechislenie",
        "Kw_povtoryaj",     "Kw_poka",          "Kw_pokuda",        "Kw_poryadok",
        "Kw_poryadok8",     "Kw_poryadok16",    "Kw_poryadok32",    "Kw_poryadok64",
        "Kw_postfiksnaya",  "Kw_preobrazuj",    "Kw_prefiksnaya",   "Kw_psevdonim",
        "Kw_pusto",         "Kw_razbor",        "Kw_rassmatrivaj",  "Kw_realizacija",
        "Kw_realizuj",      "Kw_samo",          "Kw_simv",          "Kw_simv8",
        "Kw_simv16",        "Kw_simv32",        "Kw_sravni_s",      "Kw_ssylka",
        "Kw_stroka",        "Kw_stroka8",       "Kw_stroka16",      "Kw_stroka32",
        "Kw_struktura",     "Kw_tip",           "Kw_to",            "Kw_funktsiya",
        "Kw_ravno",         "Kw_tsel",          "Kw_tsel8",         "Kw_tsel16",
        "Kw_tsel32",        "Kw_tsel64",        "Kw_tsel128",       "Kw_chistaya",
        "Kw_shkala",        "Kw_shkalu",        "Kw_ekviv",         "Kw_eksport",
        "Kw_element"
    };

    static const std::string float_subkind_strs[] = {
        "[Float32] ", "[Float64] ", "[Float80] ", "[Float128] "
    };

    static const std::string complex_subkind_strs[] = {
        "[Complex32] ", "[Complex64] ", "[Complex80] ", "[Complex128] "
    };

    static const std::string quat_subkind_strs[] = {
        "[Quat32] ", "[Quat64] ", "[Quat80] ", "[Quat128] "
    };

    static const std::string char_kind_strs[] = {
        "[Char8] ", "[Char16] ", "[Char32] "
    };

    static const std::string string_kind_strs[] = {
        "[String8] ", "[String16] ", "[String32] "
    };

    static const std::string delim_subkind_strs[] = {
        "Copy",                   "Assign",                      "Logical_or_assign",
        "Logical_or_full_assign", "Logical_or_not_assign",       "Logical_or_not_full_assign",
        "Logical_xor_assign",     "Logical_and_assign",          "Logical_and_full_assign",
        "Logical_and_not_assign", "Logical_and_not_full_assign", "Bitwise_or_assign",
        "Bitwise_or_not_assign",  "Bitwise_xor_assign",          "Bitwise_and_assign",
        "Bitwise_and_not_assign", "Left_shift_assign",           "Right_shift_assign",
        "Plus_assign",            "Minus_assign",                "Meta_plus_assign",
        "Mul_assign",             "Div_assign",                  "Remainder_assign",
        "Float_remainder_assign", "Symmetric_difference_assign", "Set_difference_assign",
        "Power_assign",           "Float_power_assign",          "Cond_op",
        "Cond_op_full",           "Logical_or",                  "Logical_or_full",
        "Logical_or_not",         "Logical_or_not_full",         "Logical_xor",
        "Logical_and",            "Logical_and_full",            "Logical_and_not",
        "Logical_and_not_full",   "Logical_not",                 "Compare_with",
        "EQ",                     "NEQ",                         "GT",
        "LT",                     "GEQ",                         "LEQ",
        "Bitwise_or",             "Bitwise_or_not",              "Bitwise_xor",
        "Bitwise_and",            "Bitwise_and_not",             "Left_shift",
        "Right_shift",            "Bitwise_not",                 "Plus",
        "Minus",                  "Algebraic_sep",               "Meta_plus",
        "Mul",                    "Div",                         "Remainder",
        "Float_remainder",        "Symmetric_difference",        "Set_difference",
        "Power",                  "Float_power",                 "Inc",
        "Wrap_inc",               "Dec",                         "Wrap_dec",
        "ElemType",               "Card",                        "Sharp",
        "Data_size",              "At",                          "Address",
        "Data_address",           "Expr_type",                   "Round_br_opened",
        "Round_br_closed",        "Sq_br_opened",                "Sq_br_closed",
        "Fig_br_opened",          "Fig_br_closed",               "Tuple_begin",
        "Tuple_end",              "Meta_open_br",                "Meta_closed_br",
        "Colon_sq_br_opened",     "Colon_sq_br_closed",          "Colon_fig_br_opened",
        "Colon_fig_br_closed",    "Comma",                       "Right_arrow",
        "Point",                  "Range",                       "Colon",
        "Scope_resol",            "Semicolon",                   "Left_arrow",
        "Non_specific",           "Cycle_name_prefix",           "Deduce_arg_type",
        "Pattern",
    };

    static const std::string scope_resolution = "::";

    std::string to_string(const Lexeme_info&                li,
                          const Char_trie_ptr&              ids_trie,
                          const Char_trie_ptr&              strs_trie,
                          const Module_name_parts_trie_ptr& module_name_parts_trie)
    {
        std::string result;
        auto        lc     = li.code_;
        auto        lk     = lc.kind_;
        auto        lsk    = lc.subkind_;
        result             = kind_strs[static_cast<unsigned>(lk)];
        switch(lk){
            case Lexem_kind::Nothing: case Lexem_kind::UnknownLexem:
                break;
            case Lexem_kind::Id:
                result += join2(idx_of_vector_to_string(module_name_parts_trie,
                                                        ids_trie,
                                                        li.id_info_.qualifying_prefix_),
                                idx_to_string(ids_trie, li.id_info_.id_idx_),
                                scope_resolution);
                break;
            case Lexem_kind::Keyword:
                result += keyword_kind_strs[lsk];
                break;
            case Lexem_kind::Char:
                result += char_kind_strs[lsk] + show_char32(li.char_val_);
                break;
            case Lexem_kind::String:
                result += string_kind_strs[lsk] + idx_to_string(strs_trie, li.str_index_);
                break;
            case Lexem_kind::Integer:
                result += ::to_string(li.int_val_);
                break;
            case Lexem_kind::Float:
                result += float_subkind_strs[lsk] + ::to_string(li.float_val_);
                break;
            case Lexem_kind::Complex:
                result += complex_subkind_strs[lsk]            +
                          ::to_string(cimagq(li.complex_val_)) +
                          "i";
                break;
            case Lexem_kind::Quat:
                result += quat_subkind_strs[lsk]       +
                          ::to_string(li.quat_val_[0]) +
                          ::to_string(li.quat_val_[1]) + "i" +
                          ::to_string(li.quat_val_[2]) + "j" +
                          ::to_string(li.quat_val_[3]) + "k";
                break;
            case Lexem_kind::Delimiter:
                result += join2(idx_of_vector_to_string(module_name_parts_trie,
                                                        ids_trie,
                                                        li.delim_info_.qualifying_prefix_),
                                delim_subkind_strs[static_cast<uint8_t>(li.delim_info_.delim_kind_)],
                                scope_resolution);
                break;
            default:
                ;
        }
        return result;
    }
};
