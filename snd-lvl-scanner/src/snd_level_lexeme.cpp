/*
    File:    snd_level_lexeme.cpp
    Created: 02 January 2020 at 15:18 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <tuple>
#include "../include/snd_level_lexeme.h"
#include <cstdio>
namespace arkona_scanner_snd_lvl{
    bool operator==(const Lexeme_info& lhs, const Lexeme_info& rhs)
    {
        bool result = true;
        if(lhs.code_.kind_ != rhs.code_.kind_){
            puts("Kinds of lexemes are not equal.");
            return false;
        }
        switch(lhs.code_.kind_){
            case Lexem_kind::Nothing: case Lexem_kind::UnknownLexem:
                break;
            case Lexem_kind::Keyword:
                if(lhs.code_.subkind_ != rhs.code_.subkind_)
                {
                    puts("These keywords are different");
                }
                return lhs.code_.subkind_ == rhs.code_.subkind_;
                break;
            case Lexem_kind::Id:
                result = std::tie(lhs.id_info_.id_idx_, lhs.id_info_.qualifying_prefix_) ==
                         std::tie(rhs.id_info_.id_idx_, rhs.id_info_.qualifying_prefix_);
                if(!result){
                    printf("lhs.id_info_.id_idx_, lhs.id_info_.qualifying_prefix_ : %zu, %zu\n",
                           lhs.id_info_.id_idx_, lhs.id_info_.qualifying_prefix_);
                    printf("rhs.id_info_.id_idx_, rhs.id_info_.qualifying_prefix_ : %zu, %zu\n",
                           rhs.id_info_.id_idx_, rhs.id_info_.qualifying_prefix_);
                }
                return result;
                break;
            case Lexem_kind::Delimiter:
                return std::tie(lhs.delim_info_.delim_kind_, lhs.delim_info_.qualifying_prefix_) ==
                       std::tie(rhs.delim_info_.delim_kind_, rhs.delim_info_.qualifying_prefix_);
                break;
            case Lexem_kind::String:
                return std::tie(lhs.code_.subkind_, lhs.str_index_) ==
                       std::tie(rhs.code_.subkind_, rhs.str_index_);
                break;
            case Lexem_kind::Integer:
                return lhs.int_val_ == rhs.int_val_;
                break;
            case Lexem_kind::Float:
                return (lhs.code_.subkind_ == rhs.code_.subkind_)          &&
                       fabsq(lhs.float_val_ - rhs.float_val_    ) < 1e-22q;
                break;
            case Lexem_kind::Complex:
                return (lhs.code_.subkind_ == rhs.code_.subkind_) &&
                       cabsq(lhs.complex_val_ - rhs.complex_val_ ) < FLT128_EPSILON;
                break;
            case Lexem_kind::Quat:
                return (lhs.code_.subkind_ == rhs.code_.subkind_)                           &&
                       sqrtq(quat::norm_in_square(lhs.quat_val_ - rhs.quat_val_)) < 1e-22q;
                break;
            case Lexem_kind::Char:
                return std::tie(lhs.code_.subkind_, lhs.char_val_) ==
                       std::tie(rhs.code_.subkind_, rhs.char_val_);
                break;
        }
        return result;
    }
};