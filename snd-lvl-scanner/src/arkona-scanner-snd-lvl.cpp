/*
    File:    arkona-scanner-snd-lvl.cpp
    Created: 03 January 2020 at 15:50 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <map>
#include <cstdio>
#include "../include/arkona-scanner-snd-lvl.h"
#include "../include/lexeme_to_str.h"
#include "../../tries/include/idx_to_string.h"
#include "../../char-conv/include/print_char32.h"

namespace arkona_scanner_snd_lvl{
    Scanner::Scanner(const ascaner::Location_ptr&                        location,
                    const Errors_and_tries&                              et,
                    const std::shared_ptr<Trie_for_vector<std::size_t>>& module_name_parts_trie)
    {
        ids_                     = et.ids_trie_;
        strs_                    = et.strs_trie_;
        en_                      = et.ec_;
        wn_                      = et.wc_;
        loc_                     = location;
        lexeme_begin_            = location->pcurrent_char_;
        token_.range_.begin_pos_ = ascaner::Position();
        token_.range_.end_pos_   = ascaner::Position();
        lexeme_pos_.begin_pos_   = ascaner::Position();
        lexeme_pos_.end_pos_     = ascaner::Position();
        low_level_scanner_       = std::make_shared<arkona_scanner::Scanner>(location, et);
        module_name_parts_trie_  = module_name_parts_trie;
    }


    static const Delimiter_kind delim_mapping[] = {
        Copy,                        Assign,                      Logical_or_assign,
        Logical_or_full_assign,      Logical_or_not_assign,       Logical_or_not_full_assign,
        Logical_xor_assign,          Logical_and_assign,          Logical_and_full_assign,
        Logical_and_not_assign,      Logical_and_not_full_assign, Bitwise_or_assign,
        Bitwise_or_not_assign,       Bitwise_xor_assign,          Bitwise_and_assign,
        Bitwise_and_not_assign,      Left_shift_assign,           Right_shift_assign,
        Plus_assign,                 Minus_assign,                Meta_plus_assign,
        Mul_assign,                  Div_assign,                  Remainder_assign,
        Float_remainder_assign,      Symmetric_difference_assign, Set_difference_assign,
        Power_assign,                Float_power_assign,          Cond_op,
        Cond_op_full,                Logical_or,                  Logical_or_full,
        Logical_or_not,              Logical_or_not_full,         Logical_xor,
        Logical_or_not,              Logical_and,                 Logical_and_full,
        Logical_and_not,             Logical_and_not_full,        Logical_and_not,
        Logical_not,                 Compare_with,                EQ,
        NEQ,                         GT,                          LT,
        GEQ,                         LEQ,                         Bitwise_or,
        Bitwise_or_not,              Bitwise_xor,                 Bitwise_and,
        Bitwise_and_not,             Left_shift,                  Right_shift,
        Bitwise_not,                 Plus,                        Minus,
        Algebraic_sep,               Meta_plus,                   Algebraic_sep,
        Meta_plus,                   Mul,                         Div,
        Remainder,                   Float_remainder,             Symmetric_difference,
        Set_difference,              Power,                       Float_power,
        Inc,                         Wrap_inc,                    Dec,
        Wrap_dec,                    ElemType,                    ElemType,
        Card,                        Sharp,                       Data_size,
        Card,                        At,                          Address,
        Data_address,                ExprType,                    Address,
        Data_address,                ExprType,                    Div,
        Mul,                         Round_br_opened,             Round_br_closed,
        Sq_br_opened,                Sq_br_closed,                Fig_br_opened,
        Fig_br_closed,               Tuple_begin,                 Tuple_end,
        Meta_open_br,                Meta_closed_br,              Colon_sq_br_opened,
        Colon_sq_br_closed,          Colon_fig_br_opened,         Colon_fig_br_closed,
        Comma,                       Right_arrow,                 Point,
        Range,                       Colon,                       Scope_resol,
        Semicolon,                   Left_arrow,                  Non_specific,
        Non_specific,                Cycle_name_prefix,           Deduce_arg_type,
        Pattern,                     Pattern,
    };

    static Arkona_token_snd_lvl convert_token(const Arkona_token_low_lvl& low_lvl_token)
    {
        Arkona_token_snd_lvl result;
        auto                 low_lvl_lexeme  = low_lvl_token.lexeme_;
        auto                 low_lvl_code    = low_lvl_lexeme.code_;
        uint8_t              low_lvl_subkind = low_lvl_code.subkind_;
        result.range_ = low_lvl_token.range_;
        switch(low_lvl_code.kind_){
            case arkona_scanner::Lexem_kind::Nothing:
                result.lexeme_.code_.kind_                    = Lexem_kind::Nothing;
                break;
            case arkona_scanner::Lexem_kind::UnknownLexem:
                result.lexeme_.code_.kind_                    = Lexem_kind::UnknownLexem;
                break;
            case arkona_scanner::Lexem_kind::Integer:
                result.lexeme_.code_.kind_                    = Lexem_kind::Integer;
                result.lexeme_.int_val_                       = low_lvl_lexeme.int_val_;
                break;
            case arkona_scanner::Lexem_kind::Float:
                result.lexeme_.code_.kind_                    = Lexem_kind::Float;
                result.lexeme_.code_.subkind_                 = low_lvl_subkind;
                result.lexeme_.float_val_                     = low_lvl_lexeme.float_val_;
                break;
            case arkona_scanner::Lexem_kind::Complex:
                result.lexeme_.code_.kind_                    = Lexem_kind::Complex;
                result.lexeme_.code_.subkind_                 = low_lvl_subkind;
                result.lexeme_.complex_val_                   = low_lvl_lexeme.complex_val_;
                break;
            case arkona_scanner::Lexem_kind::Quat:
                result.lexeme_.code_.kind_                    = Lexem_kind::Quat;
                result.lexeme_.code_.subkind_                 = low_lvl_subkind;
                result.lexeme_.quat_val_                      = low_lvl_lexeme.quat_val_;
                break;
            case arkona_scanner::Lexem_kind::Keyword:
                result.lexeme_.code_.kind_                    = Lexem_kind::Keyword;
                result.lexeme_.code_.subkind_                 = low_lvl_subkind;
                if(result.lexeme_.code_.subkind_ == static_cast<uint8_t>(Keyword_kind::Kw_sravni_s)){
                    result.lexeme_.code_.kind_                    = Lexem_kind::Delimiter;
                    result.lexeme_.delim_info_.qualifying_prefix_ = 0;
                    result.lexeme_.delim_info_.delim_kind_        = Delimiter_kind::Compare_with;
                }
                break;
            case arkona_scanner::Lexem_kind::Char:
                result.lexeme_.code_.kind_                    = Lexem_kind::Char;
                result.lexeme_.code_.subkind_                 = static_cast<uint8_t>(Char_kind::Char32);
                result.lexeme_.char_val_                      = low_lvl_lexeme.char_val_;
                break;
            case arkona_scanner::Lexem_kind::Delimiter:
                result.lexeme_.code_.kind_                    = Lexem_kind::Delimiter;
                result.lexeme_.delim_info_.qualifying_prefix_ = 0;
                result.lexeme_.delim_info_.delim_kind_        = delim_mapping[static_cast<uint8_t>(low_lvl_subkind)];
                break;
            default:
                ;
        }
        return result;
    }


    struct Encodings{
        Char_kind   char_encoding_;
        String_kind str_encoding_;
    };

    static const std::map<std::u32string, Encodings> encodings = {
        {U"u8",  {Char_kind::Char8,  String_kind::String8 }},
        {U"u16", {Char_kind::Char16, String_kind::String16}},
        {U"u32", {Char_kind::Char32, String_kind::String32}}
    };

    template<typename... T>
    static void print_diagnostic(const char* msg, T... args)
    {
        printf(msg, args...);
    }


    Low_level_lexem_kind Scanner::get_low_level_kind()
    {
        low_lvl_token_         = low_level_scanner_->current_lexeme();
        low_lvl_lexem_code_    = low_lvl_token_.lexeme_.code_;
        low_lvl_lexem_kind_    = low_lvl_token_.lexeme_.code_.kind_;
        low_lvl_lexem_subkind_ = low_lvl_token_.lexeme_.code_.subkind_;
        return low_lvl_lexem_kind_;
    }


    void Scanner::get_maybe_qualified_lexeme()
    {
        auto id_str                                = ids_->get_string(low_lvl_token_.lexeme_.id_index_);
        auto it                                    = encodings.find(id_str);
        token_.lexeme_.code_.kind_                 = Lexem_kind::Id;
        token_.lexeme_.id_info_.qualifying_prefix_ = 0;
        token_.lexeme_.id_info_.id_idx_            = low_lvl_token_.lexeme_.id_index_;

        if(it == encodings.end()){
            return;
        }

        auto enc = it->second;
        ch_      = *(loc_->pcurrent_char_);
        switch(ch_){
            case U'\'': case U'$':
                low_lvl_token_                = low_level_scanner_->current_lexeme();
                token_.lexeme_.char_val_      = low_lvl_token_.lexeme_.char_val_;
                token_.lexeme_.code_.kind_    = Lexem_kind::Char;
                token_.lexeme_.code_.subkind_ = static_cast<uint8_t>(enc.char_encoding_);
                token_.range_.end_pos_        = low_lvl_token_.range_.end_pos_;
                lexeme_pos_                   = token_.range_;
                break;
            case U'\"':
                low_lvl_token_                = low_level_scanner_->current_lexeme();
                token_.lexeme_.str_index_     = low_lvl_token_.lexeme_.str_index_;
                token_.lexeme_.code_.kind_    = Lexem_kind::String;
                token_.lexeme_.code_.subkind_ = static_cast<uint8_t>(enc.str_encoding_);
                token_.range_.end_pos_        = low_lvl_token_.range_.end_pos_;
                lexeme_pos_                   = token_.range_;
                break;
//             case U'$':
//                 low_lvl_token_                = low_level_scanner_->current_lexeme();
//                 token_.lexeme_.char_val_      = low_lvl_token_.lexeme_.char_val_;
//                 token_.lexeme_.code_.kind_    = Lexem_kind::Char;
//                 token_.lexeme_.code_.subkind_ = static_cast<uint8_t>(enc.char_encoding_);
//                 token_.range_.end_pos_        = low_lvl_token_.range_.end_pos_;
//                 lexeme_pos_                   = token_.range_;
//                 break;
            default:
                ;
//                 low_level_scanner_->back();
//                 get_qualified_lexeme();
        }

//         auto low_lvl_kind = get_low_level_kind();
//         switch(low_lvl_kind){
//             case arkona_scanner::Lexem_kind::Char:
//             case arkona_scanner::Lexem_kind::Encoded_char:
//                 token_.lexeme_.char_val_      = low_lvl_token_.lexeme_.char_val_;
//                 token_.lexeme_.code_.kind_    = Lexem_kind::Char;
//                 token_.lexeme_.code_.subkind_ = static_cast<uint8_t>(enc.char_encoding_);
//                 token_.range_.end_pos_        = low_lvl_token_.range_.end_pos_;
//                 lexeme_pos_                   = token_.range_;
//                 break;
//             case arkona_scanner::Lexem_kind::String:
//                 token_.lexeme_.str_index_     = low_lvl_token_.lexeme_.str_index_;
//                 token_.lexeme_.code_.kind_    = Lexem_kind::String;
//                 token_.lexeme_.code_.subkind_ = static_cast<uint8_t>(enc.str_encoding_);
//                 token_.range_.end_pos_        = low_lvl_token_.range_.end_pos_;
//                 lexeme_pos_                   = token_.range_;
//                 break;
//             case arkona_scanner::Lexem_kind::Delimiter:
// // // //                 low_level_scanner_->back();
// // // //                 if(low_lvl_token_.lexeme_.code_.subkind_ == Delimiter_kind::Scope_resol){
// // // //                     get_qualified_lexeme();
// // // //                 }
//                 break;
//             default:
//                 low_level_scanner_->back();
//         }
//         if(it == encodings.end()){
//             low_level_scanner_->back();
//             get_qualified_lexeme();
//             return;
//         }
//         ch_         = *(loc_->pcurrent_char_);
//         auto enc    = it->second;
//         switch(ch_){
//             case U'\'':
//                 low_lvl_token_                = low_level_scanner_->current_lexeme();
//                 token_.lexeme_.char_val_      = low_lvl_token_.lexeme_.char_val_;
//                 token_.lexeme_.code_.kind_    = Lexem_kind::Char;
//                 token_.lexeme_.code_.subkind_ = static_cast<uint8_t>(enc.char_encoding_);
//                 token_.range_.end_pos_        = low_lvl_token_.range_.end_pos_;
//                 lexeme_pos_                   = token_.range_;
//                 break;
//             case U'\"':
//                 low_lvl_token_                = low_level_scanner_->current_lexeme();
//                 token_.lexeme_.str_index_     = low_lvl_token_.lexeme_.str_index_;
//                 token_.lexeme_.code_.kind_    = Lexem_kind::String;
//                 token_.lexeme_.code_.subkind_ = static_cast<uint8_t>(enc.str_encoding_);
//                 token_.range_.end_pos_        = low_lvl_token_.range_.end_pos_;
//                 lexeme_pos_                   = token_.range_;
//                 break;
//             case U'$':
//                 low_lvl_token_                = low_level_scanner_->current_lexeme();
//                 token_.lexeme_.char_val_      = low_lvl_token_.lexeme_.char_val_;
//                 token_.lexeme_.code_.kind_    = Lexem_kind::Char;
//                 token_.lexeme_.code_.subkind_ = static_cast<uint8_t>(enc.char_encoding_);
//                 token_.range_.end_pos_        = low_lvl_token_.range_.end_pos_;
//                 lexeme_pos_                   = token_.range_;
//                 break;
//             default:
//                 low_level_scanner_->back();
//                 get_qualified_lexeme();
//         }
    }

    Arkona_token_snd_lvl Scanner::current_lexeme()
    {
        low_lvl_token_ = low_level_scanner_->current_lexeme();
        lexeme_begin_  = low_level_scanner_->lexeme_begin_ptr();
// #define CURRENT_LEXEME_DEBUG_ON
// #ifdef CURRENT_LEXEME_DEBUG_ON
//         printf("Current low level lexeme position: {%zu: %zu}--{%zu: %zu}\n",
//                low_lvl_token_.range_.begin_pos_.line_no_,
//                low_lvl_token_.range_.begin_pos_.line_pos_,
//                low_lvl_token_.range_.end_pos_.line_no_,
//                low_lvl_token_.range_.end_pos_.line_pos_);
// #endif
        token_.range_  = low_lvl_token_.range_;
        lexeme_pos_    = low_lvl_token_.range_;
        switch(low_lvl_token_.lexeme_.code_.kind_){
            case arkona_scanner::Lexem_kind::Nothing:
            case arkona_scanner::Lexem_kind::UnknownLexem:
            case arkona_scanner::Lexem_kind::Integer:
            case arkona_scanner::Lexem_kind::Float:
            case arkona_scanner::Lexem_kind::Complex:
            case arkona_scanner::Lexem_kind::Quat:
            case arkona_scanner::Lexem_kind::Keyword:
            case arkona_scanner::Lexem_kind::Char:
            case arkona_scanner::Lexem_kind::Delimiter:
                token_ = convert_token(low_lvl_token_);
// #ifdef CURRENT_LEXEME_DEBUG_ON
//         printf("Current high level lexeme position: {%zu: %zu}--{%zu: %zu}\n",
//                token_.range_.begin_pos_.line_no_,
//                token_.range_.begin_pos_.line_pos_,
//                token_.range_.end_pos_.line_no_,
//                token_.range_.end_pos_.line_pos_);
// #endif
                break;
            case arkona_scanner::Lexem_kind::Id:
//                 token_.lexeme_.code_.kind_                    = Lexem_kind::Id;
//                 token_.lexeme_.id_info_.qualifying_prefix_    = 0;
//                 token_.lexeme_.id_info_.id_idx_               = low_lvl_token_.lexeme_.id_index_;
//                 parts_of_name_.clear();
//                 parts_of_name_.push_back(low_lvl_token_);
                get_maybe_qualified_lexeme();
//                 build_qualified_lexeme();
//                 lexeme_pos_ = token_.range_;
                break;
            case arkona_scanner::Lexem_kind::String:
                token_.lexeme_.code_.kind_    = Lexem_kind::String;
                token_.lexeme_.code_.subkind_ = static_cast<uint8_t>(String_kind::String32);
                token_.lexeme_.str_index_     = low_lvl_token_.lexeme_.str_index_;
                break;
            case arkona_scanner::Lexem_kind::Encoded_char:
                token_.lexeme_.code_.kind_    = Lexem_kind::Char;
                token_.lexeme_.code_.subkind_ = static_cast<uint8_t>(Char_kind::Char32);
                token_.lexeme_.char_val_      = low_lvl_token_.lexeme_.char_val_;
                break;
        }
        return token_;
    }

    void Scanner::back()
    {
        loc_->pcurrent_char_ = lexeme_begin_;
        loc_->pos_           = token_.range_.begin_pos_;
    }

    ascaner::Position_range Scanner::lexeme_pos() const
    {
        return lexeme_pos_;
    }

    char32_t* Scanner::lexeme_begin_ptr() const
    {
        return lexeme_begin_;
    }

    std::string Scanner::lexeme_to_string(const arkona_scanner_snd_lvl::Lexeme_info& li)
    {
        return arkona_scanner_snd_lvl::to_string(li, ids_, strs_, module_name_parts_trie_);
    }

    std::string Scanner::token_to_string(const Arkona_token_snd_lvl& tok)
    {
        std::string result;
        auto&       p      = tok.range_;
        auto&       b      = p.begin_pos_;
        auto&       e      = p.end_pos_;
        result             = "[line: "   + std::to_string(b.line_no_)  +
                             ", pos: "   + std::to_string(b.line_pos_) + "]"
                             "--[line: " + std::to_string(e.line_no_)  +
                             ", pos: "   + std::to_string(e.line_pos_) + "]";
        result += " lexeme: " + lexeme_to_string(tok.lexeme_);
        return result;
    }
};