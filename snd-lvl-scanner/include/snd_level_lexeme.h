/*
    File:    snd_level_lexeme.h
    Created: 02 January 2020 at 13:00 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SND_LEVEL_LEXEME_H
#define SND_LEVEL_LEXEME_H
#   include <cstddef>
#   include <cstdint>
#   include <quadmath.h>
#   include "../../numbers/include/quaternion.h"
namespace arkona_scanner_snd_lvl{
    enum Keyword_kind : uint8_t{
        Kw_bezzn,         Kw_bezzn8,        Kw_bezzn16,       Kw_bezzn32,
        Kw_bezzn64,       Kw_bezzn128,      Kw_bolshe,        Kw_bolshoe,
        Kw_v,             Kw_vechno,        Kw_veshch,        Kw_veshch32,
        Kw_veshch64,      Kw_veshch80,      Kw_veshch128,     Kw_vozvrat,
        Kw_vyberi,        Kw_vydeli,        Kw_vyjdi,         Kw_golovnaya,
        Kw_dlya,          Kw_esli,          Kw_iz,            Kw_inache,
        Kw_ines,          Kw_ispolzuet,     Kw_istina,        Kw_kak,
        Kw_kategorija,    Kw_kvat,          Kw_kvat32,        Kw_kvat64,
        Kw_kvat80,        Kw_kvat128,       Kw_kompl,         Kw_kompl32,
        Kw_kompl64,       Kw_kompl80,       Kw_kompl128,      Kw_konst,
        Kw_log,           Kw_log8,          Kw_log16,         Kw_log32,
        Kw_log64,         Kw_lozh,          Kw_lyambda,       Kw_malenkoe,
        Kw_massiv,        Kw_menshe,        Kw_meta,          Kw_modul,
        Kw_nichto,        Kw_operaciya,     Kw_osvobodi,      Kw_paket,
        Kw_pauk,          Kw_perem,         Kw_perech_mnozh,  Kw_perechislenie,
        Kw_povtoryaj,     Kw_poka,          Kw_pokuda,        Kw_poryadok,
        Kw_poryadok8,     Kw_poryadok16,    Kw_poryadok32,    Kw_poryadok64,
        Kw_postfiksnaya,  Kw_preobrazuj,    Kw_prefiksnaya,   Kw_psevdonim,
        Kw_pusto,         Kw_razbor,        Kw_rassmatrivaj,  Kw_realizacija,
        Kw_realizuj,      Kw_samo,          Kw_simv,          Kw_simv8,
        Kw_simv16,        Kw_simv32,        Kw_sravni_s,      Kw_ssylka,
        Kw_stroka,        Kw_stroka8,       Kw_stroka16,      Kw_stroka32,
        Kw_struktura,     Kw_tip,           Kw_to,            Kw_funktsiya,
        Kw_ravno,         Kw_tsel,          Kw_tsel8,         Kw_tsel16,
        Kw_tsel32,        Kw_tsel64,        Kw_tsel128,       Kw_chistaya,
        Kw_shkala,        Kw_shkalu,        Kw_ekviv,         Kw_eksport,
        Kw_element
    };

    enum class Float_kind : uint8_t{
        Float32, Float64, Float80, Float128
    };

    enum class Complex_kind : uint8_t{
        Complex32, Complex64, Complex80, Complex128
    };

    enum class Quat_kind : uint8_t{
        Quat32, Quat64, Quat80, Quat128
    };

    enum class Char_kind : uint8_t{
        Char8, Char16, Char32
    };

    enum class String_kind : uint8_t{
        String8, String16, String32
    };

    enum Delimiter_kind : uint8_t{
        Copy,                        Assign,                      Logical_or_assign,
        Logical_or_full_assign,      Logical_or_not_assign,       Logical_or_not_full_assign,
        Logical_xor_assign,          Logical_and_assign,          Logical_and_full_assign,
        Logical_and_not_assign,      Logical_and_not_full_assign, Bitwise_or_assign,
        Bitwise_or_not_assign,       Bitwise_xor_assign,          Bitwise_and_assign,
        Bitwise_and_not_assign,      Left_shift_assign,           Right_shift_assign,
        Plus_assign,                 Minus_assign,                Meta_plus_assign,
        Mul_assign,                  Div_assign,                  Remainder_assign,
        Float_remainder_assign,      Symmetric_difference_assign, Set_difference_assign,
        Power_assign,                Float_power_assign,          Cond_op,
        Cond_op_full,                Logical_or,                  Logical_or_full,
        Logical_or_not,              Logical_or_not_full,         Logical_xor,
        Logical_and,                 Logical_and_full,            Logical_and_not,
        Logical_and_not_full,        Logical_not,                 Compare_with,
        EQ,                          NEQ,                         GT,
        LT,                          GEQ,                         LEQ,
        Bitwise_or,                  Bitwise_or_not,              Bitwise_xor,
        Bitwise_and,                 Bitwise_and_not,             Left_shift,
        Right_shift,                 Bitwise_not,                 Plus,
        Minus,                       Algebraic_sep,               Meta_plus,
        Mul,                         Div,                         Remainder,
        Float_remainder,             Symmetric_difference,        Set_difference,
        Power,                       Float_power,                 Inc,
        Wrap_inc,                    Dec,                         Wrap_dec,
        ElemType,                    Card,                        Sharp,
        Data_size,                   At,                          Address,
        Data_address,                ExprType,                    Round_br_opened,
        Round_br_closed,             Sq_br_opened,                Sq_br_closed,
        Fig_br_opened,               Fig_br_closed,               Tuple_begin,
        Tuple_end,                   Meta_open_br,                Meta_closed_br,
        Colon_sq_br_opened,          Colon_sq_br_closed,          Colon_fig_br_opened,
        Colon_fig_br_closed,         Comma,                       Right_arrow,
        Point,                       Range,                       Colon,
        Scope_resol,                 Semicolon,                   Left_arrow,
        Non_specific,                Cycle_name_prefix,           Deduce_arg_type,
        Pattern,
    };

    enum class Lexem_kind : uint8_t{
        Nothing,   UnknownLexem, Keyword, Id,
        Delimiter, Char,         String,  Integer,
        Float,     Complex,      Quat
    };

    struct Id_info{
        std::size_t qualifying_prefix_;
        std::size_t id_idx_;
    };

    struct Delimiter_info{
        std::size_t    qualifying_prefix_;
        Delimiter_kind delim_kind_;
    };

    struct Lexem_code{
        Lexem_kind kind_;
        uint8_t    subkind_;
    };

    struct Lexeme_info{
        union{
            unsigned __int128        int_val_;
            __float128               float_val_;
            __complex128             complex_val_;
            quat::quat_t<__float128> quat_val_;
            char32_t                 char_val_;
            std::size_t              str_index_;
            Id_info                  id_info_;
            Delimiter_info           delim_info_;
        };
        Lexem_code code_;
    };

    bool operator==(const Lexeme_info& lhs, const Lexeme_info& rhs);
};
#endif