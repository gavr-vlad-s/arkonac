/*
    File:    arkona-scanner-snd-lvl.h
    Created: 02 January 2020 at 18:25 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ARKONA_SCANER_SND_LVL_H
#define ARKONA_SCANER_SND_LVL_H
#   include <cstddef>
#   include <cstdint>
#   include <string>
#   include <memory>
#   include "../../scanner/include/position.h"
#   include "../../scanner/include/location.h"
#   include "../../scanner/include/token.h"
#   include "../../tries/include/errors_and_tries.h"
#   include "../../tries/include/error_count.h"
#   include "../../tries/include/char_trie.h"
#   include "../../tries/include/trie_for_vector.h"
#   include "../../scanner/include/arkona-scanner.h"
#   include "../include/snd_level_lexeme.h"
namespace arkona_scanner_snd_lvl{
    using Arkona_token_snd_lvl = ascaner::Token<arkona_scanner_snd_lvl::Lexeme_info>;
    using Arkona_token_low_lvl = arkona_scanner::Arkona_token;
    using Low_level_lexem_code = arkona_scanner::Lexem_code;
    using Low_level_lexem_kind = arkona_scanner::Lexem_kind;

    class Scanner{
    public:
        Scanner()               = default;
        Scanner(const Scanner&) = default;
        virtual ~Scanner()      = default;

        Scanner(const ascaner::Location_ptr&                         location,
                const Errors_and_tries&                              et,
                const std::shared_ptr<Trie_for_vector<std::size_t>>& module_name_parts_trie);

        /*  Function back() return the current lexem into the input stream. */
        void                  back();

        /* Function current_lexeme() returns information about current lexem,
         * i.e. returns a lexeme code and a lexeme value. */
        Arkona_token_snd_lvl  current_lexeme();

        ascaner::Position_range lexeme_pos()       const;
        char32_t*               lexeme_begin_ptr() const;

        std::string             lexeme_to_string(const arkona_scanner_snd_lvl::Lexeme_info& li);

        std::string             token_to_string(const Arkona_token_snd_lvl& tok);
    private:
        ascaner::Location_ptr                         loc_;
        char32_t*                                     lexeme_begin_; /* pointer to the lexem begin */
        char32_t                                      ch_;           /* current character */

        /* intermediate value of the lexem information */
        Arkona_token_snd_lvl                          token_;

        Arkona_token_low_lvl                          low_lvl_token_;
        Low_level_lexem_code                          low_lvl_lexem_code_;
        uint8_t                                       low_lvl_lexem_subkind_;
        Low_level_lexem_kind                          low_lvl_lexem_kind_;

        ascaner::Position_range                       lexeme_pos_;

        /* a pointer to a class that counts the number of errors: */
        std::shared_ptr<Error_count>                  en_;
        /* a pointer to a class that counts the number of warnings: */
        std::shared_ptr<Warning_count>                wn_;
        /* a pointer to the prefix tree for identifiers: */
        std::shared_ptr<Char_trie>                    ids_;
        /* a pointer to the prefix tree for string literals: */
        std::shared_ptr<Char_trie>                    strs_;
        /* a pointer to the prefix tree for qualified identifiers */
        std::shared_ptr<Trie_for_vector<std::size_t>> module_name_parts_trie_;

        std::shared_ptr<arkona_scanner::Scanner>      low_level_scanner_;

        void get_maybe_qualified_lexeme();
        void get_rest_parts_of_maybe_qualified_lexeme();

        Low_level_lexem_kind get_low_level_kind();

        std::vector<Arkona_token_low_lvl> parts_of_name_;
    };
};
#endif