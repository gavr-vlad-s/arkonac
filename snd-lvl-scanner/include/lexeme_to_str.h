/*
    File:    lexeme_to_str.h
    Created: 04 January 2020 at 15:47 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SND_LVL_LEXEME_TO_STR_H
#define SND_LVL_LEXEME_TO_STR_H
#   include <string>
#   include <memory>
#   include <cstdint>
#   include <cstddef>
#   include "../include/snd_level_lexeme.h"
#   include "../../tries/include/trie_for_vector.h"
#   include "../../tries/include/char_trie.h"
namespace arkona_scanner_snd_lvl{
    using Char_trie_ptr              = std::shared_ptr<Char_trie>;
    using Module_name_parts_trie_ptr = std::shared_ptr<Trie_for_vector<std::size_t>>;

    std::string to_string(const Lexeme_info&                li,
                          const Char_trie_ptr&              ids_trie,
                          const Char_trie_ptr&              strs_trie,
                          const Module_name_parts_trie_ptr& module_name_parts_trie);
};
#endif