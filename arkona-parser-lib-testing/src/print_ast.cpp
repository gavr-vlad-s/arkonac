/*
    File:    print_ast.cpp
    Created: 26 September 2020 at 15:22 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstddef>
#include "../include/print_ast.hpp"
#include "../../tries/include/char_trie.h"
#include "../../strings-lib/include/join.h"

class To_string_visitor : public arkona_parser::ast::Visitor
{
public:
    To_string_visitor()                         = default;
    To_string_visitor(const To_string_visitor&) = default;
    virtual ~To_string_visitor()                = default;

    To_string_visitor(const std::shared_ptr<arkona_parser::Symbol_table>& symb_table)
        : symb_table_{symb_table}
    {
    }

    std::string to_string(const std::shared_ptr<arkona_parser::ast::AST>& tree);

    void visit(arkona_parser::ast::Module&       ref) override;
    void visit(arkona_parser::ast::Qualified_id& ref) override;
    void visit(arkona_parser::ast::Imports&      ref) override;
    void visit(arkona_parser::ast::Block&        ref) override;

private:
    std::shared_ptr<arkona_parser::Symbol_table> symb_table_;
    size_t                                       indent_     = 0;
    std::string                                  str_repres_;
};

std::string ast_to_string(const std::shared_ptr<arkona_parser::ast::AST>&     tree,
                          const std::shared_ptr<arkona_parser::Symbol_table>& symb_table)
{
    std::string result;
    To_string_visitor v{symb_table};
    result = v.to_string(tree);
    return result;
}

std::string To_string_visitor::to_string(const std::shared_ptr<arkona_parser::ast::AST>& tree)
{
    indent_ = 0;
    str_repres_.clear();
    tree->traverse(*this);
    return str_repres_;
}

static constexpr size_t indent_increment = 4;


void To_string_visitor::visit(arkona_parser::ast::Module& ref)
{
    indent_     += indent_increment;
    auto indent =  std::string(indent_, ' ');

    str_repres_ =  "Module{\n";

    str_repres_ += "name:\n";
    ref.name_->accept(*this);

    str_repres_ += "used_modules:\n";
    ref.used_modules_->accept(*this);

    str_repres_ += "body:\n";
    ref.body_->accept(*this);

    str_repres_ += "}\n";
}

void To_string_visitor::visit(arkona_parser::ast::Qualified_id& ref)
{
    std::vector<size_t> id_parts(ref.name_components_.size());
    size_t              i = 0;
    for(const auto& c : ref.name_components_){
        size_t p    = c.lexeme_.id_info_.id_idx_;
        id_parts[i] = p;
        ++i;
    }

    str_repres_ += std::string(indent_, ' ')                    +
                   "Qualified_id {\n"                           +
                   std::string(indent_ + indent_increment, ' ') +
                   symb_table_->get_string(id_parts)            +
                   "\n"                                         +
                   std::string(indent_, ' ')                    +
                   "}\n";
}

void To_string_visitor::visit(arkona_parser::ast::Imports& ref)
{
    auto indent = std::string(indent_, ' ');
    str_repres_ += indent + "Imports{\n";

    for(auto& imp : ref.imports_){
        indent_ += indent_increment;
        imp->accept(*this);
        indent_ -= indent_increment;
    }

    str_repres_ += indent + "}\n";
}

void To_string_visitor::visit(arkona_parser::ast::Block&)
{
    auto indent =  std::string(indent_, ' ');
    str_repres_ += indent + "Block{}\n";
}