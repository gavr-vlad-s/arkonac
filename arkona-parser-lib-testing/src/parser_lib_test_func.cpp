/*
    File:    parser_lib_test_func.cpp
    Created: 20 September 2020 at 15:58 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstdio>
#include "../include/parser_lib_test_func.hpp"
#include "../../scanner/include/location.h"
#include "../../tries/include/errors_and_tries.h"
#include "../../tries/include/error_count.h"
#include "../../tries/include/warning_count.h"
#include "../../tries/include/char_trie.h"
#include "../../tries/include/trie_for_vector.h"
#include "../../snd-lvl-scanner/include/arkona-scanner-snd-lvl.h"
#include "../../arkona-parser-lib/include/symbol_table.hpp"
#include "../../arkona-parser-lib/include/parser.hpp"
#include "../../arkona-parser-lib/include/ast_node.hpp"
#include "../include/print_ast.hpp"

void test_func(const std::u32string& text)
{
    char32_t*         p         = const_cast<char32_t*>(text.c_str());
    auto              loc       = std::make_shared<ascaner::Location>(p);
    Errors_and_tries  et;
    et.ec_                      = std::make_shared<Error_count>();
    et.wc_                      = std::make_shared<Warning_count>();
    et.ids_trie_                = std::make_shared<Char_trie>();
    et.strs_trie_               = std::make_shared<Char_trie>();

    auto module_name_parts_trie = std::make_shared<Trie_for_vector<std::size_t>>();
    auto snd_lvl_scanner        = std::make_shared<arkona_scanner_snd_lvl::Scanner>(loc, et, module_name_parts_trie);

    auto symbol_table           = std::make_shared<arkona_parser::Symbol_table>(module_name_parts_trie, et.ids_trie_);

    auto parser                 = std::make_shared<arkona_parser::Parser>(snd_lvl_scanner, et, symbol_table);

    auto parsing_result         = (*parser)();
    auto root                   = parsing_result.pnode_;
    auto ast                    = std::make_shared<arkona_parser::ast::AST>(root);

    auto ast_str                = ast_to_string(ast, symbol_table);
    printf("AST: \n%s\n", ast_str.c_str());
}