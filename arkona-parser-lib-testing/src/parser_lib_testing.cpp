/*
    File:    parser_lib_testing.cpp
    Created: 19 September 2020 at 12:09 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../../file_utils/include/get_processed_text.h"
#include "../include/usage.hpp"
#include "../include/parser_lib_test_func.hpp"

enum Exit_codes{
    Success, No_args, File_processing_error
};

int main(int argc, char* argv[])
{
    if(1 == argc){
        usage(argv[0]);
        return No_args;
    }

    auto text = file_utils::get_processed_text(argv[1]);
    if(!text.length()){
        return File_processing_error;
    }

    test_func(text);

    return Success;
}