/*
    File:    print_ast.hpp
    Created: 22 September 2020 at 21:46 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef PRINT_AST_H
#define PRINT_AST_H
#   include <memory>
#   include <string>
#   include "../../arkona-parser-lib/include/ast_node.hpp"
#   include "../../arkona-parser-lib/include/symbol_table.hpp"
std::string ast_to_string(const std::shared_ptr<arkona_parser::ast::AST>&     tree,
                          const std::shared_ptr<arkona_parser::Symbol_table>& symb_table);
#endif