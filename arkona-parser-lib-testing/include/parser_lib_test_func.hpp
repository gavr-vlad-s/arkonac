/*
    File:    parser_lib_test_func.hpp
    Created: 06 September 2020 at 19:37 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef PARSER_LIB_TEST_FUNC_H
#define PARSER_LIB_TEST_FUNC_H
#   include <string>
void test_func(const std::u32string& arkonap);
#endif