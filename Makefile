LINKER          = g++
LINKERFLAGS     = -s
CXX             = g++
CXXFLAGS        = -O3 -Wall -std=gnu++17 -Wextra
TGEN_SRC_PATH   = table-gen
TGEN_BUILD_PATH = table-gen/build
TGEN_BIN_NAME   = tgen
SRC_EXT         = cpp
TGEN_SRCS       = $(shell find $(TGEN_SRC_PATH) -name '*.$(SRC_EXT)')
TGEN_OBJECTS    = $(TGEN_SRCS:$(TGEN_SRC_PATH)/%.$(SRC_EXT)=$(TGEN_BUILD_PATH)/%.o)

CHAR_CONV_SRC_PATH   = char-conv/src
CHAR_CONV_BUILD_PATH = char-conv/build
CHAR_CONV_LIB_NAME   = libcharconv.a
CHAR_CONV_SRCS       = $(shell find $(CHAR_CONV_SRC_PATH) -name '*.$(SRC_EXT)')
CHAR_CONV_OBJECTS    = $(CHAR_CONV_SRCS:$(CHAR_CONV_SRC_PATH)/%.$(SRC_EXT)=$(CHAR_CONV_BUILD_PATH)/%.o)

TRIES_SRC_PATH   = tries/src
TRIES_BUILD_PATH = tries/build
TRIES_LIB_NAME   = libtries.a
TRIES_SRCS       = $(shell find $(TRIES_SRC_PATH) -name '*.$(SRC_EXT)')
TRIES_OBJECTS    = $(TRIES_SRCS:$(TRIES_SRC_PATH)/%.$(SRC_EXT)=$(TRIES_BUILD_PATH)/%.o)

LFILES_SRC_PATH   = file_utils/src
LFILES_BUILD_PATH = file_utils/build
LFILES_LIB_NAME   = libfiles.a
LFILES_SRCS       = $(shell find $(LFILES_SRC_PATH) -name '*.$(SRC_EXT)')
LFILES_OBJECTS    = $(LFILES_SRCS:$(LFILES_SRC_PATH)/%.$(SRC_EXT)=$(LFILES_BUILD_PATH)/%.o)

LSCANNER_SRC_PATH   = scanner/src
LSCANNER_BUILD_PATH = scanner/build
LSCANNER_LIB_NAME   = libscanner.a
LSCANNER_SRCS       = $(shell find $(LSCANNER_SRC_PATH) -name '*.$(SRC_EXT)')
LSCANNER_OBJECTS    = $(LSCANNER_SRCS:$(LSCANNER_SRC_PATH)/%.$(SRC_EXT)=$(LSCANNER_BUILD_PATH)/%.o)

TESTING_SC_SRC_PATH   = scanner-testing/src
TESTING_SC_BUILD_PATH = scanner-testing/build
TESTING_SC_BIN_NAME   = testing-sc
TESTING_SC_SRCS       = $(shell find $(TESTING_SC_SRC_PATH) -name '*.$(SRC_EXT)')
TESTING_SC_OBJECTS    = $(TESTING_SC_SRCS:$(TESTING_SC_SRC_PATH)/%.$(SRC_EXT)=$(TESTING_SC_BUILD_PATH)/%.o)

NUMBERS_SRC_PATH   = numbers/src
NUMBERS_BUILD_PATH = numbers/build
NUMBERS_LIB_NAME   = libnumbers.a
NUMBERS_SRCS       = $(shell find $(NUMBERS_SRC_PATH) -name '*.$(SRC_EXT)')
NUMBERS_OBJECTS    = $(NUMBERS_SRCS:$(NUMBERS_SRC_PATH)/%.$(SRC_EXT)=$(NUMBERS_BUILD_PATH)/%.o)

LSCANNER_SND_LVL_SRC_PATH   = snd-lvl-scanner/src
LSCANNER_SND_LVL_BUILD_PATH = snd-lvl-scanner/build
LSCANNER_SND_LVL_LIB_NAME   = libsnd-lvl-scanner.a
LSCANNER_SND_LVL_SRCS       = $(shell find $(LSCANNER_SND_LVL_SRC_PATH) -name '*.$(SRC_EXT)')
LSCANNER_SND_LVL_OBJECTS    = $(LSCANNER_SND_LVL_SRCS:$(LSCANNER_SND_LVL_SRC_PATH)/%.$(SRC_EXT)=$(LSCANNER_SND_LVL_BUILD_PATH)/%.o)

TESTING_SND_LVL_SC_SRC_PATH   = snd-lvl-scanner-testing/src
TESTING_SND_LVL_SC_BUILD_PATH = snd-lvl-scanner-testing/build
TESTING_SND_LVL_SC_BIN_NAME   = testing-sc-snd-lvl
TESTING_SND_LVL_SC_SRCS       = $(shell find $(TESTING_SND_LVL_SC_SRC_PATH) -name '*.$(SRC_EXT)')
TESTING_SND_LVL_SC_OBJECTS    = $(TESTING_SND_LVL_SC_SRCS:$(TESTING_SND_LVL_SC_SRC_PATH)/%.$(SRC_EXT)=$(TESTING_SND_LVL_SC_BUILD_PATH)/%.o)

PARSER_GENERATOR_SRC_PATH   = parser-generator-lib/src
PARSER_GENERATOR_BUILD_PATH = parser-generator-lib/build
PARSER_GENERATOR_LIB_NAME   = libparser-gen.a
PARSER_GENERATOR_SRCS       = $(shell find $(PARSER_GENERATOR_SRC_PATH) -name '*.$(SRC_EXT)')
PARSER_GENERATOR_OBJECTS    = $(PARSER_GENERATOR_SRCS:$(PARSER_GENERATOR_SRC_PATH)/%.$(SRC_EXT)=$(PARSER_GENERATOR_BUILD_PATH)/%.o)

PARSER_GENERATOR_TESTING_SRC_PATH   = parser-generator-lib-testing/src
PARSER_GENERATOR_TESTING_BUILD_PATH = parser-generator-lib-testing/build
PARSER_GENERATOR_TESTING_BIN_NAME   = pgen-lib-testing
PARSER_GENERATOR_TESTING_SRCS       = $(shell find $(PARSER_GENERATOR_TESTING_SRC_PATH) -name '*.$(SRC_EXT)')
PARSER_GENERATOR_TESTING_OBJECTS    = $(PARSER_GENERATOR_TESTING_SRCS:$(PARSER_GENERATOR_TESTING_SRC_PATH)/%.$(SRC_EXT)=$(PARSER_GENERATOR_TESTING_BUILD_PATH)/%.o)

ARKONA_PARSER_GENERATOR_SRC_PATH   = arkona-parser-lib-generator/src
ARKONA_PARSER_GENERATOR_BUILD_PATH = arkona-parser-lib-generator/build
ARKONA_PARSER_GENERATOR_BIN_NAME   = arkona-parser-lib-generator
ARKONA_PARSER_GENERATOR_SRCS       = $(shell find $(ARKONA_PARSER_GENERATOR_SRC_PATH) -name '*.$(SRC_EXT)')
ARKONA_PARSER_GENERATOR_OBJECTS    = $(ARKONA_PARSER_GENERATOR_SRCS:$(ARKONA_PARSER_GENERATOR_SRC_PATH)/%.$(SRC_EXT)=$(ARKONA_PARSER_GENERATOR_BUILD_PATH)/%.o)

ARKONA_PARSER_LIB_DIRECTORY = arkona-parser-lib

LARKONA_PARSER_SRC_PATH   = arkona-parser-lib/src
LARKONA_PARSER_BUILD_PATH = arkona-parser-lib/build
LARKONA_PARSER_LIB_NAME   = libaparser.a
LARKONA_PARSER_SRCS       = $(shell find $(LARKONA_PARSER_SRC_PATH) -name '*.$(SRC_EXT)')
LARKONA_PARSER_OBJECTS    = $(LARKONA_PARSER_SRCS:$(LARKONA_PARSER_SRC_PATH)/%.$(SRC_EXT)=$(LARKONA_PARSER_BUILD_PATH)/%.o)

LBITSET_SRC_PATH   = bitset/src
LBITSET_BUILD_PATH = bitset/build
LBITSET_LIB_NAME   = libbitscale.a
LBITSET_SRCS       = $(shell find $(LBITSET_SRC_PATH) -name '*.$(SRC_EXT)')
LBITSET_OBJECTS    = $(LBITSET_SRCS:$(LBITSET_SRC_PATH)/%.$(SRC_EXT)=$(LBITSET_BUILD_PATH)/%.o)

LRECURSIVE_PARSER_SRC_PATH   = recursive-parser-lib/src
LRECURSIVE_PARSER_BUILD_PATH = recursive-parser-lib/build
LRECURSIVE_PARSER_LIB_NAME   = librecursiveparser.a
LRECURSIVE_PARSER_SRCS       = $(shell find $(LRECURSIVE_PARSER_SRC_PATH) -name '*.$(SRC_EXT)')
LRECURSIVE_PARSER_OBJECTS    = $(LRECURSIVE_PARSER_SRCS:$(LRECURSIVE_PARSER_SRC_PATH)/%.$(SRC_EXT)=$(LRECURSIVE_PARSER_BUILD_PATH)/%.o)

ARKONA_PARSER_LIB_TESTING_SRC_PATH   = arkona-parser-lib-testing/src
ARKONA_PARSER_LIB_TESTING_BUILD_PATH = arkona-parser-lib-testing/build
ARKONA_PARSER_LIB_TESTING_BIN_NAME   = aparser-lib-testing
ARKONA_PARSER_LIB_TESTING_SRCS       = $(shell find $(ARKONA_PARSER_LIB_TESTING_SRC_PATH) -name '*.$(SRC_EXT)')
ARKONA_PARSER_LIB_TESTING_OBJECTS    = $(ARKONA_PARSER_LIB_TESTING_SRCS:$(ARKONA_PARSER_LIB_TESTING_SRC_PATH)/%.$(SRC_EXT)=$(ARKONA_PARSER_LIB_TESTING_BUILD_PATH)/%.o)


RECURSIVE_PARSER_LIB_TESTING_SRC_PATH   = recursive-parser-lib-testing/src
RECURSIVE_PARSER_LIB_TESTING_BUILD_PATH = recursive-parser-lib-testing/build
RECURSIVE_PARSER_LIB_TESTING_BIN_NAME   = recursive-parser-testing
RECURSIVE_PARSER_LIB_TESTING_SRCS       = $(shell find $(RECURSIVE_PARSER_LIB_TESTING_SRC_PATH) -name '*.$(SRC_EXT)')
RECURSIVE_PARSER_LIB_TESTING_OBJECTS    = $(RECURSIVE_PARSER_LIB_TESTING_SRCS:$(RECURSIVE_PARSER_LIB_TESTING_SRC_PATH)/%.$(SRC_EXT)=$(RECURSIVE_PARSER_LIB_TESTING_BUILD_PATH)/%.o)

BOOST_LIBS      = -lboost_filesystem -lboost_system
QUAD_LIB        = -lquadmath
LFILE_LIB       = -L$(LFILES_BUILD_PATH) -lfiles
CHAR_CONV_LIB   = -L$(CHAR_CONV_BUILD_PATH) -lcharconv
TRIES_LIB       = -L$(TRIES_BUILD_PATH) -ltries
SC_LIB          = -L$(LSCANNER_BUILD_PATH) -lscanner
NUMBERS_LIB     = -L$(NUMBERS_BUILD_PATH) -lnumbers
SND_LVL_SC_LIB  = -L$(LSCANNER_SND_LVL_BUILD_PATH) -lsnd-lvl-scanner
PARSER_GEN_LIB  = -L$(PARSER_GENERATOR_BUILD_PATH) -lparser-gen
LBITSET_LIB     = -L$(LBITSET_BUILD_PATH) -lbitscale
APARSER_LIB     = -L$(LARKONA_PARSER_BUILD_PATH) -laparser
RECURSIVE_PARSER_LIB = -L$(LRECURSIVE_PARSER_BUILD_PATH) -lrecursiveparser

TESTING_SC_LIBS = $(LFILE_LIB) $(SC_LIB) $(NUMBERS_LIB) $(TRIES_LIB) $(CHAR_CONV_LIB) $(BOOST_LIBS) $(QUAD_LIB)

TESTING_SND_LVL_SC_LIBS = $(SND_LVL_SC_LIB) $(LFILE_LIB) $(SC_LIB) $(NUMBERS_LIB) $(TRIES_LIB) $(CHAR_CONV_LIB) $(BOOST_LIBS) $(QUAD_LIB)

TESTING_PARSER_GEN_LIBS = $(PARSER_GEN_LIB) $(LBITSET_LIB)

TESTING_ARKONA_PARSER_LIBS = $(APARSER_LIB) $(TESTING_SND_LVL_SC_LIBS)

TESTING_RECURSIVE_ARKONA_PARSER_LIBS = $(RECURSIVE_PARSER_LIB) $(TESTING_SND_LVL_SC_LIBS)

BUILD_MSG_PRINT = @tput bold; tput setaf 6; echo "$(1)"; tput sgr0

.PHONY: all clean scanner-prj lscan libs-for-scanner charconv lnumbers triesl lfiles sc-testing snd-lvl-scanner-prj snd-lvl-scanner-lib snd-lvl-sc-testing parser-gen-prj parser-gen-lib parser-gen-lib-testing arkona-parser-generator generate-arkona-parser-lib arkona-parsing-lib lbitset testing-aparser-lib recursive-parsing-lib recursive-parsing-lib-testing

all: scanner-prj snd-lvl-scanner-prj parser-gen-prj recursive-parsing-lib recursive-parsing-lib-testing

scanner-prj: libs-for-scanner lscan sc-testing

lscan: table.txt $(LSCANNER_BUILD_PATH)/$(LSCANNER_LIB_NAME)

libs-for-scanner: charconv lnumbers triesl lfiles

table.txt: $(TGEN_BUILD_PATH)/$(TGEN_BIN_NAME)
	$(call BUILD_MSG_PRINT,"Generating of classification table of chars.")
	$(TGEN_BUILD_PATH)/$(TGEN_BIN_NAME) > $@
	@echo "             "

$(TGEN_BUILD_PATH)/$(TGEN_BIN_NAME): $(TGEN_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for chars classification table.")
	$(LINKER) -o $@ $(TGEN_OBJECTS) $(LINKERFLAGS)
	@echo "             "

$(TGEN_BUILD_PATH)/%.o: $(TGEN_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

charconv: $(CHAR_CONV_BUILD_PATH)/$(CHAR_CONV_LIB_NAME)

$(CHAR_CONV_BUILD_PATH)/$(CHAR_CONV_LIB_NAME): $(CHAR_CONV_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking character convertation library.")
	ar cr $@ $(CHAR_CONV_OBJECTS)
	@echo "             "

$(CHAR_CONV_BUILD_PATH)/%.o: $(CHAR_CONV_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

lnumbers: $(NUMBERS_BUILD_PATH)/$(NUMBERS_LIB_NAME)

$(NUMBERS_BUILD_PATH)/$(NUMBERS_LIB_NAME): $(NUMBERS_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking numbers processing library.")
	ar cr $@ $(NUMBERS_OBJECTS)
	@echo "             "

$(NUMBERS_BUILD_PATH)/%.o: $(NUMBERS_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

triesl: $(TRIES_BUILD_PATH)/$(TRIES_LIB_NAME)

$(TRIES_BUILD_PATH)/$(TRIES_LIB_NAME): $(TRIES_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library for tries.")
	ar cr $@ $(TRIES_OBJECTS)
	@echo "             "

$(TRIES_BUILD_PATH)/%.o: $(TRIES_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

lfiles: $(LFILES_BUILD_PATH)/$(LFILES_LIB_NAME)

$(LFILES_BUILD_PATH)/$(LFILES_LIB_NAME): $(LFILES_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking file utils library.")
	ar cr $@ $(LFILES_OBJECTS)
	@echo "             "

$(LFILES_BUILD_PATH)/%.o: $(LFILES_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(LSCANNER_BUILD_PATH)/$(LSCANNER_LIB_NAME): $(LSCANNER_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking scanner library.")
	ar cr $@ $(LSCANNER_OBJECTS)
	@echo "             "

$(LSCANNER_BUILD_PATH)/%.o: $(LSCANNER_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

sc-testing: $(TESTING_SC_BUILD_PATH)/$(TESTING_SC_BIN_NAME)

$(TESTING_SC_BUILD_PATH)/$(TESTING_SC_BIN_NAME): $(TESTING_SC_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for lexical analysis testing.")
	$(LINKER) -o $@ $(TESTING_SC_OBJECTS) $(TESTING_SC_LIBS) $(LINKERFLAGS)
	@echo "             "

$(TESTING_SC_BUILD_PATH)/%.o: $(TESTING_SC_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

snd-lvl-scanner-prj: snd-lvl-scanner-lib snd-lvl-sc-testing

snd-lvl-scanner-lib: $(LSCANNER_SND_LVL_BUILD_PATH)/$(LSCANNER_SND_LVL_LIB_NAME)

$(LSCANNER_SND_LVL_BUILD_PATH)/$(LSCANNER_SND_LVL_LIB_NAME): $(LSCANNER_SND_LVL_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking second level scanner library.")
	ar cr $@ $(LSCANNER_SND_LVL_OBJECTS)
	@echo "             "

$(LSCANNER_SND_LVL_BUILD_PATH)/%.o: $(LSCANNER_SND_LVL_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

snd-lvl-sc-testing: $(TESTING_SND_LVL_SC_BUILD_PATH)/$(TESTING_SND_LVL_SC_BIN_NAME)

$(TESTING_SND_LVL_SC_BUILD_PATH)/$(TESTING_SND_LVL_SC_BIN_NAME): $(TESTING_SND_LVL_SC_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for second level of lexical analysis testing.")
	$(LINKER) -o $@ $(TESTING_SND_LVL_SC_OBJECTS) $(TESTING_SND_LVL_SC_LIBS) $(LINKERFLAGS)
	@echo "             "

$(TESTING_SND_LVL_SC_BUILD_PATH)/%.o: $(TESTING_SND_LVL_SC_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

parser-gen-prj: parser-gen-lib lbitset parser-gen-lib-testing arkona-parser-generator generate-arkona-parser-lib arkona-parsing-lib testing-aparser-lib

parser-gen-lib: $(PARSER_GENERATOR_BUILD_PATH)/$(PARSER_GENERATOR_LIB_NAME)

$(PARSER_GENERATOR_BUILD_PATH)/$(PARSER_GENERATOR_LIB_NAME): $(PARSER_GENERATOR_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library of parser generator.")
	ar cr $@ $(PARSER_GENERATOR_OBJECTS)
	@echo "             "

$(PARSER_GENERATOR_BUILD_PATH)/%.o: $(PARSER_GENERATOR_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

lbitset: $(LBITSET_BUILD_PATH)/$(LBITSET_LIB_NAME)

$(LBITSET_BUILD_PATH)/$(LBITSET_LIB_NAME): $(LBITSET_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library bitset.")
	ar cr $@ $(LBITSET_OBJECTS)
	@echo "             "

$(LBITSET_BUILD_PATH)/%.o: $(LBITSET_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@


parser-gen-lib-testing: $(PARSER_GENERATOR_TESTING_BUILD_PATH)/$(PARSER_GENERATOR_TESTING_BIN_NAME)

$(PARSER_GENERATOR_TESTING_BUILD_PATH)/$(PARSER_GENERATOR_TESTING_BIN_NAME): $(PARSER_GENERATOR_TESTING_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for parser generating library testing.")
	$(LINKER) -o $@ $(PARSER_GENERATOR_TESTING_OBJECTS) $(TESTING_PARSER_GEN_LIBS) $(CHAR_CONV_LIB) $(LINKERFLAGS)
	@echo "             "

$(PARSER_GENERATOR_TESTING_BUILD_PATH)/%.o: $(PARSER_GENERATOR_TESTING_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

arkona-parser-generator: $(ARKONA_PARSER_GENERATOR_BUILD_PATH)/$(ARKONA_PARSER_GENERATOR_BIN_NAME)

$(ARKONA_PARSER_GENERATOR_BUILD_PATH)/$(ARKONA_PARSER_GENERATOR_BIN_NAME): $(ARKONA_PARSER_GENERATOR_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for Arkona language parser generating.")
	$(LINKER) -o $@ $(ARKONA_PARSER_GENERATOR_OBJECTS) $(PARSER_GEN_LIB) $(CHAR_CONV_LIB) $(LINKERFLAGS) $(LBITSET_LIB)
	@echo "             "

$(ARKONA_PARSER_GENERATOR_BUILD_PATH)/%.o: $(ARKONA_PARSER_GENERATOR_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

generate-arkona-parser-lib:
	$(call BUILD_MSG_PRINT,"Generating of library of parsing of Arkona language.")
	$(ARKONA_PARSER_GENERATOR_BUILD_PATH)/$(ARKONA_PARSER_GENERATOR_BIN_NAME) --output-dir $(ARKONA_PARSER_LIB_DIRECTORY)

arkona-parsing-lib: $(LARKONA_PARSER_BUILD_PATH)/$(LARKONA_PARSER_LIB_NAME)

$(LARKONA_PARSER_BUILD_PATH)/$(LARKONA_PARSER_LIB_NAME): $(LARKONA_PARSER_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library of parsing of the programming language Arkona.")
	ar cr $@ $(LARKONA_PARSER_OBJECTS)
	@echo "             "

$(LARKONA_PARSER_BUILD_PATH)/%.o: $(LARKONA_PARSER_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

testing-aparser-lib: $(ARKONA_PARSER_LIB_TESTING_BUILD_PATH)/$(ARKONA_PARSER_LIB_TESTING_BIN_NAME)

$(ARKONA_PARSER_LIB_TESTING_BUILD_PATH)/$(ARKONA_PARSER_LIB_TESTING_BIN_NAME): $(ARKONA_PARSER_LIB_TESTING_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for Arkona language parser library testing.")
	$(LINKER) -o $@ $(ARKONA_PARSER_LIB_TESTING_OBJECTS) $(TESTING_ARKONA_PARSER_LIBS)
	@echo "             "

$(ARKONA_PARSER_LIB_TESTING_BUILD_PATH)/%.o: $(ARKONA_PARSER_LIB_TESTING_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

recursive-parsing-lib: $(LRECURSIVE_PARSER_BUILD_PATH)/$(LRECURSIVE_PARSER_LIB_NAME)

$(LRECURSIVE_PARSER_BUILD_PATH)/$(LRECURSIVE_PARSER_LIB_NAME): $(LRECURSIVE_PARSER_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library of parsing of the programming language Arkona.")
	ar cr $@ $(LRECURSIVE_PARSER_OBJECTS)
	@echo "             "

$(LRECURSIVE_PARSER_BUILD_PATH)/%.o: $(LRECURSIVE_PARSER_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@
	
recursive-parsing-lib-testing: $(RECURSIVE_PARSER_LIB_TESTING_BUILD_PATH)/$(RECURSIVE_PARSER_LIB_TESTING_BIN_NAME)

$(RECURSIVE_PARSER_LIB_TESTING_BUILD_PATH)/$(RECURSIVE_PARSER_LIB_TESTING_BIN_NAME): $(RECURSIVE_PARSER_LIB_TESTING_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for Arkona language recursive parser library testing.")
	$(LINKER) -o $@ $(RECURSIVE_PARSER_LIB_TESTING_OBJECTS) $(TESTING_RECURSIVE_ARKONA_PARSER_LIBS)
	@echo "             "

$(RECURSIVE_PARSER_LIB_TESTING_BUILD_PATH)/%.o: $(RECURSIVE_PARSER_LIB_TESTING_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	@tput bold; tput setaf 6
	@echo "Removing object files, library files, and executable files."
	@tput sgr0
	rm -f $(TGEN_BUILD_PATH)/$(TGEN_BIN_NAME)
	rm -f $(TGEN_OBJECTS)
	rm -f $(CHAR_CONV_OBJECTS)
	rm -f $(CHAR_CONV_BUILD_PATH)/$(CHAR_CONV_LIB_NAME)
	rm -f $(TRIES_OBJECTS)
	rm -f $(TRIES_BUILD_PATH)/$(TRIES_LIB_NAME)
	rm -f $(LFILES_OBJECTS)
	rm -f $(LFILES_BUILD_PATH)/$(LFILES_LIB_NAME)
	rm -f $(LSCANNER_OBJECTS)
	rm -f $(LSCANNER_BUILD_PATH)/$(LSCANNER_LIB_NAME)
	rm -f $(TESTING_SC_OBJECTS)
	rm -f $(TESTING_SC_BUILD_PATH)/$(TESTING_SC_BIN_NAME)
	rm -f $(NUMBERS_OBJECTS)
	rm -f $(NUMBERS_BUILD_PATH)/$(NUMBERS_LIB_NAME)
	rm -f $(LSCANNER_SND_LVL_OBJECTS)
	rm -f $(LSCANNER_SND_LVL_BUILD_PATH)/$(LSCANNER_SND_LVL_LIB_NAME)
	rm -f $(TESTING_SND_LVL_SC_OBJECTS)
	rm -f $(TESTING_SND_LVL_SC_BUILD_PATH)/$(TESTING_SND_LVL_SC_BIN_NAME)
	rm -f $(PARSER_GENERATOR_OBJECTS)
	rm -f $(PARSER_GENERATOR_BUILD_PATH)/$(PARSER_GENERATOR_LIB_NAME)
	rm -f $(PARSER_GENERATOR_TESTING_OBJECTS)
	rm -f $(PARSER_GENERATOR_TESTING_BUILD_PATH)/$(PARSER_GENERATOR_TESTING_BIN_NAME)
	rm -f $(ARKONA_PARSER_GENERATOR_OBJECTS)
	rm -f $(ARKONA_PARSER_GENERATOR_BUILD_PATH)/$(ARKONA_PARSER_GENERATOR_BIN_NAME)
	rm -f $(LARKONA_PARSER_OBJECTS)
	rm -f $(LARKONA_PARSER_BUILD_PATH)/$(LARKONA_PARSER_LIB_NAME)
	rm -f $(LBITSET_OBJECTS)
	rm -f $(LBITSET_BUILD_PATH)/$(LBITSET_LIB_NAME)
	rm -f $(ARKONA_PARSER_LIB_TESTING_BUILD_PATH)/$(ARKONA_PARSER_LIB_TESTING_BIN_NAME)
	rm -f $(ARKONA_PARSER_LIB_TESTING_OBJECTS)
	rm -f $(LRECURSIVE_PARSER_OBJECTS)
	rm -f $(LRECURSIVE_PARSER_BUILD_PATH)/$(LRECURSIVE_PARSER_LIB_NAME)
	rm -f $(RECURSIVE_PARSER_LIB_TESTING_OBJECTS)
	rm -f $(RECURSIVE_PARSER_LIB_TESTING_BUILD_PATH)/$(RECURSIVE_PARSER_LIB_TESTING_BIN_NAME)
