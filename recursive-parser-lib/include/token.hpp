/*
    File:    token.hpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ATOKEN_H
#define ATOKEN_H
#   include "../../snd-lvl-scanner/include/arkona-scanner-snd-lvl.h"
namespace arkona_parser{
    using Token = arkona_scanner_snd_lvl::Arkona_token_snd_lvl;
};
#endif