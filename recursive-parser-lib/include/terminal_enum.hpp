/*
    File:    terminal_enum.hpp
    Created: 11 October 2020 at 10:38 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef TERMINAL_ENUM_H
#define TERMINAL_ENUM_H
#   include <cstdint>
namespace arkona_parser{
    enum class Terminal_category : std::uint16_t{
        End_of_text,          Unrecognized,         Module,                Id,
        Literal,              Fixed_size_type,      Sizeable_type,         Type_size_modifier,
        In,                   Forever,              Return,                Switch,
        New,                  Delete,               If,                    Then,
        Else,                 Elif,                 For,                   From,
        Dijkstra_loop,        Do,                   While,                 Exit,
        As_long_as,           Match,                As,                    Interpret,
        Main,                 Convert,              Type,                  Enum,
        Array,                Auto,                 Struct,                Enum_set,
        Reference,            Function,             Shkala,                Shkalu,
        Uses,                 Export,               Alias,                 Pure,
        Const,                Var,                  Lambda,                Meta,
        Package,              Type_trait,           Implementation,        Implement,
        Operation,            Postfix_or_prefix,    Type_equivalence,      Belongs,
        Compare,              By_definition,        Assignment,            Cond_op,
        Logical_or_like,      Logical_and_like,     Logical_not,           Relation_op,
        Bitwise_or_like,      Bitwise_and_like,     Bitwise_not,           Addition_like,
        Unary_plus_like,      Mul_like,             Power_like,            Pre_inc_like,
        Post_inc_like,        Cardinality_like,     Round_br_opened,       Round_br_closed,
        Sq_br_opened,         Sq_br_closed,         Fig_br_opened,         Fig_br_closed,
        Tuple_begin,          Tuple_end,            Meta_open_br,          Meta_closed_br,
        Colon_sq_br_opened,   Colon_sq_br_closed,   Colon_fig_br_opened,   Colon_fig_br_closed,
        Comma,                Right_arrow,          Dot,                   Range,
        Colon,                Scope_resol,          Semicolon,             Left_arrow,
        Pattern,              Cycle_name_prefix,    Sizeof_like,           Index_cardinalty,
        Address_like,         Expr_type,            Deref_op,              Pointer_def,
        Non_specific,         Deduce_arg_type,      Block_entry_begin,     Expr_begin
    };
};
#endif