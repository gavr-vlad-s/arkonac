/*
    File:    recursive-parser.hpp
    Created: 04 October 2020 at 14:20 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef RECURSIVE_PARSER_HPP
#define RECURSIVE_PARSER_HPP
#   include <cstddef>
#   include <memory>
#   include "../include/ast_node.hpp"
#   include "../include/symbol_table.hpp"
#   include "../../tries/include/errors_and_tries.h"
#   include "../../tries/include/error_count.h"
#   include "../../tries/include/char_trie.h"
#   include "../../snd-lvl-scanner/include/arkona-scanner-snd-lvl.h"
namespace arkona_parser{
    struct ParserArgs{
        std::shared_ptr<Symbol_table>                    symbol_table_;
        Errors_and_tries                                 et_;
        std::shared_ptr<arkona_scanner_snd_lvl::Scanner> scanner_;
    };

    using AST = ast::AST;

    class Parser final{
    public:
        Parser();
        Parser(const Parser& rhs)            = default;
        ~Parser();
        Parser& operator=(const Parser& rhs) = default;

        explicit Parser(const ParserArgs& args);

        AST compile();

        size_t get_number_of_errors() const;
        size_t get_number_of_warnings() const;
    private:
        struct Impl;
        std::shared_ptr<Impl> pimpl_;
    };
};
#endif