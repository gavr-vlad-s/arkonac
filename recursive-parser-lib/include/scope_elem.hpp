/*
    File:    scope_elem.hpp
    Created: 07 January 2021 at 19:22 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SCOPE_ELEM_HPP
#define SCOPE_ELEM_HPP
#   include <cstdint>
namespace arkona_parser{
    enum class Scope_elem_kind : uint16_t {
        Unknown,  Label,        Variable,     Constant,        Type,
        Function, Metavariable, Metaconstant, Metafunction,    Alias,
        Enum,     Structure,    Enum_elem,    Structure_field
    };

    enum class Standard_math_func_id : uint16_t {
        CIm_func,     CRe_func,    Im_func,      Re_func,
        Lg_func,      Ln_func,     Log2_func,    abs_func,
        arccos_func,  arcctg_func, arcsin_func,  arctg_func,
        cos_func,     ctg_func,    sin_func,     tg_func,
        exp_func,     sqrt_func,   arccosh_func, arcctgh_func,
        arcsinh_func, arctgh_func, cosh_func,    ctgh_func,
        sinh_func,    tgh_func,    sign_func,    ceil_func,
        floor_func,   round_func,  trunc_func,   conj_func,
        Log_func,     min_func,    max_func,     pi_func
    };

    struct Scope_elem
    {
        Scope_elem_kind kind_;
    };
};
#endif