/*
    File:    ast_node.hpp
    Created: 11 October 2020 at 10:38 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef AST_NODE_H
#define AST_NODE_H
#   include <cstddef>
#   include <cstdint>
#   include <list>
#   include <memory>
#   include <quadmath.h>
#   include "../include/token.hpp"
#   include "../../numbers/include/quaternion.h"
namespace arkona_parser{
    namespace ast{
        struct Module;
        struct Qualified_id;
        struct Imports;
        struct Block;
        struct If_stmt;
        struct If_branch;
        struct Assignment_stmt;
        struct Ternary_op;
        struct Not_op;
        struct Range_op;
        struct Binary_op;
        struct Postfix_op;
        struct Prefix_op;
        struct Select_stmt;
        struct Select_branch;
        struct Meta_if_stmt;
        struct Meta_if_branch;
        struct Meta_select_stmt;
        struct Meta_select_branch;
        struct While_stmt;
        struct Meta_while_stmt;
        struct Do_while_stmt;
        struct Meta_do_while_stmt;
        struct Forever_stmt;
        struct For_with_counter_stmt;
        struct Meta_for_with_counter_stmt;
        struct Foreach_stmt;
        struct Meta_foreach_stmt;
        struct Indexed_foreach_stmt;
        struct Meta_indexed_foreach_stmt;
        struct Zipped_foreach_stmt;
        struct Meta_zipped_foreach_stmt;
        struct Return_stmt;
        struct Meta_return_stmt;
        struct Exit_stmt;
        struct Meta_exit_stmt;
        struct Dijkstra_loop_stmt;
        struct Dijkstra_branch;
        struct Meta_dijkstra_loop_stmt;
        struct Meta_dijkstra_branch;
        struct Match_stmt;
        struct Match_branch;
        struct Meta_match_stmt;
        struct Meta_match_branch;
        struct Variables_definition;
        struct Meta_variables_definition;
        struct Constants_definition;
        struct Constant_definition;
        struct Meta_constants_definition;
        struct Meta_constant_definition;
        struct Types_definition;
        struct Type_definition;
        struct Func_definition;
        struct Func_header;
        struct Func_signature;
        struct Formal_aguments_group;
        struct Operation_definition;
        struct Operation_header;
        struct Meta_func_definition;
        struct Meta_func_header;
        struct Meta_func_signature;
        struct Meta_func_formal_aguments_group;
        struct Alias_definition;
        struct Scope_resolution;
        struct Call_expr;
        struct Indexed_expr;
        struct Meta_call_expr;
        struct Field_access_expr;
        struct Struct_construction_from_field_values;
        struct Struct_field_value_expr;
        struct Reinterpret_expr;
        struct Convert_expr;
        struct Tuple_expression;
        struct Set_expression;
        struct Array_expression;
        struct BitArray_expression;
        struct Alloc_expr;
        struct Free_expr;
        struct Alloc_array_expr;
        struct Alloc_bitarray_expr;
        struct Lambda;
        struct Literal_expr;
        struct Fixed_size_base_type;
        struct Sizeable_base_type;
        struct Array_type_expr;
        struct BitArray_type_expr;
        struct Reference_type_expr;
        struct Enum_type_expr;
        struct EnumSet_type_expr;
        struct Auto_type_expr;
        struct Type;
        struct Func_ptr_type_expr;
        struct Struct_type_expr;
        struct Struct_fields_group;
        struct Used_categories;
        struct Used_category;
        struct Category_definition;
        struct Category_definition_body;
        struct Const_entry;
        struct Implement_category;
        struct Category_implementation;

        class Visitor{
        public:
            Visitor()                   = default;
            Visitor(const Visitor& rhs) = default;
            virtual ~Visitor()          = default;

            virtual void visit(Module&                                ref) = 0;
            virtual void visit(Qualified_id&                          ref) = 0;
            virtual void visit(Imports&                               ref) = 0;
            virtual void visit(Block&                                 ref) = 0;
            virtual void visit(If_stmt&                               ref) = 0;
            virtual void visit(If_branch&                             ref) = 0;
            virtual void visit(Assignment_stmt&                       ref) = 0;
            virtual void visit(Ternary_op&                            ref) = 0;
            virtual void visit(Not_op&                                ref) = 0;
            virtual void visit(Range_op&                              ref) = 0;
            virtual void visit(Binary_op&                             ref) = 0;
            virtual void visit(Postfix_op&                            ref) = 0;
            virtual void visit(Prefix_op&                             ref) = 0;
            virtual void visit(Select_stmt&                           ref) = 0;
            virtual void visit(Select_branch&                         ref) = 0;
            virtual void visit(Meta_if_stmt&                          ref) = 0;
            virtual void visit(Meta_if_branch&                        ref) = 0;
            virtual void visit(Meta_select_stmt&                      ref) = 0;
            virtual void visit(Meta_select_branch&                    ref) = 0;
            virtual void visit(While_stmt&                            ref) = 0;
            virtual void visit(Meta_while_stmt&                       ref) = 0;
            virtual void visit(Do_while_stmt&                         ref) = 0;
            virtual void visit(Meta_do_while_stmt&                    ref) = 0;
            virtual void visit(Forever_stmt&                          ref) = 0;
            virtual void visit(For_with_counter_stmt&                 ref) = 0;
            virtual void visit(Meta_for_with_counter_stmt&            ref) = 0;
            virtual void visit(Foreach_stmt&                          ref) = 0;
            virtual void visit(Meta_foreach_stmt&                     ref) = 0;
            virtual void visit(Indexed_foreach_stmt&                  ref) = 0;
            virtual void visit(Meta_indexed_foreach_stmt&             ref) = 0;
            virtual void visit(Zipped_foreach_stmt&                   ref) = 0;
            virtual void visit(Meta_zipped_foreach_stmt&              ref) = 0;
            virtual void visit(Return_stmt&                           ref) = 0;
            virtual void visit(Meta_return_stmt&                      ref) = 0;
            virtual void visit(Exit_stmt&                             ref) = 0;
            virtual void visit(Meta_exit_stmt&                        ref) = 0;
            virtual void visit(Dijkstra_loop_stmt&                    ref) = 0;
            virtual void visit(Dijkstra_branch&                       ref) = 0;
            virtual void visit(Meta_dijkstra_loop_stmt&               ref) = 0;
            virtual void visit(Meta_dijkstra_branch&                  ref) = 0;
            virtual void visit(Match_stmt&                            ref) = 0;
            virtual void visit(Match_branch&                          ref) = 0;
            virtual void visit(Meta_match_stmt&                       ref) = 0;
            virtual void visit(Meta_match_branch&                     ref) = 0;
            virtual void visit(Variables_definition&                  ref) = 0;
            virtual void visit(Meta_variables_definition&             ref) = 0;
            virtual void visit(Constants_definition&                  ref) = 0;
            virtual void visit(Constant_definition&                   ref) = 0;
            virtual void visit(Meta_constants_definition&             ref) = 0;
            virtual void visit(Meta_constant_definition&              ref) = 0;
            virtual void visit(Types_definition&                      ref) = 0;
            virtual void visit(Type_definition&                       ref) = 0;
            virtual void visit(Func_definition&                       ref) = 0;
            virtual void visit(Func_header&                           ref) = 0;
            virtual void visit(Func_signature&                        ref) = 0;
            virtual void visit(Formal_aguments_group&                 ref) = 0;
            virtual void visit(Operation_definition&                  ref) = 0;
            virtual void visit(Operation_header&                      ref) = 0;
            virtual void visit(Meta_func_definition&                  ref) = 0;
            virtual void visit(Meta_func_header&                      ref) = 0;
            virtual void visit(Meta_func_signature&                   ref) = 0;
            virtual void visit(Meta_func_formal_aguments_group&       ref) = 0;
            virtual void visit(Alias_definition&                      ref) = 0;
            virtual void visit(Scope_resolution&                      ref) = 0;
            virtual void visit(Call_expr&                             ref) = 0;
            virtual void visit(Indexed_expr&                          ref) = 0;
            virtual void visit(Meta_call_expr&                        ref) = 0;
            virtual void visit(Field_access_expr&                     ref) = 0;
            virtual void visit(Struct_construction_from_field_values& ref) = 0;
            virtual void visit(Struct_field_value_expr&               ref) = 0;
            virtual void visit(Reinterpret_expr&                      ref) = 0;
            virtual void visit(Convert_expr&                          ref) = 0;
            virtual void visit(Tuple_expression&                      ref) = 0;
            virtual void visit(Set_expression&                        ref) = 0;
            virtual void visit(Array_expression&                      ref) = 0;
            virtual void visit(BitArray_expression&                   ref) = 0;
            virtual void visit(Alloc_expr&                            ref) = 0;
            virtual void visit(Free_expr&                             ref) = 0;
            virtual void visit(Alloc_array_expr&                      ref) = 0;
            virtual void visit(Alloc_bitarray_expr&                   ref) = 0;
            virtual void visit(Lambda&                                ref) = 0;
            virtual void visit(Literal_expr&                          ref) = 0;
            virtual void visit(Fixed_size_base_type&                  ref) = 0;
            virtual void visit(Sizeable_base_type&                    ref) = 0;
            virtual void visit(Array_type_expr&                       ref) = 0;
            virtual void visit(BitArray_type_expr&                    ref) = 0;
            virtual void visit(Reference_type_expr&                   ref) = 0;
            virtual void visit(Enum_type_expr&                        ref) = 0;
            virtual void visit(EnumSet_type_expr&                     ref) = 0;
            virtual void visit(Auto_type_expr&                        ref) = 0;
            virtual void visit(Type&                                  ref) = 0;
            virtual void visit(Func_ptr_type_expr&                    ref) = 0;
            virtual void visit(Struct_type_expr&                      ref) = 0;
            virtual void visit(Struct_fields_group&                   ref) = 0;
            virtual void visit(Used_categories&                       ref) = 0;
            virtual void visit(Used_category&                         ref) = 0;
            virtual void visit(Category_definition&                   ref) = 0;
            virtual void visit(Category_definition_body&              ref) = 0;
            virtual void visit(Const_entry&                           ref) = 0;
            virtual void visit(Implement_category&                    ref) = 0;
            virtual void visit(Category_implementation&               ref) = 0;
        };

        class Node{
        public:
            Node()                = default;
            Node(const Node& rhs) = default;
            virtual ~Node()       = default;

            virtual void accept(Visitor& v) = 0;
        };

        using Node_ptr = std::shared_ptr<Node>;

        template<typename Derived>
        class Visitable : public Node{
        public:
            using Node::Node;
            void accept(Visitor& v) override
            {
                v.visit(*static_cast<Derived*>(this));
            }
        };

        struct Module : public Visitable<Module>{
        public:
            using Visitable<Module>::Visitable;

            Module()                  = default;
            Module(const Module& rhs) = default;
            virtual ~Module()         = default;

            Module(const std::shared_ptr<Qualified_id>& name,
                   const std::shared_ptr<Block>&        body,
                   const std::shared_ptr<Imports>&      used_modules)
            : name_{name}, body_{body}, used_modules_{used_modules}
            {
            }

            std::shared_ptr<Qualified_id> name_;
            std::shared_ptr<Block>        body_;
            std::shared_ptr<Imports>      used_modules_;
        };

        struct Qualified_id : public Visitable<Qualified_id>{
        public:
            using Visitable<Qualified_id>::Visitable;

            Qualified_id()                        = default;
            Qualified_id(const Qualified_id& rhs) = default;
            virtual ~Qualified_id()               = default;

            explicit Qualified_id(const std::list<Token>& name_components)
            : name_components_{name_components}
            {
            }

            std::list<Token> name_components_;
        };

        struct Imports : public Visitable<Imports>{
        public:
            using Visitable<Imports>::Visitable;

            Imports()                   = default;
            Imports(const Imports& rhs) = default;
            virtual ~Imports()          = default;

            explicit Imports(const std::list<std::shared_ptr<Qualified_id>>& imports)
            : imports_{imports}
            {
            }

            std::list<std::shared_ptr<Qualified_id>> imports_;
        };

        struct Block : public Visitable<Block>{
        public:
            using Visitable<Block>::Visitable;

            Block()                 = default;
            Block(const Block& rhs) = default;
            virtual ~Block()        = default;

            explicit Block(const std::list<std::shared_ptr<Node>>& entries)
            : entries_{entries}
            {
            }

            std::list<std::shared_ptr<Node>> entries_;
        };

        enum class Prefix_op_kind : size_t{
            Non_prefix_op, Prefix_inc,                  Prefixfix_inc_with_wrapping,
            Prefix_dec,    Prefixfix_dec_with_wrapping, ElemType,
            Unary_plus,    Unary_minus,                 Data_size,
            Sharp,         Card,                        Address,
            Data_address,  Pointer_def
        };

        struct Prefix_op : public Visitable<Prefix_op>{
            using Visitable<Prefix_op>::Visitable;

            Prefix_op()                     = default;
            Prefix_op(const Prefix_op& rhs) = default;
            virtual ~Prefix_op()            = default;

            Prefix_op(Prefix_op_kind               kind,
                      const std::shared_ptr<Node>& operand)
            : kind_{kind}, operand_{operand}
            {
            }

            Prefix_op_kind        kind_ = Prefix_op_kind::Non_prefix_op;
            std::shared_ptr<Node> operand_;
        };

        enum class Postfix_op_kind : size_t{
            Non_postfix_op, Postfix_inc,               Postfix_inc_with_wrapping,
            Postfix_dec,    Postfix_dec_with_wrapping, Dereference
        };

        struct Postfix_op : public Visitable<Postfix_op>{
            using Visitable<Postfix_op>::Visitable;

            Postfix_op()                      = default;
            Postfix_op(const Postfix_op& rhs) = default;
            virtual ~Postfix_op()             = default;

            Postfix_op(Postfix_op_kind              kind,
                       const std::shared_ptr<Node>& operand)
            : kind_{kind}, operand_{operand}
            {
            }

            Postfix_op_kind       kind_ = Postfix_op_kind::Non_postfix_op;
            std::shared_ptr<Node> operand_;
        };

        enum class Binary_op_kind : size_t{
            Non_binary_op,         Logical_or,          Logical_or_full,
            Logical_or_not,        Logical_or_not_full, Logical_xor,
            Logical_and,           Logical_and_full,    Logical_and_not,
            Logical_and_not_full,  Compare,             Eq,
            Neq,                   Lt,                  Gt,
            Leq,                   Geq,                 Type_equiv,
            Bitwise_or,            Bitwise_or_not,      Bitwise_xor,
            Bitwise_and,           Bitwise_and_not,     Left_shift,
            Right_shift,           Add,                 Sub,
            Algebraic_sep,         Meta_add,            Mul,
            Div,                   Remainder,           Float_remainder,
            Symmetric_difference,  Set_difference,      Power,
            Float_power,           Dimension_size
        };

        struct Binary_op : public Visitable<Binary_op>{
            using Visitable<Binary_op>::Visitable;

            Binary_op()                     = default;
            Binary_op(const Binary_op& rhs) = default;
            virtual ~Binary_op()            = default;

            Binary_op(Binary_op_kind               kind,
                      const std::shared_ptr<Node>& left_operand,
                      const std::shared_ptr<Node>& right_operand)
            : kind_{kind}, left_operand_{left_operand}, right_operand_{right_operand}
            {
            }

            Binary_op_kind        kind_ = Binary_op_kind::Non_binary_op;
            std::shared_ptr<Node> left_operand_;
            std::shared_ptr<Node> right_operand_;
        };

        enum class Range_op_kind : size_t{
            Non_range_op, Range
        };

        struct Range_op : public Visitable<Range_op>{
            using Visitable<Range_op>::Visitable;

            Range_op()                    = default;
            Range_op(const Range_op& rhs) = default;
            virtual ~Range_op()           = default;

            Range_op(Range_op_kind                kind,
                     const std::shared_ptr<Node>& start,
                     const std::shared_ptr<Node>& end,
                     const std::shared_ptr<Node>& step)
            : kind_{kind}, start_{start}, end_{end}, step_{step}
            {
            }

            Range_op_kind         kind_ = Range_op_kind::Non_range_op;
            std::shared_ptr<Node> start_;
            std::shared_ptr<Node> end_;
            std::shared_ptr<Node> step_;
        };

        enum class Not_op_kind : size_t{
            Logical, Bitwise
        };

        struct Not_op : public Visitable<Not_op>{
            using Visitable<Not_op>::Visitable;

            Not_op()                  = default;
            Not_op(const Not_op& rhs) = default;
            virtual ~Not_op()         = default;

            Not_op(Not_op_kind                  kind,
                   std::size_t                  order,
                   const std::shared_ptr<Node>& operand)
            : kind_{kind}, order_{order}, operand_{operand}
            {
            }

            Not_op_kind           kind_  = Not_op_kind::Logical;
            std::size_t           order_ = 0;
            std::shared_ptr<Node> operand_;
        };

        enum class Cond_op_kind : size_t{
            Not_cond_op, Complete_calc, Short_circuit_calc
        };

        struct Ternary_op : public Visitable<Ternary_op>{
            using Visitable<Ternary_op>::Visitable;

            Ternary_op()                      = default;
            Ternary_op(const Ternary_op& rhs) = default;
            virtual ~Ternary_op()             = default;

            Ternary_op(Cond_op_kind                 kind,
                       const std::shared_ptr<Node>& condition,
                       const std::shared_ptr<Node>& true_branch,
                       const std::shared_ptr<Node>& false_branch)
            : kind_{kind}, condition_{condition}, true_branch_{true_branch}, false_branch_{false_branch}
            {
            }

            Cond_op_kind                       kind_ = Cond_op_kind::Not_cond_op;
            std::shared_ptr<Node> condition_;
            std::shared_ptr<Node> true_branch_;
            std::shared_ptr<Node> false_branch_;
        };

        enum class Assignment_kind : size_t{
            Non_assignment,          Assignment,                  Copy,
            Logical_or_assign,       Logical_or_not_full_assign,  Logical_or_not_assign,
            Logical_or_full_assign,  Logical_xor_assign,          Logical_and_assign,
            Logical_and_full_assign, Logical_and_not_assign,      Logical_and_not_full_assign,
            Bitwise_or_assign,       Bitwise_or_not_assign,       Bitwise_xor_assign,
            Bitwise_and_assign,      Bitwise_and_not_assign,      Left_shift_assign,
            Right_shift_assign,      Plus_assign,                 Minus_assign,
            Mul_assign,              Div_assign,                  Remainder_assign,
            Float_remainder_assign,  Symmetric_difference_assign, Set_difference_assign,
            Power_assign,            Float_power_assign,          Meta_plus_assign
        };

        struct Assignment_stmt : public Visitable<Assignment_stmt>{
            using Visitable<Assignment_stmt>::Visitable;

            Assignment_stmt()                           = default;
            Assignment_stmt(const Assignment_stmt& rhs) = default;
            virtual ~Assignment_stmt()                  = default;

            Assignment_stmt(Assignment_kind                    kind,
                            const std::shared_ptr<Ternary_op>& lhs,
                            const std::shared_ptr<Ternary_op>& rhs)
            : kind_{kind}, lhs_{lhs}, rhs_{rhs}
            {
            }

            Assignment_kind        kind_ = Assignment_kind::Non_assignment;
            std::shared_ptr<Ternary_op> lhs_;
            std::shared_ptr<Ternary_op> rhs_;
        };

        struct If_branch : public Visitable<If_branch>{
            using Visitable<If_branch>::Visitable;

            If_branch()                     = default;
            If_branch(const If_branch& rhs) = default;
            virtual ~If_branch()            = default;

            If_branch(const std::shared_ptr<Node>&  condition,
                      const std::shared_ptr<Block>& actions)
            : condition_{condition}, actions_{actions}
            {
            }

            std::shared_ptr<Node>  condition_;
            std::shared_ptr<Block> actions_;
        };

        struct If_stmt : public Visitable<If_stmt>{
        public:
            using Visitable<If_stmt>::Visitable;

            If_stmt()                   = default;
            If_stmt(const If_stmt& rhs) = default;
            virtual ~If_stmt()          = default;

            If_stmt(const std::list<std::shared_ptr<If_branch>>& non_else_branches,
                    const std::shared_ptr<Block>&                else_branch)
            : non_else_branches_{non_else_branches}, else_branch_{else_branch}
            {
            }

            std::list<std::shared_ptr<If_branch>> non_else_branches_;
            std::shared_ptr<Block>                else_branch_;
        };

        struct Meta_if_branch : public Visitable<Meta_if_branch>{
            using Visitable<Meta_if_branch>::Visitable;

            Meta_if_branch()                          = default;
            Meta_if_branch(const Meta_if_branch& rhs) = default;
            virtual ~Meta_if_branch()                 = default;

            Meta_if_branch(const std::shared_ptr<Node>&  condition,
                           const std::shared_ptr<Block>& actions)
            : condition_{condition}, actions_{actions}
            {
            }

            std::shared_ptr<Node>  condition_;
            std::shared_ptr<Block> actions_;
        };

        struct Meta_if_stmt : public Visitable<Meta_if_stmt>{
        public:
            using Visitable<Meta_if_stmt>::Visitable;

            Meta_if_stmt()                        = default;
            Meta_if_stmt(const Meta_if_stmt& rhs) = default;
            virtual ~Meta_if_stmt()               = default;

            Meta_if_stmt(const std::list<std::shared_ptr<Meta_if_branch>>& non_else_branches,
                         const std::shared_ptr<Block>&                     else_branch)
            : non_else_branches_{non_else_branches}, else_branch_{else_branch}
            {
            }

            std::list<std::shared_ptr<Meta_if_branch>> non_else_branches_;
            std::shared_ptr<Block>                     else_branch_;
        };

        struct Select_branch : public Visitable<Select_branch>{
        public:
            using Visitable<Select_branch>::Visitable;

            Select_branch()                         = default;
            Select_branch(const Select_branch& rhs) = default;
            virtual ~Select_branch()                = default;

            Select_branch(const std::list<std::shared_ptr<Node>>& expressions,
                          const std::shared_ptr<Block>&           actions)
            : expressions_{expressions}, actions_{actions}
            {
            }

            std::list<std::shared_ptr<Node>> expressions_;
            std::shared_ptr<Block>           actions_;
        };

        struct Select_stmt : public Visitable<Select_stmt>{
        public:
            using Visitable<Select_stmt>::Visitable;

            Select_stmt()                       = default;
            Select_stmt(const Select_stmt& rhs) = default;
            virtual ~Select_stmt()              = default;

            Select_stmt(const std::shared_ptr<Node>&                     value_to_select,
                        const std::list<std::shared_ptr<Select_branch>>& non_else_branches,
                        const std::shared_ptr<Block>&                    else_branch)
            : value_to_select_{value_to_select}
            , non_else_branches_{non_else_branches}
            , else_branch_{else_branch}
            {
            }

            std::shared_ptr<Node>                     value_to_select_;
            std::list<std::shared_ptr<Select_branch>> non_else_branches_;
            std::shared_ptr<Block>                    else_branch_;
        };

        struct Meta_select_branch : public Visitable<Meta_select_branch>{
        public:
            using Visitable<Meta_select_branch>::Visitable;

            Meta_select_branch()                              = default;
            Meta_select_branch(const Meta_select_branch& rhs) = default;
            virtual ~Meta_select_branch()                     = default;

            Meta_select_branch(const std::list<std::shared_ptr<Node>>& expressions,
                               const std::shared_ptr<Block>&           actions)
            : expressions_{expressions}, actions_{actions}
            {
            }

            std::list<std::shared_ptr<Node>> expressions_;
            std::shared_ptr<Block>           actions_;
        };

        struct Meta_select_stmt : public Visitable<Meta_select_stmt>{
        public:
            using Visitable<Meta_select_stmt>::Visitable;

            Meta_select_stmt()                            = default;
            Meta_select_stmt(const Meta_select_stmt& rhs) = default;
            virtual ~Meta_select_stmt()                   = default;

            Meta_select_stmt(const std::shared_ptr<Node>&                          value_to_select,
                             const std::list<std::shared_ptr<Meta_select_branch>>& non_else_branches,
                             const std::shared_ptr<Block>&                         else_branch)
            : value_to_select_{value_to_select}
            , non_else_branches_{non_else_branches}
            , else_branch_{else_branch}
            {
            }

            std::shared_ptr<Node>                          value_to_select_;
            std::list<std::shared_ptr<Meta_select_branch>> non_else_branches_;
            std::shared_ptr<Block>                         else_branch_;
        };

        struct Meta_while_stmt : public Visitable<Meta_while_stmt>{
        public:
            using Visitable<Meta_while_stmt>::Visitable;

            Meta_while_stmt()                           = default;
            Meta_while_stmt(const Meta_while_stmt& rhs) = default;
            virtual ~Meta_while_stmt()             = default;

            Meta_while_stmt(std::size_t                   cycle_label,
                            const std::shared_ptr<Node>&  cycle_condition,
                            const std::shared_ptr<Block>& cycle_body)
            : cycle_label_{cycle_label}, cycle_condition_{cycle_condition}, cycle_body_{cycle_body}
            {
            }

            std::size_t            cycle_label_ = 0;
            std::shared_ptr<Node>  cycle_condition_;
            std::shared_ptr<Block> cycle_body_;
        };

        struct While_stmt : public Visitable<While_stmt>{
        public:
            using Visitable<While_stmt>::Visitable;

            While_stmt()                      = default;
            While_stmt(const While_stmt& rhs) = default;
            virtual ~While_stmt()             = default;

            While_stmt(std::size_t                   cycle_label,
                       const std::shared_ptr<Node>&  cycle_condition,
                       const std::shared_ptr<Block>& cycle_body)
            : cycle_label_{cycle_label}, cycle_condition_{cycle_condition}, cycle_body_{cycle_body}
            {
            }

            std::size_t            cycle_label_ = 0;
            std::shared_ptr<Node>  cycle_condition_;
            std::shared_ptr<Block> cycle_body_;
        };

        struct Do_while_stmt : public Visitable<Do_while_stmt>{
        public:
            using Visitable<Do_while_stmt>::Visitable;

            Do_while_stmt()                         = default;
            Do_while_stmt(const Do_while_stmt& rhs) = default;
            virtual ~Do_while_stmt()                = default;

            Do_while_stmt(std::size_t                   cycle_label,
                          const std::shared_ptr<Node>&  cycle_condition,
                          const std::shared_ptr<Block>& cycle_body)
            : cycle_label_{cycle_label}, cycle_condition_{cycle_condition}, cycle_body_{cycle_body}
            {
            }

            std::size_t            cycle_label_ = 0;
            std::shared_ptr<Node>  cycle_condition_;
            std::shared_ptr<Block> cycle_body_;
        };

        struct Meta_do_while_stmt : public Visitable<Meta_do_while_stmt>{
        public:
            using Visitable<Meta_do_while_stmt>::Visitable;

            Meta_do_while_stmt()                              = default;
            Meta_do_while_stmt(const Meta_do_while_stmt& rhs) = default;
            virtual ~Meta_do_while_stmt()                     = default;

            Meta_do_while_stmt(std::size_t                   cycle_label,
                               const std::shared_ptr<Node>&  cycle_condition,
                               const std::shared_ptr<Block>& cycle_body)
            : cycle_label_{cycle_label}, cycle_condition_{cycle_condition}, cycle_body_{cycle_body}
            {
            }

            std::size_t            cycle_label_ = 0;
            std::shared_ptr<Node>  cycle_condition_;
            std::shared_ptr<Block> cycle_body_;
        };

        struct Forever_stmt : public Visitable<Forever_stmt>{
        public:
            using Visitable<Forever_stmt>::Visitable;

            Forever_stmt()                        = default;
            Forever_stmt(const Forever_stmt& rhs) = default;
            virtual ~Forever_stmt()               = default;

            Forever_stmt(std::size_t                   cycle_label,
                         const std::shared_ptr<Block>& cycle_body)
            : cycle_label_{cycle_label}, cycle_body_{cycle_body}
            {
            }

            std::size_t            cycle_label_ = 0;
            std::shared_ptr<Block> cycle_body_;
        };

        struct For_with_counter_stmt : public Visitable<For_with_counter_stmt>{
        public:
            using Visitable<For_with_counter_stmt>::Visitable;

            For_with_counter_stmt()                                 = default;
            For_with_counter_stmt(const For_with_counter_stmt& rhs) = default;
            virtual ~For_with_counter_stmt()                        = default;

            For_with_counter_stmt(std::size_t                   cycle_label,
                                  std::size_t                   loop_var,
                                  const std::shared_ptr<Node>&  start_value,
                                  const std::shared_ptr<Node>&  end_value,
                                  const std::shared_ptr<Node>&  step_value,
                                  const std::shared_ptr<Block>& cycle_body)
            : cycle_label_{cycle_label}
            , loop_var_{loop_var}
            , start_value_{start_value}
            , end_value_{end_value}
            , step_value_{step_value}
            , cycle_body_{cycle_body}
            {
            }

            std::size_t            cycle_label_ = 0;
            std::size_t            loop_var_    = 0;
            std::shared_ptr<Node>  start_value_;
            std::shared_ptr<Node>  end_value_;
            std::shared_ptr<Node>  step_value_;
            std::shared_ptr<Block> cycle_body_;
        };

        struct Meta_for_with_counter_stmt : public Visitable<Meta_for_with_counter_stmt>{
        public:
            using Visitable<Meta_for_with_counter_stmt>::Visitable;

            Meta_for_with_counter_stmt()                                      = default;
            Meta_for_with_counter_stmt(const Meta_for_with_counter_stmt& rhs) = default;
            virtual ~Meta_for_with_counter_stmt()                             = default;

            Meta_for_with_counter_stmt(std::size_t                   cycle_label,
                                       std::size_t                   loop_var,
                                       const std::shared_ptr<Node>&  start_value,
                                       const std::shared_ptr<Node>&  end_value,
                                       const std::shared_ptr<Node>&  step_value,
                                       const std::shared_ptr<Block>& cycle_body)
            : cycle_label_{cycle_label}
            , loop_var_{loop_var}
            , start_value_{start_value}
            , end_value_{end_value}
            , step_value_{step_value}
            , cycle_body_{cycle_body}
            {
            }

            std::size_t            cycle_label_ = 0;
            std::size_t            loop_var_    = 0;
            std::shared_ptr<Node>  start_value_;
            std::shared_ptr<Node>  end_value_;
            std::shared_ptr<Node>  step_value_;
            std::shared_ptr<Block> cycle_body_;
        };

        struct Foreach_stmt : public Visitable<Foreach_stmt>{
        public:
            using Visitable<Foreach_stmt>::Visitable;

            Foreach_stmt()                        = default;
            Foreach_stmt(const Foreach_stmt& rhs) = default;
            virtual ~Foreach_stmt()               = default;

            Foreach_stmt(std::size_t                   cycle_label,
                         std::size_t                   loop_var,
                         const std::shared_ptr<Node>&  collection,
                         const std::shared_ptr<Block>& cycle_body)
            : cycle_label_{cycle_label}
            , loop_var_{loop_var}
            , collection_{collection}
            , cycle_body_{cycle_body}
            {
            }

            std::size_t            cycle_label_ = 0;
            std::size_t            loop_var_    = 0;
            std::shared_ptr<Node>  collection_;
            std::shared_ptr<Block> cycle_body_;
        };

        struct Meta_foreach_stmt : public Visitable<Meta_foreach_stmt>{
        public:
            using Visitable<Meta_foreach_stmt>::Visitable;

            Meta_foreach_stmt()                             = default;
            Meta_foreach_stmt(const Meta_foreach_stmt& rhs) = default;
            virtual ~Meta_foreach_stmt()                    = default;

            Meta_foreach_stmt(std::size_t                   cycle_label,
                              std::size_t                   loop_var,
                              const std::shared_ptr<Node>&  collection,
                              const std::shared_ptr<Block>& cycle_body)
            : cycle_label_{cycle_label}
            , loop_var_{loop_var}
            , collection_{collection}
            , cycle_body_{cycle_body}
            {
            }

            std::size_t            cycle_label_ = 0;
            std::size_t            loop_var_    = 0;
            std::shared_ptr<Node>  collection_;
            std::shared_ptr<Block> cycle_body_;
        };

        struct Indexed_foreach_stmt : public Visitable<Indexed_foreach_stmt>{
        public:
            using Visitable<Indexed_foreach_stmt>::Visitable;

            Indexed_foreach_stmt()                                = default;
            Indexed_foreach_stmt(const Indexed_foreach_stmt& rhs) = default;
            virtual ~Indexed_foreach_stmt()                       = default;

            Indexed_foreach_stmt(std::size_t                   cycle_label,
                                 std::size_t                   index_var,
                                 std::size_t                   value_var,
                                 const std::shared_ptr<Node>&  collection,
                                 const std::shared_ptr<Block>& cycle_body)
            : cycle_label_{cycle_label}
            , index_var_{index_var}
            , value_var_{value_var}
            , collection_{collection}
            , cycle_body_{cycle_body}
            {
            }

            std::size_t            cycle_label_ = 0;
            std::size_t            index_var_   = 0;
            std::size_t            value_var_   = 0;
            std::shared_ptr<Node>  collection_;
            std::shared_ptr<Block> cycle_body_;
        };

        struct Meta_indexed_foreach_stmt : public Visitable<Meta_indexed_foreach_stmt>{
        public:
            using Visitable<Meta_indexed_foreach_stmt>::Visitable;

            Meta_indexed_foreach_stmt()                                     = default;
            Meta_indexed_foreach_stmt(const Meta_indexed_foreach_stmt& rhs) = default;
            virtual ~Meta_indexed_foreach_stmt()                            = default;

            Meta_indexed_foreach_stmt(std::size_t                   cycle_label,
                                      std::size_t                   index_var,
                                      std::size_t                   value_var,
                                      const std::shared_ptr<Node>&  collection,
                                      const std::shared_ptr<Block>& cycle_body)
            : cycle_label_{cycle_label}
            , index_var_{index_var}
            , value_var_{value_var}
            , collection_{collection}
            , cycle_body_{cycle_body}
            {
            }

            std::size_t            cycle_label_ = 0;
            std::size_t            index_var_   = 0;
            std::size_t            value_var_   = 0;
            std::shared_ptr<Node>  collection_;
            std::shared_ptr<Block> cycle_body_;
        };

        struct Zipped_foreach_stmt : public Visitable<Zipped_foreach_stmt>{
        public:
            using Visitable<Zipped_foreach_stmt>::Visitable;

            Zipped_foreach_stmt()                               = default;
            Zipped_foreach_stmt(const Zipped_foreach_stmt& rhs) = default;
            virtual ~Zipped_foreach_stmt()                      = default;

            Zipped_foreach_stmt(std::size_t                   cycle_label,
                                std::list<std::size_t>        loop_vars,
                                const std::shared_ptr<Node>&  collection,
                                const std::shared_ptr<Block>& cycle_body)
            : cycle_label_{cycle_label}
            , loop_vars_{loop_vars}
            , collection_{collection}
            , cycle_body_{cycle_body}
            {
            }

            std::size_t            cycle_label_ = 0;
            std::list<std::size_t> loop_vars_;
            std::shared_ptr<Node>  collection_;
            std::shared_ptr<Block> cycle_body_;
        };

        struct Meta_zipped_foreach_stmt : public Visitable<Meta_zipped_foreach_stmt>{
        public:
            using Visitable<Meta_zipped_foreach_stmt>::Visitable;

            Meta_zipped_foreach_stmt()                                    = default;
            Meta_zipped_foreach_stmt(const Meta_zipped_foreach_stmt& rhs) = default;
            virtual ~Meta_zipped_foreach_stmt()                           = default;

            Meta_zipped_foreach_stmt(std::size_t                   cycle_label,
                                     std::list<std::size_t>        loop_vars,
                                     const std::shared_ptr<Node>&  collection,
                                     const std::shared_ptr<Block>& cycle_body)
            : cycle_label_{cycle_label}
            , loop_vars_{loop_vars}
            , collection_{collection}
            , cycle_body_{cycle_body}
            {
            }

            std::size_t            cycle_label_ = 0;
            std::list<std::size_t> loop_vars_;
            std::shared_ptr<Node>  collection_;
            std::shared_ptr<Block> cycle_body_;
        };

        struct Return_stmt : public Visitable<Return_stmt>{
        public:
            using Visitable<Return_stmt>::Visitable;

            Return_stmt()                       = default;
            Return_stmt(const Return_stmt& rhs) = default;
            virtual ~Return_stmt()              = default;

            Return_stmt(const std::shared_ptr<Node>& returned_value)
            : returned_value_{returned_value}
            {
            }

            std::shared_ptr<Node> returned_value_;
        };

        struct Meta_return_stmt : public Visitable<Meta_return_stmt>{
        public:
            using Visitable<Meta_return_stmt>::Visitable;

            Meta_return_stmt()                            = default;
            Meta_return_stmt(const Meta_return_stmt& rhs) = default;
            virtual ~Meta_return_stmt()                   = default;

            Meta_return_stmt(const std::shared_ptr<Node>& returned_value)
            : returned_value_{returned_value}
            {
            }

            std::shared_ptr<Node> returned_value_;
        };

        struct Exit_stmt : public Visitable<Exit_stmt>{
        public:
            using Visitable<Exit_stmt>::Visitable;

            Exit_stmt()                     = default;
            Exit_stmt(const Exit_stmt& rhs) = default;
            virtual ~Exit_stmt()            = default;

            explicit Exit_stmt(std::size_t loop_label)
            : loop_label_{loop_label}
            {
            }

            std::size_t loop_label_ = 0;
        };

        struct Meta_exit_stmt : public Visitable<Meta_exit_stmt>{
        public:
            using Visitable<Meta_exit_stmt>::Visitable;

            Meta_exit_stmt()                          = default;
            Meta_exit_stmt(const Meta_exit_stmt& rhs) = default;
            virtual ~Meta_exit_stmt()                 = default;

            explicit Meta_exit_stmt(std::size_t loop_label)
            : loop_label_{loop_label}
            {
            }

            std::size_t loop_label_ = 0;
        };

        struct Dijkstra_branch : public Visitable<Dijkstra_branch>{
        public:
            using Visitable<Dijkstra_branch>::Visitable;

            Dijkstra_branch()                           = default;
            Dijkstra_branch(const Dijkstra_branch& rhs) = default;
            virtual ~Dijkstra_branch()                  = default;

            Dijkstra_branch(const std::shared_ptr<Node>&  condition,
                            const std::shared_ptr<Block>& actions)
            : condition_{condition}
            , actions_{actions}
            {
            }

            std::shared_ptr<Node>  condition_;
            std::shared_ptr<Block> actions_;
        };

        struct Dijkstra_loop_stmt : public Visitable<Dijkstra_loop_stmt>{
        public:
            using Visitable<Dijkstra_loop_stmt>::Visitable;

            Dijkstra_loop_stmt()                              = default;
            Dijkstra_loop_stmt(const Dijkstra_loop_stmt& rhs) = default;
            virtual ~Dijkstra_loop_stmt()                     = default;

            Dijkstra_loop_stmt(std::size_t                                        cycle_label,
                               const std::list<std::shared_ptr<Dijkstra_branch>>& non_else_branches,
                               const std::shared_ptr<Block>&                      else_branch)
            : cycle_label_{cycle_label}
            , non_else_branches_{non_else_branches}
            , else_branch_{else_branch}
            {
            }

            std::size_t                                 cycle_label_ = 0;
            std::list<std::shared_ptr<Dijkstra_branch>> non_else_branches_;
            std::shared_ptr<Block>                      else_branch_;
        };

        struct Meta_dijkstra_branch : public Visitable<Meta_dijkstra_branch>{
        public:
            using Visitable<Meta_dijkstra_branch>::Visitable;

            Meta_dijkstra_branch()                           = default;
            Meta_dijkstra_branch(const Meta_dijkstra_branch& rhs) = default;
            virtual ~Meta_dijkstra_branch()                  = default;

            Meta_dijkstra_branch(const std::shared_ptr<Node>&  condition,
                                 const std::shared_ptr<Block>& actions)
            : condition_{condition}
            , actions_{actions}
            {
            }

            std::shared_ptr<Node>  condition_;
            std::shared_ptr<Block> actions_;
        };

        struct Meta_dijkstra_loop_stmt : public Visitable<Meta_dijkstra_loop_stmt>{
        public:
            using Visitable<Meta_dijkstra_loop_stmt>::Visitable;

            Meta_dijkstra_loop_stmt()                                   = default;
            Meta_dijkstra_loop_stmt(const Meta_dijkstra_loop_stmt& rhs) = default;
            virtual ~Meta_dijkstra_loop_stmt()                          = default;

            Meta_dijkstra_loop_stmt(std::size_t                                             cycle_label,
                                    const std::list<std::shared_ptr<Meta_dijkstra_branch>>& non_else_branches,
                                    const std::shared_ptr<Block>&                           else_branch)
            : cycle_label_{cycle_label}
            , non_else_branches_{non_else_branches}
            , else_branch_{else_branch}
            {
            }

            std::size_t                                      cycle_label_ = 0;
            std::list<std::shared_ptr<Meta_dijkstra_branch>> non_else_branches_;
            std::shared_ptr<Block>                           else_branch_;
        };

        struct Match_branch : public Visitable<Match_branch>{
        public:
            using Visitable<Match_branch>::Visitable;

            Match_branch()                        = default;
            Match_branch(const Match_branch& rhs) = default;
            virtual ~Match_branch()               = default;

            Match_branch(const std::shared_ptr<Node>&  condition,
                         const std::shared_ptr<Block>& actions)
            : condition_{condition}
            , actions_{actions}
            {
            }

            std::shared_ptr<Node>  condition_;
            std::shared_ptr<Block> actions_;
        };

        struct Match_stmt : public Visitable<Match_stmt>{
        public:
            using Visitable<Match_stmt>::Visitable;

            Match_stmt()                      = default;
            Match_stmt(const Match_stmt& rhs) = default;
            virtual ~Match_stmt()             = default;

            Match_stmt(const std::shared_ptr<Node>&                    matched_value,
                       const std::list<std::shared_ptr<Match_branch>>& non_else_branches,
                       const std::shared_ptr<Block>&                   else_branch)
            : matched_value_{matched_value}
            , non_else_branches_{non_else_branches}
            , else_branch_{else_branch}
            {
            }

            std::shared_ptr<Node>                    matched_value_;
            std::list<std::shared_ptr<Match_branch>> non_else_branches_;
            std::shared_ptr<Block>                   else_branch_;
        };

        struct Meta_match_branch : public Visitable<Meta_match_branch>{
        public:
            using Visitable<Meta_match_branch>::Visitable;

            Meta_match_branch()                        = default;
            Meta_match_branch(const Meta_match_branch& rhs) = default;
            virtual ~Meta_match_branch()               = default;

            Meta_match_branch(const std::shared_ptr<Node>&  condition,
                              const std::shared_ptr<Block>& actions)
            : condition_{condition}
            , actions_{actions}
            {
            }

            std::shared_ptr<Node>  condition_;
            std::shared_ptr<Block> actions_;
        };

        struct Meta_match_stmt : public Visitable<Meta_match_stmt>{
        public:
            using Visitable<Meta_match_stmt>::Visitable;

            Meta_match_stmt()                           = default;
            Meta_match_stmt(const Meta_match_stmt& rhs) = default;
            virtual ~Meta_match_stmt()                  = default;

            Meta_match_stmt(const std::shared_ptr<Node>&                         matched_value,
                            const std::list<std::shared_ptr<Meta_match_branch>>& non_else_branches,
                            const std::shared_ptr<Block>&                        else_branch)
            : matched_value_{matched_value}
            , non_else_branches_{non_else_branches}
            , else_branch_{else_branch}
            {
            }

            std::shared_ptr<Node>                         matched_value_;
            std::list<std::shared_ptr<Meta_match_branch>> non_else_branches_;
            std::shared_ptr<Block>                        else_branch_;
        };

        struct Variables_definition : public Visitable<Variables_definition>{
        public:
            using Visitable<Variables_definition>::Visitable;

            Variables_definition()                                = default;
            Variables_definition(const Variables_definition& rhs) = default;
            virtual ~Variables_definition()                       = default;

            Variables_definition(const std::list<std::size_t>& var_names,
                                 const std::shared_ptr<Node>&  vars_type,
                                 const std::shared_ptr<Node>&  vars_value,
                                 bool  is_exported)
            : var_names_{var_names}
            , vars_type_{vars_type}
            , vars_value_{vars_value}
            , is_exported_{is_exported}
            {
            }

            std::list<std::size_t> var_names_;
            std::shared_ptr<Node>  vars_type_;
            std::shared_ptr<Node>  vars_value_;
            bool                   is_exported_ = false;
        };

        struct Meta_variables_definition : public Visitable<Meta_variables_definition>{
        public:
            using Visitable<Meta_variables_definition>::Visitable;

            Meta_variables_definition()                                     = default;
            Meta_variables_definition(const Meta_variables_definition& rhs) = default;
            virtual ~Meta_variables_definition()                            = default;

            Meta_variables_definition(const std::list<std::size_t>& var_names,
                                      const std::shared_ptr<Node>&  vars_type,
                                      const std::shared_ptr<Node>&  vars_value,
                                      bool  is_exported)
            : var_names_{var_names}
            , vars_type_{vars_type}
            , vars_value_{vars_value}
            , is_exported_{is_exported}
            {
            }

            std::list<std::size_t> var_names_;
            std::shared_ptr<Node>  vars_type_;
            std::shared_ptr<Node>  vars_value_;
            bool                   is_exported_ = false;
        };

        struct Constant_definition : public Visitable<Constant_definition>{
        public:
            using Visitable<Constant_definition>::Visitable;

            Constant_definition()                               = default;
            Constant_definition(const Constant_definition& rhs) = default;
            virtual ~Constant_definition()                      = default;

            Constant_definition(std::size_t                  name_of_constant,
                                const std::shared_ptr<Node>& type_of_constant,
                                const std::shared_ptr<Node>& value_of_constant)
            : name_of_constant_{name_of_constant}
            , type_of_constant_{type_of_constant}
            , value_of_constant_{value_of_constant}
            {
            }

            std::size_t           name_of_constant_ = 0;
            std::shared_ptr<Node> type_of_constant_;
            std::shared_ptr<Node> value_of_constant_;
        };

        struct Constants_definition : public Visitable<Constants_definition>{
        public:
            using Visitable<Constants_definition>::Visitable;

            Constants_definition()                                = default;
            Constants_definition(const Constants_definition& rhs) = default;
            virtual ~Constants_definition()                       = default;

            Constants_definition(const std::list<std::shared_ptr<Constant_definition>>& constants,
                                 bool                                                   is_exported)
            : constants_{constants}
            , is_exported_{is_exported}
            {
            }

            std::list<std::shared_ptr<Constant_definition>> constants_;
            bool                                            is_exported_ = false;
        };

        struct Meta_constant_definition : public Visitable<Meta_constant_definition>{
        public:
            using Visitable<Meta_constant_definition>::Visitable;

            Meta_constant_definition()                                    = default;
            Meta_constant_definition(const Meta_constant_definition& rhs) = default;
            virtual ~Meta_constant_definition()                           = default;

            Meta_constant_definition(std::size_t                  name_of_constant,
                                const std::shared_ptr<Node>& type_of_constant,
                                const std::shared_ptr<Node>& value_of_constant)
            : name_of_constant_{name_of_constant}
            , type_of_constant_{type_of_constant}
            , value_of_constant_{value_of_constant}
            {
            }

            std::size_t           name_of_constant_ = 0;
            std::shared_ptr<Node> type_of_constant_;
            std::shared_ptr<Node> value_of_constant_;
        };

        struct Meta_constants_definition : public Visitable<Meta_constants_definition>{
        public:
            using Visitable<Meta_constants_definition>::Visitable;

            Meta_constants_definition()                                     = default;
            Meta_constants_definition(const Meta_constants_definition& rhs) = default;
            virtual ~Meta_constants_definition()                            = default;

            Meta_constants_definition(const std::list<std::shared_ptr<Meta_constant_definition>>& constants,
                                      bool                                                        is_exported)
            : constants_{constants}
            , is_exported_{is_exported}
            {
            }

            std::list<std::shared_ptr<Meta_constant_definition>> constants_;
            bool                                                 is_exported_ = false;
        };

        struct Type_definition : public Visitable<Type_definition>{
        public:
            using Visitable<Type_definition>::Visitable;

            Type_definition()                           = default;
            Type_definition(const Type_definition& rhs) = default;
            virtual ~Type_definition()                  = default;

            Type_definition(std::size_t                  name_of_type,
                            const std::shared_ptr<Node>& definition_of_type)
            : name_of_type_{name_of_type}
            , definition_of_type_{definition_of_type}
            {
            }

            std::size_t           name_of_type_ = 0;
            std::shared_ptr<Node> definition_of_type_;
        };

        struct Types_definition : public Visitable<Types_definition>{
        public:
            using Visitable<Types_definition>::Visitable;

            Types_definition()                            = default;
            Types_definition(const Types_definition& rhs) = default;
            virtual ~Types_definition()                   = default;

            Types_definition(const std::list<std::shared_ptr<Type_definition>>& types,
                             bool                                               is_exported)
            : types_{types}
            , is_exported_{is_exported}
            {
            }

            std::list<std::shared_ptr<Type_definition>> types_;
            bool                                        is_exported_ = false;
        };

        struct Formal_aguments_group : public Visitable<Formal_aguments_group>{
        public:
            using Visitable<Formal_aguments_group>::Visitable;

            Formal_aguments_group()                                 = default;
            Formal_aguments_group(const Formal_aguments_group& rhs) = default;
            virtual ~Formal_aguments_group()                        = default;

            Formal_aguments_group(std::list<std::size_t>&      names_of_args,
                                  const std::shared_ptr<Node>& type_of_args)
            : names_of_args_{names_of_args}
            , type_of_args_{type_of_args}
            {
            }

            std::list<std::size_t> names_of_args_;
            std::shared_ptr<Node>  type_of_args_;
        };

        struct Func_signature : public Visitable<Func_signature>{
        public:
            using Visitable<Func_signature>::Visitable;

            Func_signature()                          = default;
            Func_signature(const Func_signature& rhs) = default;
            virtual ~Func_signature()                 = default;

            Func_signature(const std::list<std::shared_ptr<Formal_aguments_group>>& arguments,
                           const std::shared_ptr<Node>&                             returned_type)
            : arguments_{arguments}
            , returned_type_{returned_type}
            {
            }

            std::list<std::shared_ptr<Formal_aguments_group>> arguments_;
            std::shared_ptr<Node>                             returned_type_;
        };

        enum class Func_kind : size_t{
            Simple, Main, Pure
        };

        struct Func_header : public Visitable<Func_header>{
        public:
            using Visitable<Func_header>::Visitable;

            Func_header()                       = default;
            Func_header(const Func_header& rhs) = default;
            virtual ~Func_header()              = default;

            Func_header(Func_kind                              kind,
                        std::size_t                            func_name,
                        const std::shared_ptr<Func_signature>& func_signature)
            : kind_{kind}
            , func_name_{func_name}
            , func_signature_{func_signature}
            {
            }

            Func_kind                       kind_           = Func_kind::Simple;
            std::size_t                     func_name_      = 0;
            std::shared_ptr<Func_signature> func_signature_ = nullptr;
        };

        struct Func_definition : public Visitable<Func_definition>{
        public:
            using Visitable<Func_definition>::Visitable;

            Func_definition()                           = default;
            Func_definition(const Func_definition& rhs) = default;
            virtual ~Func_definition()                  = default;

            Func_definition(const std::shared_ptr<Func_header>& func_header,
                            const std::shared_ptr<Block>&       func_body,
                            bool                                is_exported)
            : func_header_{func_header}
            , func_body_{func_body}
            , is_exported_{is_exported}
            {
            }

            std::shared_ptr<Func_header> func_header_ = nullptr;
            std::shared_ptr<Block>       func_body_   = nullptr;
            bool                         is_exported_ = false;
        };

        enum class Operation_name : size_t{
            Prefix_inc,                  Prefixfix_inc_with_wrapping, Prefix_dec,
            Prefixfix_dec_with_wrapping, ElemType,                    Unary_plus,
            Unary_minus,                 Data_size,                   Sharp,
            Card,                        Address,                     Data_address,
            Pointer_def,                 Postfix_inc,                 Postfix_inc_with_wrapping,
            Postfix_dec,                 Postfix_dec_with_wrapping,   Dereference,
            Logical_or,                  Logical_or_full,             Logical_or_not,
            Logical_or_not_full,         Logical_xor,                 Logical_and,
            Logical_and_full,            Logical_and_not,             Logical_and_not_full,
            Compare,                     Eq,                          Neq,
            Lt,                          Gt,                          Leq,
            Geq,                         Type_equiv,                  Bitwise_or,
            Bitwise_or_not,              Bitwise_xor,                 Bitwise_and,
            Bitwise_and_not,             Left_shift, Right_shift,     Add,
            Sub,                         Mul,                         Div,
            Remainder,                   Float_remainder,             Symmetric_difference,
            Set_difference,              Power,                       Float_power,
            Dimension_size,              Assignment,                  Copy,
            Logical_or_assign,           Logical_or_not_full_assign,  Logical_or_not_assign,
            Logical_or_full_assign,      Logical_xor_assign,          Logical_and_assign,
            Logical_and_full_assign,     Logical_and_not_assign,      Logical_and_not_full_assign,
            Bitwise_or_assign,           Bitwise_or_not_assign,       Bitwise_xor_assign,
            Bitwise_and_assign,          Bitwise_and_not_assign,      Left_shift_assign,
            Right_shift_assign,          Plus_assign,                 Minus_assign,
            Mul_assign,                  Div_assign,                  Remainder_assign,
            Float_remainder_assign,      Symmetric_difference_assign, Set_difference_assign,
            Power_assign,                Float_power_assign,          Indexing,
            Call,                        Field_access
        };

        enum class Operation_kind : size_t{
            Ordinary, Pure
        };

        struct Operation_header : public Visitable<Operation_header>{
        public:
            using Visitable<Operation_header>::Visitable;

            Operation_header()                            = default;
            Operation_header(const Operation_header& rhs) = default;
            virtual ~Operation_header()                   = default;

            Operation_header(Operation_kind                         kind,
                             std::size_t                            op_name,
                             const std::shared_ptr<Func_signature>& op_signature)
            : kind_{kind}
            , op_name_{op_name}
            , op_signature_{op_signature}
            {
            }

            Operation_kind                  kind_         = Operation_kind::Ordinary;
            Operation_name                  op_name_;
            std::shared_ptr<Func_signature> op_signature_ = nullptr;
        };

        struct Operation_definition : public Visitable<Operation_definition>{
        public:
            using Visitable<Operation_definition>::Visitable;

            Operation_definition()                                = default;
            Operation_definition(const Operation_definition& rhs) = default;
            virtual ~Operation_definition()                       = default;

            Operation_definition(const std::shared_ptr<Operation_header>& op_header,
                                 const std::shared_ptr<Block>&            op_body,
                                 bool                                     is_exported)
            : op_header_{op_header}
            , op_body_{op_body}
            , is_exported_{is_exported}
            {
            }
            std::shared_ptr<Operation_header> op_header_   = nullptr;
            std::shared_ptr<Block>            op_body_     = nullptr;
            bool                              is_exported_ = false;
        };

        enum class Meta_group_kind : size_t{
            Identical_types, Maybe_various_types, Non_clarified_types
        };

        struct Meta_func_formal_aguments_group : public Visitable<Meta_func_formal_aguments_group>{
        public:
            using Visitable<Meta_func_formal_aguments_group>::Visitable;

            Meta_func_formal_aguments_group()                                           = default;
            Meta_func_formal_aguments_group(const Meta_func_formal_aguments_group& rhs) = default;
            virtual ~Meta_func_formal_aguments_group()                                  = default;

            Meta_func_formal_aguments_group(std::list<std::size_t>&      names_of_args,
                                            const std::shared_ptr<Node>& type_of_args,
                                            bool                         is_package,
                                            Meta_group_kind              kind)
            : names_of_args_{names_of_args}
            , type_of_args_{type_of_args}
            , is_package_{is_package}
            , kind_{kind}
            {
            }

            std::list<std::size_t> names_of_args_;
            std::shared_ptr<Node>  type_of_args_;
            bool                   is_package_ = false;
            Meta_group_kind        kind_       = Meta_group_kind::Identical_types;
        };

        struct Meta_func_signature : public Visitable<Meta_func_signature>{
        public:
            using Visitable<Meta_func_signature>::Visitable;

            Meta_func_signature()                               = default;
            Meta_func_signature(const Meta_func_signature& rhs) = default;
            virtual ~Meta_func_signature()                      = default;

            Meta_func_signature(const std::list<std::shared_ptr<Meta_func_formal_aguments_group>>& arguments,
                                const std::shared_ptr<Node>&                                       returned_type)
            : arguments_{arguments}
            , returned_type_{returned_type}
            {
            }

            std::list<std::shared_ptr<Meta_func_formal_aguments_group>> arguments_;
            std::shared_ptr<Node>                                       returned_type_;
        };

        struct Meta_func_header : public Visitable<Meta_func_header>{
        public:
            using Visitable<Meta_func_header>::Visitable;

            Meta_func_header()                            = default;
            Meta_func_header(const Meta_func_header& rhs) = default;
            virtual ~Meta_func_header()                   = default;

            Meta_func_header(std::size_t                                 func_name,
                             const std::shared_ptr<Meta_func_signature>& func_signature,
                             const std::shared_ptr<Used_categories>&     used_categories)
            : func_name_{func_name}
            , func_signature_{func_signature}
            , used_categories_{used_categories}
            {
            }

            std::size_t                          func_name_       = 0;
            std::shared_ptr<Meta_func_signature> func_signature_  = nullptr;
            std::shared_ptr<Used_categories>     used_categories_ = nullptr;
        };

        struct Meta_func_definition : public Visitable<Meta_func_definition>{
        public:
            using Visitable<Meta_func_definition>::Visitable;

            Meta_func_definition()                                = default;
            Meta_func_definition(const Meta_func_definition& rhs) = default;
            virtual ~Meta_func_definition()                       = default;

            Meta_func_definition(const std::shared_ptr<Meta_func_header>& func_header,
                                 const std::shared_ptr<Block>&            func_body,
                                 bool                                     is_exported)
            : func_header_{func_header}
            , func_body_{func_body}
            , is_exported_{is_exported}
            {
            }

            std::shared_ptr<Meta_func_header> func_header_ = nullptr;
            std::shared_ptr<Block>            func_body_   = nullptr;
            bool                              is_exported_ = false;
        };

        struct Alias_definition : public Visitable<Alias_definition>{
        public:
            using Visitable<Alias_definition>::Visitable;

            Alias_definition()                            = default;
            Alias_definition(const Alias_definition& rhs) = default;
            virtual ~Alias_definition()                   = default;

            Alias_definition(std::size_t                  name,
                             const std::shared_ptr<Node>& body,
                             bool                         is_exported)
            : name_{name}
            , body_{body}
            , is_exported_{is_exported}
            {
            }

            std::size_t           name_        = 0;
            std::shared_ptr<Node> body_        = nullptr;
            bool                  is_exported_ = false;
        };

        struct Scope_resolution : public Visitable<Scope_resolution>{
        public:
            using Visitable<Scope_resolution>::Visitable;

            Scope_resolution()                            = default;
            Scope_resolution(const Scope_resolution& rhs) = default;
            virtual ~Scope_resolution()                   = default;

            Scope_resolution(const std::shared_ptr<Node>& scope_for_name,
                             std::size_t                  name)
            : scope_for_name_{scope_for_name}
            , name_{name}
            {
            }

            std::shared_ptr<Node> scope_for_name_ = nullptr;
            std::size_t           name_           = 0;
        };

        struct Call_expr : public Visitable<Call_expr>{
        public:
            using Visitable<Call_expr>::Visitable;

            Call_expr()                     = default;
            Call_expr(const Call_expr& rhs) = default;
            virtual ~Call_expr()            = default;

            Call_expr(const std::shared_ptr<Node>&            caller,
                      const std::list<std::shared_ptr<Node>>& args)
            : caller_{caller}
            , args_{args}
            {
            }

            std::shared_ptr<Node>            caller_;
            std::list<std::shared_ptr<Node>> args_;
        };

        struct Indexed_expr : public Visitable<Indexed_expr>{
        public:
            using Visitable<Indexed_expr>::Visitable;

            Indexed_expr()                        = default;
            Indexed_expr(const Indexed_expr& rhs) = default;
            virtual ~Indexed_expr()               = default;

            Indexed_expr(const std::shared_ptr<Node>&            indexed,
                         const std::list<std::shared_ptr<Node>>& indices)
            : indexed_{indexed}
            , indices_{indices}
            {
            }

            std::shared_ptr<Node>            indexed_;
            std::list<std::shared_ptr<Node>> indices_;
        };

        struct Meta_call_expr : public Visitable<Meta_call_expr>{
        public:
            using Visitable<Meta_call_expr>::Visitable;

            Meta_call_expr()                          = default;
            Meta_call_expr(const Meta_call_expr& rhs) = default;
            virtual ~Meta_call_expr()                 = default;

            Meta_call_expr(const std::shared_ptr<Node>&            caller,
                           const std::list<std::shared_ptr<Node>>& args)
            : caller_{caller}
            , args_{args}
            {
            }

            std::shared_ptr<Node>            caller_;
            std::list<std::shared_ptr<Node>> args_;
        };

        struct Field_access_expr : public Visitable<Field_access_expr>{
        public:
            using Visitable<Field_access_expr>::Visitable;

            Field_access_expr()                             = default;
            Field_access_expr(const Field_access_expr& rhs) = default;
            virtual ~Field_access_expr()                    = default;

            Field_access_expr(const std::shared_ptr<Node>& scope_for_name,
                              std::size_t                  name)
            : scope_for_name_{scope_for_name}
            , name_{name}
            {
            }

            std::shared_ptr<Node> scope_for_name_ = nullptr;
            std::size_t           name_           = 0;
        };

        struct Struct_field_value_expr : public Visitable<Struct_field_value_expr>{
        public:
            using Visitable<Struct_field_value_expr>::Visitable;

            Struct_field_value_expr()                                   = default;
            Struct_field_value_expr(const Struct_field_value_expr& rhs) = default;
            virtual ~Struct_field_value_expr()                          = default;

            Struct_field_value_expr(std::size_t                  field_name,
                                    const std::shared_ptr<Node>& field_value)
            : field_name_{field_name}
            , field_value_{field_value}
            {
            }

            std::size_t           field_name_  = 0;
            std::shared_ptr<Node> field_value_ = nullptr;
        };

        struct Struct_construction_from_field_values : public Visitable<Struct_construction_from_field_values>{
        public:
            using Visitable<Struct_construction_from_field_values>::Visitable;

            Struct_construction_from_field_values()                                                 = default;
            Struct_construction_from_field_values(const Struct_construction_from_field_values& rhs) = default;
            virtual ~Struct_construction_from_field_values()                                        = default;

            Struct_construction_from_field_values(const std::shared_ptr<Node>&                               structure,
                                                  const std::list<std::shared_ptr<Struct_field_value_expr>>& values)
            : structure_{structure}
            , values_{values}
            {
            }

            std::shared_ptr<Node>                               structure_;
            std::list<std::shared_ptr<Struct_field_value_expr>> values_;
        };

        struct Reinterpret_expr : public Visitable<Reinterpret_expr>{
        public:
            using Visitable<Reinterpret_expr>::Visitable;

            Reinterpret_expr()                            = default;
            Reinterpret_expr(const Reinterpret_expr& rhs) = default;
            virtual ~Reinterpret_expr()                   = default;

            Reinterpret_expr(const std::shared_ptr<Node>& source_expr,
                             const std::shared_ptr<Node>& dest_type)
            : source_expr_{source_expr}
            , dest_type_{dest_type}
            {
            }

            std::shared_ptr<Node> source_expr_;
            std::shared_ptr<Node> dest_type_;
        };

        struct Convert_expr : public Visitable<Convert_expr>{
        public:
            using Visitable<Convert_expr>::Visitable;

            Convert_expr()                        = default;
            Convert_expr(const Convert_expr& rhs) = default;
            virtual ~Convert_expr()               = default;

            Convert_expr(const std::shared_ptr<Node>& source_expr,
                         const std::shared_ptr<Node>& dest_type)
            : source_expr_{source_expr}
            , dest_type_{dest_type}
            {
            }

            std::shared_ptr<Node> source_expr_;
            std::shared_ptr<Node> dest_type_;
        };

        struct Tuple_expression : public Visitable<Tuple_expression>{
        public:
            using Visitable<Tuple_expression>::Visitable;

            Tuple_expression()                            = default;
            Tuple_expression(const Tuple_expression& rhs) = default;
            virtual ~Tuple_expression()                   = default;

            explicit Tuple_expression(const std::list<std::shared_ptr<Node>>& components)
            : components_{components}
            {
            }

            std::list<std::shared_ptr<Node>> components_;
        };

        struct Set_expression : public Visitable<Set_expression>{
        public:
            using Visitable<Set_expression>::Visitable;

            Set_expression()                          = default;
            Set_expression(const Set_expression& rhs) = default;
            virtual ~Set_expression()                 = default;

            explicit Set_expression(const std::list<std::shared_ptr<Node>>& values)
            : values_{values}
            {
            }

            std::list<std::shared_ptr<Node>> values_;
        };

        struct Array_expression : public Visitable<Array_expression>{
        public:
            using Visitable<Array_expression>::Visitable;

            Array_expression()                            = default;
            Array_expression(const Array_expression& rhs) = default;
            virtual ~Array_expression()                   = default;

            explicit Array_expression(const std::list<std::shared_ptr<Node>>& values)
            : values_{values}
            {
            }

            std::list<std::shared_ptr<Node>> values_;
        };

        struct BitArray_expression : public Visitable<BitArray_expression>{
        public:
            using Visitable<BitArray_expression>::Visitable;

            BitArray_expression()                               = default;
            BitArray_expression(const BitArray_expression& rhs) = default;
            virtual ~BitArray_expression()                      = default;

            explicit BitArray_expression(const std::list<std::shared_ptr<Node>>& values)
            : values_{values}
            {
            }

            std::list<std::shared_ptr<Node>> values_;
        };

        struct Alloc_expr : public Visitable<Alloc_expr>{
        public:
            using Visitable<Alloc_expr>::Visitable;

            Alloc_expr()                      = default;
            Alloc_expr(const Alloc_expr& rhs) = default;
            virtual ~Alloc_expr()             = default;

            explicit Alloc_expr(const std::shared_ptr<Node>& var)
            : var_{var}
            {
            }

            std::shared_ptr<Node> var_;
        };

        struct Free_expr : public Visitable<Free_expr>{
        public:
            using Visitable<Free_expr>::Visitable;

            Free_expr()                     = default;
            Free_expr(const Free_expr& rhs) = default;
            virtual ~Free_expr()            = default;

            explicit Free_expr(const std::shared_ptr<Node>& var)
            : var_{var}
            {
            }

            std::shared_ptr<Node> var_;
        };

        struct Alloc_array_expr : public Visitable<Alloc_array_expr>{
        public:
            using Visitable<Alloc_array_expr>::Visitable;

            Alloc_array_expr()                            = default;
            Alloc_array_expr(const Alloc_array_expr& rhs) = default;
            virtual ~Alloc_array_expr()                   = default;

            explicit Alloc_array_expr(const std::list<std::shared_ptr<Node>>& dim_sizes)
            : dim_sizes_{dim_sizes}
            {
            }

            std::list<std::shared_ptr<Node>> dim_sizes_;
        };

        struct Alloc_bitarray_expr : public Visitable<Alloc_bitarray_expr>{
        public:
            using Visitable<Alloc_bitarray_expr>::Visitable;

            Alloc_bitarray_expr()                               = default;
            Alloc_bitarray_expr(const Alloc_bitarray_expr& rhs) = default;
            virtual ~Alloc_bitarray_expr()                      = default;

            explicit Alloc_bitarray_expr(const std::list<std::shared_ptr<Node>>& dim_sizes)
            : dim_sizes_{dim_sizes}
            {
            }

            std::list<std::shared_ptr<Node>> dim_sizes_;
        };

        struct Lambda : public Visitable<Lambda>{
        public:
            using Visitable<Lambda>::Visitable;

            Lambda()                  = default;
            Lambda(const Lambda& rhs) = default;
            virtual ~Lambda()         = default;

            Lambda(const std::shared_ptr<Func_signature>& func_signature,
                   const std::shared_ptr<Block>&          body)
            : func_signature_{func_signature}
            , body_{body}
            {
            }

            std::shared_ptr<Func_signature> func_signature_;
            std::shared_ptr<Block>          body_;
        };

        enum class Literal_kind : size_t{
            Boolean, Ord,     Nullptr,
            Char,    String,  Integer,
            Float,   Complex, Quaternion
        };

        enum class Bool_kind{
            Bool8, Bool16, Bool32, Bool64
        };

        enum class Ord_kind{
            Ord8, Ord16, Ord32, Ord64
        };

        enum class Char_kind{
            Char8, Char16, Char32
        };

        enum class String_kind{
            String8, String16, String32
        };

        enum class Float_kind{
            Float32, Float64, Float80, Float128
        };

        enum class Complex_kind{
            Complex32, Complex64, Complex80, Complex128
        };

        enum class Quaternion_kind{
            Quaternion32, Quaternion64, Quaternion80, Quaternion128
        };

        enum class Ordering : size_t{
            Lower, Equal, Greater
        };

        struct Literal_value{
            Literal_kind kind_;
            size_t       subkind_;
            union{
                bool                     bool_value_;
                Ordering                 ordering_value_;
                unsigned __int128        int_val_;
                __float128               float_val_;
                __complex128             complex_val_;
                quat::quat_t<__float128> quat_val_;
                char32_t                 char_val_;
                std::size_t              str_index_;
            };
        };

        inline Literal_value make_bool_literal_value(bool      v,
                                                     Bool_kind k = Bool_kind::Bool8)
        {
            Literal_value result;
            result.kind_       = Literal_kind::Boolean;
            result.subkind_    = static_cast<size_t>(k);
            result.bool_value_ = v;
            return result;
        }

        inline Literal_value make_ord_literal_value(Ordering v,
                                                    Ord_kind k = Ord_kind::Ord8)
        {
            Literal_value result;
            result.kind_           = Literal_kind::Ord;
            result.subkind_        = static_cast<size_t>(k);
            result.ordering_value_ = v;
            return result;
        }

        inline Literal_value make_char_literal_value(char32_t  v,
                                                     Char_kind k = Char_kind::Char32)
        {
            Literal_value result;
            result.kind_     = Literal_kind::Char;
            result.subkind_  = static_cast<size_t>(k);
            result.char_val_ = v;
            return result;
        }

        inline Literal_value make_str_literal_value(std::size_t v,
                                                    String_kind k = String_kind::String32)
        {
            Literal_value result;
            result.kind_      = Literal_kind::String;
            result.subkind_   = static_cast<size_t>(k);
            result.str_index_ = v;
            return result;
        }

        inline Literal_value make_float_literal_value(__float128 v,
                                                      Float_kind k = Float_kind::Float64)
        {
            Literal_value result;
            result.kind_      = Literal_kind::Float;
            result.subkind_   = static_cast<size_t>(k);
            result.float_val_ = v;
            return result;
        }

        inline Literal_value make_complex_literal_value(__complex128 v,
                                                        Complex_kind k = Complex_kind::Complex64)
        {
            Literal_value result;
            result.kind_        = Literal_kind::Complex;
            result.subkind_     = static_cast<size_t>(k);
            result.complex_val_ = v;
            return result;
        }

        inline Literal_value make_quat_literal_value(quat::quat_t<__float128> v,
                                                     Quaternion_kind          k = Quaternion_kind::Quaternion64)
        {
            Literal_value result;
            result.kind_     = Literal_kind::Quaternion;
            result.subkind_  = static_cast<size_t>(k);
            result.quat_val_ = v;
            return result;
        }

        inline Literal_value make_int_literal_value(unsigned __int128 v)
        {
            Literal_value result;
            result.kind_    = Literal_kind::Integer;
            result.subkind_ = 0;
            result.int_val_ = v;
            return result;
        }

        inline Literal_value make_nullptr_literal_value()
        {
            Literal_value result;
            result.kind_    = Literal_kind::Nullptr;
            result.subkind_ = 0;
            return result;
        }

        struct Literal_expr : public Visitable<Literal_expr>{
        public:
            using Visitable<Literal_expr>::Visitable;

            Literal_expr()                        = default;
            Literal_expr(const Literal_expr& rhs) = default;
            virtual ~Literal_expr()               = default;

            explicit Literal_expr(const Literal_value& value)
            : value_{value}
            {
            }

            Literal_value value_;

            static std::shared_ptr<Literal_expr> make_bool_literal(bool      v,
                                                                   Bool_kind k = Bool_kind::Bool8)
            {
                return std::make_shared<Literal_expr>(make_bool_literal_value(v, k));
            }

            static std::shared_ptr<Literal_expr> make_ord_literal(Ordering v,
                                                                  Ord_kind k = Ord_kind::Ord8)
            {
                return std::make_shared<Literal_expr>(make_ord_literal_value(v, k));
            }

            static std::shared_ptr<Literal_expr> make_char_literal(char32_t  v,
                                                                   Char_kind k = Char_kind::Char32)
            {
                return std::make_shared<Literal_expr>(make_char_literal_value(v, k));
            }

            static std::shared_ptr<Literal_expr> make_str_literal(std::size_t v,
                                                                  String_kind k = String_kind::String32)
            {
                return std::make_shared<Literal_expr>(make_str_literal_value(v, k));
            }

            static std::shared_ptr<Literal_expr> make_float_literal(__float128 v,
                                                                    Float_kind k = Float_kind::Float64)
            {
                return std::make_shared<Literal_expr>(make_float_literal_value(v, k));
            }

            static std::shared_ptr<Literal_expr> make_complex_literal(__complex128 v,
                                                                      Complex_kind k = Complex_kind::Complex64)
            {
                return std::make_shared<Literal_expr>(make_complex_literal_value(v, k));
            }

            static std::shared_ptr<Literal_expr> make_quat_literal(quat::quat_t<__float128> v,
                                                                   Quaternion_kind          k = Quaternion_kind::Quaternion64)
            {
                return std::make_shared<Literal_expr>(make_quat_literal_value(v, k));
            }

            static std::shared_ptr<Literal_expr> make_int_literal(unsigned __int128 v)
            {
                return std::make_shared<Literal_expr>(make_int_literal_value(v));
            }

            static std::shared_ptr<Literal_expr> make_nullptr_literal()
            {
                return std::make_shared<Literal_expr>(make_nullptr_literal_value());
            }
        };

        enum Fixed_size_base_type_kind : size_t{
            unsigned8,  unsigned16,  unsigned32,  unsigned64, unsigned128,
            signed8,    signed16,    signed32,    signed64,   signed128,
            fp32,       fp64,        fp80,        fp128,      q32,
            q64,        q80,         q128,        c32,        c64,
            c80,        c128,        bool8,       bool16,     bool32,
            bool64,     ord8,        ord16,       ord32,      ord64,
            character8, character16, character32, string8,    string16,
            string32,   pusto
        };

        struct Fixed_size_base_type : public Visitable<Fixed_size_base_type>{
        public:
            using Visitable<Fixed_size_base_type>::Visitable;

            Fixed_size_base_type()                                = default;
            Fixed_size_base_type(const Fixed_size_base_type& rhs) = default;
            virtual ~Fixed_size_base_type()                       = default;

            Fixed_size_base_type(Fixed_size_base_type_kind kind)
            : kind_{kind}
            {
            }

            Fixed_size_base_type_kind kind_;
        };

        enum class Sizeable_base_type_kind : size_t{
            unsigned_int, fp,      quat,
            complex,      boolean, ordering,
            character,    string,  signed_int
        };

        enum class Length_modifier {
            short_modifier, long_modifier
        };

        struct Sizeable_base_type : public Visitable<Sizeable_base_type>{
        public:
            using Visitable<Sizeable_base_type>::Visitable;

            Sizeable_base_type()                              = default;
            Sizeable_base_type(const Sizeable_base_type& rhs) = default;
            virtual ~Sizeable_base_type()                     = default;

            Sizeable_base_type(const std::list<Length_modifier>& modifiers,
                               Sizeable_base_type_kind           kind)
            : modifiers_{modifiers}
            , kind_{kind}
            {
            }

            std::list<Length_modifier> modifiers_;
            Sizeable_base_type_kind    kind_;
        };

        struct Array_type_expr : public Visitable<Array_type_expr>{
        public:
            using Visitable<Array_type_expr>::Visitable;

            Array_type_expr()                           = default;
            Array_type_expr(const Array_type_expr& rhs) = default;
            virtual ~Array_type_expr()                  = default;

            Array_type_expr(const std::list<std::shared_ptr<Node>>& dims,
                            const std::shared_ptr<Node>&            elem_type)
            : dims_{dims}
            , elem_type_{elem_type}
            {
            }

            std::list<std::shared_ptr<Node>> dims_;
            std::shared_ptr<Node>            elem_type_;
        };

        struct BitArray_type_expr : public Visitable<BitArray_type_expr>{
        public:
            using Visitable<BitArray_type_expr>::Visitable;

            BitArray_type_expr()                              = default;
            BitArray_type_expr(const BitArray_type_expr& rhs) = default;
            virtual ~BitArray_type_expr()                     = default;

            explicit BitArray_type_expr(const std::list<std::shared_ptr<Node>>& dims)
            : dims_{dims}
            {
            }

            std::list<std::shared_ptr<Node>> dims_;
        };

        struct Reference_type_expr : public Visitable<Reference_type_expr>{
        public:
            using Visitable<Reference_type_expr>::Visitable;

            Reference_type_expr()                               = default;
            Reference_type_expr(const Reference_type_expr& rhs) = default;
            virtual ~Reference_type_expr()                      = default;

            Reference_type_expr(bool                         is_const,
                                const std::shared_ptr<Node>& elem_type)
            : is_const_{is_const}
            , elem_type_{elem_type}
            {
            }

            bool                  is_const_;
            std::shared_ptr<Node> elem_type_;
        };

        struct Enum_type_expr : public Visitable<Enum_type_expr>{
        public:
            using Visitable<Enum_type_expr>::Visitable;

            Enum_type_expr()                          = default;
            Enum_type_expr(const Enum_type_expr& rhs) = default;
            virtual ~Enum_type_expr()                 = default;

            Enum_type_expr(std::size_t                   enum_name,
                           const std::list<std::size_t>& components)
            : enum_name_{enum_name}
            , components_{components}
            {
            }
            std::size_t            enum_name_;
            std::list<std::size_t> components_;
        };

        struct EnumSet_type_expr : public Visitable<EnumSet_type_expr>{
        public:
            using Visitable<EnumSet_type_expr>::Visitable;

            EnumSet_type_expr()                             = default;
            EnumSet_type_expr(const EnumSet_type_expr& rhs) = default;
            virtual ~EnumSet_type_expr()                    = default;

            explicit EnumSet_type_expr(const std::shared_ptr<Node>& elem_type)
            : elem_type_{elem_type}
            {
            }

            std::shared_ptr<Node> elem_type_;
        };

        struct Auto_type_expr : public Visitable<Auto_type_expr>{
        public:
            using Visitable<Auto_type_expr>::Visitable;

            Auto_type_expr()                             = default;
            Auto_type_expr(const Auto_type_expr& rhs) = default;
            virtual ~Auto_type_expr()                    = default;
        };

        struct Type : public Visitable<Type>{
        public:
            using Visitable<Type>::Visitable;

            Type()                = default;
            Type(const Type& rhs) = default;
            virtual ~Type()       = default;
        };

        struct Func_ptr_type_expr : public Visitable<Func_ptr_type_expr>{
        public:
            using Visitable<Func_ptr_type_expr>::Visitable;

            Func_ptr_type_expr()                              = default;
            Func_ptr_type_expr(const Func_ptr_type_expr& rhs) = default;
            virtual ~Func_ptr_type_expr()                     = default;

            Func_ptr_type_expr(bool is_pure,
                               const std::shared_ptr<Func_signature>& func_signature)
            : is_pure_{is_pure}
            , func_signature_{func_signature}
            {
            }

            bool                            is_pure_        = false;
            std::shared_ptr<Func_signature> func_signature_ = nullptr;
        };

        struct Struct_fields_group : public Visitable<Struct_fields_group>{
        public:
            using Visitable<Struct_fields_group>::Visitable;

            Struct_fields_group()                               = default;
            Struct_fields_group(const Struct_fields_group& rhs) = default;
            virtual ~Struct_fields_group()                      = default;

            Struct_fields_group(const std::list<std::size_t>& names_of_fields,
                                const std::shared_ptr<Node>&  type_of_fields)
            : names_of_fields_{names_of_fields}
            , type_of_fields_{type_of_fields}
            {
            }

            std::list<std::size_t> names_of_fields_;
            std::shared_ptr<Node>  type_of_fields_;
        };

        struct Struct_type_expr : public Visitable<Struct_type_expr>{
        public:
            using Visitable<Struct_type_expr>::Visitable;

            Struct_type_expr()                            = default;
            Struct_type_expr(const Struct_type_expr& rhs) = default;
            virtual ~Struct_type_expr()                   = default;

            Struct_type_expr(std::size_t                                            struct_name,
                             const std::list<std::shared_ptr<Struct_fields_group>>& groups)
            : struct_name_{struct_name}
            , groups_{groups}
            {
            }

            std::size_t                                     struct_name_;
            std::list<std::shared_ptr<Struct_fields_group>> groups_;
        };

        struct Used_category : public Visitable<Used_category>{
        public:
            using Visitable<Used_category>::Visitable;

            Used_category()                         = default;
            Used_category(const Used_category& rhs) = default;
            virtual ~Used_category()                = default;

            Used_category(const std::shared_ptr<Qualified_id>&    name,
                          const std::list<std::shared_ptr<Node>>& args)
            : name_{name}
            , args_{args}
            {
            }

            std::shared_ptr<Qualified_id>    name_;
            std::list<std::shared_ptr<Node>> args_;
        };

        struct Used_categories : public Visitable<Used_categories>{
        public:
            using Visitable<Used_categories>::Visitable;

            Used_categories()                           = default;
            Used_categories(const Used_categories& rhs) = default;
            virtual ~Used_categories()                  = default;

            explicit Used_categories(const std::list<std::shared_ptr<Used_category>>& categories)
            : categories_{categories}
            {
            }

            std::list<std::shared_ptr<Used_category>> categories_;
        };

        struct Category_definition_body : public Visitable<Category_definition_body>{
        public:
            using Visitable<Category_definition_body>::Visitable;

            Category_definition_body()                                    = default;
            Category_definition_body(const Category_definition_body& rhs) = default;
            virtual ~Category_definition_body()                           = default;

            explicit Category_definition_body(const std::list<std::shared_ptr<Node>>& entries)
            : entries_{entries}
            {
            }

            std::list<std::shared_ptr<Node>> entries_;
        };

        struct Category_definition : public Visitable<Category_definition>{
        public:
            using Visitable<Category_definition>::Visitable;

            Category_definition()                               = default;
            Category_definition(const Category_definition& rhs) = default;
            virtual ~Category_definition()                      = default;

            Category_definition(std::size_t                                                        name,
                                const std::list<std::shared_ptr<Meta_func_formal_aguments_group>>& formal_args,
                                const std::shared_ptr<Used_categories>&                            used_categories,
                                const std::shared_ptr<Category_definition_body>&                   body)

            : name_{name}
            , formal_args_{formal_args}
            , used_categories_{used_categories}
            , body_{body}
            {
            }

            std::size_t                                                 name_;
            std::list<std::shared_ptr<Meta_func_formal_aguments_group>> formal_args_;
            std::shared_ptr<Used_categories>                            used_categories_;
            std::shared_ptr<Category_definition_body>                   body_;
        };

        struct Const_entry : public Visitable<Const_entry>{
        public:
            using Visitable<Const_entry>::Visitable;

            Const_entry()                       = default;
            Const_entry(const Const_entry& rhs) = default;
            virtual ~Const_entry()              = default;

            Const_entry(std::size_t                  name,
                        const std::shared_ptr<Node>& type)

            : name_{name}
            , type_{type}
            {
            }

            std::size_t            name_;
            std::shared_ptr<Node>  type_;
        };

        struct Implement_category : public Visitable<Implement_category>{
        public:
            using Visitable<Implement_category>::Visitable;

            Implement_category()                              = default;
            Implement_category(const Implement_category& rhs) = default;
            virtual ~Implement_category()                     = default;

            Implement_category(const std::shared_ptr<Qualified_id>& name,
                          const std::shared_ptr<Node>&              type)
            : name_{name}
            , type_{type}
            {
            }

            std::shared_ptr<Qualified_id> name_;
            std::shared_ptr<Node>         type_;
        };

        struct Category_implementation : public Visitable<Category_implementation>{
        public:
            using Visitable<Category_implementation>::Visitable;

            Category_implementation()                                   = default;
            Category_implementation(const Category_implementation& rhs) = default;
            virtual ~Category_implementation()                          = default;

//             Category_implementation(const std::shared_ptr<Qualified_id>& name,
//                                     const std::shared_ptr<Node>&         type)
//             : name_{name}
//             , type_{type}
//             {
//             }
//
//             std::shared_ptr<Qualified_id> name_;
//             std::shared_ptr<Node>         type_;
        };

        class AST{
        public:
            AST()               = default;
            AST(const AST& rhs) = default;
            virtual ~AST()      = default;

            explicit AST(const Node_ptr& root) : root_{root} {}

            void traverse(Visitor& v) const
            {
                if(root_){
                    root_->accept(v);
                }
            }

            void traverse(Visitor& v)
            {
                if(root_){
                    root_->accept(v);
                }
            }
        private:
            Node_ptr root_;
        };
    };
};
#endif