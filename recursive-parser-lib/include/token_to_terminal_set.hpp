/*
    File:    token_to_terminal_set.hpp
    Created: 29 November 2020 at 09:55 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef TOKEN_TO_TERMINAL_SET_H
#define TOKEN_TO_TERMINAL_SET_H
#   include "../../snd-lvl-scanner/include/snd_level_lexeme.h"
#   include "../../snd-lvl-scanner/include/arkona-scanner-snd-lvl.h"
#   include "../../bitset/include/static_bitset.hpp"
#   include "../include/terminal_enum.hpp"
namespace arkona_parser{
    using Token        = arkona_scanner_snd_lvl::Arkona_token_snd_lvl;
    using Terminal_set = pascal_set::set<Terminal_category,
                                         Terminal_category::End_of_text,
                                         Terminal_category::Expr_begin>;

    Terminal_set token_to_terminal_set(const Token& token);
};
#endif