/*
    File:    scope.hpp
    Created: 06 January 2021 at 16:56 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SCOPE_HPP
#define SCOPE_HPP
#   include <cstdint>
#   include "../include/scope_elem.hpp"
namespace arkona_parser{
    enum class Predefined_module_id : uint16_t {
        System_IO
    };

    class Scope final{
    public:
        Scope()                 = default;
        Scope(const Scope& rhs) = default;
        ~Scope()                = default;
    private:
    };
};
#endif