/*
    File:    recursive-parser.cpp
    Created: 04 January 2021 at 09:49 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstdio>
#include "../include/recursive-parser.hpp"
#include "../../recursive-parser-lib/include/token_to_terminal_set.hpp"
#include "../../recursive-parser-lib/include/terminal_enum.hpp"

namespace arkona_parser{
    struct Parser::Impl final{
    public:
        Impl()                           = default;
        Impl& operator=(const Impl& rhs) = default;
        Impl(const Impl& rhs)            = default;
        ~Impl()                          = default;

        Impl(const ParserArgs& args)
            : symbol_table_{args.symbol_table_}
            , en_{args.et_.ec_}
            , wn_{args.et_.wc_}
            , ids_{args.et_.ids_trie_}
            , strs_{args.et_.strs_trie_}
            , scanner_{args.scanner_}
            {
            }

        size_t get_number_of_errors() const;
        size_t get_number_of_warnings() const;

        AST compile();
    private:
        /* a pointer to the symbol table */
        std::shared_ptr<Symbol_table>                    symbol_table_;
        /* a pointer to a class that counts the number of errors: */
        std::shared_ptr<Error_count>                     en_;
        /* a pointer to a class that counts the number of warnings: */
        std::shared_ptr<Warning_count>                   wn_;
        /* a pointer to the prefix tree for identifiers: */
        std::shared_ptr<Char_trie>                       ids_;
        /* a pointer to the prefix tree for string literals: */
        std::shared_ptr<Char_trie>                       strs_;
        /* a pointer to the second level scanner: */
        std::shared_ptr<arkona_scanner_snd_lvl::Scanner> scanner_;

        std::shared_ptr<ast::Module>       module_proc();
        std::shared_ptr<ast::Qualified_id> qualified_id_proc();
        std::shared_ptr<ast::Block>        block_proc();
        std::shared_ptr<ast::Imports>      imports_proc();

        Terminal_set get_current(Token& token);
    };

    size_t Parser::Impl::get_number_of_errors() const
    {
        return en_->get_number_of_errors();
    }

    size_t Parser::Impl::get_number_of_warnings() const
    {
        return wn_->get_number_of_warnings();
    }

    Terminal_set Parser::Impl::get_current(Token& token)
    {
        token = scanner_->current_lexeme();
        return token_to_terminal_set(token);
    }


    static std::vector<std::size_t> token_sequence2indeces(const std::list<Token>& tokens)
    {
        if(tokens.empty()){
            return std::vector<std::size_t>();
        }

        std::vector<std::size_t> result(tokens.size());
        std::size_t              i = 0;

        for(const auto& token : tokens){
            result[i++] = token.lexeme_.id_info_.id_idx_;
        }

        return result;
    }

    std::shared_ptr<ast::Module> Parser::Impl::module_proc()
    {
        /*
         * This method handles the rule
         *      arkona_module -> модуль qualified_id imports? block (1)
         * In terms of terminals it means
         *      arkona_module -> Module qualified_id imports? block (1')
         * If we construct the minimal DFA for the regexp in the body of the rule (1'),
         * we get the following transition table:
         *
         * |-------|---|---|---|---|------------------|
         * | State | m | q | i | b | Remark           |
         * |-------|---|---|---|---|------------------|
         * |   A   | B |   |   |   | Start state      |
         * |-------|---|---|---|---|------------------|
         * |   B   |   | C |   |   |                  |
         * |-------|---|---|---|---|------------------|
         * |   C   |   |   | E | D |                  |
         * |-------|---|---|---|---|------------------|
         * |   D   |   |   |   |   | Final state      |
         * |-------|---|---|---|---|------------------|
         * |   E   |   |   |   | D |                  |
         * |-------|---|---|---|---|------------------|
         *
         * Here, for brevity,
         *
         *    m    the terminal Module
         *    q    the non-terminal qualified_id
         *    i    the non-terminal imports
         *    b    the non-terminal block
         *
         * The transition by a non-terminal means a call of the corresponding function.
         */

        auto result = std::make_shared<ast::Module>(std::make_shared<ast::Qualified_id>(),
                                                    std::make_shared<ast::Block>(),
                                                    std::make_shared<ast::Imports>());
        Token        current_token;
        Terminal_set set_of_terminals;

        enum class State{
            A, B, C, D, E
        };

        State state = State::A;

        while(!(set_of_terminals = get_current(current_token))[Terminal_category::End_of_text])
        {
            switch(state){
                case State::A:
                    if(set_of_terminals[Terminal_category::Module])
                    {
                        state = State::B;
                    }else
                    {
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected keyword модуль.\n",
                               pos.line_no_,
                               pos.line_pos_);
                        en_->increment_number_of_errors();
                        return result;
                    }
                    break;
                case State::B:
                    scanner_->back();
                    result->name_ = qualified_id_proc();
                    state = State::C;
                    if(en_->get_number_of_errors()){
                        return result;
                    }
                    symbol_table_->set_module_name(token_sequence2indeces(result->name_->name_components_));
                    break;
                case State::C:
                    if(set_of_terminals[Terminal_category::Fig_br_opened]){
                        scanner_->back();
                        result->body_ = block_proc();
                        state = State::D;
                    }else if(set_of_terminals[Terminal_category::Uses]){
                        scanner_->back();
                        result->used_modules_ = imports_proc();
                        state = State::E;
                        if(en_->get_number_of_errors()){
                            return result;
                        }
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected keyword использует or {\n",
                               pos.line_no_,
                               pos.line_pos_);
                        en_->increment_number_of_errors();
                        return result;
                    }
                    break;
                case State::D:
                    scanner_->back();
                    return result;
                    break;
                case State::E:
                    scanner_->back();
                    result->body_ = block_proc();
                    state = State::D;
                    break;
            }
        }

        if(state != State::D){
            const auto& pos = current_token.range_.begin_pos_;
            printf("Error at %zu:%zu: unexpected end of text.\n", pos.line_no_, pos.line_pos_);
            en_->increment_number_of_errors();
        }

        return result;
    }

    std::shared_ptr<ast::Qualified_id> Parser::Impl::qualified_id_proc()
    {
        /*
         * This method handles the rule
         *      qualified_id -> id(::id)* (2)
         * In terms of terminals it means
         *      arkona_module -> Id (Scope_resol Id)* (2')
         * If we construct the minimal DFA for the regexp in the body of the rule (2'),
         * we get the following transition table:
         *
         * |-------|---|---|------------------|
         * | State | i | s | Remark           |
         * |-------|---|---|------------------|
         * |   A   | B |   | Start state      |
         * |-------|---|---|------------------|
         * |   B   |   | A | Final state      |
         * |-------|---|---|------------------|
         *
         * Here, for brevity,
         *
         *    i    the terminal Id
         *    s    the terminal Scope_resol
         *
         * The transition by a non-terminal means a call of the corresponding function.
         */
        auto result = std::make_shared<ast::Qualified_id>();
        Token        current_token;
        Terminal_set set_of_terminals;

        enum class State{
            A, B
        };

        State state = State::A;

        while(!(set_of_terminals = get_current(current_token))[Terminal_category::End_of_text])
        {
            switch(state){
                case State::A:
                    if(set_of_terminals[Terminal_category::Id]){
                        result->name_components_.push_back(current_token);
                        state = State::B;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected identifier\n", pos.line_no_, pos.line_pos_);
                        en_->increment_number_of_errors();
                        return result;
                    }
                    break;
                case State::B:
                    if(set_of_terminals[Terminal_category::Scope_resol]){
                        state = State::A;
                    }else{
                        scanner_->back();
                        return result;
                    }
                    break;
            }
        }

        if(state != State::B){
            const auto& pos = current_token.range_.begin_pos_;
            printf("Error at %zu:%zu: unexpected end of text.\n", pos.line_no_, pos.line_pos_);
            en_->increment_number_of_errors();
        }

        return result;
    }

    std::shared_ptr<ast::Imports> Parser::Impl::imports_proc()
    {
        /*
         * This method handles the rule
         *      imports -> Uses qualified_id (Comma qualified_id)* (Semicolon Uses qualified_id (Comma qualified_id)*)* (3)
         * If we construct the minimal DFA for the regexp in the body of the rule (3),
         * we get the following transition table:
         *
         * |-------|---|---|---|---|------------------|
         * | State | i | q | c | s | Remark           |
         * |-------|---|---|---|---|------------------|
         * |   A   | B |   |   |   | Start state      |
         * |-------|---|---|---|---|------------------|
         * |   B   |   | C |   |   |                  |
         * |-------|---|---|---|---|------------------|
         * |   C   |   |   | B | A | Final state      |
         * |-------|---|---|---|---|------------------|
         *
         * Here, for brevity,
         *
         *    i    the terminal Uses
         *    q    the non-terminal qualified_id
         *    c    the terminal Comma
         *    s    the terminal Semicolon
         *
         * The transition by a non-terminal means a call of the corresponding function.
         */

        auto result = std::make_shared<ast::Imports>();

        Token        current_token;
        Terminal_set set_of_terminals;

        enum class State{
            A, B, C
        };

        State state = State::A;

        while(!(set_of_terminals = get_current(current_token))[Terminal_category::End_of_text])
        {
            switch(state){
                case State::A:
                    if(set_of_terminals[Terminal_category::Uses]){
                        state = State::B;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected keyword использует\n",
                               pos.line_no_,
                               pos.line_pos_);
                        en_->increment_number_of_errors();
                        return result;
                    }
                    break;
                case State::B:
                    {
                        scanner_->back();
                        auto q = qualified_id_proc();
                        if(en_->get_number_of_errors()){
                            return result;
                        }
                        result->imports_.push_back(q);
                        auto name_components = q->name_components_;
                        auto parts           = token_sequence2indeces(name_components);
                        if(symbol_table_->is_module_name(parts)){
                            const auto& pos = current_token.range_.begin_pos_;
                            auto name_as_string  = symbol_table_->get_string(parts);
                            printf("Error at %zu:%zu: qualified identifier %s is already defined as current "
                                   "module name.\n",
                                   pos.line_no_,
                                   pos.line_pos_,
                                   name_as_string.c_str());
                            en_->increment_number_of_errors();
                        }else if(symbol_table_->has_imported_module_name(parts)){
                            const auto& pos = current_token.range_.begin_pos_;
                            auto name_as_string  = symbol_table_->get_string(parts);
                            printf("Error at %zu:%zu: qualified identifier %s is already defined as name of "
                                   "imported module.\n",
                                   pos.line_no_,
                                   pos.line_pos_,
                                   name_as_string.c_str());
                            en_->increment_number_of_errors();
                        }else{
                            symbol_table_->insert_imported_module_name(parts);
                        }
                        state  = State::C;
                    }
                    break;
                case State::C:
                    if(set_of_terminals[Terminal_category::Comma]){
                        state = State::B;
                    }else if(set_of_terminals[Terminal_category::Semicolon]){
                        state = State::A;
                    }else{
                        scanner_->back();
                        return result;
                    }
                    break;
            }
        }

        if(state != State::C){
            const auto& pos = current_token.range_.begin_pos_;
            printf("Error at %zu:%zu: unexpected end of text.\n", pos.line_no_, pos.line_pos_);
            en_->increment_number_of_errors();
        }

        return result;
    }

    std::shared_ptr<ast::Block> Parser::Impl::block_proc()
    {
        auto result = std::make_shared<ast::Block>();
        return result;
    }

    AST Parser::Impl::compile()
    {
        auto module_ptr = module_proc();
        AST result{module_ptr};
        return result;
    }


    Parser::~Parser() = default;

    Parser::Parser(const ParserArgs& args) : pimpl_{std::make_unique<Impl>(args)} {}

    Parser::Parser() : pimpl_{std::make_unique<Impl>()} {}

    AST Parser::compile()
    {
        return pimpl_->compile();
    }

    size_t Parser::get_number_of_errors() const
    {
        return pimpl_->get_number_of_errors();
    }

    size_t Parser::get_number_of_warnings() const
    {
        return pimpl_->get_number_of_warnings();
    }
};