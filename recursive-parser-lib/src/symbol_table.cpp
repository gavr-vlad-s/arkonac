/*
    File:    symbol_table.cpp
    Created: 22 October 2020 at 20:40 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/symbol_table.hpp"
#include "../../strings-lib/include/join.h"
#include "../../char-conv/include/char_conv.h"

namespace arkona_parser{
    void Symbol_table::insert_imported_module_name(const std::vector<std::size_t>& parts)
    {
        std::size_t result = module_parts_trie_->insert_vector(parts);
        indeces_of_imported_modules_names_.insert(result);
    }

    bool Symbol_table::has_imported_module_name(const std::vector<std::size_t>& parts)
    {
        std::size_t idx = module_parts_trie_->insert_vector(parts);
        return indeces_of_imported_modules_names_.count(idx) != 0;
    }

    void Symbol_table::set_module_name(const std::vector<std::size_t>& parts)
    {
        index_of_module_name_ = module_parts_trie_->insert_vector(parts);
    }

    bool Symbol_table::is_module_name(const std::vector<std::size_t>& parts)
    {
        std::size_t idx = module_parts_trie_->insert_vector(parts);
        return index_of_module_name_ == idx;
    }

    std::string Symbol_table::get_string(const std::vector<std::size_t>& parts)
    {
        std::string result;
        if(!has_imported_module_name(parts) && !is_module_name(parts)){
            return result;
        }

        std::vector<std::string> components;
        for(size_t part : parts){
            auto component_as_u32string = ids_trie_->get_string(part);
            auto component              = u32string_to_utf8(component_as_u32string);
            components.push_back(component);
        }

        result = join(components.begin(), components.end(), std::string{"::"});

        return result;
    }
};