/*
    File:    token_to_terminal_set.cpp
    Created: 29 November 2020 at 10:09 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <map>
#include <cstddef>
#include "../include/token_to_terminal_set.hpp"

namespace arkona_parser{
    using Lexem_kind     = arkona_scanner_snd_lvl::Lexem_kind;
    using Keyword_kind   = arkona_scanner_snd_lvl::Keyword_kind;
    using Delimiter_kind = arkona_scanner_snd_lvl::Delimiter_kind;


    static const std::map<Keyword_kind, Terminal_set> keywords_mapping = {
        {Keyword_kind::Kw_bezzn8,        {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_bezzn16,       {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_bezzn32,       {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_bezzn64,       {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_bezzn128,      {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_tsel8,         {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_tsel16,        {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_tsel32,        {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_tsel64,        {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_tsel128,       {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_veshch32,      {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_veshch64,      {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_veshch80,      {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_veshch128,     {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_kvat32,        {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_kvat64,        {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_kvat80,        {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_kvat128,       {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_kompl32,       {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_kompl64,       {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_kompl80,       {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_kompl128,      {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_log8,          {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_log16,         {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_log32,         {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_log64,         {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_poryadok8,     {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_poryadok16,    {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_poryadok32,    {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_poryadok64,    {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_simv8,         {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_simv16,        {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_simv32,        {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_stroka8,       {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_stroka16,      {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_stroka32,      {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_bezzn,         {Terminal_category::Sizeable_type, Terminal_category::Block_entry_begin     }},
        {Keyword_kind::Kw_veshch,        {Terminal_category::Sizeable_type, Terminal_category::Block_entry_begin     }},
        {Keyword_kind::Kw_kvat,          {Terminal_category::Sizeable_type, Terminal_category::Block_entry_begin     }},
        {Keyword_kind::Kw_kompl,         {Terminal_category::Sizeable_type, Terminal_category::Block_entry_begin     }},
        {Keyword_kind::Kw_log,           {Terminal_category::Sizeable_type, Terminal_category::Block_entry_begin     }},
        {Keyword_kind::Kw_poryadok,      {Terminal_category::Sizeable_type, Terminal_category::Block_entry_begin     }},
        {Keyword_kind::Kw_simv,          {Terminal_category::Sizeable_type, Terminal_category::Block_entry_begin     }},
        {Keyword_kind::Kw_stroka,        {Terminal_category::Sizeable_type, Terminal_category::Block_entry_begin     }},
        {Keyword_kind::Kw_tsel,          {Terminal_category::Sizeable_type, Terminal_category::Block_entry_begin     }},
        {Keyword_kind::Kw_bolshe,        {Terminal_category::Literal, Terminal_category::Block_entry_begin           }},
        {Keyword_kind::Kw_istina,        {Terminal_category::Literal, Terminal_category::Block_entry_begin           }},
        {Keyword_kind::Kw_lozh,          {Terminal_category::Literal, Terminal_category::Block_entry_begin           }},
        {Keyword_kind::Kw_menshe,        {Terminal_category::Literal, Terminal_category::Block_entry_begin           }},
        {Keyword_kind::Kw_ravno,         {Terminal_category::Literal, Terminal_category::Block_entry_begin           }},
        {Keyword_kind::Kw_nichto,        {Terminal_category::Literal, Terminal_category::Block_entry_begin           }},
        {Keyword_kind::Kw_bolshoe,       {Terminal_category::Type_size_modifier, Terminal_category::Block_entry_begin}},
        {Keyword_kind::Kw_malenkoe,      {Terminal_category::Type_size_modifier, Terminal_category::Block_entry_begin}},
        {Keyword_kind::Kw_v,             {Terminal_category::In                                                      }},
        {Keyword_kind::Kw_vechno,        {Terminal_category::Forever, Terminal_category::Block_entry_begin           }},
        {Keyword_kind::Kw_vozvrat,       {Terminal_category::Return, Terminal_category::Block_entry_begin            }},
        {Keyword_kind::Kw_vyberi,        {Terminal_category::Switch, Terminal_category::Block_entry_begin            }},
        {Keyword_kind::Kw_pusto,         {Terminal_category::Fixed_size_type, Terminal_category::Block_entry_begin   }},
        {Keyword_kind::Kw_vydeli,        {Terminal_category::New, Terminal_category::Block_entry_begin               }},
        {Keyword_kind::Kw_osvobodi,      {Terminal_category::Delete, Terminal_category::Block_entry_begin            }},
        {Keyword_kind::Kw_esli,          {Terminal_category::If, Terminal_category::Block_entry_begin                }},
        {Keyword_kind::Kw_to,            {Terminal_category::Then                                                    }},
        {Keyword_kind::Kw_inache,        {Terminal_category::Else                                                    }},
        {Keyword_kind::Kw_ines,          {Terminal_category::Elif                                                    }},
        {Keyword_kind::Kw_modul,         {Terminal_category::Module                                                  }},
        {Keyword_kind::Kw_dlya,          {Terminal_category::For, Terminal_category::Block_entry_begin               }},
        {Keyword_kind::Kw_iz,            {Terminal_category::From                                                    }},
        {Keyword_kind::Kw_pauk,          {Terminal_category::Dijkstra_loop, Terminal_category::Block_entry_begin     }},
        {Keyword_kind::Kw_povtoryaj,     {Terminal_category::Do, Terminal_category::Block_entry_begin                }},
        {Keyword_kind::Kw_poka,          {Terminal_category::While, Terminal_category::Block_entry_begin             }},
        {Keyword_kind::Kw_vyjdi,         {Terminal_category::Exit, Terminal_category::Block_entry_begin              }},
        {Keyword_kind::Kw_pokuda,        {Terminal_category::As_long_as                                              }},
        {Keyword_kind::Kw_razbor,        {Terminal_category::Match, Terminal_category::Block_entry_begin             }},
        {Keyword_kind::Kw_kak,           {Terminal_category::As                                                      }},
        {Keyword_kind::Kw_rassmatrivaj,  {Terminal_category::Interpret, Terminal_category::Block_entry_begin         }},
        {Keyword_kind::Kw_golovnaya,     {Terminal_category::Main, Terminal_category::Block_entry_begin              }},
        {Keyword_kind::Kw_preobrazuj,    {Terminal_category::Convert, Terminal_category::Block_entry_begin           }},
        {Keyword_kind::Kw_tip,           {Terminal_category::Type, Terminal_category::Block_entry_begin              }},
        {Keyword_kind::Kw_perechislenie, {Terminal_category::Enum, Terminal_category::Block_entry_begin              }},
        {Keyword_kind::Kw_massiv,        {Terminal_category::Array, Terminal_category::Block_entry_begin             }},
        {Keyword_kind::Kw_samo,          {Terminal_category::Auto, Terminal_category::Block_entry_begin              }},
        {Keyword_kind::Kw_struktura,     {Terminal_category::Struct, Terminal_category::Block_entry_begin            }},
        {Keyword_kind::Kw_perech_mnozh,  {Terminal_category::Enum_set, Terminal_category::Block_entry_begin          }},
        {Keyword_kind::Kw_ssylka,        {Terminal_category::Reference, Terminal_category::Block_entry_begin         }},
        {Keyword_kind::Kw_funktsiya,     {Terminal_category::Function, Terminal_category::Block_entry_begin          }},
        {Keyword_kind::Kw_shkala,        {Terminal_category::Shkala, Terminal_category::Block_entry_begin            }},
        {Keyword_kind::Kw_shkalu,        {Terminal_category::Shkalu                                                  }},
        {Keyword_kind::Kw_ispolzuet,     {Terminal_category::Uses                                                    }},
        {Keyword_kind::Kw_eksport,       {Terminal_category::Export, Terminal_category::Block_entry_begin            }},
        {Keyword_kind::Kw_psevdonim,     {Terminal_category::Alias, Terminal_category::Block_entry_begin             }},
        {Keyword_kind::Kw_chistaya,      {Terminal_category::Pure, Terminal_category::Block_entry_begin              }},
        {Keyword_kind::Kw_konst,         {Terminal_category::Const, Terminal_category::Block_entry_begin             }},
        {Keyword_kind::Kw_perem,         {Terminal_category::Var, Terminal_category::Block_entry_begin               }},
        {Keyword_kind::Kw_lyambda,       {Terminal_category::Lambda, Terminal_category::Block_entry_begin            }},
        {Keyword_kind::Kw_meta,          {Terminal_category::Meta, Terminal_category::Block_entry_begin              }},
        {Keyword_kind::Kw_paket,         {Terminal_category::Package                                                 }},
        {Keyword_kind::Kw_kategorija,    {Terminal_category::Type_trait, Terminal_category::Block_entry_begin        }},
        {Keyword_kind::Kw_realizacija,   {Terminal_category::Implementation, Terminal_category::Block_entry_begin    }},
        {Keyword_kind::Kw_realizuj,      {Terminal_category::Implement, Terminal_category::Block_entry_begin         }},
        {Keyword_kind::Kw_operaciya,     {Terminal_category::Operation, Terminal_category::Block_entry_begin         }},
        {Keyword_kind::Kw_postfiksnaya,  {Terminal_category::Postfix_or_prefix, Terminal_category::Block_entry_begin }},
        {Keyword_kind::Kw_prefiksnaya,   {Terminal_category::Postfix_or_prefix, Terminal_category::Block_entry_begin }},
        {Keyword_kind::Kw_ekviv,         {Terminal_category::Type_equivalence                                        }},
        {Keyword_kind::Kw_element,       {Terminal_category::Belongs                                                 }},
        {Keyword_kind::Kw_sravni_s,      {Terminal_category::Compare                                                 }},
    };

    static Terminal_set keyword_conversion(const Token& token)
    {
        Terminal_set result;

        Keyword_kind k  = static_cast<Keyword_kind>(token.lexeme_.code_.subkind_);
        auto         it = keywords_mapping.find(k);
        if(it != keywords_mapping.end()){
            result = it->second;
        }

        return result;
    }


    static const std::map<Delimiter_kind, Terminal_set> delimiters_mapping = {
        {Delimiter_kind::Assign,                      {Terminal_category::Assignment, Terminal_category::By_definition     }},
        {Delimiter_kind::Copy,                        {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Logical_or_assign,           {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Logical_or_not_full_assign,  {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Logical_or_not_assign,       {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Logical_or_full_assign,      {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Logical_xor_assign,          {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Logical_and_assign,          {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Logical_and_full_assign,     {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Logical_and_not_assign,      {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Logical_and_not_full_assign, {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Bitwise_or_assign,           {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Bitwise_or_not_assign,       {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Bitwise_xor_assign,          {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Bitwise_and_assign,          {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Bitwise_and_not_assign,      {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Left_shift_assign,           {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Right_shift_assign,          {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Plus_assign,                 {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Minus_assign,                {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Mul_assign,                  {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Div_assign,                  {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Remainder_assign,            {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Float_remainder_assign,      {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Symmetric_difference_assign, {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Set_difference_assign,       {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Power_assign,                {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Float_power_assign,          {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Meta_plus_assign,            {Terminal_category::Assignment                                       }},
        {Delimiter_kind::Cond_op,                     {Terminal_category::Cond_op                                          }},
        {Delimiter_kind::Cond_op_full,                {Terminal_category::Cond_op                                          }},
        {Delimiter_kind::Logical_or,                  {Terminal_category::Logical_or_like                                  }},
        {Delimiter_kind::Logical_or_full,             {Terminal_category::Logical_or_like                                  }},
        {Delimiter_kind::Logical_or_not,              {Terminal_category::Logical_or_like                                  }},
        {Delimiter_kind::Logical_or_not_full,         {Terminal_category::Logical_or_like                                  }},
        {Delimiter_kind::Logical_xor,                 {Terminal_category::Logical_or_like                                  }},
        {Delimiter_kind::Logical_and,                 {Terminal_category::Logical_and_like                                 }},
        {Delimiter_kind::Logical_and_full,            {Terminal_category::Logical_and_like                                 }},
        {Delimiter_kind::Logical_and_not,             {Terminal_category::Logical_and_like                                 }},
        {Delimiter_kind::Logical_and_not_full,        {Terminal_category::Logical_and_like                                 }},
        {Delimiter_kind::Logical_not,                 {Terminal_category::Logical_not, Terminal_category::Block_entry_begin}},
        {Delimiter_kind::EQ,                          {Terminal_category::Relation_op                                      }},
        {Delimiter_kind::NEQ,                         {Terminal_category::Relation_op                                      }},
        {Delimiter_kind::GT,                          {Terminal_category::Relation_op                                      }},
        {Delimiter_kind::LT,                          {Terminal_category::Relation_op                                      }},
        {Delimiter_kind::GEQ,                         {Terminal_category::Relation_op                                      }},
        {Delimiter_kind::LEQ,                         {Terminal_category::Relation_op                                      }},
        {Delimiter_kind::Bitwise_or,                  {Terminal_category::Bitwise_or_like                                  }},
        {Delimiter_kind::Bitwise_or_not,              {Terminal_category::Bitwise_or_like                                  }},
        {Delimiter_kind::Bitwise_xor,                 {Terminal_category::Bitwise_or_like                                  }},
        {Delimiter_kind::Bitwise_and,                 {Terminal_category::Bitwise_and_like                                 }},
        {Delimiter_kind::Bitwise_and_not,             {Terminal_category::Bitwise_and_like                                 }},
        {Delimiter_kind::Left_shift,                  {Terminal_category::Bitwise_and_like                                 }},
        {Delimiter_kind::Right_shift,                 {Terminal_category::Bitwise_and_like                                 }},
        {Delimiter_kind::Bitwise_not,                 {Terminal_category::Bitwise_not, Terminal_category::Block_entry_begin}},
        {Delimiter_kind::Plus,                        {Terminal_category::Addition_like, Terminal_category::Unary_plus_like,
                                                       Terminal_category::Block_entry_begin}},
        {Delimiter_kind::Minus,                       {Terminal_category::Addition_like, Terminal_category::Unary_plus_like,
                                                       Terminal_category::Block_entry_begin}},
        {Delimiter_kind::Algebraic_sep,               {Terminal_category::Addition_like                                    }},
        {Delimiter_kind::Meta_plus,                   {Terminal_category::Addition_like                                    }},
        {Delimiter_kind::Mul,                         {Terminal_category::Mul_like                                         }},
        {Delimiter_kind::Div,                         {Terminal_category::Mul_like                                         }},
        {Delimiter_kind::Remainder,                   {Terminal_category::Mul_like                                         }},
        {Delimiter_kind::Float_remainder,             {Terminal_category::Mul_like                                         }},
        {Delimiter_kind::Symmetric_difference,        {Terminal_category::Mul_like                                         }},
        {Delimiter_kind::Set_difference,              {Terminal_category::Mul_like                                         }},
        {Delimiter_kind::Power,                       {Terminal_category::Power_like                                       }},
        {Delimiter_kind::Float_power,                 {Terminal_category::Power_like                                       }},
        {Delimiter_kind::Inc,                         {Terminal_category::Pre_inc_like, Terminal_category::Post_inc_like   }},
        {Delimiter_kind::Wrap_inc,                    {Terminal_category::Pre_inc_like, Terminal_category::Post_inc_like   }},
        {Delimiter_kind::Dec,                         {Terminal_category::Pre_inc_like, Terminal_category::Post_inc_like   }},
        {Delimiter_kind::Wrap_dec,                    {Terminal_category::Pre_inc_like, Terminal_category::Post_inc_like   }},
        {Delimiter_kind::ElemType,                    {Terminal_category::Unary_plus_like, Terminal_category::Block_entry_begin}},
        {Delimiter_kind::Card,                        {Terminal_category::Cardinality_like, Terminal_category::Block_entry_begin}},
        {Delimiter_kind::Round_br_opened,             {Terminal_category::Round_br_opened, Terminal_category::Block_entry_begin}},
        {Delimiter_kind::Round_br_closed,             {Terminal_category::Round_br_closed                                  }},
        {Delimiter_kind::Sq_br_opened,                {Terminal_category::Sq_br_opened                                     }},
        {Delimiter_kind::Sq_br_closed,                {Terminal_category::Sq_br_closed                                     }},
        {Delimiter_kind::Fig_br_opened,               {Terminal_category::Fig_br_opened                                    }},
        {Delimiter_kind::Fig_br_closed,               {Terminal_category::Fig_br_closed                                    }},
        {Delimiter_kind::Tuple_begin,                 {Terminal_category::Tuple_begin, Terminal_category::Block_entry_begin}},
        {Delimiter_kind::Tuple_end,                   {Terminal_category::Tuple_end                                        }},
        {Delimiter_kind::Meta_open_br,                {Terminal_category::Meta_open_br                                     }},
        {Delimiter_kind::Meta_closed_br,              {Terminal_category::Meta_closed_br                                   }},
        {Delimiter_kind::Colon_sq_br_opened,          {Terminal_category::Colon_sq_br_opened, Terminal_category::Block_entry_begin}},
        {Delimiter_kind::Colon_sq_br_closed,          {Terminal_category::Colon_sq_br_closed                               }},
        {Delimiter_kind::Colon_fig_br_opened,         {Terminal_category::Colon_fig_br_opened, Terminal_category::Block_entry_begin}},
        {Delimiter_kind::Colon_fig_br_closed,         {Terminal_category::Colon_fig_br_closed                              }},
        {Delimiter_kind::Comma,                       {Terminal_category::Comma                                            }},
        {Delimiter_kind::Right_arrow,                 {Terminal_category::Right_arrow                                      }},
        {Delimiter_kind::Point,                       {Terminal_category::Dot                                              }},
        {Delimiter_kind::Range,                       {Terminal_category::Range                                            }},
        {Delimiter_kind::Colon,                       {Terminal_category::Colon                                            }},
        {Delimiter_kind::Scope_resol,                 {Terminal_category::Scope_resol                                      }},
        {Delimiter_kind::Semicolon,                   {Terminal_category::Semicolon, Terminal_category::Block_entry_begin  }},
        {Delimiter_kind::Left_arrow,                  {Terminal_category::Left_arrow                                       }},
        {Delimiter_kind::Pattern,                     {Terminal_category::Pattern                                          }},
        {Delimiter_kind::Cycle_name_prefix,           {Terminal_category::Cycle_name_prefix                                }},
        {Delimiter_kind::Data_size,                   {Terminal_category::Sizeof_like, Terminal_category::Block_entry_begin}},
        {Delimiter_kind::Sharp,                       {Terminal_category::Sizeof_like, Terminal_category::Index_cardinalty,
                                                       Terminal_category::Block_entry_begin}},
        {Delimiter_kind::Address,                     {Terminal_category::Address_like                                     }},
        {Delimiter_kind::Data_address,                {Terminal_category::Address_like                                     }},
        {Delimiter_kind::ExprType,                    {Terminal_category::Expr_type                                        }},
        {Delimiter_kind::At,                          {Terminal_category::Deref_op, Terminal_category::Pointer_def         }},
        {Delimiter_kind::Non_specific,                {Terminal_category::Non_specific                                     }},
        {Delimiter_kind::Deduce_arg_type,             {Terminal_category::Deduce_arg_type                                  }},
        {Delimiter_kind::Compare_with,                {Terminal_category::Compare                                          }},
    };

    static Terminal_set delimiter_conversion(const Token& token)
    {
        Terminal_set result;

        Delimiter_kind k  = token.lexeme_.delim_info_.delim_kind_;
        auto           it = delimiters_mapping.find(k);
        if(it != delimiters_mapping.end()){
            result = it->second;
        }

        return result;
    }


    Terminal_set token_to_terminal_set(const Token& token)
    {
        Terminal_set result;

        const auto& lexeme = token.lexeme_;
        const auto& code   = lexeme.code_;

        switch(code.kind_){
            case Lexem_kind::Nothing:
                result[Terminal_category::End_of_text] = true;
                break;
            case Lexem_kind::UnknownLexem:
                result[Terminal_category::Unrecognized] = true;
                break;
            case Lexem_kind::Keyword:
                result = keyword_conversion(token);
                break;
            case Lexem_kind::Id:
                result[Terminal_category::Id]                = true;
                result[Terminal_category::Block_entry_begin] = true;
                break;
            case Lexem_kind::Delimiter:
                result = delimiter_conversion(token);
                break;
            case Lexem_kind::Char:  case Lexem_kind::String:  case Lexem_kind::Integer:
            case Lexem_kind::Float: case Lexem_kind::Complex: case Lexem_kind::Quat:
                result[Terminal_category::Literal]           = true;
                result[Terminal_category::Block_entry_begin] = true;
                break;
            default:
                break;
        }

        return result;
    }
};