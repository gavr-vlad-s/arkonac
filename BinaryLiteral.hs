module BinaryLiteral(
    binaryFromString
)where

import qualified Data.List as L
import Data.Char

binaryFromString :: String -> Integer
binaryFromString xs = L.foldl' (\n c -> n * 2 + (toInteger . digitToInt $ c)) 0 . drop 2 $ xs