/*
    File:    bitset.cpp
    Created: 28 June 2020 at 09:47 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstring>
#include <algorithm>
#include <stdexcept>
#include <list>
#include "../include/bitset.hpp"
#include "../include/popcount.hpp"
#include "../../strings-lib/include/join.h"

namespace bitscale{
    bitset::bitset()
    {
        set_value_.kind_ = Value_kind::Small;
        for(std::uint64_t& v : set_value_.value_.small_set_){
            v = 0;
        }
        set_value_.num_of_int64s_    = bitset::num_of_int64s_for_small_set_;
        set_value_.max_num_of_elems_ = maximal_num_of_bits_in_small_set_;
    }

    bitset::~bitset()
    {
        if(set_value_.kind_ == Value_kind::Large){
            delete [] set_value_.value_.large_set_ptr_;
            set_value_.value_.large_set_ptr_ = nullptr;
        }
    }

    bitset::bitset(std::size_t num_of_elements)
    {
        set_value_.max_num_of_elems_ = (num_of_elements != 0) ? num_of_elements : maximal_num_of_bits_in_small_set_;
        std::size_t num_of_int64s    = set_value_.max_num_of_elems_ / num_of_bits_in_uint64;
        if(set_value_.max_num_of_elems_ % num_of_bits_in_uint64){
            num_of_int64s++;
        }
        if(num_of_int64s <= num_of_int64s_for_small_set_){
            set_value_.kind_ = Value_kind::Small;
            for(std::uint64_t& v : set_value_.value_.small_set_){
                v = 0;
            }
        }else{
            set_value_.kind_      = Value_kind::Large;
            auto*  large_set_ptr  = new std::uint64_t[num_of_int64s];
            memset(large_set_ptr, 0, num_of_int64s);
            set_value_.value_.large_set_ptr_ = large_set_ptr;
        }
        set_value_.num_of_int64s_ = num_of_int64s;
    }

    void swap(bitset &lhs, bitset &rhs) noexcept
    {
        using std::swap;

        bitset::Set_value temp{lhs.set_value_};
        lhs.set_value_ = rhs.set_value_;
        rhs.set_value_ = temp;
    }

    bitset::bitset(const bitset &rhs)
    {
        set_value_.kind_             = rhs.set_value_.kind_;
        set_value_.max_num_of_elems_ = rhs.set_value_.max_num_of_elems_;
        set_value_.num_of_int64s_    = rhs.set_value_.num_of_int64s_;

        if(rhs.set_value_.kind_ == Value_kind::Small){
            for(std::size_t i = 0; i < bitset::num_of_int64s_for_small_set_; ++i){
                set_value_.value_.small_set_[i] = rhs.set_value_.value_.small_set_[i];
            }
        }else{
            std::size_t num_of_int64s = rhs.set_value_.num_of_int64s_;
            auto * p                  = new uint64_t[num_of_int64s];
            for(std::size_t i = 0; i < num_of_int64s; ++i){
                p[i] = rhs.set_value_.value_.large_set_ptr_[i];
            }
            set_value_.value_.large_set_ptr_ = p;
        }
    }

    bitset& bitset::operator=(const bitset& rhs)
    {
        if(this == &rhs){
            return *this;
        }

        bitset temp{rhs};
        swap(*this, temp);
        return *this;
    }

    bool bitset::empty() const
    {
        std::uint64_t temp = 0;
        if(set_value_.kind_ == Value_kind::Small){
            for(std::uint64_t v : set_value_.value_.small_set_){
                temp |= v;
            }
        }else{
            std::size_t n    = set_value_.num_of_int64s_;
            std::uint64_t* p = set_value_.value_.large_set_ptr_;
            for(std::size_t i = 0; i < n; ++i){
                temp |= p[i];
            }
        }
        return temp == 0;
    }

    bitset bitset::complement() const
    {
        bitset result{*this};
        std::size_t n                             = set_value_.num_of_int64s_;
        std::size_t num_of_unused_bits            = n * num_of_bits_in_uint64 - set_value_.max_num_of_elems_;
        std::size_t num_of_used_bits_of_last_word = num_of_bits_in_uint64 - num_of_unused_bits;
        std::size_t mask                          = (static_cast<std::size_t>(1u) << num_of_used_bits_of_last_word) - 1;
        if(set_value_.kind_ == Value_kind::Small){
            for(std::size_t i = 0; i < n; ++i){
                result.set_value_.value_.small_set_[i] = ~set_value_.value_.small_set_[i];
            }
            result.set_value_.value_.small_set_[n - 1] &= mask;
        }else{
            std::uint64_t* p = set_value_.value_.large_set_ptr_;
            for(std::size_t i = 0; i < n; ++i){
                p[i] = ~p[i];
            }
            p[n - 1] &= mask;
        }
        return result;
    }

    bitset& bitset::operator += (const bitset &rhs)
    {
        if(incompatible_sets(rhs)){
            throw std::logic_error{"Sizes of bitsets do not match."};
        }

        std::size_t n = set_value_.num_of_int64s_;
        if(set_value_.kind_ == Value_kind::Small){
            for(std::size_t i = 0; i < n; ++i){
                set_value_.value_.small_set_[i] |= rhs.set_value_.value_.small_set_[i];
            }
        }else{
            std::uint64_t* dst = set_value_.value_.large_set_ptr_;
            std::uint64_t* src = rhs.set_value_.value_.large_set_ptr_;
            for(std::size_t i = 0; i < n; ++i){
                dst[i] |= src[i];
            }
        }

        return *this;
    }

    bitset& bitset::operator *= (const bitset &rhs)
    {
        if(incompatible_sets(rhs)){
            throw std::logic_error{"Sizes of bitsets do not match."};
        }

        std::size_t n = set_value_.num_of_int64s_;
        if(set_value_.kind_ == Value_kind::Small){
            for(std::size_t i = 0; i < n; ++i){
                set_value_.value_.small_set_[i] &= rhs.set_value_.value_.small_set_[i];
            }
        }else{
            std::uint64_t* dst = set_value_.value_.large_set_ptr_;
            std::uint64_t* src = rhs.set_value_.value_.large_set_ptr_;
            for(std::size_t i = 0; i < n; ++i){
                dst[i] &= src[i];
            }
        }

        return *this;
    }

    bitset& bitset::operator ^= (const bitset &rhs)
    {
        if(incompatible_sets(rhs)){
            throw std::logic_error{"Sizes of bitsets do not match."};
        }

        std::size_t n = set_value_.num_of_int64s_;
        if(set_value_.kind_ == Value_kind::Small){
            for(std::size_t i = 0; i < n; ++i){
                set_value_.value_.small_set_[i] ^= rhs.set_value_.value_.small_set_[i];
            }
        }else{
            std::uint64_t* dst = set_value_.value_.large_set_ptr_;
            std::uint64_t* src = rhs.set_value_.value_.large_set_ptr_;
            for(std::size_t i = 0; i < n; ++i){
                dst[i] ^= src[i];
            }
        }

        return *this;
    }

    bitset& bitset::operator-=(const bitset &rhs)
    {
        if(incompatible_sets(rhs)){
            throw std::logic_error{"Sizes of bitsets do not match."};
        }

        std::size_t n = set_value_.num_of_int64s_;
        if(set_value_.kind_ == Value_kind::Small){
            for(std::size_t i = 0; i < n; ++i){
                set_value_.value_.small_set_[i] &= ~rhs.set_value_.value_.small_set_[i];
            }
        }else{
            std::uint64_t* dst = set_value_.value_.large_set_ptr_;
            std::uint64_t* src = rhs.set_value_.value_.large_set_ptr_;
            for(std::size_t i = 0; i < n; ++i){
                dst[i] &= ~src[i];
            }
        }

        return *this;
    }

    std::size_t bitset::size() const
    {
        std::size_t result = 0;
        std::size_t n      = set_value_.num_of_int64s_;
        if(set_value_.kind_ == Value_kind::Small){
            for(std::size_t i = 0; i < n; ++i){
                result += popcnt(set_value_.value_.small_set_[i]);
            }
        }else{
            std::uint64_t* p = set_value_.value_.large_set_ptr_;
            for(std::size_t i = 0; i < n; ++i){
                result += popcnt(p[i]);
            }
        }
        return result;
    }

    bitset::bitset(bitset &&rhs) noexcept
    {
        set_value_                           = rhs.set_value_;
        rhs.set_value_.value_.large_set_ptr_ = nullptr;
    }

    bool bitset::get_bit(std::size_t position) const
    {
        std::size_t word_number = get_word_number(position);
        std::size_t bit_mask    = get_mask(position);
        if(set_value_.kind_ == Value_kind::Small){
            return (set_value_.value_.small_set_[word_number] & bit_mask)     != 0;
        }else{
            return (set_value_.value_.large_set_ptr_[word_number] & bit_mask) != 0;
        }
    }

    void bitset::set_bit(std::size_t position)
    {
        std::size_t word_number = get_word_number(position);
        std::size_t bit_mask    = get_mask(position);
        if(set_value_.kind_ == Value_kind::Small){
            set_value_.value_.small_set_[word_number]     |= bit_mask;
        }else{
            set_value_.value_.large_set_ptr_[word_number] |= bit_mask;
        }
    }

    void bitset::reset_bit(std::size_t position)
    {
        std::size_t word_number = get_word_number(position);
        std::size_t bit_mask    = get_mask(position);
        if(set_value_.kind_ == Value_kind::Small){
            set_value_.value_.small_set_[word_number]     &= ~bit_mask;
        }else{
            set_value_.value_.large_set_ptr_[word_number] &= ~bit_mask;
        }

    }

    void bitset::flip_bit(std::size_t position)
    {
        std::size_t word_number = get_word_number(position);
        std::size_t bit_mask    = get_mask(position);
        if(set_value_.kind_ == Value_kind::Small){
            set_value_.value_.small_set_[word_number]     ^= bit_mask;
        }else{
            set_value_.value_.large_set_ptr_[word_number] ^= bit_mask;
        }
    }

    std::string bitset::to_string() const
    {
        std::list<std::string> elements;
        for(std::size_t i = 0; i < set_value_.max_num_of_elems_; ++i){
            if(get_bit(i)){
                elements.push_back(std::to_string(i));
            }
        }
        std::string body   = join(elements.begin(), elements.end(), std::string{", "});
        std::string result = "{" + body + "}";
        return result;
    }

    std::vector<std::size_t> bitset::to_vector() const
    {
        std::vector<std::size_t> result;
        for(std::size_t i = 0; i < set_value_.max_num_of_elems_; ++i){
            if(get_bit(i)){
                result.push_back(i);
            }
        }
        return result;
    }

    bool bitset::operator==(const bitset &rhs) const
    {
        if(incompatible_sets(rhs)){
            throw std::logic_error{"Sizes of bitsets do not match."};
        }

        std::size_t result = 0;
        std::size_t n      = set_value_.num_of_int64s_;
        if(set_value_.kind_ == Value_kind::Small){
            for(std::size_t i = 0; i < n; ++i){
                result |= set_value_.value_.small_set_[i] ^ rhs.set_value_.value_.small_set_[i];
            }
        }else{
            std::uint64_t* dst = set_value_.value_.large_set_ptr_;
            std::uint64_t* src = rhs.set_value_.value_.large_set_ptr_;
            for(std::size_t i = 0; i < n; ++i){
                result |= dst[i] ^ src[i];
            }
        }

        return result == 0;
    }

    bool bitset::operator<=(const bitset &rhs) const
    {
        if(incompatible_sets(rhs)){
            throw std::logic_error{"Sizes of bitsets do not match."};
        }

        std::size_t result = 0;
        std::size_t n      = set_value_.num_of_int64s_;
        if(set_value_.kind_ == Value_kind::Small){
            for(std::size_t i = 0; i < n; ++i){
                std::uint64_t a = set_value_.value_.small_set_[i];
                std::uint64_t b = rhs.set_value_.value_.small_set_[i];
                result |= a ^ (a & b);
            }
        }else{
            std::uint64_t* dst = set_value_.value_.large_set_ptr_;
            std::uint64_t* src = rhs.set_value_.value_.large_set_ptr_;
            for(std::size_t i = 0; i < n; ++i){
                std::uint64_t a = src[i];
                std::uint64_t b = dst[i];
                result |= a ^ (a & b);
            }
        }

        return result == 0;
    }

    bool bitset::incompatible_sets(const bitset &rhs) const
    {
        bool result = (set_value_.kind_          != rhs.set_value_.kind_         ) ||
                      (set_value_.num_of_int64s_ != rhs.set_value_.num_of_int64s_);
        return result;
    }

    bitset operator + (const bitset &lhs, const bitset &rhs)
    {
        bitset result{lhs};
        result += rhs;
        return result;
    }

    bitset operator * (const bitset &lhs, const bitset &rhs)
    {
        bitset result{lhs};
        result *= rhs;
        return result;
    }

    bitset operator ^ (const bitset &lhs, const bitset &rhs)
    {
        bitset result{lhs};
        result ^= rhs;
        return result;
    }

    bitset operator ~ (const bitset& b)
    {
        return b.complement();
    }

    bitset operator-(const bitset &lhs, const bitset &rhs)
    {
        bitset result{lhs};
        result -= rhs;
        return result;
    }

    bool operator!=(const bitset &lhs, const bitset &rhs)
    {
        return !(lhs == rhs);
    }

    bool operator>=(const bitset &lhs, const bitset &rhs)
    {
        return rhs <= lhs;
    }

    bool operator<(const bitset &lhs, const bitset &rhs)
    {
        return (lhs <= rhs) && (lhs != rhs);
    }

    bool operator>(const bitset &lhs, const bitset &rhs)
    {
        return (rhs > lhs);
    }

    void bitset::flip()
    {
        std::size_t n = set_value_.max_num_of_elems_;
        for(std::size_t pos = 0; pos < n; ++pos){
            flip_bit(pos);
        }
    }
};