/*
    File:    popcount.cpp
    Created: 28 June 2020 at 09:40 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/popcount.hpp"

static const std::uint64_t masks[] = {
    0x5555'5555'5555'5555, 0x3333'3333'3333'3333, 0x0f0f'0f0f'0f0f'0f0f,
    0x00ff'00ff'00ff'00ff, 0x0000'ffff'0000'ffff, 0x0000'0000'ffff'ffff
};

std::size_t popcnt(std::uint64_t z)
{
    std::uint64_t x     = z;
    std::uint64_t shift = 1;
    for(std::uint64_t mask : masks){
        x     =   (x & mask) + ((x >> shift) & mask);
        shift <<= 1;
    }
    return x;
}