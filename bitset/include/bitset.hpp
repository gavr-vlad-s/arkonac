/*
    File:    bitset.hpp
    Created: 28 June 2020 at 09:45 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef BITSET_H
#define BITSET_H
#   include <climits>
#   include <cstdint>
#   include <cstddef>
#   include <initializer_list>
#   include <string>
#   include <vector>
namespace bitscale{
    static constexpr std::size_t num_of_bits_in_uint64 = sizeof(std::uint64_t) * CHAR_BIT;

    class bitset final{
    public:
        bitset();
        bitset(const bitset& rhs);
        bitset(bitset&& rhs) noexcept;
        bitset& operator = (const bitset& rhs);
        bitset& operator = (bitset&& rhs) = default;

        explicit bitset(std::size_t num_of_elements);

        template<typename ForwardIterator>
        void set(ForwardIterator first, ForwardIterator last)
        {
            for(auto it = first; it != last; ++it){
                set_bit(*it);
            }
        }

        void set(std::initializer_list<std::size_t> l)
        {
            set(l.begin(), l.end());
        }

        template<typename ForwardIterator>
        void reset(ForwardIterator first, ForwardIterator last)
        {
            for(auto it = first; it != last; ++it){
                reset_bit(*it);
            }
        }

        void reset(std::initializer_list<std::size_t> l)
        {
            reset(l.begin(), l.end());
        }

        ~bitset();

        bitset& operator += (const bitset& rhs);
        bitset& operator *= (const bitset& rhs);
        bitset& operator ^= (const bitset& rhs);
        bitset& operator -= (const bitset& rhs);

        std::size_t size() const;

        bool empty() const;

        bitset complement() const;

        void flip();

        friend void swap(bitset& lhs, bitset& rhs) noexcept;

        class reference{
        public:
            reference() : bitset_ptr_{nullptr}, position_{0} {}
            reference(bitset* bitset_ptr, std::size_t position) :
                bitset_ptr_{bitset_ptr}, position_{position} {}
            reference(const reference&) = default;
            ~reference() = default;

            // For x = b[i]
            operator bool() const noexcept
            {
                return bitset_ptr_->get_bit(position_);
            }

            // For b[i] = x
            reference& operator = (bool x) noexcept
            {
                if(x){
                    bitset_ptr_->set_bit(position_);
                }else{
                    bitset_ptr_->reset_bit(position_);
                }
                return *this;
            }

            // For b[i] = b[j]
            reference& operator = (const reference& j) noexcept
            {
                if(j){
                    bitset_ptr_->set_bit(position_);
                }else{
                    bitset_ptr_->reset_bit(position_);
                }
                return *this;
            }

            bool operator ~ () const noexcept
            {
                return bitset_ptr_->get_bit(position_) == 0;
            }

            reference& flip() noexcept
            {
                bitset_ptr_->flip_bit(position_);
                return *this;
            }
        private:
            friend class bitset;
            bitset*     bitset_ptr_;
            std::size_t position_;
        };

        friend class reference;

        reference operator[](std::size_t position)
        {
            return reference{this, position};
        }

        bool operator[](std::size_t position) const
        {
            return get_bit(position);
        }

        std::string to_string() const;
        std::vector<std::size_t> to_vector() const;

        bool operator == (const bitset& rhs) const;
        bool operator <= (const bitset& rhs) const;

        void set_bit(std::size_t position);
        void reset_bit(std::size_t position);
        void flip_bit(std::size_t position);

        std::size_t get_maximal_number_of_elements() const
        {
            return set_value_.max_num_of_elems_;
        }
    private:
        static constexpr std::size_t num_of_int64s_for_small_set_      = 4u;
        static constexpr std::size_t maximal_num_of_bits_in_small_set_ = num_of_int64s_for_small_set_ *
                                                                         num_of_bits_in_uint64;

        enum class Value_kind{
            Small, Large
        };

        struct Set_value{
            Value_kind kind_;
            union {
                std::uint64_t  small_set_[num_of_int64s_for_small_set_];
                std::uint64_t* large_set_ptr_;
            } value_;
            std::size_t num_of_int64s_;
            std::size_t max_num_of_elems_;
        };

        Set_value set_value_;

        bool get_bit(std::size_t position) const;

        std::uint64_t get_mask(std::size_t position) const
        {
            std::size_t bit_number  = position % num_of_bits_in_uint64;
            std::size_t bit_mask    = static_cast<std::uint64_t>(1u) << bit_number;
            return bit_mask;
        }

        std::size_t get_word_number(std::size_t position) const
        {
            return position / num_of_bits_in_uint64;
        }

        bool incompatible_sets(const bitset& rhs) const;
    };

    bitset operator + (const bitset& lhs, const bitset& rhs);
    bitset operator * (const bitset& lhs, const bitset& rhs);
    bitset operator ^ (const bitset& lhs, const bitset& rhs);
    bitset operator - (const bitset& lhs, const bitset& rhs);

    bitset operator ~ (const bitset& b);
    bool operator != (const bitset& lhs, const bitset& rhs);
    bool operator >= (const bitset& lhs, const bitset& rhs);
    bool operator < (const bitset& lhs, const bitset& rhs);
    bool operator > (const bitset& lhs, const bitset& rhs);
};
#endif