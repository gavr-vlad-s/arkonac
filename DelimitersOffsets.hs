module DelimitersOffsets(
    strs, uniqueSortedStrs, constructOffsetInfo
)where
import qualified Data.List as L
import qualified Data.Map as M

strs = ["",
        "&=|",
        "&",
        ".=",
        "=",
        "|",
        "#",
        ".=",
        "=",
        "&=",
        ":",
        "*/=",
        "+=",
        "<",
        "-=>",
        "<",
        ".|",
        ".",
        "*=\\",
        "):=>]}",
        "!&+-:<=?|",
        ">",
        "&>",
        ">",
        ">",
        "=",
        ">",
        ">?",
        "=>",
        ":",
        "^=",
        "=",
        ".:",
        "}",
        "#=|",
        "|",
        ".=",
        "=",
        "&|",
        "="]

uniqueSortedStrs = L.nub . L.sort $ strs

translitMap :: M.Map Char String
translitMap = M.fromList [
    ('!',  "exclamation"),
    ('#',  "sharp"      ),
    ('&',  "amp"        ),
    (')',  "rbc"        ),
    ('*',  "ast"        ),
    ('+',  "plus"       ),
    ('-',  "minus"      ),
    ('.',  "point"      ),
    ('/',  "slash"      ),
    (':',  "colon"      ),
    ('<',  "lt"         ),
    ('=',  "eq"         ),
    ('>',  "gt"         ),
    ('?',  "qm"         ),
    ('\\', "bslash"     ),
    (']',  "sqbrc"      ),
    ('^',  "hat"        ),
    ('|',  "vert"       ),
    ('}',  "figbrc"     )]


errorMsg :: String
errorMsg = "Unsupported character."


translit :: String -> String
translit [] = "Empty"
translit s  = concat ["Str_", L.intercalate "_" . map (\c -> translitChar c) $ s]
    where
        translitChar c = case M.lookup c translitMap of
                              Just s  -> s
                              Nothing -> error errorMsg



data OffsetInfo = OffsetInfo{enumElems :: [String],
                             transStrings :: [String]} deriving(Eq, Ord)


indent = replicate 8 ' '


instance Show OffsetInfo where
    show oi = concat ["    enum class Offsets : uint16_t{\n",
                      showEnumElems oi,
                      "\n    };\n\n",
                      "    static const trans_strs[] = {\n",
                      showTransStrs oi,
                      "\n  };"]
        where
            showEnumElems oi =  L.intercalate ",\n"     .
                                map (\s -> indent ++ s) .
                                enumElems               $ oi
            showTransStrs oi = L.intercalate ",\n" . transStrings $ oi


emptyInfo :: OffsetInfo
emptyInfo = OffsetInfo{enumElems = [], transStrings = []}

transStringAsSequence :: String -> String
transStringAsSequence s = indent ++ transStringAsSequence' s
    where
        transStringAsSequence' = L.intercalate ", " . transStringAsSequence''
        transStringAsSequence'' s = map (\c -> ['U', '\'', c, '\'']) s ++ ["U\'\\0\'"]

constructOffsetInfo :: OffsetInfo
constructOffsetInfo = OffsetInfo{enumElems    = reverse . enumElems    $ auxOffsetInfo,
                                 transStrings = reverse . transStrings $ auxOffsetInfo}
    where
        auxOffsetInfo = fst . L.foldl' (\(oi, n) s -> f s oi n) (emptyInfo, 0) $ uniqueSortedStrs
        f s oi n = (OffsetInfo{enumElems    = (translit s ++ " = " ++ show n):(enumElems oi),
                               transStrings = (transStringAsSequence s):(transStrings oi)},
                    n + length s + 1)
