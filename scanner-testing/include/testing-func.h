/*
    File:    test_func.h
    Created: 09 July 2019 at 08:10 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef TESTING_FUNC_H
#define TESTING_FUNC_H
#   include <memory>
#   include "../../scanner/include/arkona-scanner.h"
namespace testing_scanner{
    void test_func(const std::shared_ptr<arkona_scanner::Scanner>& arkonasc);
};
#endif