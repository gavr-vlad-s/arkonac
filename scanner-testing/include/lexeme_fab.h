/*
    File:    lexeme_fab.h
    Created: 19 December 2019 at 06:55 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef LEXEME_FAB_H
#define LEXEME_FAB_H
#   include <cstddef>
#   include "../../scanner/include/lexeme.h"
namespace testing_scanner{
    using Lexeme       = arkona_scanner::Lexeme_info;
    using Keyword_kind = arkona_scanner::Keyword_kind;

    //! Functions that return special lexemes.
    const Lexeme unknown_lexeme();
    const Lexeme nothing_lexeme();

    //! Function that return keyword.
    const Lexeme keyword_lexeme(Keyword_kind kw);

    //! Function that return identifier.
    const Lexeme id_lexeme(std::size_t idx);

    //! Function that return character lexeme.
    const Lexeme char_lexeme(char32_t c);

    //! Function that return encoded character lexeme.
    const Lexeme encoded_char_lexeme(char32_t c);

    //! Function that return string lexeme.
    const Lexeme string_lexeme(size_t str_idx);

    //! Function that return integer lexeme.
    const Lexeme integer_lexeme(unsigned __int128 int_val);

    //! Function that return float lexeme.
    using Float_kind = arkona_scanner::Float_kind;
    const Lexeme float_lexeme(__float128 float_val,
                              Float_kind precision = Float_kind::Float64);

    //! Function that return complex lexeme.
    using Complex_kind = arkona_scanner::Complex_kind;
    const Lexeme complex_lexeme(__complex128 complex_val,
                                Complex_kind precision = Complex_kind::Complex64);

    //! Function that return quaternion lexeme.
    using Quat_kind = arkona_scanner::Quat_kind;
    const Lexeme quat_lexeme(const quat::quat_t<__float128>& quat_val,
                             Quat_kind                       precision = Quat_kind::Quat64);

    //! Function that return delimiter lexeme.
    using Delimiter_kind = arkona_scanner::Delimiter_kind;
    const Lexeme delim_lexeme(Delimiter_kind delim);
};
#endif