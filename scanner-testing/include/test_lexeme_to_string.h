/*
    File:    test_lexeme_to_string.h
    Created: 14 August 2019 at 06:25 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef TEST_LEXEME_TO_STRING_H
#define TEST_LEXEME_TO_STRING_H
namespace testing_scanner{
    void test_lexeme_to_string();
};
#endif