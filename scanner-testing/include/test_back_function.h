/*
    File:    test_back_function.h
    Created: 22 December 2019 at 16:25 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef TEST_BACK_FUNCTION_H
#define TEST_BACK_FUNCTION_H
namespace testing_scanner{
    void test_back_function();
};
#endif