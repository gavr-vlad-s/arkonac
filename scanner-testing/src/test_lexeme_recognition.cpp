/*
    File:    test_lexeme_recognition.cpp
    Created: 05 November 2019 at 05:47 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <vector>
#include <string>
#include <iterator>
#include <memory>
#include <cstddef>
#include <cstdio>
#include "../include/test_lexeme_recognition.h"
#include "../include/lexeme_fab.h"
#include "../../scanner/include/arkona-scanner.h"
#include "../../other/include/testing.hpp"
#include "../../tries/include/errors_and_tries.h"
#include "../../tries/include/char_trie.h"
#include "../../scanner/include/location.h"
#include "../../numbers/include/digit_to_int.h"

namespace testing_scanner{
    static bool is_digit(char32_t c)
    {
        bool result;
        result = ((U'0' <= c) && (c <= U'9')) ||
                 ((U'A' <= c) && (c <= U'F')) ||
                 ((U'a' <= c) && (c <= U'f'));
        return result;
    }

    static unsigned __int128 str_to_int(const char32_t* str, unsigned base = 10)
    {
        unsigned __int128 result = 0;
        char32_t          c;
        while((c = *str++)){
            if(is_digit(c)){
                result = result * base + digit_to_int(c);
            }
        }
        return result;
    }

    static const char32_t* test_texts[] = {
        // Text #0:
        UR"~(
    б              бег

            бор
                 бе
        бо

белый     безмолвный                  безмятежный
        бурый
                болото)~",

        // Text #1:
        UR"~(безликий            беззнаковый
        беззн1024           больной_кот

            большеголовый

                    беззн16             беззн8
    беззн32     беззн64         беззн128

            большое

                        беззн
        больше

  __лунатики_прилетели _негр_литературный)~",

        // Text #2:
        UR"~(веский_довод

                в
        выйди         вон        выбери

                выбери_меня         выдели_цветом

    возврат        выдели

            вещественных_чисел_поле

                                        вещ    вечно

        вечный_кипр   вещ80       вещ128     вещ32     вещ64    вещ256)~",

        // Text #3:
        UR"~(веский_довод

                в
        выйди         вон        выбери

                выбери_меня         выдели_цветом

    возврат        выдели


главный головная       грач

            вещественных_чисел_поле

            для     длительный         деловая_колбаса

                                        вещ    вечно

                         если          есть          ел_ем_и_буду_есть

        вечный_кипр   вещ80       вещ128     вещ32     вещ64    вещ256


из    инес иначе


            конст           как            компл

    кват       компл64            кват32          компл80

        кват64            компл128         кват80             компл32

                кват128

  категория

                комплексное_значение     кватернион          колонист

        исторический        использует

                исконный       истина           используемый     истинный

            иностранный



    логарифм       лог         ложь         лживый     логический

        лягушка       лямбда        лог64     линейный

            лог8              ля           лог32          лог16)~",

        // Text #4:
        UR"~(

        маляр       маленькое        массив

                мандрагора      маска


    мета             мечта             метафора

            меньше          меньшинство            модуль



        магический

                    ничто     нечто     начальный        новый




            операция       оператор         освободи       крылатый


 паук   опёнок  поучайте_лучше_ваших_паучат         пакет

            пакетный_менеджер                 повторяй        повторение

  пока        позволить          показывать     покуда          порядок
    порядковое_числительное  порядок64   порядок8   порядок16     порядок32

        постфиксная      преобразуй
                префиксная

            псевдоним                          пусто         пустота   вакуум)~",


        // Text #5:
        UR"~(
    равенство    равно     разность

        разбор    рассматривай   рассказ

реализуй            реализация      концепция_концепции_категории


        сослать        ссылочный_тип    ссылка

                самовар    само        структура


    строка32       строка8               строка           строка16

                сравнение     сравни_с              символьные_данные

        симв         симв32              симв8

                симв16



то       тип  функция          трогательный     типовой

    целостный             целый  цел

    феррит         флот     фумигатор


            цел128                         цел8
                    цел16      цел64             цел32   честно      чистая

    шкала      шкалу                        область_целостности


            элементарный      элемент           экспортный_товар        экспорт
                эспериментальный        эквив       эквипотенциальный)~",

        // Text #6:
        UR"~(''''

        'Ϣ'   'ϣ'            'Ϥ' 'ϥ'

    'Ϧ'  'ϧ'   'Ϩ'     'ϩ'    'Ϫ'    'ϫ'


            'Ϭ'  'ϭ'  'Ϯ'    'ϯ'   'ϰ'  'ϱ'   'ϲ'   'ϳ'   'ϴ'   'ϵ'  '϶'


                'Ϸ'   'ϸ'  'Ϲ'  'Ϻ'  'ϻ'  'ϼ'  'Ͻ'  'Ͼ'  'Ͽ'  'Ѐ'  'Ё'     'Ђ'   'Ѓ'   'Є'   'Ѕ'

  'І'   'Ї'   'Ј'  'Љ'   'Њ'   'Ћ'   'Ќ'  'Ѝ'  'Ў'  'Џ'  'А'  'Б'  'В'  'Г'   'Д'    'Е'     'Ж'     'З'    'И'

            'Й'   'К'       'Л'    'М'       'Н'


                       'О'      'џ'   'Ѡ'          'ѡ'     'Ѣ'           'ѣ'

          'Ѥ'               'ѥ'            'Ѧ'           'ѧ'      'Ѩ'          'ѩ'

      'Ѫ'         'ѫ'                'Ѭ'             'ѭ'       'Ѯ'      'ѯ'

            'Ѱ'        'ѱ'    'Ѳ'        'ѳ'    'Ѵ'       'ѵ'       'Ѷ'   '"')~",

        // Text #7:
        UR"~(    $123
          $0x0401    $0x41

            $0o103

               $0b01000010)~",

        // Text #8:
        UR"~(  $123

$1025
      $10'25
            $0x0'46'C
                $0x4d'e   $0b0001'1010'0010

                    $0o1746  $0o1'74'5)~",

        // Text #9:
        UR"~(

              "Bittida en morgon innan solen upprann
Innan foglarna började sjunga
Bergatroliet friade till fager ungersven
Hon hade en falskeliger tunga "

            "Тебе, девка, житье у меня будет лёгкое, --- не столько работать, сколько отдыхать будешь!
Утром станешь, ну, как подобат, --- до свету. Избу вымоешь, двор уберешь, коров подоишь, на поскотину выпустишь, в хлеву приберешься и --- спи-отдыхай!
Завтрак состряпаешь, самовар согреешь, нас с матушкой завтраком накормишь --- спи-отдыхай!
В поле поработашь, али в огороде пополешь, коли зимой --- за дровами али за сеном съездишь и --- спи-отдыхай!
Обед сваришь, пирогов напечешь: мы с матушкой обедать сядем, а ты --- спи-отдыхай!
После обеда посуду вымоешь, избу приберешь и --- спи-отдыхай!
Коли время подходяче --- в лес по ягоду, по грибы сходишь, али матушка в город спосылат, дак сбегашь. До городу --- рукой подать, и восьми верст не будет, а потом --- спи-отдыхай!
Из городу прибежишь, самовар поставишь. Мы с матушкой чай станем пить, а ты --- спи-отды­хай!
Вечером коров встретишь, подоишь, попоишь, корм задашь и --- спи-отдыхай!
Ужену сваришь, мы с матушкой съедим, а ты --- спи-отдыхай!
Воды наносишь, дров наколешь --- это к завтрему, и --- спи-отдыхай!
Постели наладишь, нас с матушкой спать повалишь. А ты, девка, день-деньской проспишь-про­отдыхаешь --- во что ночь-то будешь спать?
Ночью попрядёшь, поткешь, повышивашь, пошьешь и опять --- спи-отдыхай!
Ну, под утро белье постирать, которо надо --- поштопашь да зашьешь и --- спи-отдыхай!
Да ведь, девка, не даром. Деньги платить буду. Кажной год по рублю! Сама подумай. Сто годов --- сто рублев. Богатейкой станешь! "


"""У попа была собака""")~",

        // Text #10:
        UR"~(
           0123'45689
                         302'231'454'903'657'293'676'544         158'456'325'028'528'675'187'087'900'672



       1'329'227'995'784'915'872'903'807'060'280'344'576
                  340'282'366'920'938'463'463'374'607'431'768'211'455

                  340282366920938463463374607431768211455



 0xffff'ffff'ffffff'ffffffffffffff'ffff

                0x1238912345                   0o123'755'5551'232'4561'2345       0o123755555123245612345



        0xdeadbeafde123456789abcde

0b1111111110000111

                           0b1111111110000111101010001110001111000001111100000001111111110111111010101000001011110011110100001111110111111011010100101010101

                           0b10101)~",

        // Text #11:
        UR"~(
          3.1415926535897932384626)~",

        // Text #12:
        UR"~(

            45678.912'34'567'8923'45E-12           3.45678e+35


        5141.592653e+8   6.141592653148e+8f


            3.141592653e-78q  9141.59265356e-78q  2.141592653e+11d


      71415926537.182818e-8d     6.141592653148e+8f


        4'5.678912'34'567'8923'45E-1'2                  45760.912'34'567'8923'45E-223i
                45760.912'34'567'8923'45E-223j    45760.912'34'567'8923'45E-223k


          0.618281828459045e-4x    0.618281828459045e+4x    0.618281828459045e+4xj
                6.141592653148e+8fk 4'5.678912'34'567'8923'45E-1'2qj 0.6)~",

        // Test text #13
        UR"~(;      !
            !&                 !=
    !&&
                !|          !||         #    ->
        !&&.        !||.
              ##               %
            !&&=            !||=
                       %.
                                    !&&.=        -=

          --<
                    !||.=

  %=                %.=

      --                        -


          &&.=                 &         (:     (
                  &&                &=
   &&.       &&=          )


:) : :: :=
    :>    :}     :]

   /*   /=  /\=  /\   /

,   ]
        */  **   *
                        **.   *=        **=
                **.=             +                     ++


      +=              ++<

               .          ..             .|         .|.


                 ==         =

  < <! <&        <+          <-           <:
           <<         <=           <?                    <|


      <!> <&&  <&>     <+>     <&&>          <+>=

         <:>               <<=           <?? <?>   <??>

  \      [:     \=        [
@    ^^      ^=             ^^=    ^

     |  |#          |#|       |=
           ||   ||.     ||=        ||.=

  {..} {    {.  {..          >  >=   >>               >>=


         ~             ~|         ~&=           ~&  ~|=

    })~"
    };

    static const unsigned __int128 int_vals[] = {
        str_to_int(UR"~(0123'45689)~"),
        str_to_int(UR"~(302'231'454'903'657'293'676'544)~"),
        str_to_int(UR"~(158'456'325'028'528'675'187'087'900'672)~"),
        str_to_int(UR"~(1'329'227'995'784'915'872'903'807'060'280'344'576)~"),
        str_to_int(UR"~(340'282'366'920'938'463'463'374'607'431'768'211'455)~"),
        str_to_int(UR"~(340282366920938463463374607431768211455)~"),
        str_to_int(UR"~(0xffff'ffff'ffffff'ffffffffffffff'ffff)~", 16),
        str_to_int(UR"~(0x1238912345)~", 16),
        str_to_int(UR"~(0o123'755'5551'232'4561'2345)~", 8),
        str_to_int(UR"~(0o123755555123245612345)~", 8),
        str_to_int(UR"~(0xdeadbeafde123456789abcde)~", 16),
        str_to_int(UR"~(1111111110000111)~", 2),
        str_to_int(UR"~(1111111110000111101010001110001111000001111100000001111111110111111010101000001011110011110100001111110111111011010100101010101)~", 2),
        str_to_int(UR"~(10101)~", 2),
    };

    static const quat::quat_t<__float128> quat_vals[] = {
        {0.0q, 0.0q, 45760.91234567892345E-223q, 0.0q                      },
        {0.0q, 0.0q, 0.0q,                       45760.91234567892345E-223q},
        {0.0q, 0.0q, 0.618281828459045e+4q,      0.0q                      },
        {0.0q, 0.0q, 0.0q,                       6.141592653148e+8q        },
        {0.0q, 0.0q, 45.67891234567892345E-12q,  0.0q                      },
    };


    static const std::vector<arkona_scanner::Arkona_token> tokens_for_texts[] = {
        {
            // References for text #0:
            {{{2, 5  }, {2, 5  }}, id_lexeme(1)                                             }, // Id б
            {{{2, 20 }, {2, 22 }}, id_lexeme(3)                                             }, // Id бег
            {{{4, 13 }, {4, 15 }}, id_lexeme(5)                                             }, // Id бор
            {{{5, 18 }, {5, 19 }}, id_lexeme(2)                                             }, // Id бе
            {{{6, 9  }, {6, 10 }}, id_lexeme(4)                                             }, // Id бо
            {{{8, 1  }, {8, 5  }}, id_lexeme(8)                                             }, // Id белый
            {{{8, 11 }, {8, 20 }}, id_lexeme(16)                                            }, // Id безмолвный
            {{{8, 39 }, {8, 49 }}, id_lexeme(23)                                            }, // Id безмятежный
            {{{9, 9  }, {9, 13 }}, id_lexeme(27)                                            }, // Id бурый
            {{{10,17 }, {10,22 }}, id_lexeme(31)                                            }, // Id болото
            {{{10,23 }, {10,23 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #1:
            {{{1, 1  }, {1, 8  }}, id_lexeme(36)                                            }, // Id безликий
            {{{1, 21 }, {1, 31 }}, id_lexeme(44)                                            }, // Id беззнаковый
            {{{2, 9  }, {2, 17 }}, id_lexeme(48)                                            }, // Id беззн1024
            {{{2, 29 }, {2, 39 }}, id_lexeme(56)                                            }, // Id больной_кот
            {{{4, 13 }, {4, 25 }}, id_lexeme(65)                                            }, // Id большеголовый
            {{{6, 21 }, {6, 27 }}, keyword_lexeme(Keyword_kind::Kw_bezzn16)                 }, // Keyword Kw_bezzn16
            {{{6, 41 }, {6, 46 }}, keyword_lexeme(Keyword_kind::Kw_bezzn8)                  }, // Keyword Kw_bezzn8
            {{{7, 5  }, {7, 11 }}, keyword_lexeme(Keyword_kind::Kw_bezzn32)                 }, // Keyword Kw_bezzn32
            {{{7, 17 }, {7, 23 }}, keyword_lexeme(Keyword_kind::Kw_bezzn64)                 }, // Keyword Kw_bezzn64
            {{{7, 33 }, {7, 40 }}, keyword_lexeme(Keyword_kind::Kw_bezzn128)                }, // Keyword Kw_bezzn128
            {{{9, 13 }, {9, 19 }}, keyword_lexeme(Keyword_kind::Kw_bolshoe)                 }, // Keyword Kw_bolshoe
            {{{11,25 }, {11,29 }}, keyword_lexeme(Keyword_kind::Kw_bezzn)                   }, // Keyword Kw_bezzn
            {{{12,9  }, {12,14 }}, keyword_lexeme(Keyword_kind::Kw_bolshe)                  }, // Keyword Kw_bolshe
            {{{14,3  }, {14,22 }}, id_lexeme(85)                                            }, // Id __лунатики_прилетели
            {{{14,24 }, {14,41 }}, id_lexeme(102)                                           }, // Id _негр_литературный
            {{{14,42 }, {14,42 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #2:
            {{{1, 1  }, {1, 12 }}, id_lexeme(114)                                           }, // Id веский_довод
            {{{3, 17 }, {3, 17 }}, keyword_lexeme(Keyword_kind::Kw_v)                       }, // Keyword Kw_v
            {{{4, 9  }, {4, 13 }}, keyword_lexeme(Keyword_kind::Kw_vyjdi)                   }, // Keyword Kw_vyjdi
            {{{4, 23 }, {4, 25 }}, id_lexeme(116)                                           }, // Id вон
            {{{4, 34 }, {4, 39 }}, keyword_lexeme(Keyword_kind::Kw_vyberi)                  }, // Keyword Kw_vyberi
            {{{6, 17 }, {6, 27 }}, id_lexeme(126)                                           }, // Id выбери_меня
            {{{6, 37 }, {6, 49 }}, id_lexeme(137)                                           }, // Id выдели_цветом
            {{{8, 5  }, {8, 11 }}, keyword_lexeme(Keyword_kind::Kw_vozvrat)                 }, // Keyword Kw_vozvrat
            {{{8, 20 }, {8, 25 }}, keyword_lexeme(Keyword_kind::Kw_vydeli)                  }, // Keyword Kw_vydeli
            {{{10,13 }, {10,35 }}, id_lexeme(158)                                           }, // Id вещественных_чисел_поле
            {{{12,41 }, {12,43 }}, keyword_lexeme(Keyword_kind::Kw_veshch)                  }, // Keyword Kw_veshch
            {{{12,48 }, {12,52 }}, keyword_lexeme(Keyword_kind::Kw_vechno)                  }, // Keyword Kw_vechno
            {{{14,9  }, {14,19 }}, id_lexeme(167)                                           }, // Id вечный_кипр
            {{{14,23 }, {14,27 }}, keyword_lexeme(Keyword_kind::Kw_veshch80)                }, // Keyword Kw_veshch80
            {{{14,35 }, {14,40 }}, keyword_lexeme(Keyword_kind::Kw_veshch128)               }, // Keyword Kw_veshch128
            {{{14,46 }, {14,50 }}, keyword_lexeme(Keyword_kind::Kw_veshch32)                }, // Keyword Kw_veshch32
            {{{14,56 }, {14,60 }}, keyword_lexeme(Keyword_kind::Kw_veshch64)                }, // Keyword Kw_veshch64
            {{{14,65 }, {14,70 }}, id_lexeme(170)                                           }, // Id вещ256
            {{{14,71 }, {14,71 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #3:
            {{{1, 1  }, {1, 12 }}, id_lexeme(114)                                           }, // Id веский_довод
            {{{3, 17 }, {3, 17 }}, keyword_lexeme(Keyword_kind::Kw_v)                       }, // Keyword Kw_v
            {{{4, 9  }, {4, 13 }}, keyword_lexeme(Keyword_kind::Kw_vyjdi)                   }, // Keyword Kw_vyjdi
            {{{4, 23 }, {4, 25 }}, id_lexeme(116)                                           }, // Id вон
            {{{4, 34 }, {4, 39 }}, keyword_lexeme(Keyword_kind::Kw_vyberi)                  }, // Keyword Kw_vyberi
            {{{6, 17 }, {6, 27 }}, id_lexeme(126)                                           }, // Id выбери_меня
            {{{6, 37 }, {6, 49 }}, id_lexeme(137)                                           }, // Id выдели_цветом
            {{{8, 5  }, {8, 11 }}, keyword_lexeme(Keyword_kind::Kw_vozvrat)                 }, // Keyword Kw_vozvrat
            {{{8, 20 }, {8, 25 }}, keyword_lexeme(Keyword_kind::Kw_vydeli)                  }, // Keyword Kw_vydeli
            {{{11,1  }, {11,7  }}, id_lexeme(177)                                           }, // Id главный
            {{{11,9  }, {11,16 }}, keyword_lexeme(Keyword_kind::Kw_golovnaya)               }, // Keyword Kw_golovnaya
            {{{11,24 }, {11,27 }}, id_lexeme(180)                                           }, // Id грач
            {{{13,13 }, {13,35 }}, id_lexeme(158)                                           }, // Id вещественных_чисел_поле
            {{{15,13 }, {15,15 }}, keyword_lexeme(Keyword_kind::Kw_dlya)                    }, // Keyword Kw_dlya
            {{{15,21 }, {15,30 }}, id_lexeme(190)                                           }, // Id длительный
            {{{15,40 }, {15,54 }}, id_lexeme(204)                                           }, // Id деловая_колбаса
            {{{17,41 }, {17,43 }}, keyword_lexeme(Keyword_kind::Kw_veshch)                  }, // Keyword Kw_veshch
            {{{17,48 }, {17,52 }}, keyword_lexeme(Keyword_kind::Kw_vechno)                  }, // Keyword Kw_vechno
            {{{19,26 }, {19,29 }}, keyword_lexeme(Keyword_kind::Kw_esli)                    }, // Keyword Kw_esli
            {{{19,40 }, {19,43 }}, id_lexeme(208)                                           }, // Id есть
            {{{19,54 }, {19,70 }}, id_lexeme(224)                                           }, // Id ел_ем_и_буду_есть
            {{{21,9  }, {21,19 }}, id_lexeme(167)                                           }, // Id вечный_кипр
            {{{21,23 }, {21,27 }}, keyword_lexeme(Keyword_kind::Kw_veshch80)                }, // Keyword Kw_veshch80
            {{{21,35 }, {21,40 }}, keyword_lexeme(Keyword_kind::Kw_veshch128)               }, // Keyword Kw_veshch128
            {{{21,46 }, {21,50 }}, keyword_lexeme(Keyword_kind::Kw_veshch32)                }, // Keyword Kw_veshch32
            {{{21,56 }, {21,60 }}, keyword_lexeme(Keyword_kind::Kw_veshch64)                }, // Keyword Kw_veshch64
            {{{21,65 }, {21,70 }}, id_lexeme(170)                                           }, // Id вещ256
            {{{24,1  }, {24,2  }}, keyword_lexeme(Keyword_kind::Kw_iz)                      }, // Keyword Kw_iz
            {{{24,7  }, {24,10 }}, keyword_lexeme(Keyword_kind::Kw_ines)                    }, // Keyword Kw_ines
            {{{24,12 }, {24,16 }}, keyword_lexeme(Keyword_kind::Kw_inache)                  }, // Keyword Kw_inache
            {{{27,13 }, {27,17 }}, keyword_lexeme(Keyword_kind::Kw_konst)                   }, // Keyword Kw_konst
            {{{27,29 }, {27,31 }}, keyword_lexeme(Keyword_kind::Kw_kak)                     }, // Keyword Kw_kak
            {{{27,44 }, {27,48 }}, keyword_lexeme(Keyword_kind::Kw_kompl)                   }, // Keyword Kw_kompl
            {{{29,5  }, {29,8  }}, keyword_lexeme(Keyword_kind::Kw_kvat)                    }, // Keyword Kw_kvat
            {{{29,16 }, {29,22 }}, keyword_lexeme(Keyword_kind::Kw_kompl64)                 }, // Keyword Kw_kompl64
            {{{29,35 }, {29,40 }}, keyword_lexeme(Keyword_kind::Kw_kvat32)                  }, // Keyword Kw_kvat32
            {{{29,51 }, {29,57 }}, keyword_lexeme(Keyword_kind::Kw_kompl80)                 }, // Keyword Kw_kompl80
            {{{31,9  }, {31,14 }}, keyword_lexeme(Keyword_kind::Kw_kvat64)                  }, // Keyword Kw_kvat64
            {{{31,27 }, {31,34 }}, keyword_lexeme(Keyword_kind::Kw_kompl128)                }, // Keyword Kw_kompl128
            {{{31,44 }, {31,49 }}, keyword_lexeme(Keyword_kind::Kw_kvat80)                  }, // Keyword Kw_kvat80
            {{{31,63 }, {31,69 }}, keyword_lexeme(Keyword_kind::Kw_kompl32)                 }, // Keyword Kw_kompl32
            {{{33,17 }, {33,23 }}, keyword_lexeme(Keyword_kind::Kw_kvat128)                 }, // Keyword Kw_kvat128
            {{{35,3  }, {35,11 }}, keyword_lexeme(Keyword_kind::Kw_kategorija)              }, // Keyword Kw_kategorija
            {{{37,17 }, {37,36 }}, id_lexeme(244)                                           }, // Id комплексное_значение
            {{{37,42 }, {37,51 }}, id_lexeme(253)                                           }, // Id кватернион
            {{{37,62 }, {37,69 }}, id_lexeme(259)                                           }, // Id колонист
            {{{39,9  }, {39,20 }}, id_lexeme(271)                                           }, // Id исторический
            {{{39,29 }, {39,38 }}, keyword_lexeme(Keyword_kind::Kw_ispolzuet)               }, // Keyword Kw_ispolzuet
            {{{41,17 }, {41,24 }}, id_lexeme(277)                                           }, // Id исконный
            {{{41,32 }, {41,37 }}, keyword_lexeme(Keyword_kind::Kw_istina)                  }, // Keyword Kw_istina
            {{{41,49 }, {41,60 }}, id_lexeme(287)                                           }, // Id используемый
            {{{41,66 }, {41,73 }}, id_lexeme(292)                                           }, // Id истинный
            {{{43,13 }, {43,23 }}, id_lexeme(302)                                           }, // Id иностранный
            {{{47,5  }, {47,12 }}, id_lexeme(310)                                           }, // Id логарифм
            {{{47,20 }, {47,22 }}, keyword_lexeme(Keyword_kind::Kw_log)                     }, // Keyword Kw_log
            {{{47,32 }, {47,35 }}, keyword_lexeme(Keyword_kind::Kw_lozh)                    }, // Keyword Kw_lozh
            {{{47,45 }, {47,50 }}, id_lexeme(315)                                           }, // Id лживый
            {{{47,56 }, {47,65 }}, id_lexeme(322)                                           }, // Id логический
            {{{49,9  }, {49,15 }}, id_lexeme(328)                                           }, // Id лягушка
            {{{49,23 }, {49,28 }}, keyword_lexeme(Keyword_kind::Kw_lyambda)                 }, // Keyword Kw_lyambda
            {{{49,37 }, {49,41 }}, keyword_lexeme(Keyword_kind::Kw_log64)                   }, // Keyword Kw_log64
            {{{49,47 }, {49,54 }}, id_lexeme(335)                                           }, // Id линейный
            {{{51,13 }, {51,16 }}, keyword_lexeme(Keyword_kind::Kw_log8)                    }, // Keyword Kw_log8
            {{{51,31 }, {51,32 }}, id_lexeme(323)                                           }, // Id ля
            {{{51,44 }, {51,48 }}, keyword_lexeme(Keyword_kind::Kw_log32)                   }, // Keyword Kw_log32
            {{{51,59 }, {51,63 }}, keyword_lexeme(Keyword_kind::Kw_log16)                   }, // Keyword Kw_log16
            {{{51,64 }, {51,64 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #4:
            {{{3, 9  }, {3, 13 }}, id_lexeme(340)                                           }, // Id маляр
            {{{3, 21 }, {3, 29 }}, keyword_lexeme(Keyword_kind::Kw_malenkoe)                }, // Keyword Kw_malenkoe
            {{{3, 38 }, {3, 43 }}, keyword_lexeme(Keyword_kind::Kw_massiv)                  }, // Keyword Kw_massiv
            {{{5, 17 }, {5, 26 }}, id_lexeme(348)                                           }, // Id мандрагора
            {{{5, 33 }, {5, 37 }}, id_lexeme(351)                                           }, // Id маска
            {{{8, 5  }, {8, 8  }}, keyword_lexeme(Keyword_kind::Kw_meta)                    }, // Keyword Kw_meta
            {{{8, 22 }, {8, 26 }}, id_lexeme(355)                                           }, // Id мечта
            {{{8, 40 }, {8, 47 }}, id_lexeme(361)                                           }, // Id метафора
            {{{10,13 }, {10,18 }}, keyword_lexeme(Keyword_kind::Kw_menshe)                  }, // Keyword Kw_menshe
            {{{10,29 }, {10,39 }}, id_lexeme(370)                                           }, // Id меньшинство
            {{{10,52 }, {10,57 }}, keyword_lexeme(Keyword_kind::Kw_modul)                   }, // Keyword Kw_modul
            {{{14,9  }, {14,18 }}, id_lexeme(378)                                           }, // Id магический
            {{{16,21 }, {16,25 }}, keyword_lexeme(Keyword_kind::Kw_nichto)                  }, // Keyword Kw_nichto
            {{{16,31 }, {16,35 }}, id_lexeme(383)                                           }, // Id нечто
            {{{16,41 }, {16,49 }}, id_lexeme(391)                                           }, // Id начальный
            {{{16,58 }, {16,62 }}, id_lexeme(395)                                           }, // Id новый
            {{{21,13 }, {21,20 }}, keyword_lexeme(Keyword_kind::Kw_operaciya)               }, // Keyword Kw_operaciya
            {{{21,28 }, {21,35 }}, id_lexeme(403)                                           }, // Id оператор
            {{{21,45 }, {21,52 }}, keyword_lexeme(Keyword_kind::Kw_osvobodi)                }, // Keyword Kw_osvobodi
            {{{21,60 }, {21,67 }}, id_lexeme(410)                                           }, // Id крылатый
            {{{24,2  }, {24,5  }}, keyword_lexeme(Keyword_kind::Kw_pauk)                    }, // Keyword Kw_pauk
            {{{24,9  }, {24,14 }}, id_lexeme(414)                                           }, // Id опёнок
            {{{24,17 }, {24,43 }}, id_lexeme(441)                                           }, // Id поучайте_лучше_ваших_паучат
            {{{24,53 }, {24,57 }}, keyword_lexeme(Keyword_kind::Kw_paket)                   }, // Keyword Kw_paket
            {{{26,13 }, {26,29 }}, id_lexeme(457)                                           }, // Id пакетный_менеджер
            {{{26,47 }, {26,54 }}, keyword_lexeme(Keyword_kind::Kw_povtoryaj)               }, // Keyword Kw_povtoryaj
            {{{26,63 }, {26,72 }}, id_lexeme(465)                                           }, // Id повторение
            {{{28,3  }, {28,6  }}, keyword_lexeme(Keyword_kind::Kw_poka)                    }, // Keyword Kw_poka
            {{{28,15 }, {28,23 }}, id_lexeme(472)                                           }, // Id позволить
            {{{28,34 }, {28,43 }}, id_lexeme(480)                                           }, // Id показывать
            {{{28,49 }, {28,54 }}, keyword_lexeme(Keyword_kind::Kw_pokuda)                  }, // Keyword Kw_pokuda
            {{{28,65 }, {28,71 }}, keyword_lexeme(Keyword_kind::Kw_poryadok)                }, // Keyword Kw_poryadok
            {{{29,5  }, {29,27 }}, id_lexeme(501)                                           }, // Id порядковое_числительное
            {{{29,30 }, {29,38 }}, keyword_lexeme(Keyword_kind::Kw_poryadok64)              }, // Keyword Kw_poryadok64
            {{{29,42 }, {29,49 }}, keyword_lexeme(Keyword_kind::Kw_poryadok8)               }, // Keyword Kw_poryadok8
            {{{29,53 }, {29,61 }}, keyword_lexeme(Keyword_kind::Kw_poryadok16)              }, // Keyword Kw_poryadok16
            {{{29,67 }, {29,75 }}, keyword_lexeme(Keyword_kind::Kw_poryadok32)              }, // Keyword Kw_poryadok32
            {{{31,9  }, {31,19 }}, keyword_lexeme(Keyword_kind::Kw_postfiksnaya)            }, // Keyword Kw_postfiksnaya
            {{{31,26 }, {31,35 }}, keyword_lexeme(Keyword_kind::Kw_preobrazuj)              }, // Keyword Kw_preobrazuj
            {{{32,17 }, {32,26 }}, keyword_lexeme(Keyword_kind::Kw_prefiksnaya)             }, // Keyword Kw_prefiksnaya
            {{{34,13 }, {34,21 }}, keyword_lexeme(Keyword_kind::Kw_psevdonim)               }, // Keyword Kw_psevdonim
            {{{34,48 }, {34,52 }}, keyword_lexeme(Keyword_kind::Kw_pusto)                   }, // Keyword Kw_pusto
            {{{34,62 }, {34,68 }}, id_lexeme(507)                                           }, // Id пустота
            {{{34,72 }, {34,77 }}, id_lexeme(512)                                           }, // Id вакуум
            {{{34,78 }, {34,78 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #5:
            {{{2, 5  }, {2, 13 }}, id_lexeme(521)                                           }, // Id равенство
            {{{2, 18 }, {2, 22 }}, keyword_lexeme(Keyword_kind::Kw_ravno)                   }, // Keyword Kw_ravno
            {{{2, 28 }, {2, 35 }}, id_lexeme(527)                                           }, // Id разность
            {{{4, 9  }, {4, 14 }}, keyword_lexeme(Keyword_kind::Kw_razbor)                  }, // Keyword Kw_razbor
            {{{4, 19 }, {4, 30 }}, keyword_lexeme(Keyword_kind::Kw_rassmatrivaj)            }, // Keyword Kw_rassmatrivaj
            {{{4, 34 }, {4, 40 }}, id_lexeme(532)                                           }, // Id рассказ
            {{{6, 1  }, {6, 8  }}, keyword_lexeme(Keyword_kind::Kw_realizuj)                }, // Keyword Kw_realizuj
            {{{6, 21 }, {6, 30 }}, keyword_lexeme(Keyword_kind::Kw_realizacija)             }, // Keyword Kw_realizacija
            {{{6, 37 }, {6, 65 }}, id_lexeme(559)                                           }, // Id концепция_концепции_категории
            {{{9, 9  }, {9, 15 }}, id_lexeme(566)                                           }, // Id сослать
            {{{9, 24 }, {9, 36 }}, id_lexeme(578)                                           }, // Id ссылочный_тип
            {{{9, 41 }, {9, 46 }}, keyword_lexeme(Keyword_kind::Kw_ssylka)                  }, // Keyword Kw_ssylka
            {{{11,17 }, {11,23 }}, id_lexeme(584)                                           }, // Id самовар
            {{{11,28 }, {11,31 }}, keyword_lexeme(Keyword_kind::Kw_samo)                    }, // Keyword Kw_samo
            {{{11,40 }, {11,48 }}, keyword_lexeme(Keyword_kind::Kw_struktura)               }, // Keyword Kw_struktura
            {{{14,5  }, {14,12 }}, keyword_lexeme(Keyword_kind::Kw_stroka32)                }, // Keyword Kw_stroka32
            {{{14,20 }, {14,26 }}, keyword_lexeme(Keyword_kind::Kw_stroka8)                 }, // Keyword Kw_stroka8
            {{{14,42 }, {14,47 }}, keyword_lexeme(Keyword_kind::Kw_stroka)                  }, // Keyword Kw_stroka
            {{{14,59 }, {14,66 }}, keyword_lexeme(Keyword_kind::Kw_stroka16)                }, // Keyword Kw_stroka16
            {{{16,17 }, {16,25 }}, id_lexeme(592)                                           }, // Id сравнение
            {{{16,31 }, {16,38 }}, keyword_lexeme(Keyword_kind::Kw_sravni_s)                }, // Keyword Kw_sravni_s
            {{{16,53 }, {16,69 }}, id_lexeme(608)                                           }, // Id символьные_данные
            {{{18,9  }, {18,12 }}, keyword_lexeme(Keyword_kind::Kw_simv)                    }, // Keyword Kw_simv
            {{{18,22 }, {18,27 }}, keyword_lexeme(Keyword_kind::Kw_simv32)                  }, // Keyword Kw_simv32
            {{{18,42 }, {18,46 }}, keyword_lexeme(Keyword_kind::Kw_simv8)                   }, // Keyword Kw_simv8
            {{{20,17 }, {20,22 }}, keyword_lexeme(Keyword_kind::Kw_simv16)                  }, // Keyword Kw_simv16
            {{{24,1  }, {24,2  }}, keyword_lexeme(Keyword_kind::Kw_to)                      }, // Keyword Kw_to
            {{{24,10 }, {24,12 }}, keyword_lexeme(Keyword_kind::Kw_tip)                     }, // Keyword Kw_tip
            {{{24,15 }, {24,21 }}, keyword_lexeme(Keyword_kind::Kw_funktsiya)               }, // Keyword Kw_funktsiya
            {{{24,32 }, {24,43 }}, id_lexeme(620)                                           }, // Id трогательный
            {{{24,49 }, {24,55 }}, id_lexeme(626)                                           }, // Id типовой
            {{{26,5  }, {26,13 }}, id_lexeme(635)                                           }, // Id целостный
            {{{26,27 }, {26,31 }}, id_lexeme(637)                                           }, // Id целый
            {{{26,34 }, {26,36 }}, keyword_lexeme(Keyword_kind::Kw_tsel)                    }, // Keyword Kw_tsel
            {{{28,5  }, {28,10 }}, id_lexeme(643)                                           }, // Id феррит
            {{{28,20 }, {28,23 }}, id_lexeme(646)                                           }, // Id флот
            {{{28,29 }, {28,37 }}, id_lexeme(654)                                           }, // Id фумигатор
            {{{31,13 }, {31,18 }}, keyword_lexeme(Keyword_kind::Kw_tsel128)                 }, // Keyword Kw_tsel128
            {{{31,44 }, {31,47 }}, keyword_lexeme(Keyword_kind::Kw_tsel8)                   }, // Keyword Kw_tsel8
            {{{32,21 }, {32,25 }}, keyword_lexeme(Keyword_kind::Kw_tsel16)                  }, // Keyword Kw_tsel16
            {{{32,32 }, {32,36 }}, keyword_lexeme(Keyword_kind::Kw_tsel64)                  }, // Keyword Kw_tsel64
            {{{32,50 }, {32,54 }}, keyword_lexeme(Keyword_kind::Kw_tsel32)                  }, // Keyword Kw_tsel32
            {{{32,58 }, {32,63 }}, id_lexeme(660)                                           }, // Id честно
            {{{32,70 }, {32,75 }}, keyword_lexeme(Keyword_kind::Kw_chistaya)                }, // Keyword Kw_chistaya
            {{{34,5  }, {34,9  }}, keyword_lexeme(Keyword_kind::Kw_shkala)                  }, // Keyword Kw_shkala
            {{{34,16 }, {34,20 }}, keyword_lexeme(Keyword_kind::Kw_shkalu)                  }, // Keyword Kw_shkalu
            {{{34,45 }, {34,63 }}, id_lexeme(678)                                           }, // Id область_целостности
            {{{37,13 }, {37,24 }}, id_lexeme(690)                                           }, // Id элементарный
            {{{37,31 }, {37,37 }}, keyword_lexeme(Keyword_kind::Kw_element)                 }, // Keyword Kw_element
            {{{37,49 }, {37,64 }}, id_lexeme(705)                                           }, // Id экспортный_товар
            {{{37,73 }, {37,79 }}, keyword_lexeme(Keyword_kind::Kw_eksport)                 }, // Keyword Kw_eksport
            {{{38,17 }, {38,32 }}, id_lexeme(720)                                           }, // Id эспериментальный
            {{{38,41 }, {38,45 }}, keyword_lexeme(Keyword_kind::Kw_ekviv)                   }, // Keyword Kw_ekviv
            {{{38,53 }, {38,69 }}, id_lexeme(735)                                           }, // Id эквипотенциальный
            {{{38,70 }, {38,70 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #6:
            {{{1, 1  }, {1, 4  }}, char_lexeme(U'\'')                                       }, // Char \'
            {{{3, 9  }, {3, 11 }}, char_lexeme(U'Ϣ')                                        }, // Char 'Ϣ'
            {{{3, 15 }, {3, 17 }}, char_lexeme(U'ϣ')                                        }, // Char 'ϣ'
            {{{3, 30 }, {3, 32 }}, char_lexeme(U'Ϥ')                                        }, // Char 'Ϥ'
            {{{3, 34 }, {3, 36 }}, char_lexeme(U'ϥ')                                        }, // Char 'ϥ'
            {{{5, 5  }, {5, 7  }}, char_lexeme(U'Ϧ')                                        }, // Char 'Ϧ'
            {{{5, 10 }, {5, 12 }}, char_lexeme(U'ϧ')                                        }, // Char 'ϧ'
            {{{5, 16 }, {5, 18 }}, char_lexeme(U'Ϩ')                                        }, // Char 'Ϩ'
            {{{5, 24 }, {5, 26 }}, char_lexeme(U'ϩ')                                        }, // Char 'ϩ'
            {{{5, 31 }, {5, 33 }}, char_lexeme(U'Ϫ')                                        }, // Char 'Ϫ'
            {{{5, 38 }, {5, 40 }}, char_lexeme(U'ϫ')                                        }, // Char 'ϫ'
            {{{8, 13 }, {8, 15 }}, char_lexeme(U'Ϭ')                                        }, // Char 'Ϭ'
            {{{8, 18 }, {8, 20 }}, char_lexeme(U'ϭ')                                        }, // Char 'ϭ'
            {{{8, 23 }, {8, 25 }}, char_lexeme(U'Ϯ')                                        }, // Char 'Ϯ'
            {{{8, 30 }, {8, 32 }}, char_lexeme(U'ϯ')                                        }, // Char 'ϯ'
            {{{8, 36 }, {8, 38 }}, char_lexeme(U'ϰ')                                        }, // Char 'ϰ'
            {{{8, 41 }, {8, 43 }}, char_lexeme(U'ϱ')                                        }, // Char 'ϱ'
            {{{8, 47 }, {8, 49 }}, char_lexeme(U'ϲ')                                        }, // Char 'ϲ'
            {{{8, 53 }, {8, 55 }}, char_lexeme(U'ϳ')                                        }, // Char 'ϳ'
            {{{8, 59 }, {8, 61 }}, char_lexeme(U'ϴ')                                        }, // Char 'ϴ'
            {{{8, 65 }, {8, 67 }}, char_lexeme(U'ϵ')                                        }, // Char 'ϵ'
            {{{8, 70 }, {8, 72 }}, char_lexeme(U'϶')                                        }, // Char '϶'
            {{{11,17 }, {11,19 }}, char_lexeme(U'Ϸ')                                        }, // Char 'Ϸ'
            {{{11,23 }, {11,25 }}, char_lexeme(U'ϸ')                                        }, // Char 'ϸ'
            {{{11,28 }, {11,30 }}, char_lexeme(U'Ϲ')                                        }, // Char 'Ϲ'
            {{{11,33 }, {11,35 }}, char_lexeme(U'Ϻ')                                        }, // Char 'Ϻ'
            {{{11,38 }, {11,40 }}, char_lexeme(U'ϻ')                                        }, // Char 'ϻ'
            {{{11,43 }, {11,45 }}, char_lexeme(U'ϼ')                                        }, // Char 'ϼ'
            {{{11,48 }, {11,50 }}, char_lexeme(U'Ͻ')                                        }, // Char 'Ͻ'
            {{{11,53 }, {11,55 }}, char_lexeme(U'Ͼ')                                        }, // Char 'Ͼ'
            {{{11,58 }, {11,60 }}, char_lexeme(U'Ͽ')                                        }, // Char 'Ͽ'
            {{{11,63 }, {11,65 }}, char_lexeme(U'Ѐ')                                        }, // Char 'Ѐ'
            {{{11,68 }, {11,70 }}, char_lexeme(U'Ё')                                        }, // Char 'Ё'
            {{{11,76 }, {11,78 }}, char_lexeme(U'Ђ')                                        }, // Char 'Ђ'
            {{{11,82 }, {11,84 }}, char_lexeme(U'Ѓ')                                        }, // Char 'Ѓ'
            {{{11,88 }, {11,90 }}, char_lexeme(U'Є')                                        }, // Char 'Є'
            {{{11,94 }, {11,96 }}, char_lexeme(U'Ѕ')                                        }, // Char 'Ѕ'
            {{{13,3  }, {13,5  }}, char_lexeme(U'І')                                        }, // Char 'І'
            {{{13,9  }, {13,11 }}, char_lexeme(U'Ї')                                        }, // Char 'Ї'
            {{{13,15 }, {13,17 }}, char_lexeme(U'Ј')                                        }, // Char 'Ј'
            {{{13,20 }, {13,22 }}, char_lexeme(U'Љ')                                        }, // Char 'Љ'
            {{{13,26 }, {13,28 }}, char_lexeme(U'Њ')                                        }, // Char 'Њ'
            {{{13,32 }, {13,34 }}, char_lexeme(U'Ћ')                                        }, // Char 'Ћ'
            {{{13,38 }, {13,40 }}, char_lexeme(U'Ќ')                                        }, // Char 'Ќ'
            {{{13,43 }, {13,45 }}, char_lexeme(U'Ѝ')                                        }, // Char 'Ѝ'
            {{{13,48 }, {13,50 }}, char_lexeme(U'Ў')                                        }, // Char 'Ў'
            {{{13,53 }, {13,55 }}, char_lexeme(U'Џ')                                        }, // Char 'Џ'
            {{{13,58 }, {13,60 }}, char_lexeme(U'А')                                        }, // Char 'А'
            {{{13,63 }, {13,65 }}, char_lexeme(U'Б')                                        }, // Char 'Б'
            {{{13,68 }, {13,70 }}, char_lexeme(U'В')                                        }, // Char 'В'
            {{{13,73 }, {13,75 }}, char_lexeme(U'Г')                                        }, // Char 'Г'
            {{{13,79 }, {13,81 }}, char_lexeme(U'Д')                                        }, // Char 'Д'
            {{{13,86 }, {13,88 }}, char_lexeme(U'Е')                                        }, // Char 'Е'
            {{{13,94 }, {13,96 }}, char_lexeme(U'Ж')                                        }, // Char 'Ж'
            {{{13,102}, {13,104}}, char_lexeme(U'З')                                        }, // Char 'З'
            {{{13,109}, {13,111}}, char_lexeme(U'И')                                        }, // Char 'И'
            {{{15,13 }, {15,15 }}, char_lexeme(U'Й')                                        }, // Char 'Й'
            {{{15,19 }, {15,21 }}, char_lexeme(U'К')                                        }, // Char 'К'
            {{{15,29 }, {15,31 }}, char_lexeme(U'Л')                                        }, // Char 'Л'
            {{{15,36 }, {15,38 }}, char_lexeme(U'М')                                        }, // Char 'М'
            {{{15,46 }, {15,48 }}, char_lexeme(U'Н')                                        }, // Char 'Н'
            {{{18,24 }, {18,26 }}, char_lexeme(U'О')                                        }, // Char 'О'
            {{{18,33 }, {18,35 }}, char_lexeme(U'џ')                                        }, // Char 'џ'
            {{{18,39 }, {18,41 }}, char_lexeme(U'Ѡ')                                        }, // Char 'Ѡ'
            {{{18,52 }, {18,54 }}, char_lexeme(U'ѡ')                                        }, // Char 'ѡ'
            {{{18,60 }, {18,62 }}, char_lexeme(U'Ѣ')                                        }, // Char 'Ѣ'
            {{{18,74 }, {18,76 }}, char_lexeme(U'ѣ')                                        }, // Char 'ѣ'
            {{{20,11 }, {20,13 }}, char_lexeme(U'Ѥ')                                        }, // Char 'Ѥ'
            {{{20,29 }, {20,31 }}, char_lexeme(U'ѥ')                                        }, // Char 'ѥ'
            {{{20,44 }, {20,46 }}, char_lexeme(U'Ѧ')                                        }, // Char 'Ѧ'
            {{{20,58 }, {20,60 }}, char_lexeme(U'ѧ')                                        }, // Char 'ѧ'
            {{{20,67 }, {20,69 }}, char_lexeme(U'Ѩ')                                        }, // Char 'Ѩ'
            {{{20,80 }, {20,82 }}, char_lexeme(U'ѩ')                                        }, // Char 'ѩ'
            {{{22,7  }, {22,9  }}, char_lexeme(U'Ѫ')                                        }, // Char 'Ѫ'
            {{{22,19 }, {22,21 }}, char_lexeme(U'ѫ')                                        }, // Char 'ѫ'
            {{{22,38 }, {22,40 }}, char_lexeme(U'Ѭ')                                        }, // Char 'Ѭ'
            {{{22,54 }, {22,56 }}, char_lexeme(U'ѭ')                                        }, // Char 'ѭ'
            {{{22,64 }, {22,66 }}, char_lexeme(U'Ѯ')                                        }, // Char 'Ѯ'
            {{{22,73 }, {22,75 }}, char_lexeme(U'ѯ')                                        }, // Char 'ѯ'
            {{{24,13 }, {24,15 }}, char_lexeme(U'Ѱ')                                        }, // Char 'Ѱ'
            {{{24,24 }, {24,26 }}, char_lexeme(U'ѱ')                                        }, // Char 'ѱ'
            {{{24,31 }, {24,33 }}, char_lexeme(U'Ѳ')                                        }, // Char 'Ѳ'
            {{{24,42 }, {24,44 }}, char_lexeme(U'ѳ')                                        }, // Char 'ѳ'
            {{{24,49 }, {24,51 }}, char_lexeme(U'Ѵ')                                        }, // Char 'Ѵ'
            {{{24,59 }, {24,61 }}, char_lexeme(U'ѵ')                                        }, // Char 'ѵ'
            {{{24,69 }, {24,71 }}, char_lexeme(U'Ѷ')                                        }, // Char 'Ѷ'
            {{{24,75 }, {24,77 }}, char_lexeme(U'"')                                        }, // Char '"'
            {{{24,78 }, {24,78 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #7:
            {{{1, 5  }, {1, 8  }}, encoded_char_lexeme(U'{')                                }, // Encoded_char '{'
            {{{2, 11 }, {2, 17 }}, encoded_char_lexeme(U'Ё')                                }, // Encoded_char 'Ё'
            {{{2, 22 }, {2, 26 }}, encoded_char_lexeme(U'A')                                }, // Encoded_char 'A' (Latin)
            {{{4, 13 }, {4, 18 }}, encoded_char_lexeme(U'C')                                }, // Encoded_char 'C' (Latin)
            {{{6, 16 }, {6, 26 }}, encoded_char_lexeme(U'B')                                }, // Encoded_char 'B' (Latin)
            {{{6, 27 }, {6, 27 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #8:
            {{{1, 3  }, {1, 6  }}, encoded_char_lexeme(U'{')                                }, // Encoded_char '{'
            {{{3, 1  }, {3, 5  }}, encoded_char_lexeme(U'Ё')                                }, // Encoded_char 'Ё'
            {{{4, 7  }, {4, 12 }}, encoded_char_lexeme(U'Ё')                                }, // Encoded_char 'Ё'
            {{{5, 13 }, {5, 21 }}, encoded_char_lexeme(U'Ѭ')                                }, // Encoded_char 'Ѭ'
            {{{6, 17 }, {6, 23 }}, encoded_char_lexeme(U'Ӟ')                                }, // Encoded_char 'Ӟ'
            {{{6, 27 }, {6, 43 }}, encoded_char_lexeme(U'Ƣ')                                }, // Encoded_char 'Ƣ'
            {{{8, 21 }, {8, 27 }}, encoded_char_lexeme(U'Ϧ')                                }, // Encoded_char 'Ϧ'
            {{{8, 30 }, {8, 38 }}, encoded_char_lexeme(U'ϥ')                                }, // Encoded_char 'ϥ'
            {{{8, 39 }, {8, 39 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #9:
#if defined(_WIN32) || defined(_WIN64)
#   define HERR_MANNELIG 142
#   define POPE_AND_MAIDEN_LEN 1494
#   define POPE_AND_MAIDEN (HERR_MANNELIG + POPE_AND_MAIDEN_LEN)
#   define POPE_AND_DOG_LEN 20
#   define POPE_AND_DOG ((POPE_AND_MAIDEN) + POPE_AND_DOG_LEN)
#else
#   define HERR_MANNELIG 139
#   define POPE_AND_MAIDEN_LEN (1494 - 14)
#   define POPE_AND_MAIDEN (HERR_MANNELIG + POPE_AND_MAIDEN_LEN)
#   define POPE_AND_DOG_LEN 20
#   define POPE_AND_DOG ((POPE_AND_MAIDEN) + POPE_AND_DOG_LEN)
#endif
            {{{3, 15 }, {6, 31 }}, string_lexeme(HERR_MANNELIG)                             }, // String "Bettida..."
            {{{8, 13 }, {22,129}}, string_lexeme(POPE_AND_MAIDEN)                           }, // String "Тебе, девка, житье..."
            {{{25,1  }, {25,24 }}, string_lexeme(POPE_AND_DOG)                              }, // String """У попа была собака..."
            {{{25,25 }, {25,25 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #10:
            {{{2, 12 }, {2, 21 }}, integer_lexeme(int_vals[0])                              }, // Integer 0123'45689
            {{{3, 26 }, {3, 56 }}, integer_lexeme(int_vals[1])                              }, // Integer 302'231'454'903'657'293'676'544
            {{{3, 66 }, {3, 104}}, integer_lexeme(int_vals[2])                              }, // Integer 158'456'325'028'528'675'187'087'900'672
            {{{7, 8  }, {7, 56 }}, integer_lexeme(int_vals[3])                              }, // Integer 1'329'227'995'784'915'872'903'807'060'280'344'576
            {{{8, 19 }, {8, 69 }}, integer_lexeme(int_vals[4])                              }, // Integer 340'282'366'920'938'463'463'374'607'431'768'211'455
            {{{10,19 }, {10,57 }}, integer_lexeme(int_vals[5])                              }, // Integer 340282366920938463463374607431768211455
            {{{14,2  }, {14,39 }}, integer_lexeme(int_vals[6])                              }, // Integer 0xffff'ffff'ffffff'ffffffffffffff'ffff
            {{{16,17 }, {16,28 }}, integer_lexeme(int_vals[7])                              }, // Integer 0x1238912345
            {{{16,48 }, {16,75 }}, integer_lexeme(int_vals[8])                              }, // Integer 0o123'755'5551'232'4561'2345
            {{{16,83 }, {16,105}}, integer_lexeme(int_vals[9])                              }, // Integer 0o123755555123245612345
            {{{20,9  }, {20,34 }}, integer_lexeme(int_vals[10])                             }, // Integer 0xdeadbeafde123456789abcde
            {{{22,1  }, {22,18 }}, integer_lexeme(int_vals[11])                             }, // Integer 0b1111111110000111
            {{{24,28 }, {24,156}}, integer_lexeme(int_vals[12])                             }, // Integer 0b1111111110000111101010001110001111000001111100000001111111110111111010101000001011110011110100001111110111111011010100101010101
            {{{26,28 }, {26,34 }}, integer_lexeme(int_vals[13])                             }, // Integer 0b10101
            {{{26,35 }, {26,35 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #11:
            {{{2, 11 }, {2, 34 }}, float_lexeme(3.1415926535897932384626q)                  }, // Float 3.1415926535897932384626
            {{{2, 35 }, {2, 35 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #12:
            {{{3, 13 }, {3, 40 }}, float_lexeme(45678.91234567892345E-12q)                  }, // Float 45678.91234567892345E-12
            {{{3, 52 }, {3, 62 }}, float_lexeme(3.45678e+35q)                               }, // Float 3.45678e+35
            {{{6, 9  }, {6, 22 }}, float_lexeme(5141.592653e+8q)                            }, // Float 5141.592653e+8
            {{{6, 26 }, {6, 43 }}, float_lexeme(6.141592653148e+8q, Float_kind::Float32)    }, // Float 6.141592653148e+8
            {{{9, 13 }, {9, 28 }}, float_lexeme(3.141592653e-78q, Float_kind::Float128)     }, // Float 3.141592653e-78
            {{{9, 31 }, {9, 48 }}, float_lexeme(9141.59265356e-78q, Float_kind::Float128)   }, // Float 9141.59265356e-78
            {{{9, 51 }, {9, 66 }}, float_lexeme(2.141592653e+11q, Float_kind::Float64)      }, // Float 2.141592653e+11
            {{{12,7  }, {12,28 }}, float_lexeme(71415926537.182818e-8q)                     }, // Float 71415926537.182818e-8
            {{{12,34 }, {12,51 }}, float_lexeme(6.141592653148e+8q, Float_kind::Float32)    }, // Float 6.141592653148e+8
            {{{15,9  }, {15,38 }}, float_lexeme(4'5.678912'34'567'8923'45E-1'2q)            }, // Float 45.67891234567892345E-12
            {{{15,57 }, {15,86 }}, complex_lexeme({0.0q, 45760.91234567892345E-223q})       }, // Complex 45760.912'34'567'8923'45E-223i
            {{{16,17 }, {16,46 }}, quat_lexeme(quat_vals[0])                                }, // Quat 45760.912'34'567'8923'45E-223j
            {{{16,51 }, {16,80 }}, quat_lexeme(quat_vals[1])                                }, // Quat 45760.912'34'567'8923'45E-223k
            {{{19,11 }, {19,31 }}, float_lexeme(0.618281828459045e-4q, Float_kind::Float80) }, // Float 0.618281828459045e-4
            {{{19,36 }, {19,56 }}, float_lexeme(0.618281828459045e+4q, Float_kind::Float80) }, // Float 0.618281828459045e+4
            {{{19,61 }, {19,82 }}, quat_lexeme(quat_vals[2], Quat_kind::Quat80)             }, // Quat 0.618281828459045e+4j
            {{{20,17 }, {20,35 }}, quat_lexeme(quat_vals[3], Quat_kind::Quat32)             }, // Quat 6.141592653148e+8k
            {{{20,37 }, {20,68 }}, quat_lexeme(quat_vals[4], Quat_kind::Quat128)            }, // Quat 45.67891234567892345E-12j
            {{{20,70 }, {20,72 }}, float_lexeme(0.6q)                                       }, // Float 0.6
            {{{20,73 }, {20,73 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #13:
            {{{1, 1  }, {1, 1  }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{1, 8  }, {1, 8  }}, delim_lexeme(Delimiter_kind::Logical_not)                }, // Delimiter Logical_not
            {{{2, 13 }, {2, 14 }}, delim_lexeme(Delimiter_kind::Maybe_logical_and_not)      }, // Delimiter Maybe_logical_and_not
            {{{2, 32 }, {2, 33 }}, delim_lexeme(Delimiter_kind::NEQ)                        }, // Delimiter NEQ
            {{{3, 5  }, {3, 7  }}, delim_lexeme(Delimiter_kind::Logical_and_not)            }, // Delimiter Logical_and_not
            {{{4, 17 }, {4, 18 }}, delim_lexeme(Delimiter_kind::Maybe_logical_or_not)       }, // Delimiter Maybe_logical_or_not
            {{{4, 29 }, {4, 31 }}, delim_lexeme(Delimiter_kind::Logical_or_not)             }, // Delimiter Logical_or_not
            {{{4, 41 }, {4, 41 }}, delim_lexeme(Delimiter_kind::Sharp)                      }, // Delimiter Sharp
            {{{4, 46 }, {4, 47 }}, delim_lexeme(Delimiter_kind::Right_arrow)                }, // Delimiter Right_arrow
            {{{5, 9  }, {5, 12 }}, delim_lexeme(Delimiter_kind::Logical_and_not_full)       }, // Delimiter Logical_and_not_full
            {{{5, 21 }, {5, 24 }}, delim_lexeme(Delimiter_kind::Logical_or_not_full)        }, // Delimiter Logical_or_not_full
            {{{6, 15 }, {6, 16 }}, delim_lexeme(Delimiter_kind::Data_size)                  }, // Delimiter Data_size
            {{{6, 32 }, {6, 32 }}, delim_lexeme(Delimiter_kind::Remainder)                  }, // Delimiter Remainder
            {{{7, 13 }, {7, 16 }}, delim_lexeme(Delimiter_kind::Logical_and_not_assign)     }, // Delimiter Logical_and_not_assign
            {{{7, 29 }, {7, 32 }}, delim_lexeme(Delimiter_kind::Logical_or_not_assign)      }, // Delimiter Logical_or_not_assign
            {{{8, 24 }, {8, 25 }}, delim_lexeme(Delimiter_kind::Float_remainder)            }, // Delimiter Float_remainder
            {{{9, 37 }, {9, 41 }}, delim_lexeme(Delimiter_kind::Logical_and_not_full_assign)}, // Delimiter Logical_and_not_full_assign
            {{{9, 50 }, {9, 51 }}, delim_lexeme(Delimiter_kind::Minus_assign)               }, // Delimiter Minus_assign
            {{{11,11 }, {11,13 }}, delim_lexeme(Delimiter_kind::Wrap_dec)                   }, // Delimiter Wrap_dec
            {{{12,21 }, {12,25 }}, delim_lexeme(Delimiter_kind::Logical_or_not_full_assign) }, // Delimiter Logical_or_not_full_assign
            {{{14,3  }, {14,4  }}, delim_lexeme(Delimiter_kind::Remainder_assign)           }, // Delimiter Remainder_assign
            {{{14,21 }, {14,23 }}, delim_lexeme(Delimiter_kind::Float_remainder_assign)     }, // Delimiter Float_remainder_assign
            {{{16,7  }, {16,8  }}, delim_lexeme(Delimiter_kind::Dec)                        }, // Delimiter Dec
            {{{16,33 }, {16,33 }}, delim_lexeme(Delimiter_kind::Minus)                      }, // Delimiter Minus
            {{{19,11 }, {19,14 }}, delim_lexeme(Delimiter_kind::Logical_and_full_assign)    }, // Delimiter Logical_and_full_assign
            {{{19,32 }, {19,32 }}, delim_lexeme(Delimiter_kind::Bitwise_and)                }, // Delimiter Bitwise_and
            {{{19,42 }, {19,43 }}, delim_lexeme(Delimiter_kind::Tuple_begin)                }, // Delimiter Tuple_begin
            {{{19,49 }, {19,49 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened
            {{{20,19 }, {20,20 }}, delim_lexeme(Delimiter_kind::Logical_and)                }, // Delimiter Logical_and
            {{{20,37 }, {20,38 }}, delim_lexeme(Delimiter_kind::Bitwise_and_assign)         }, // Delimiter Bitwise_and_assign
            {{{21,4  }, {21,6  }}, delim_lexeme(Delimiter_kind::Logical_and_full)           }, // Delimiter Logical_and_full
            {{{21,14 }, {21,16 }}, delim_lexeme(Delimiter_kind::Logical_and_assign)         }, // Delimiter Logical_and_assign
            {{{21,27 }, {21,27 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed
            {{{24,1  }, {24,2  }}, delim_lexeme(Delimiter_kind::Tuple_end)                  }, // Delimiter Tuple_end
            {{{24,4  }, {24,4  }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{24,6  }, {24,7  }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
            {{{24,9  }, {24,10 }}, delim_lexeme(Delimiter_kind::Copy)                       }, // Delimiter Copy
            {{{25,5  }, {25,6  }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br
            {{{25,11 }, {25,12 }}, delim_lexeme(Delimiter_kind::Colon_fig_br_closed)        }, // Delimiter Colon_fig_br_closed
            {{{25,18 }, {25,19 }}, delim_lexeme(Delimiter_kind::Colon_sq_br_closed)         }, // Delimiter Colon_sq_br_closed
            {{{27,4  }, {27,5  }}, delim_lexeme(Delimiter_kind::Comment_begin)              }, // Delimiter Comment_begin
            {{{27,9  }, {27,10 }}, delim_lexeme(Delimiter_kind::Div_assign)                 }, // Delimiter Div_assign
            {{{27,13 }, {27,15 }}, delim_lexeme(Delimiter_kind::Symmetric_difference_assign)}, // Delimiter Symmetric_difference_assign
            {{{27,18 }, {27,19 }}, delim_lexeme(Delimiter_kind::Symmetric_difference)       }, // Delimiter Symmetric_difference
            {{{27,23 }, {27,23 }}, delim_lexeme(Delimiter_kind::Div)                        }, // Delimiter Div
            {{{29,1  }, {29,1  }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma
            {{{29,5  }, {29,5  }}, delim_lexeme(Delimiter_kind::Sq_br_closed)               }, // Delimiter Sq_br_closed
            {{{30,9  }, {30,10 }}, delim_lexeme(Delimiter_kind::Comment_end)                }, // Delimiter Comment_end
            {{{30,13 }, {30,14 }}, delim_lexeme(Delimiter_kind::Power)                      }, // Delimiter Power
            {{{30,18 }, {30,18 }}, delim_lexeme(Delimiter_kind::Mul)                        }, // Delimiter Mul
            {{{31,25 }, {31,27 }}, delim_lexeme(Delimiter_kind::Float_power)                }, // Delimiter Float_power
            {{{31,31 }, {31,32 }}, delim_lexeme(Delimiter_kind::Mul_assign)                 }, // Delimiter Mul_assign
            {{{31,41 }, {31,43 }}, delim_lexeme(Delimiter_kind::Power_assign)               }, // Delimiter Power_assign
            {{{32,17 }, {32,20 }}, delim_lexeme(Delimiter_kind::Float_power_assign)         }, // Delimiter Float_power_assign
            {{{32,34 }, {32,34 }}, delim_lexeme(Delimiter_kind::Plus)                       }, // Delimiter Plus
            {{{32,56 }, {32,57 }}, delim_lexeme(Delimiter_kind::Inc)                        }, // Delimiter Inc
            {{{35,7  }, {35,8  }}, delim_lexeme(Delimiter_kind::Plus_assign)                }, // Delimiter Plus_assign
            {{{35,23 }, {35,25 }}, delim_lexeme(Delimiter_kind::Wrap_inc)                   }, // Delimiter Wrap_inc
            {{{37,16 }, {37,16 }}, delim_lexeme(Delimiter_kind::Point)                      }, // Delimiter Point
            {{{37,27 }, {37,28 }}, delim_lexeme(Delimiter_kind::Range)                      }, // Delimiter Range
            {{{37,42 }, {37,43 }}, delim_lexeme(Delimiter_kind::Maybe_algebraic_sep)        }, // Delimiter Maybe_algebraic_sep
            {{{37,53 }, {37,55 }}, delim_lexeme(Delimiter_kind::Algebraic_sep)              }, // Delimiter Algebraic_sep
            {{{40,18 }, {40,19 }}, delim_lexeme(Delimiter_kind::EQ)                         }, // Delimiter EQ
            {{{40,29 }, {40,29 }}, delim_lexeme(Delimiter_kind::Assign)                     }, // Delimiter Assign
            {{{42,3  }, {42,3  }}, delim_lexeme(Delimiter_kind::LT)                         }, // Delimiter LT
            {{{42,5  }, {42,6  }}, delim_lexeme(Delimiter_kind::Maybe_non_specific)         }, // Delimiter Maybe_non_specific
            {{{42,8  }, {42,9  }}, delim_lexeme(Delimiter_kind::Maybe_address)              }, // Delimiter Maybe_address
            {{{42,18 }, {42,19 }}, delim_lexeme(Delimiter_kind::Maybe_meta_plus)            }, // Delimiter Maybe_meta_plus
            {{{42,30 }, {42,31 }}, delim_lexeme(Delimiter_kind::Left_arrow)                 }, // Delimiter Left_arrow
            {{{42,43 }, {42,44 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{43,12 }, {43,13 }}, delim_lexeme(Delimiter_kind::Left_shift)                 }, // Delimiter Left_shift
            {{{43,23 }, {43,24 }}, delim_lexeme(Delimiter_kind::LEQ)                        }, // Delimiter LEQ
            {{{43,36 }, {43,37 }}, delim_lexeme(Delimiter_kind::Maybe_ElemType)             }, // Delimiter Maybe_ElemType
            {{{43,58 }, {43,59 }}, delim_lexeme(Delimiter_kind::Cycle_name_prefix)          }, // Delimiter Cycle_name_prefix
            {{{46,7  }, {46,9  }}, delim_lexeme(Delimiter_kind::Non_specific)               }, // Delimiter Non_specific
            {{{46,11 }, {46,13 }}, delim_lexeme(Delimiter_kind::Maybe_data_address)         }, // Delimiter Maybe_data_address
            {{{46,16 }, {46,18 }}, delim_lexeme(Delimiter_kind::Address)                    }, // Delimiter Address
            {{{46,24 }, {46,26 }}, delim_lexeme(Delimiter_kind::Meta_plus)                  }, // Delimiter Meta_plus
            {{{46,32 }, {46,35 }}, delim_lexeme(Delimiter_kind::Data_address)               }, // Delimiter Data_address
            {{{46,46 }, {46,49 }}, delim_lexeme(Delimiter_kind::Meta_plus_assign)           }, // Delimiter Meta_plus_assign
            {{{48,10 }, {48,12 }}, delim_lexeme(Delimiter_kind::Deduce_arg_type)            }, // Delimiter Deduce_arg_type
            {{{48,28 }, {48,30 }}, delim_lexeme(Delimiter_kind::Left_shift_assign)          }, // Delimiter Left_shift_assign
            {{{48,42 }, {48,44 }}, delim_lexeme(Delimiter_kind::Maybe_expr_type)            }, // Delimiter Maybe_expr_type
            {{{48,46 }, {48,48 }}, delim_lexeme(Delimiter_kind::ElemType)                   }, // Delimiter ElemType
            {{{48,52 }, {48,55 }}, delim_lexeme(Delimiter_kind::ExprType)                   }, // Delimiter ExprType
            {{{50,3  }, {50,3  }}, delim_lexeme(Delimiter_kind::Set_difference)             }, // Delimiter Set_difference
            {{{50,10 }, {50,11 }}, delim_lexeme(Delimiter_kind::Colon_sq_br_opened)         }, // Delimiter Colon_sq_br_opened
            {{{50,17 }, {50,18 }}, delim_lexeme(Delimiter_kind::Set_difference_assign)      }, // Delimiter Set_difference_assign
            {{{50,27 }, {50,27 }}, delim_lexeme(Delimiter_kind::Sq_br_opened)               }, // Delimiter Sq_br_opened
            {{{51,1  }, {51,1  }}, delim_lexeme(Delimiter_kind::At)                         }, // Delimiter At
            {{{51,6  }, {51,7  }}, delim_lexeme(Delimiter_kind::Logical_xor)                }, // Delimiter Logical_xor
            {{{51,14 }, {51,15 }}, delim_lexeme(Delimiter_kind::Bitwise_xor_assign)         }, // Delimiter Bitwise_xor_assign
            {{{51,29 }, {51,31 }}, delim_lexeme(Delimiter_kind::Logical_xor_assign)         }, // Delimiter Logical_xor_assign
            {{{51,36 }, {51,36 }}, delim_lexeme(Delimiter_kind::Bitwise_xor)                }, // Delimiter Bitwise_xor
            {{{53,6  }, {53,6  }}, delim_lexeme(Delimiter_kind::Bitwise_or)                 }, // Delimiter Bitwise_or
            {{{53,9  }, {53,10 }}, delim_lexeme(Delimiter_kind::Maybe_card)                 }, // Delimiter Maybe_card
            {{{53,21 }, {53,23 }}, delim_lexeme(Delimiter_kind::Card)                       }, // Delimiter Card
            {{{53,31 }, {53,32 }}, delim_lexeme(Delimiter_kind::Bitwise_or_assign)          }, // Delimiter Bitwise_or_assign
            {{{54,12 }, {54,13 }}, delim_lexeme(Delimiter_kind::Logical_or)                 }, // Delimiter Logical_or
            {{{54,17 }, {54,19 }}, delim_lexeme(Delimiter_kind::Logical_or_full)            }, // Delimiter Logical_or_full
            {{{54,25 }, {54,27 }}, delim_lexeme(Delimiter_kind::Logical_or_assign)          }, // Delimiter Logical_or_assign
            {{{54,36 }, {54,39 }}, delim_lexeme(Delimiter_kind::Logical_or_full_assign)     }, // Delimiter Logical_or_full_assign
            {{{56,3  }, {56,6  }}, delim_lexeme(Delimiter_kind::Pattern)                    }, // Delimiter Pattern
            {{{56,8  }, {56,8  }}, delim_lexeme(Delimiter_kind::Fig_br_opened)              }, // Delimiter Fig_br_opened
            {{{56,13 }, {56,14 }}, delim_lexeme(Delimiter_kind::Maybe_pattern)              }, // Delimiter Maybe_pattern
            {{{56,17 }, {56,19 }}, delim_lexeme(Delimiter_kind::Maybe_pattern)              }, // Delimiter Maybe_pattern
            {{{56,30 }, {56,30 }}, delim_lexeme(Delimiter_kind::GT)                         }, // Delimiter GT
            {{{56,33 }, {56,34 }}, delim_lexeme(Delimiter_kind::GEQ)                        }, // Delimiter GEQ
            {{{56,38 }, {56,39 }}, delim_lexeme(Delimiter_kind::Right_shift)                }, // Delimiter Right_shift
            {{{56,55 }, {56,57 }}, delim_lexeme(Delimiter_kind::Right_shift_assign)         }, // Delimiter Right_shift_assign
            {{{59,10 }, {59,10 }}, delim_lexeme(Delimiter_kind::Bitwise_not)                }, // Delimiter Bitwise_not
            {{{59,24 }, {59,25 }}, delim_lexeme(Delimiter_kind::Bitwise_or_not)             }, // Delimiter Bitwise_or_not
            {{{59,35 }, {59,37 }}, delim_lexeme(Delimiter_kind::Bitwise_and_not_assign)     }, // Delimiter Bitwise_and_not_assign
            {{{59,49 }, {59,50 }}, delim_lexeme(Delimiter_kind::Bitwise_and_not)            }, // Delimiter Bitwise_and_not
            {{{59,53 }, {59,55 }}, delim_lexeme(Delimiter_kind::Bitwise_or_not_assign)      }, // Delimiter Bitwise_or_not_assign
            {{{61,5  }, {61,5  }}, delim_lexeme(Delimiter_kind::Fig_br_closed)              }, // Delimiter Fig_br_closed
            {{{61,6  }, {61,6  }}, nothing_lexeme()                                         }, // Nothing
        }
    };

    template<typename ForwardIterator>
    void insert_strings_to_trie(std::shared_ptr<Char_trie>& trie,
                                ForwardIterator             first,
                                ForwardIterator             last)
    {
        for(auto it = first; it != last; ++it){
            trie->insert(*it);
        }
    }


    using Mode = arkona_scanner::Scanner::Begin_comment_mode;

    class Scanner_as_generator : public testing::Generator<arkona_scanner::Arkona_token>{
    public:
        Scanner_as_generator()                            = default;
        Scanner_as_generator(const Scanner_as_generator&) = default;
        virtual ~Scanner_as_generator()                   = default;

        Scanner_as_generator(const ascaner::Location_ptr& location,
                             const Errors_and_tries&      et,
                             Mode                         mode = Mode::As_delimiter):
            scanner_{std::make_shared<arkona_scanner::Scanner>(location, et)}
        {
            scanner_->set_begin_comment_mode(mode);
        }

        arkona_scanner::Arkona_token yield()                                      override;
        bool                   is_finished(const arkona_scanner::Arkona_token& t) override;
    private:
        std::shared_ptr<arkona_scanner::Scanner> scanner_;
    };

    arkona_scanner::Arkona_token Scanner_as_generator::yield()
    {
        return scanner_->current_lexeme();
    }

    bool Scanner_as_generator::is_finished(const arkona_scanner::Arkona_token& t)
    {
        return t.lexeme_.code_.kind_ == arkona_scanner::Lexem_kind::Nothing;
    }

    void test_lexeme_recognition()
    {
        Errors_and_tries  et;
        et.ec_                      = std::make_shared<Error_count>();
        et.wc_                      = std::make_shared<Warning_count>();
        et.ids_trie_                = std::make_shared<Char_trie>();
        et.strs_trie_               = std::make_shared<Char_trie>();

        std::size_t i = 0;
        for(auto test_text: test_texts){
//             insert_ids_and_strings(et, ids_and_strs[i]);
            auto                 p   = const_cast<char32_t*>(test_text);
            auto                 loc = std::make_shared<ascaner::Location>(p);
            Scanner_as_generator gen{loc, et};
            printf("Testing for text #%zu\n", i);
            testing::test(gen,                             [](auto t){return t;},
                          std::begin(tokens_for_texts[i]), std::end(tokens_for_texts[i]));
            ++i;
        }
    }
};