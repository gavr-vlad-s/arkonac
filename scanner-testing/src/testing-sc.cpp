/*
     File:    testing-sc.cpp
     Created: 09 July 2019 at 07:57 Moscow time.
     Author:  Гаврилов Владимир Сергеевич
     E-mails: vladimir.s.gavrilov@gmail.com
              gavrilov.vladimir.s@mail.ru
              gavvs1977@yandex.ru
    License: GPLv3
*/

#include <string>
#include <cstdio>
#include <memory>
#include "../../file_utils/include/get_processed_text.h"
#include "../../scanner/include/location.h"
#include "../../tries/include/errors_and_tries.h"
#include "../../tries/include/error_count.h"
#include "../../tries/include/warning_count.h"
#include "../../tries/include/char_trie.h"
#include "../../scanner/include/arkona-scanner.h"
#include "../include/usage.h"
#include "../include/testing-func.h"
#include "../include/test_lexeme_to_string.h"
#include "../include/test_lexeme_recognition.h"
#include "../include/test_back_function.h"

enum Exit_codes{
    Success, No_args, File_processing_error
};

int main(int argc, char* argv[])
{
    if(1 == argc){
        usage(argv[0]);
        return No_args;
    }

    auto              text   = file_utils::get_processed_text(argv[1]);
    if(!text.length()){
        return File_processing_error;
    }

    char32_t*         p         = const_cast<char32_t*>(text.c_str());
    auto              loc       = std::make_shared<ascaner::Location>(p);
    Errors_and_tries  et;
    et.ec_                      = std::make_shared<Error_count>();
    et.wc_                      = std::make_shared<Warning_count>();
    et.ids_trie_                = std::make_shared<Char_trie>();
    et.strs_trie_               = std::make_shared<Char_trie>();
    auto              arkona_sc = std::make_shared<arkona_scanner::Scanner>(loc, et);

    testing_scanner::test_lexeme_to_string();

    testing_scanner::test_func(arkona_sc);
    et.print();

    testing_scanner::test_lexeme_recognition();

    testing_scanner::test_back_function();

    return Success;
}