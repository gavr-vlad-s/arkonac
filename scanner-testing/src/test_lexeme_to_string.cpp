/*
    File:    test_lexeme_to_string.cpp
    Created: 14 August 2019 at 06:29 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <string>
#include <utility>
#include <cstdio>
#include <memory>
#include <vector>
#include <iterator>
#include "../include/test_lexeme_to_string.h"
#include "../include/lexeme_fab.h"
#include "../../other/include/testing.hpp"
#include "../../scanner/include/lexeme.h"
#include "../../scanner/include/lexeme_to_str.h"
#include "../../tries/include/char_trie.h"
#include "../../char-conv/include/char_conv.h"

namespace testing_scanner{
    using Pair_for_test = std::pair<arkona_scanner::Lexeme_info, std::string>;

    //! Tests for special lexemes.
    static const Pair_for_test special_lexemes_tests[] = {
        {unknown_lexeme(), "UnknownLexem"}, {nothing_lexeme(), "Nothing"}
    };

    //! Tests for keywords.
    static const Pair_for_test keywords_tests[] = {
        {keyword_lexeme(arkona_scanner::Kw_bolshoe),       "Keyword Kw_bolshoe"      },
        {keyword_lexeme(arkona_scanner::Kw_bezzn),         "Keyword Kw_bezzn"        },
        {keyword_lexeme(arkona_scanner::Kw_bezzn64),       "Keyword Kw_bezzn64"      },
        {keyword_lexeme(arkona_scanner::Kw_bezzn8),        "Keyword Kw_bezzn8"       },
        {keyword_lexeme(arkona_scanner::Kw_bolshe),        "Keyword Kw_bolshe"       },
        {keyword_lexeme(arkona_scanner::Kw_bezzn16),       "Keyword Kw_bezzn16"      },
        {keyword_lexeme(arkona_scanner::Kw_bezzn128),      "Keyword Kw_bezzn128"     },
        {keyword_lexeme(arkona_scanner::Kw_bezzn32),       "Keyword Kw_bezzn32"      },
        {keyword_lexeme(arkona_scanner::Kw_dlya),          "Keyword Kw_dlya"         },
        {keyword_lexeme(arkona_scanner::Kw_v),             "Keyword Kw_v"            },
        {keyword_lexeme(arkona_scanner::Kw_vozvrat),       "Keyword Kw_vozvrat"      },
        {keyword_lexeme(arkona_scanner::Kw_vechno),        "Keyword Kw_vechno"       },
        {keyword_lexeme(arkona_scanner::Kw_golovnaya),     "Keyword Kw_golovnaya"    },
        {keyword_lexeme(arkona_scanner::Kw_veshch),        "Keyword Kw_veshch"       },
        {keyword_lexeme(arkona_scanner::Kw_vydeli),        "Keyword Kw_vydeli"       },
        {keyword_lexeme(arkona_scanner::Kw_veshch80),      "Keyword Kw_veshch80"     },
        {keyword_lexeme(arkona_scanner::Kw_veshch32),      "Keyword Kw_veshch32"     },
        {keyword_lexeme(arkona_scanner::Kw_vyjdi),         "Keyword Kw_vyjdi"        },
        {keyword_lexeme(arkona_scanner::Kw_veshch64),      "Keyword Kw_veshch64"     },
        {keyword_lexeme(arkona_scanner::Kw_vyberi),        "Keyword Kw_vyberi"       },
        {keyword_lexeme(arkona_scanner::Kw_veshch128),     "Keyword Kw_veshch128"    },
        {keyword_lexeme(arkona_scanner::Kw_konst),         "Keyword Kw_konst"        },
        {keyword_lexeme(arkona_scanner::Kw_esli),          "Keyword Kw_esli"         },
        {keyword_lexeme(arkona_scanner::Kw_kvat),          "Keyword Kw_kvat"         },
        {keyword_lexeme(arkona_scanner::Kw_kompl128),      "Keyword Kw_kompl128"     },
        {keyword_lexeme(arkona_scanner::Kw_iz),            "Keyword Kw_iz"           },
        {keyword_lexeme(arkona_scanner::Kw_kvat32),        "Keyword Kw_kvat32"       },
        {keyword_lexeme(arkona_scanner::Kw_kompl80),       "Keyword Kw_kompl80"      },
        {keyword_lexeme(arkona_scanner::Kw_inache),        "Keyword Kw_inache"       },
        {keyword_lexeme(arkona_scanner::Kw_kvat64),        "Keyword Kw_kvat64"       },
        {keyword_lexeme(arkona_scanner::Kw_kompl64),       "Keyword Kw_kompl64"      },
        {keyword_lexeme(arkona_scanner::Kw_ines),          "Keyword Kw_ines"         },
        {keyword_lexeme(arkona_scanner::Kw_kompl32),       "Keyword Kw_kompl32"      },
        {keyword_lexeme(arkona_scanner::Kw_ispolzuet),     "Keyword Kw_ispolzuet"    },
        {keyword_lexeme(arkona_scanner::Kw_kompl),         "Keyword Kw_kompl"        },
        {keyword_lexeme(arkona_scanner::Kw_istina),        "Keyword Kw_istina"       },
        {keyword_lexeme(arkona_scanner::Kw_kvat128),       "Keyword Kw_kvat128"      },
        {keyword_lexeme(arkona_scanner::Kw_kak),           "Keyword Kw_kak"          },
        {keyword_lexeme(arkona_scanner::Kw_kvat80),        "Keyword Kw_kvat80"       },
        {keyword_lexeme(arkona_scanner::Kw_kategorija),    "Keyword Kw_kategorija"   },
        {keyword_lexeme(arkona_scanner::Kw_lyambda),       "Keyword Kw_lyambda"      },
        {keyword_lexeme(arkona_scanner::Kw_realizuj),      "Keyword Kw_realizuj"     },
        {keyword_lexeme(arkona_scanner::Kw_element),       "Keyword Kw_element"      },
        {keyword_lexeme(arkona_scanner::Kw_lozh),          "Keyword Kw_lozh"         },
        {keyword_lexeme(arkona_scanner::Kw_shkalu),        "Keyword Kw_shkalu"       },
        {keyword_lexeme(arkona_scanner::Kw_modul),         "Keyword Kw_modul"        },
        {keyword_lexeme(arkona_scanner::Kw_pusto),         "Keyword Kw_pusto"        },
        {keyword_lexeme(arkona_scanner::Kw_realizacija),   "Keyword Kw_realizacija"  },
        {keyword_lexeme(arkona_scanner::Kw_malenkoe),      "Keyword Kw_malenkoe"     },
        {keyword_lexeme(arkona_scanner::Kw_perem),         "Keyword Kw_perem"        },
        {keyword_lexeme(arkona_scanner::Kw_stroka),        "Keyword Kw_stroka"       },
        {keyword_lexeme(arkona_scanner::Kw_perech_mnozh),  "Keyword Kw_perech_mnozh" },
        {keyword_lexeme(arkona_scanner::Kw_stroka8),       "Keyword Kw_stroka8"      },
        {keyword_lexeme(arkona_scanner::Kw_log),           "Keyword Kw_log"          },
        {keyword_lexeme(arkona_scanner::Kw_funktsiya),     "Keyword Kw_funktsiya"    },
        {keyword_lexeme(arkona_scanner::Kw_ekviv),         "Keyword Kw_ekviv"        },
        {keyword_lexeme(arkona_scanner::Kw_pokuda),        "Keyword Kw_pokuda"       },
        {keyword_lexeme(arkona_scanner::Kw_eksport),       "Keyword Kw_eksport"      },
        {keyword_lexeme(arkona_scanner::Kw_paket),         "Keyword Kw_paket"        },
        {keyword_lexeme(arkona_scanner::Kw_nichto),        "Keyword Kw_nichto"       },
        {keyword_lexeme(arkona_scanner::Kw_pauk),          "Keyword Kw_pauk"         },
        {keyword_lexeme(arkona_scanner::Kw_perechislenie), "Keyword Kw_perechislenie"},
        {keyword_lexeme(arkona_scanner::Kw_simv),          "Keyword Kw_simv"         },
        {keyword_lexeme(arkona_scanner::Kw_shkala),        "Keyword Kw_shkala"       },
        {keyword_lexeme(arkona_scanner::Kw_postfiksnaya),  "Keyword Kw_postfiksnaya" },
        {keyword_lexeme(arkona_scanner::Kw_stroka32),      "Keyword Kw_stroka32"     },
        {keyword_lexeme(arkona_scanner::Kw_chistaya),      "Keyword Kw_chistaya"     },
        {keyword_lexeme(arkona_scanner::Kw_osvobodi),      "Keyword Kw_osvobodi"     },
        {keyword_lexeme(arkona_scanner::Kw_tsel128),       "Keyword Kw_tsel128"      },
        {keyword_lexeme(arkona_scanner::Kw_struktura),     "Keyword Kw_struktura"    },
        {keyword_lexeme(arkona_scanner::Kw_psevdonim),     "Keyword Kw_psevdonim"    },
        {keyword_lexeme(arkona_scanner::Kw_simv8),         "Keyword Kw_simv8"        },
        {keyword_lexeme(arkona_scanner::Kw_razbor),        "Keyword Kw_razbor"       },
        {keyword_lexeme(arkona_scanner::Kw_prefiksnaya),   "Keyword Kw_prefiksnaya"  },
        {keyword_lexeme(arkona_scanner::Kw_tsel32),        "Keyword Kw_tsel32"       },
        {keyword_lexeme(arkona_scanner::Kw_povtoryaj),     "Keyword Kw_povtoryaj"    },
        {keyword_lexeme(arkona_scanner::Kw_operaciya),     "Keyword Kw_operaciya"    },
        {keyword_lexeme(arkona_scanner::Kw_ssylka),        "Keyword Kw_ssylka"       },
        {keyword_lexeme(arkona_scanner::Kw_tsel16),        "Keyword Kw_tsel16"       },
        {keyword_lexeme(arkona_scanner::Kw_preobrazuj),    "Keyword Kw_preobrazuj"   },
        {keyword_lexeme(arkona_scanner::Kw_tsel8),         "Keyword Kw_tsel8"        },
        {keyword_lexeme(arkona_scanner::Kw_rassmatrivaj),  "Keyword Kw_rassmatrivaj" },
        {keyword_lexeme(arkona_scanner::Kw_log16),         "Keyword Kw_log16"        },
        {keyword_lexeme(arkona_scanner::Kw_tsel64),        "Keyword Kw_tsel64"       },
        {keyword_lexeme(arkona_scanner::Kw_samo),          "Keyword Kw_samo"         },
        {keyword_lexeme(arkona_scanner::Kw_tsel),          "Keyword Kw_tsel"         },
        {keyword_lexeme(arkona_scanner::Kw_simv32),        "Keyword Kw_simv32"       },
        {keyword_lexeme(arkona_scanner::Kw_to),            "Keyword Kw_to"           },
        {keyword_lexeme(arkona_scanner::Kw_massiv),        "Keyword Kw_massiv"       },
        {keyword_lexeme(arkona_scanner::Kw_log8),          "Keyword Kw_log8"         },
        {keyword_lexeme(arkona_scanner::Kw_poka),          "Keyword Kw_poka"         },
        {keyword_lexeme(arkona_scanner::Kw_tip),           "Keyword Kw_tip"          },
        {keyword_lexeme(arkona_scanner::Kw_log32),         "Keyword Kw_log32"        },
        {keyword_lexeme(arkona_scanner::Kw_stroka16),      "Keyword Kw_stroka16"     },
        {keyword_lexeme(arkona_scanner::Kw_meta),          "Keyword Kw_meta"         },
        {keyword_lexeme(arkona_scanner::Kw_simv16),        "Keyword Kw_simv16"       },
        {keyword_lexeme(arkona_scanner::Kw_log64),         "Keyword Kw_log64"        },
        {keyword_lexeme(arkona_scanner::Kw_sravni_s),      "Keyword Kw_sravni_s"     },
        {keyword_lexeme(arkona_scanner::Kw_poryadok16),    "Keyword Kw_poryadok16"   },
        {keyword_lexeme(arkona_scanner::Kw_poryadok),      "Keyword Kw_poryadok"     },
        {keyword_lexeme(arkona_scanner::Kw_menshe),        "Keyword Kw_menshe"       },
        {keyword_lexeme(arkona_scanner::Kw_poryadok64),    "Keyword Kw_poryadok64"   },
        {keyword_lexeme(arkona_scanner::Kw_poryadok32),    "Keyword Kw_poryadok32"   },
        {keyword_lexeme(arkona_scanner::Kw_ravno),         "Keyword Kw_ravno"        },
        {keyword_lexeme(arkona_scanner::Kw_poryadok8),     "Keyword Kw_poryadok8"    }
    };


    static const Pair_for_test create_test_for_identifier(const std::u32string&             id_str,
                                                          const std::shared_ptr<Char_trie>& trie)
    {
        Pair_for_test result;
        arkona_scanner::Lexeme_info li;
        li.code_.kind_    = arkona_scanner::Lexem_kind::Id;
        li.code_.subkind_ = 0;
        li.id_index_      = trie->insert(id_str);
        result.first      = li;
        result.second     = u32string_to_utf8(U"Id " + id_str);
        return result;
    }

    static const std::u32string identifiers[] = {
        U"крокодил_Гена128_и_Cheburashka", U"__cosinus"
    };

    static std::vector<Pair_for_test> tests_for_identifiers;

    static void create_identifiers_tests(const std::shared_ptr<Char_trie>& trie)
    {
        for(auto it = std::begin(identifiers); it != std::end(identifiers); ++it){
            tests_for_identifiers.push_back(create_test_for_identifier(*it, trie));
        }
    }

    void test_lexeme_to_string()
    {
        auto ids_trie         = std::make_shared<Char_trie>();
        auto strs_trie        = std::make_shared<Char_trie>();
        auto to_string_lambda = [&ids_trie, &strs_trie](const arkona_scanner::Lexeme_info& li){
            return arkona_scanner::to_string(li, ids_trie, strs_trie);
        };
        puts("Testing of converting of a lexeme into the string...\n\n");

        puts("Running tests for special lexemes (UnknownLexem and None)...");
        testing::test(std::begin(special_lexemes_tests),
                      std::end(special_lexemes_tests),
                      to_string_lambda);

        puts("Running tests for keywords...");
        testing::test(std::begin(keywords_tests),
                      std::end(keywords_tests),
                      to_string_lambda);

        create_identifiers_tests(ids_trie);
        puts("Running tests for identifiers...");
        testing::test(std::begin(tests_for_identifiers),
                      std::end(tests_for_identifiers),
                      to_string_lambda);

    }
};
