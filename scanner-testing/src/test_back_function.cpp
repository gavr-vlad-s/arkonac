/*
    File:    test_back_function.cpp
    Created: 22 December 2019 at 17:29 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <vector>
#include <string>
#include <iterator>
#include <memory>
#include <cstddef>
#include <cstdio>
#include "../include/test_back_function.h"
#include "../../scanner/include/arkona-scanner.h"
#include "../../other/include/testing.hpp"
#include "../../tries/include/errors_and_tries.h"
#include "../../tries/include/char_trie.h"
#include "../../scanner/include/location.h"
#include "../include/lexeme_fab.h"

namespace testing_scanner{
    static const char32_t* test_text = UR"~(категория аддитивный_моноид<:T : тип:>
{
    конст нуль: T;
    операция + (x: T, y: T) : T;
    операция += (x: ссылка T, y: T) : ссылка T ;
    /*
       Данная категория определяет моноид по сложению, называемый ещё
       аддитивным моноидом.

       Напомним, что моноидом называется алгебраическая
       структура G с определённой на ней операцией ◊ : G x G → G,
       которая обладает следующими свойствами:
       1) для всех a, b, c справедливо соотношение
          (a ◊ b) ◊ c = a ◊ (b ◊ c)
       2) найдётся элемент e из G, такой, что
          a ◊ e = e ◊ a
          для всех элементов a из G.

       Первое свойство называется ассоциативностью операции, а элемент e
       из второго свойства --- нейтральным элементом.

       Аддитивным же моноидом называется моноид, в котором операция обозначается
       знаком +, а нейтральный элемент обозначается как 0.
    */
}

категория мультипликативный_моноид<:T : тип:>
{
    конст единица: T; операция * (x: T, y: T) : T;
    операция *= (x: ссылка T, y: T) : ссылка T ;
    /*
       Данная категория определяет моноид по умножению, называемый ещё
       мультипликативным моноидом.

       Напомним, что моноидом называется алгебраическая
       структура G с определённой на ней операцией ◊ : G x G → G,
       которая обладает следующими свойствами:
       1) для всех a, b, c справедливо соотношение
          (a ◊ b) ◊ c = a ◊ (b ◊ c)
       2) найдётся элемент e из G, такой, что
          a ◊ e = e ◊ a
          для всех элементов a из G.

       Первое свойство называется ассоциативностью операции, а элемент e
       из второго свойства --- нейтральным элементом.

       Мультипликативным же моноидом называется моноид, в котором операция обозначается
       знаком *, а нейтральный элемент обозначается как 1.

            /*
                Данная категория определяет моноид по сложению.
                Напомним, что моноидом называется алгебраическая
                структура G с определённой на ней операцией ◊ : G x G → G,
                которая обладает следующими свойствами:
                1) для всех a, b, c ∊ G справедливо соотношение
                    (a ◊ b) ◊ c = a ◊ (b ◊ c)
                2) найдётся элемент e ∊ G, такой, что
                    a ◊ e = e ◊ a
                    для всех элементов a из G.
                    /* А здесь ещё комментарий. */

                Первое свойство называется ассоциативностью операции, а элемент e
                из второго свойства --- нейтральным элементом.

                Аддитивным же моноидом называется моноид, в котором операция обозначается
                знаком +, а нейтральный элемент обозначается как 0.
            */

    */
}

мета функция фибоначчи<:n <:> T:> : T -> аддитивный_моноид<:T:>,
                                         мультипликативный_моноид<:T:>
{
    мета перем fnm1, fn, fnp1 : T
    мета перем i : беззн
    fnm1 = аддитивный_моноид<:T:>::нуль;
    fn = мультипликативный_моноид<:T:>::единица;
    мета для i = 0, n, 1
    {
        fnp1 = fn + fnm1;
        fnm1 = fn;
        fn   = fnp1
    };
    мета возврат fn_p1
})~";

    enum class Action{
        Get_current, Get_previous
    };


    using Scanner_ptr     = std::shared_ptr<arkona_scanner::Scanner>;
    using Token_generator = testing::Generator<arkona_scanner::Arkona_token>;

    class Scanner_as_generator_with_actions : public Token_generator{
    public:
        Scanner_as_generator_with_actions()                                         = default;
        Scanner_as_generator_with_actions(const Scanner_as_generator_with_actions&) = default;
        virtual ~Scanner_as_generator_with_actions()                                = default;

        Scanner_as_generator_with_actions(const Scanner_ptr&         scanner,
                                          const std::vector<Action>& actions):
            scanner_{scanner}, actions_{actions}
        {
            current_idx_ = 0;
        }

        arkona_scanner::Arkona_token yield()                                            override;
        bool                         is_finished(const arkona_scanner::Arkona_token& t) override;
    private:
        std::shared_ptr<arkona_scanner::Scanner> scanner_;
        const std::vector<Action>&               actions_;
        std::size_t                              current_idx_;
    };

    arkona_scanner::Arkona_token Scanner_as_generator_with_actions::yield()
    {
        if(actions_[current_idx_] == Action::Get_previous){
            scanner_->back();
        }
        current_idx_++;
        return scanner_->current_lexeme();
    }

    bool Scanner_as_generator_with_actions::is_finished(const arkona_scanner::Arkona_token& t)
    {
        return t.lexeme_.code_.kind_ == arkona_scanner::Lexem_kind::Nothing;
    }


    static const std::vector<Action> actions = {
        Action::Get_current,  Action::Get_current,  Action::Get_previous,
        Action::Get_current,  Action::Get_previous, Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_previous, // 9

        Action::Get_previous, Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 18

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 27

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 36

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 45

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_previous, Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 54

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 63

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 72

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_previous,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 81

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 90

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 99

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 108

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 117

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 126

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 135

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 144

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 153

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 162

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 171

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
    };

    static const std::vector<arkona_scanner::Arkona_token> tokens_for_text = {
            {{{1, 1  }, {1, 9  }}, keyword_lexeme(Keyword_kind::Kw_kategorija)              }, // Keyword Kw_kategorija
            {{{1, 11 }, {1, 27 }}, id_lexeme(17)                                            }, // Id аддитивный_моноид
            {{{1, 11 }, {1, 27 }}, id_lexeme(17)                                            }, // Id аддитивный_моноид

            {{{1, 28 }, {1, 29 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{1, 28 }, {1, 29 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{1, 30 }, {1, 30 }}, id_lexeme(18)                                            }, // Id T

            {{{1, 32 }, {1, 32 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{1, 34 }, {1, 36 }}, keyword_lexeme(Keyword_kind::Kw_tip)                     }, // Keyword Kw_tip
            {{{1, 34 }, {1, 36 }}, keyword_lexeme(Keyword_kind::Kw_tip)                     }, // Keyword Kw_tip
// 9
            {{{1, 34 }, {1, 36 }}, keyword_lexeme(Keyword_kind::Kw_tip)                     }, // Keyword Kw_tip
            {{{1, 37 }, {1, 38 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br
            {{{2, 1  }, {2, 1  }}, delim_lexeme(Delimiter_kind::Fig_br_opened)              }, // Delimiter Fig_br_opened

            {{{3, 5  }, {3, 9  }}, keyword_lexeme(Keyword_kind::Kw_konst)                   }, // Keyword Kw_konst
            {{{3, 11 }, {3, 14 }}, id_lexeme(22)                                            }, // Id нуль
            {{{3, 15 }, {3, 15 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon

            {{{3, 17 }, {3, 17 }}, id_lexeme(18)                                            }, // Id T
            {{{3, 18 }, {3, 18 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{4, 5  }, {4, 12 }}, keyword_lexeme(Keyword_kind::Kw_operaciya)               }, // Keyword Kw_operaciya
// 18
            {{{4, 14 }, {4, 14 }}, delim_lexeme(Delimiter_kind::Plus)                       }, // Delimiter Plus
            {{{4, 16 }, {4, 16 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened
            {{{4, 17 }, {4, 17 }}, id_lexeme(23)                                            }, // Id x

            {{{4, 18 }, {4, 18 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{4, 20 }, {4, 20 }}, id_lexeme(18)                                            }, // Id T
            {{{4, 21 }, {4, 21 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma

            {{{4, 23 }, {4, 23 }}, id_lexeme(24)                                            }, // Id y
            {{{4, 24 }, {4, 24 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{4, 26 }, {4, 26 }}, id_lexeme(18)                                            }, // Id T
// 27
            {{{4, 27 }, {4, 27 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed
            {{{4, 29 }, {4, 29 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{4, 31 }, {4, 31 }}, id_lexeme(18)                                            }, // Id T

            {{{4, 32 }, {4, 32 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{5, 5  }, {5, 12 }}, keyword_lexeme(Keyword_kind::Kw_operaciya)               }, // Keyword Kw_operaciya
            {{{5, 14 }, {5, 15 }}, delim_lexeme(Delimiter_kind::Plus_assign)                }, // Delimiter Plus_assign

            {{{5, 17 }, {5, 17 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened
            {{{5, 18 }, {5, 18 }}, id_lexeme(23)                                            }, // Id x
            {{{5, 19 }, {5, 19 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
// 36
            {{{5, 21 }, {5, 26 }}, keyword_lexeme(Keyword_kind::Kw_ssylka)                  }, // Keyword Kw_ssylka
            {{{5, 28 }, {5, 28 }}, id_lexeme(18)                                            }, // Id x
            {{{5, 29 }, {5, 29 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma

            {{{5, 31 }, {5, 31 }}, id_lexeme(24)                                            }, // Id y
            {{{5, 32 }, {5, 32 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{5, 34 }, {5, 34 }}, id_lexeme(18)                                            }, // Id T

            {{{5, 35 }, {5, 35 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed
            {{{5, 37 }, {5, 37 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{5, 39 }, {5, 44 }}, keyword_lexeme(Keyword_kind::Kw_ssylka)                  }, // Keyword Kw_ssylka
// 45
            {{{5, 46 }, {5, 46 }}, id_lexeme(18)                                            }, // Id T
            {{{5, 48 }, {5, 48 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{25,1  }, {25,1  }}, delim_lexeme(Delimiter_kind::Fig_br_closed)              }, // Delimiter Fig_br_closed

            {{{27,1  }, {27,9  }}, keyword_lexeme(Keyword_kind::Kw_kategorija)              }, // Keyword Kw_kategorija
            {{{27,1  }, {27,9  }}, keyword_lexeme(Keyword_kind::Kw_kategorija)              }, // Keyword Kw_kategorija
            {{{27,11 }, {27,34 }}, id_lexeme(48)                                            }, // Id мультипликативный_моноид

            {{{27,35 }, {27,36 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{27,37 }, {27,37 }}, id_lexeme(18)                                            }, // Id T
            {{{27,39 }, {27,39 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
// 54
            {{{27,41 }, {27,43 }}, keyword_lexeme(Keyword_kind::Kw_tip)                     }, // Keyword Kw_tip
            {{{27,44 }, {27,45 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br
            {{{28,1  }, {28,1  }}, delim_lexeme(Delimiter_kind::Fig_br_opened)              }, // Delimiter Fig_br_opened

            {{{29,5  }, {29,9  }}, keyword_lexeme(Keyword_kind::Kw_konst)                   }, // Keyword Kw_konst
            {{{29,11 }, {29,17 }}, id_lexeme(55)                                            }, // Id нуль
            {{{29,18 }, {29,18 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon

            {{{29,20 }, {29,20 }}, id_lexeme(18)                                            }, // Id T
            {{{29,21 }, {29,21 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{29,23 }, {29,30 }}, keyword_lexeme(Keyword_kind::Kw_operaciya)               }, // Keyword Kw_operaciya
// 63
            {{{29,32 }, {29,32 }}, delim_lexeme(Delimiter_kind::Mul)                        }, // Delimiter Mul
            {{{29,34 }, {29,34 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened
            {{{29,35 }, {29,35 }}, id_lexeme(23)                                            }, // Id x

            {{{29,36 }, {29,36 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{29,38 }, {29,38 }}, id_lexeme(18)                                            }, // Id T
            {{{29,39 }, {29,39 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma

            {{{29,41 }, {29,41 }}, id_lexeme(24)                                            }, // Id y
            {{{29,42 }, {29,42 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{29,44 }, {29,44 }}, id_lexeme(18)                                            }, // Id T
// 72
            {{{29,45 }, {29,45 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed
            {{{29,47 }, {29,47 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{29,49 }, {29,49 }}, id_lexeme(18)                                            }, // Id T

            {{{29,50 }, {29,50 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{30,5  }, {30,12 }}, keyword_lexeme(Keyword_kind::Kw_operaciya)               }, // Keyword Kw_operaciya
            {{{30,5  }, {30,12 }}, keyword_lexeme(Keyword_kind::Kw_operaciya)               }, // Keyword Kw_operaciya

            {{{30,14 }, {30,15 }}, delim_lexeme(Delimiter_kind::Mul_assign)                 }, // Delimiter Mul_assign
            {{{30,17 }, {30,17 }}, delim_lexeme(Delimiter_kind::Round_br_opened)            }, // Delimiter Round_br_opened
            {{{30,18 }, {30,18 }}, id_lexeme(23)                                            }, // Id x
// 81
            {{{30,19 }, {30,19 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{30,21 }, {30,26 }}, keyword_lexeme(Keyword_kind::Kw_ssylka)                  }, // Keyword Kw_ssylka
            {{{30,28 }, {30,28 }}, id_lexeme(18)                                            }, // Id x

            {{{30,29 }, {30,29 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma
            {{{30,31 }, {30,31 }}, id_lexeme(24)                                            }, // Id y
            {{{30,32 }, {30,32 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon

            {{{30,34 }, {30,34 }}, id_lexeme(18)                                            }, // Id T
            {{{30,35 }, {30,35 }}, delim_lexeme(Delimiter_kind::Round_br_closed)            }, // Delimiter Round_br_closed
            {{{30,37 }, {30,37 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
// 90
            {{{30,39 }, {30,44 }}, keyword_lexeme(Keyword_kind::Kw_ssylka)                  }, // Keyword Kw_ssylka
            {{{30,46 }, {30,46 }}, id_lexeme(18)                                            }, // Id T
            {{{30,48 }, {30,48 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon

            {{{70,1  }, {70,1  }}, delim_lexeme(Delimiter_kind::Fig_br_closed)              }, // Delimiter Fig_br_closed
            {{{72,1  }, {72,4  }}, keyword_lexeme(Keyword_kind::Kw_meta)                    }, // Keyword Kw_meta
            {{{72,6  }, {72,12 }}, keyword_lexeme(Keyword_kind::Kw_funktsiya)               }, // Keyword Kw_funktsiya

            {{{72,14 }, {72,22 }}, id_lexeme(64)                                            }, // Id фибоначчи
            {{{72,23 }, {72,24 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{72,25 }, {72,25 }}, id_lexeme(65)                                            }, // Id n
// 99
            {{{72,27 }, {72,29 }}, delim_lexeme(Delimiter_kind::Deduce_arg_type)            }, // Delimiter Deduce_arg_type
            {{{72,31 }, {72,31 }}, id_lexeme(18)                                            }, // Id T
            {{{72,32 }, {72,33 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br

            {{{72,35 }, {72,35 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{72,37 }, {72,37 }}, id_lexeme(18)                                            }, // Id T
            {{{72,39 }, {72,40 }}, delim_lexeme(Delimiter_kind::Right_arrow)                }, // Delimiter Right_arrow

            {{{72,42 }, {72,58 }}, id_lexeme(17)                                            }, // Id аддитивный_моноид
            {{{72,59 }, {72,60 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{72,61 }, {72,61 }}, id_lexeme(18)                                            }, // Id T
//108
            {{{72,62 }, {72,63 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br
            {{{72,64 }, {72,64 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma
            {{{73,42 }, {73,65 }}, id_lexeme(48)                                            }, // Id мультипликативный_моноид

            {{{73,66 }, {73,67 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{73,68 }, {73,68 }}, id_lexeme(18)                                            }, // Id T
            {{{73,69 }, {73,70 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br

            {{{74,1  }, {74,1  }}, delim_lexeme(Delimiter_kind::Fig_br_opened)              }, // Delimiter Fig_br_opened
            {{{75,5  }, {75,8  }}, keyword_lexeme(Keyword_kind::Kw_meta)                    }, // Keyword Kw_meta
            {{{75,10 }, {75,14 }}, keyword_lexeme(Keyword_kind::Kw_perem)                   }, // Keyword Kw_perem
// 117
            {{{75,16 }, {75,19 }}, id_lexeme(69)                                            }, // Id fnm1
            {{{75,20 }, {75,20 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma
            {{{75,22 }, {75,23 }}, id_lexeme(67)                                            }, // Id fn

            {{{75,24 }, {75,24 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma
            {{{75,26 }, {75,29 }}, id_lexeme(71)                                            }, // Id fnp1
            {{{75,31 }, {75,31 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon

            {{{75,33 }, {75,33 }}, id_lexeme(18)                                            }, // Id T
            {{{76,5  }, {76,8  }}, keyword_lexeme(Keyword_kind::Kw_meta)                    }, // Keyword Kw_meta
            {{{76,10 }, {76,14 }}, keyword_lexeme(Keyword_kind::Kw_perem)                   }, // Keyword Kw_perem
// 126
            {{{76,16 }, {76,16 }}, id_lexeme(72)                                            }, // Id i
            {{{76,18 }, {76,18 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{76,20 }, {76,24 }}, keyword_lexeme(Keyword_kind::Kw_bezzn)                   }, // Keyword Kw_bezzn

            {{{77,5  }, {77,8  }}, id_lexeme(69)                                            }, // Id fnm1
            {{{77,10 }, {77,10 }}, delim_lexeme(Delimiter_kind::Assign)                     }, // Delimiter Assign
            {{{77,12 }, {77,28 }}, id_lexeme(17)                                            }, // Id аддитивный_моноид

            {{{77,29 }, {77,30 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{77,31 }, {77,31 }}, id_lexeme(18)                                            }, // Id T
            {{{77,32 }, {77,33 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br
// 135
            {{{77,34 }, {77,35 }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
            {{{77,36 }, {77,39 }}, id_lexeme(22)                                            }, // Id нуль
            {{{77,40 }, {77,40 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon

            {{{78,5  }, {78,6  }}, id_lexeme(67)                                            }, // Id fn
            {{{78,8  }, {78,8  }}, delim_lexeme(Delimiter_kind::Assign)                     }, // Delimiter Assign
            {{{78,10 }, {78,33 }}, id_lexeme(48)                                            }, // Id мультипликативный_моноид

            {{{78,34 }, {78,35 }}, delim_lexeme(Delimiter_kind::Meta_open_br)               }, // Delimiter Meta_open_br
            {{{78,36 }, {78,36 }}, id_lexeme(18)                                            }, // Id T
            {{{78,37 }, {78,38 }}, delim_lexeme(Delimiter_kind::Meta_closed_br)             }, // Delimiter Meta_closed_br
// 144
            {{{78,39 }, {78,40 }}, delim_lexeme(Delimiter_kind::Scope_resol)                }, // Delimiter Scope_resol
            {{{78,41 }, {78,47 }}, id_lexeme(55)                                            }, // Id нуль
            {{{78,48 }, {78,48 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon

            {{{79,5  }, {79,8  }}, keyword_lexeme(Keyword_kind::Kw_meta)                    }, // Keyword Kw_meta
            {{{79,10 }, {79,12 }}, keyword_lexeme(Keyword_kind::Kw_dlya)                    }, // Keyword Kw_dlya
            {{{79,14 }, {79,14 }}, id_lexeme(72)                                            }, // Id i

            {{{79,16 }, {79,16 }}, delim_lexeme(Delimiter_kind::Assign)                     }, // Delimiter Assign
            {{{79,18 }, {79,18 }}, integer_lexeme(0)                                        }, // Integer 0
            {{{79,19 }, {79,19 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma
// 153
            {{{79,21 }, {79,21 }}, id_lexeme(65)                                            }, // Id n
            {{{79,22 }, {79,22 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma
            {{{79,24 }, {79,24 }}, integer_lexeme(1)                                        }, // Integer 1

            {{{80,5  }, {80,5  }}, delim_lexeme(Delimiter_kind::Fig_br_opened)              }, // Delimiter Fig_br_opened
            {{{81,9  }, {81,12 }}, id_lexeme(71)                                            }, // Id fnp1
            {{{81,14 }, {81,14 }}, delim_lexeme(Delimiter_kind::Assign)                     }, // Delimiter Assign

            {{{81,16 }, {81,17 }}, id_lexeme(67)                                            }, // Id fn
            {{{81,19 }, {81,19 }}, delim_lexeme(Delimiter_kind::Plus)                       }, // Delimiter Plus
            {{{81,21 }, {81,24 }}, id_lexeme(69)                                            }, // Id fnm1
// 162
            {{{81,25 }, {81,25 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{82,9  }, {82,12 }}, id_lexeme(69)                                            }, // Id fnm1
            {{{82,14 }, {82,14 }}, delim_lexeme(Delimiter_kind::Assign)                     }, // Delimiter Assign

            {{{82,16 }, {82,17 }}, id_lexeme(67)                                            }, // Id fn
            {{{82,18 }, {82,18 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{83,9  }, {83,10 }}, id_lexeme(67)                                            }, // Id fn

            {{{83,14 }, {83,14 }}, delim_lexeme(Delimiter_kind::Assign)                     }, // Delimiter Assign
            {{{83,16 }, {83,19 }}, id_lexeme(71)                                            }, // Id fnp1
            {{{84,5  }, {84,5  }}, delim_lexeme(Delimiter_kind::Fig_br_closed)              }, // Delimiter Fig_br_closed
// 171
            {{{84,6  }, {84,6  }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{85,5  }, {85,8  }}, keyword_lexeme(Keyword_kind::Kw_meta)                    }, // Keyword Kw_meta
            {{{85,10 }, {85,16 }}, keyword_lexeme(Keyword_kind::Kw_vozvrat)                 }, // Keyword Kw_vozvrat

            {{{85,18 }, {85,22 }}, id_lexeme(75)                                            }, // Id fn_p1
            {{{86,1  }, {86,1  }}, delim_lexeme(Delimiter_kind::Fig_br_closed)              }, // Delimiter Fig_br_closed
            {{{86,2  }, {86,2  }}, nothing_lexeme()                                         }, // Nothing
    };

    static const arkona_scanner::Arkona_token nothing = {
        {{86,2  }, {86,2  }}, nothing_lexeme()
    };

    void test_back_function()
    {
        Errors_and_tries  et;
        et.ec_         = std::make_shared<Error_count>();
        et.wc_         = std::make_shared<Warning_count>();
        et.ids_trie_   = std::make_shared<Char_trie>();
        et.strs_trie_  = std::make_shared<Char_trie>();

        auto p         = const_cast<char32_t*>(test_text);
        auto loc       = std::make_shared<ascaner::Location>(p);
        auto scanner   = std::make_shared<arkona_scanner::Scanner>(loc, et);
        Scanner_as_generator_with_actions gen{scanner, actions};
        puts("Testing of the method back().");
        testing::test(gen,                         [](auto t){return t;},
                      std::begin(tokens_for_text), std::end(tokens_for_text));

        puts("Additional tests of the method back().");
        scanner->back();
        auto token                         = scanner->current_lexeme();
        bool first_additional_test_passed  = token == nothing;
        printf("First additional test %s.\n",
               first_additional_test_passed ? "passed" : "failed");

        scanner->back();
        token                              = scanner->current_lexeme();
        bool second_additional_test_passed = token == nothing;
        printf("Second additional test %s.\n",
               second_additional_test_passed ? "passed" : "failed");
    }
};