/*
    File:    testing-func.cpp
    Created: 09 July 2019 at 08:09 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstdio>
#include "../include/testing-func.h"
// #include "../../scanner/include/lexeme.h"
namespace testing_scanner{
    void test_func(const std::shared_ptr<arkona_scanner::Scanner>& arkonasc)
    {
        arkona_scanner::Arkona_token ati;
        arkona_scanner::Lexem_kind   alk;
        do{
            ati    = arkonasc->current_lexeme();
            alk    = ati.lexeme_.code_.kind_;
            auto s = arkonasc->token_to_string(ati);
            puts(s.c_str());
        }while(alk != arkona_scanner::Lexem_kind::Nothing);
    }
};
