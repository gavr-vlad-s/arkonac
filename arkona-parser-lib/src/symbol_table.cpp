/*
    File:    symbol_table.cpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/symbol_table.hpp"
#include "../../strings-lib/include/join.h"
#include "../../char-conv/include/char_conv.h"

namespace arkona_parser{
    std::size_t Symbol_table::insert_module_name(const std::vector<std::size_t>& parts)
    {
        std::size_t result = module_parts_trie_->insert_vector(parts);
        indeces_of_inserted_names_.insert(result);
        return result;
    }

    bool Symbol_table::has_module_name(const std::vector<std::size_t>& parts)
    {
        std::size_t idx = module_parts_trie_->insert_vector(parts);
        return indeces_of_inserted_names_.count(idx) != 0;
    }

    std::string Symbol_table::get_string(const std::vector<std::size_t>& parts)
    {
        std::string result;
        if(!has_module_name(parts)){
            return result;
        }

        std::vector<std::string> components;
        for(size_t part : parts){
            auto component_as_u32string = ids_trie_->get_string(part);
            auto component              = u32string_to_utf8(component_as_u32string);
            components.push_back(component);
        }

        result = join(components.begin(), components.end(), std::string{"::"});

        return result;
    }
};