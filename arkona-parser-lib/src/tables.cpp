// File: tables.cpp
// This file was generated automatically.
#include <utility>
#include <map>
#include <cstddef>
#include <cstdint>
#include "../include/tables.hpp"
#include "../include/nonterminal_enum.hpp"
namespace arkona_parser{
    namespace slr1_tables{
        using GOTO_index   = std::pair<std::size_t, std::uint16_t>;

        static const std::map<GOTO_index, std::size_t> gotos = {
            {{0, 1}, 2},  {{1, 2}, 4},   {{4, 3}, 8}, {{4, 4}, 9}, {{5, 2}, 16}, 
            {{9, 3}, 11}, {{12, 2}, 13}
        };

        static const std::size_t lengths_of_rules[] = {
            1, 3, 4, 1, 3, 2, 4, 2
        };

        using State_and_terminal = std::pair<std::size_t, Terminal>;

        static const std::map<State_and_terminal, Action> acts = {
            {{0, Terminal::module}, Action{Action_kind::Shift, 1}},             
            {{1, Terminal::id}, Action{Action_kind::Shift, 3}},                 
            {{2, Terminal::End_of_text}, Action{Action_kind::OK, 0}},           
            {{3, Terminal::uses}, Action{Action_kind::Reduce, 3}},              
            {{3, Terminal::scope_resolution}, Action{Action_kind::Reduce, 3}},  
            {{3, Terminal::fig_br_opened}, Action{Action_kind::Reduce, 3}},     
            {{3, Terminal::semicolon}, Action{Action_kind::Reduce, 3}},         
            {{4, Terminal::uses}, Action{Action_kind::Shift, 5}},               
            {{4, Terminal::scope_resolution}, Action{Action_kind::Shift, 6}},   
            {{4, Terminal::fig_br_opened}, Action{Action_kind::Shift, 7}},      
            {{5, Terminal::id}, Action{Action_kind::Shift, 3}},                 
            {{6, Terminal::id}, Action{Action_kind::Shift, 15}},                
            {{7, Terminal::fig_br_closed}, Action{Action_kind::Shift, 14}},     
            {{8, Terminal::End_of_text}, Action{Action_kind::Reduce, 1}},       
            {{9, Terminal::fig_br_opened}, Action{Action_kind::Shift, 7}},      
            {{9, Terminal::semicolon}, Action{Action_kind::Shift, 10}},         
            {{10, Terminal::uses}, Action{Action_kind::Shift, 12}},             
            {{11, Terminal::End_of_text}, Action{Action_kind::Reduce, 2}},      
            {{12, Terminal::id}, Action{Action_kind::Shift, 3}},                
            {{13, Terminal::scope_resolution}, Action{Action_kind::Shift, 6}},  
            {{13, Terminal::fig_br_opened}, Action{Action_kind::Reduce, 6}},    
            {{13, Terminal::semicolon}, Action{Action_kind::Reduce, 6}},        
            {{14, Terminal::End_of_text}, Action{Action_kind::Reduce, 7}},      
            {{15, Terminal::uses}, Action{Action_kind::Reduce, 4}},             
            {{15, Terminal::scope_resolution}, Action{Action_kind::Reduce, 4}}, 
            {{15, Terminal::fig_br_opened}, Action{Action_kind::Reduce, 4}},    
            {{15, Terminal::semicolon}, Action{Action_kind::Reduce, 4}},        
            {{16, Terminal::scope_resolution}, Action{Action_kind::Shift, 6}},  
            {{16, Terminal::fig_br_opened}, Action{Action_kind::Reduce, 5}},    
            {{16, Terminal::semicolon}, Action{Action_kind::Reduce, 5}}
        };

        static const Nonterminal targets[] = {
            Nonterminal::fictive,      Nonterminal::arkona_module, Nonterminal::arkona_module, 
            Nonterminal::qualified_id, Nonterminal::qualified_id,  Nonterminal::imports,       
            Nonterminal::imports,      Nonterminal::block
        };

        Action Tables::get_action(std::size_t state, Terminal t) const
        {
            auto   it     = acts.find({state, t});
            Action result = Action{Action_kind::Error, 0};
            if(it != acts.end()){
                result = it->second;
            }
            return result;
        }

        std::size_t Tables::get_goto(std::size_t state, std::size_t non_terminal) const
        {
            std::size_t result = 0;
            auto        it     = gotos.find({state, non_terminal});
            if(it != gotos.end()){
                result = it->second;
            }
            return result;
        }

        std::size_t Tables::get_rule_len(std::size_t rule_idx) const
        {
            return lengths_of_rules[rule_idx];
        }

        std::size_t Tables::get_rule_target(std::size_t rule_idx) const
        {
            return static_cast<std::size_t>(targets[rule_idx]);
        }
    };
};