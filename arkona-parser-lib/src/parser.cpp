// File: parser.cpp
// This file was generated automatically.
// Preamble:
#include <cstdio>
#include "../include/parser.hpp"
#include "../include/tables.hpp"
#include "../include/diagnostic.hpp"
#include "../include/token_to_terminal.hpp"
#include "../include/shift_function.hpp"

void add_module_name(const std::shared_ptr<arkona_parser::Symbol_table>&      symbol_table,
                     const std::shared_ptr<arkona_parser::ast::Qualified_id>& pqual_id,
                     const std::shared_ptr<Error_count>&                      ec)
{
    std::vector<size_t> parts;
    for(const auto& token : pqual_id->name_components_){
        parts.push_back(token.lexeme_.id_info_.id_idx_);
    }

    bool has_qual_id = symbol_table->has_module_name(parts);

    if(has_qual_id){
        auto str = symbol_table->get_string(parts);
        printf("Квалифицированный идентификатор %s, являющийся именем модуля, уже определён.\n",
               str.c_str());
        ec->increment_number_of_errors();
    }else{
        symbol_table->insert_module_name(parts);
    }
}
namespace arkona_parser{
// Reduction functions block:
    Parser::Reduce_function Parser::reductions_[] = {
        &Parser::reduce1, &Parser::reduce2, 
        &Parser::reduce3, &Parser::reduce4, 
        &Parser::reduce5, &Parser::reduce6, 
        &Parser::reduce7
    };

    Shift_result Parser::reduce1()
    {
        Shift_result result;
        
    auto pqual_id   = std::dynamic_pointer_cast<ast::Qualified_id>(current_rule_body_[1].shift_result_.pnode_);
    auto pblck      = std::dynamic_pointer_cast<ast::Block>(current_rule_body_[2].shift_result_.pnode_);

    if(!pqual_id || !pblck){
        return result;
    }

    add_module_name(symbol_table_, pqual_id, en_);

    result.token_ = current_rule_body_[1].shift_result_.token_;
    result.pnode_ = std::make_shared<ast::Module>(pqual_id, pblck, nullptr);

        return result;
    }

    Shift_result Parser::reduce2()
    {
        Shift_result result;
        
    auto pqual_id   = std::dynamic_pointer_cast<ast::Qualified_id>(current_rule_body_[1].shift_result_.pnode_);
    auto pimports   = std::dynamic_pointer_cast<ast::Imports>(current_rule_body_[2].shift_result_.pnode_);
    auto pblck      = std::dynamic_pointer_cast<ast::Block>(current_rule_body_[3].shift_result_.pnode_);

    if(!pqual_id || !pblck || !pimports){
        return result;
    }

    add_module_name(symbol_table_, pqual_id, en_);

    result.token_ = current_rule_body_[1].shift_result_.token_;
    result.pnode_ = std::make_shared<ast::Module>(pqual_id, pblck, pimports);

        return result;
    }

    Shift_result Parser::reduce3()
    {
        Shift_result result;
        
        std::list<Token> components;
        auto id_token = current_rule_body_[0].shift_result_.token_;
        components.push_back(id_token);

        result.token_ = id_token;
        result.pnode_ = std::make_shared<ast::Qualified_id>(components);

        return result;
    }

    Shift_result Parser::reduce4()
    {
        Shift_result result;
        
        auto pqual_id   = std::dynamic_pointer_cast<ast::Qualified_id>(current_rule_body_[0].shift_result_.pnode_);
        if(!pqual_id){
            return result;
        }

        auto components = pqual_id->name_components_;
        auto id_token   = current_rule_body_[2].shift_result_.token_;
        components.push_back(id_token);

        result.token_ = id_token;
        result.pnode_ = std::make_shared<ast::Qualified_id>(components);

        return result;
    }

    Shift_result Parser::reduce5()
    {
        Shift_result result;
        
        auto pqual_id   = std::dynamic_pointer_cast<ast::Qualified_id>(current_rule_body_[1].shift_result_.pnode_);
        if(!pqual_id){
            return result;
        }

        add_module_name(symbol_table_, pqual_id, en_);

        std::list<std::shared_ptr<arkona_parser::ast::Qualified_id>> imports;
        imports.push_back(pqual_id);

        result.token_ = current_rule_body_[1].shift_result_.token_;
        result.pnode_ = std::make_shared<ast::Imports>(imports);

        return result;
    }

    Shift_result Parser::reduce6()
    {
        Shift_result result;
        
        auto pqual_id   = std::dynamic_pointer_cast<ast::Qualified_id>(current_rule_body_[2].shift_result_.pnode_);
        if(!pqual_id){
            return result;
        }

        auto pimports   = std::dynamic_pointer_cast<ast::Imports>(current_rule_body_[0].shift_result_.pnode_);
        if(!pimports){
            return result;
        }

        add_module_name(symbol_table_, pqual_id, en_);

        auto imports = pimports->imports_;

        imports.push_back(pqual_id);

        result.token_ = current_rule_body_[2].shift_result_.token_;
        result.pnode_ = std::make_shared<ast::Imports>(imports);

        return result;
    }

    Shift_result Parser::reduce7()
    {
        Shift_result result;
        
        auto blck     = std::make_shared<ast::Block>();
        result.pnode_ = blck;
        result.token_ = current_rule_body_[0].shift_result_.token_;

        return result;
    }

    Shift_result Parser::reduction(std::size_t rule_number){
        return (this->*reductions_[rule_number - 1])();
    }
// Implementation of operator():

    Shift_result Parser::operator ()(){
        Shift_result result;
        Stack_elem initial_elem_;
        initial_elem_.state_ = 0;
        parser_stack_.push(initial_elem_);

        slr1_tables::Tables parsing_tables;
        Diagnostic          diagnostic;

        for( ; ; ){
            token_                    = scanner_->current_lexeme();
            terminal_                 = token_to_terminal(token_);
            std::size_t current_state = parser_stack_.top().state_;
            auto        action        = parsing_tables.get_action(current_state, terminal_);

            switch(action.kind_){
                case Shift:
                    {
                        Stack_elem elem;
                        elem.state_        = action.idx_;
                        elem.shift_result_ = shift_function(token_);
                        parser_stack_.push(elem);
                    }
                    break;
                case Reduce:
                    {
                        current_rule_length_ = parsing_tables.get_rule_len(action.idx_);
                        current_rule_target_ = parsing_tables.get_rule_target(action.idx_);

                        parser_stack_.ntop(current_rule_body_, current_rule_length_);

                        Stack_elem elem;
                        elem.shift_result_   = reduction(action.idx_);

                        parser_stack_.npop(current_rule_length_);

                        Stack_elem top_elem  = parser_stack_.top();
                        elem.state_          = parsing_tables.get_goto(top_elem.state_, current_rule_target_);
                        parser_stack_.push(elem);
                    }
                    break;
                case OK:
                    result = parser_stack_.top().shift_result_;
                    return result;
                    break;
                case Error:
                    {
                        auto msg = diagnostic.get_diagnostic_msg(current_state, terminal_);
                        printf("Ошибка в строке %zu, позиции %zu: %s\n",
                               token_.range_.begin_pos_.line_no_,
                               token_.range_.begin_pos_.line_pos_,
                               msg.c_str());
                        en_->increment_number_of_errors();
                        return result;
                    }
                    break;
            }
        }
        return result;
    }
};