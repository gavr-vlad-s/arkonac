/*
    File:    shift_function.cpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/shift_function.hpp"
namespace arkona_parser{
    Shift_result shift_function(const Token& token)
    {
        Shift_result result;
        result.token_ = token;
        result.pnode_ = nullptr;
        return result;
    }
};