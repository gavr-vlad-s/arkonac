/*
    File:    token_to_terminal.cpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <map>
#include "../include/token_to_terminal.hpp"
namespace arkona_parser{
    using Lexem_code     = arkona_scanner_snd_lvl::Lexem_code;
    using Lexem_kind     = arkona_scanner_snd_lvl::Lexem_kind;
    using Keyword_kind   = arkona_scanner_snd_lvl::Keyword_kind;
    using Delimiter_kind = arkona_scanner_snd_lvl::Delimiter_kind;

    static const std::map<Keyword_kind, Terminal> keyword_to_terminal_mapping = {
        {Keyword_kind::Kw_modul,     Terminal::module},
        {Keyword_kind::Kw_ispolzuet, Terminal::uses  },
    };

    static const std::map<Delimiter_kind, Terminal> delimiter_to_terminal_mapping = {
        {Delimiter_kind::Scope_resol,   Terminal::scope_resolution},
        {Delimiter_kind::Comma,         Terminal::comma           },
        {Delimiter_kind::Fig_br_opened, Terminal::fig_br_opened   },
        {Delimiter_kind::Fig_br_closed, Terminal::fig_br_closed   },
        {Delimiter_kind::Semicolon,     Terminal::semicolon       },
    };

    static Terminal convert_keyword_to_terminal(Keyword_kind kw_kind)
    {
        auto     it     = keyword_to_terminal_mapping.find(kw_kind);
        Terminal result = Terminal::End_of_text;
        if(it != keyword_to_terminal_mapping.end()){
            result = it->second;
        }
        return result;
    }

    static Terminal convert_delimiter_to_terminal(Delimiter_kind del_kind)
    {
        auto     it     = delimiter_to_terminal_mapping.find(del_kind);
        Terminal result = Terminal::End_of_text;
        if(it != delimiter_to_terminal_mapping.end()){
            result = it->second;
        }
        return result;
    }

    Terminal token_to_terminal(const Token& token)
    {
        auto       code = token.lexeme_.code_;
        Lexem_kind kind = code.kind_;
        switch(kind){
            case Lexem_kind::Nothing: case Lexem_kind::UnknownLexem:
                return Terminal::End_of_text;
                break;
            case Lexem_kind::Keyword:
                return convert_keyword_to_terminal(static_cast<Keyword_kind>(code.subkind_));
                break;
            case Lexem_kind::Id:
                return Terminal::id;
                break;
            case Lexem_kind::Delimiter:
                return convert_delimiter_to_terminal(static_cast<Delimiter_kind>(code.subkind_));
                break;
            case Lexem_kind::Char:  case Lexem_kind::String:  case Lexem_kind::Integer:
            case Lexem_kind::Float: case Lexem_kind::Complex: case Lexem_kind::Quat:
                return Terminal::End_of_text;
                break;
        }
        return Terminal::End_of_text;
    }
}