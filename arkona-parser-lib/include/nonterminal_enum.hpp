// File: nonterminal_enum.hpp
// This file was generated automatically.
#ifndef NONTERMINAL_ENUM_H
#define NONTERMINAL_ENUM_H
#   include <cstdint>
namespace arkona_parser{
    enum class Nonterminal : std::uint16_t{
        fictive, arkona_module, qualified_id, block, 
        imports
    };
};
#endif