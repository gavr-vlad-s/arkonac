// File: parser.hpp
// This file was generated automatically.
#ifndef PARSER_H
#define PARSER_H
// Preamble:
#   include <memory>
#   include "../include/multipop_stack.hpp"
#   include "../include/terminal_enum.hpp"
#   include "../../snd-lvl-scanner/include/arkona-scanner-snd-lvl.h"
#   include "../include/ast_node.hpp"
#   include "../../tries/include/errors_and_tries.h"
#   include "../../tries/include/error_count.h"
#   include "../../tries/include/char_trie.h"
#   include "../include/shift_result.hpp"
#   include "../include/symbol_table.hpp"
namespace arkona_parser{
    using scanner_ptr_t = std::shared_ptr<arkona_scanner_snd_lvl::Scanner>;

    class Parser{
    public:
        Parser() = default;
        Parser(const Parser& rhs) = default;
        ~Parser() = default;
        Parser(const scanner_ptr_t& scanner, const Errors_and_tries& et, const std::shared_ptr<Symbol_table>& symbol_table) :
            scanner_{scanner}, en_{et.ec_}, wn_{et.wc_}, ids_{et.ids_trie_}, strs_{et.strs_trie_}, symbol_table_{symbol_table}
            {}
        Shift_result operator ()();
    private:
        scanner_ptr_t scanner_;
        /* a pointer to a class that counts the number of errors: */
        std::shared_ptr<Error_count>                  en_;
        /* a pointer to a class that counts the number of warnings: */
        std::shared_ptr<Warning_count>                wn_;
        /* a pointer to the prefix tree for identifiers: */
        std::shared_ptr<Char_trie>                    ids_;
        /* a pointer to the prefix tree for string literals: */
        std::shared_ptr<Char_trie>                    strs_;
        /* a pointer to the symbol table */
        std::shared_ptr<Symbol_table>                 symbol_table_;

        struct Stack_elem{
            std::size_t state_;
            Shift_result shift_result_;
        };

        stack::Stack<Stack_elem> parser_stack_;
        Stack_elem               current_rule_body_[4];
        std::size_t              current_rule_length_;
        std::size_t              current_rule_target_;

        Token token_;
        Terminal terminal_;

        typedef Shift_result (Parser::*Reduce_function)();
        static Reduce_function reductions_[];

        Shift_result reduction(std::size_t rule_number);

        Shift_result reduce1();
        Shift_result reduce2();
        Shift_result reduce3();
        Shift_result reduce4();
        Shift_result reduce5();
        Shift_result reduce6();
        Shift_result reduce7();
    };
};
#endif