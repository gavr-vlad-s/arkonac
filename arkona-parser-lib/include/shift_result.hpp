/*
    File:    shift_result.hpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SHIFT_RESULT_H
#define SHIFT_RESULT_H
#   include "../include/token.hpp"
#   include "../include/ast_node.hpp"
namespace arkona_parser{
    struct Shift_result{
        Token         token_;
        ast::Node_ptr pnode_;
    };
};
#endif