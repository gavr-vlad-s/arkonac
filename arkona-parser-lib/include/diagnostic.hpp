// File: diagnostic.hpp
// This file was generated automatically.
#ifndef DIAGNOSTIC_H
#define DIAGNOSTIC_H
#   include <cstddef>
#   include <string>
#   include "../include/terminal_enum.hpp"
namespace arkona_parser{
    class Diagnostic final{
    public:
        Diagnostic()                      = default;
        Diagnostic(const Diagnostic& rhs) = default;
        ~Diagnostic()                     = default;

        std::string get_diagnostic_msg(std::size_t state, Terminal t) const;
    };
};
#endif