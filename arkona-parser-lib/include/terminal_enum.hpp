// File: terminal_enum.hpp
// This file was generated automatically.
#ifndef TERMINAL_ENUM_H
#define TERMINAL_ENUM_H
#   include <cstdint>
namespace arkona_parser{
    enum class Terminal : std::uint16_t{
        End_of_text, module,        uses,          scope_resolution, 
        comma,       fig_br_opened, fig_br_closed, id,               
        semicolon
    };
};
#endif