// File: tables.hpp
// This file was generated automatically.
#ifndef TABLES_H
#define TABLES_H
#   include <cstddef>
#   include "../include/action.hpp"
#   include "../include/terminal_enum.hpp"
namespace arkona_parser{
    namespace slr1_tables{
        class Tables final{
        public:
            Tables()                  = default;
            Tables(const Tables& rhs) = default;
            ~Tables()                 = default;

            Action      get_action(std::size_t state, Terminal t) const;
            std::size_t get_goto(std::size_t state, std::size_t non_terminal) const;
            std::size_t get_rule_len(std::size_t rule_idx) const;
            std::size_t get_rule_target(std::size_t rule_idx) const;
        };
    };
};
#endif