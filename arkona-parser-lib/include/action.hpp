// File: action.hpp
// This file was generated automatically.
#ifndef ACTION_H
#define ACTION_H
#   include <cstdint>
namespace arkona_parser{
    enum Action_kind{
        Shift, Reduce, OK, Error
    };

    struct Action{
        unsigned      kind_ : 2;
        std::uint16_t idx_  : 14;
    };
};
#endif