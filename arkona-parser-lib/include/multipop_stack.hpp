// File: multipop_stack.hpp
// This file was generated automatically.
#ifndef MULTIPOP_STACK_H
#define MULTIPOP_STACK_H
#   include <deque>
#   include <cassert>
#   include <iostream>
namespace stack{
    template<typename T, template<typename Elem,
                                  typename = std::allocator<Elem>> class ContainerT = std::deque>
    class Stack{
    public:
        Stack()          = default;
        virtual ~Stack() = default;

        template<typename ForwardIterator>
        Stack(ForwardIterator first, ForwardIterator last);

        template<typename U, template<typename Elem, typename = std::allocator<Elem>> class ContainerU>
        Stack& operator = (const Stack<U, ContainerU>& rhs);

        template<typename, template<typename, typename> class>
        friend class Stack;

        bool empty() const
        {
            return elems_.empty();
        }

        std::size_t size() const
        {
            return elems_.size();
        }

        void push(const T& e);
        void pop();
        const T& top() const;

        template<typename ForwardIterator>
        void push(ForwardIterator first, ForwardIterator last);

        void npop(std::size_t n);

        void ntop(T* first, std::size_t n);

        void print(std::ostream& stream) const;
    private:
        ContainerT<T> elems_;
    };

    template<typename T, template<typename, typename> class ContainerT>
    void Stack<T, ContainerT>::push(const T &e)
    {
        elems_.push_back(e);
    }

    template<typename T, template<typename, typename> class ContainerT>
    void Stack<T, ContainerT>::pop()
    {
        assert(!empty());
        elems_.pop_back();
    }

    template<typename T, template<typename, typename> class ContainerT>
    const T& Stack<T, ContainerT>::top() const
    {
        assert(!empty());
        return elems_.back();
    }


    template<typename T, template<typename, typename> class ContainerT>
    template<typename ForwardIterator>
    Stack<T, ContainerT>::Stack(ForwardIterator first, ForwardIterator last)
    {

        for(auto it = first; it != last; it++){
            elems_.push_back(*it);
        }
    }

    template<typename T, template<typename, typename> class ContainerT>
    template<typename U, template<typename, typename> class ContainerU>
    Stack<T, ContainerT>& Stack<T, ContainerT>::operator = (const Stack<U, ContainerU>& rhs)
    {
        elems_.clear();
        elems_.insert(elems_.end(), rhs.elems_.begin(), rhs.elems_.end());
        return *this;
    }

    template<typename T, template<typename, typename> class ContainerT>
    template<typename ForwardIterator>
    void Stack<T, ContainerT>::push(ForwardIterator first, ForwardIterator last)
    {
        for(auto it = first; it != last; it++){
            elems_.push_back(*it);
        }
    }

    template<typename T, template<typename, typename> class ContainerT>
    void Stack<T, ContainerT>::npop(std::size_t n)
    {
        assert(size() >= n);
        for(std::size_t i = 0; i < n; ++i){
            pop();
        }
    }

    template<typename T, template<typename, typename> class ContainerT>
    void Stack<T, ContainerT>::ntop(T *first, std::size_t n)
    {
        assert(size() >= n);
        std::size_t num_of_elems = size();
        for(std::size_t i = 0; i < n; ++i){
            first[i] = elems_[num_of_elems - n + i];
        }
    }

    template<typename T, template<typename, typename> class ContainerT>
    void Stack<T, ContainerT>::print(std::ostream &stream) const
    {
        for(const auto& e : elems_){
            stream << e << "\n";
        }
    }
};
#endif