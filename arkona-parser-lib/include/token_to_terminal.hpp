/*
    File:    token_to_terminal.hpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef TOKEN_TO_TERMINAL_H
#define TOKEN_TO_TERMINAL_H
#   include "../include/terminal_enum.hpp"
#   include "../include/token.hpp"
namespace arkona_parser{
    Terminal token_to_terminal(const Token& token);
};
#endif