/*
    File:    symbol_table.hpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H
#   include <vector>
#   include <memory>
#   include <set>
#   include <cstddef>
#   include <string>
#   include "../../tries/include/trie_for_vector.h"
#   include "../../tries/include/char_trie.h"
namespace arkona_parser{
    using Module_parts_trie_ptr = std::shared_ptr<Trie_for_vector<std::size_t>>;

    class Symbol_table{
    public:
        Symbol_table()                                     = default;
        virtual ~Symbol_table()                            = default;
        Symbol_table(const Symbol_table& rhs)              = default;
        Symbol_table& operator = (const Symbol_table& rhs) = default;

        explicit Symbol_table(const Module_parts_trie_ptr& module_parts_trie,
                              const std::shared_ptr<Char_trie>& ids_trie) :
            indeces_of_inserted_names_{},
            module_parts_trie_{module_parts_trie},
            ids_trie_{ids_trie}
        {
        }

        bool        has_module_name(const std::vector<std::size_t>& parts);
        std::size_t insert_module_name(const std::vector<std::size_t>& parts);

        std::string get_string(const std::vector<std::size_t>& parts);
    private:
        std::set<std::size_t>      indeces_of_inserted_names_;
        Module_parts_trie_ptr      module_parts_trie_;
        std::shared_ptr<Char_trie> ids_trie_;
    };
};
#endif