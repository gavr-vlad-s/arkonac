/*
    File:    shift_function.hpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SHIFT_FUNCTION_H
#define SHIFT_FUNCTION_H
#   include "../include/shift_result.hpp"
#   include "../include/token.hpp"
namespace arkona_parser{
    Shift_result shift_function(const Token& token);
};
#endif