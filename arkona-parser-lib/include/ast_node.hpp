/*
    File:    ast_node.hpp
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef AST_NODE_H
#define AST_NODE_H
#   include <memory>
#   include <list>
#   include <cstddef>
#   include "../include/token.hpp"
namespace arkona_parser{
    namespace ast{
        struct Module;
        struct Qualified_id;
        struct Imports;
        struct Block;

        class Visitor{
        public:
            Visitor()                   = default;
            Visitor(const Visitor& rhs) = default;
            virtual ~Visitor()          = default;

            virtual void visit(Module&       ref) = 0;
            virtual void visit(Qualified_id& ref) = 0;
            virtual void visit(Imports&      ref) = 0;
            virtual void visit(Block&        ref) = 0;
        };

        class Node{
        public:
            Node()                = default;
            Node(const Node& rhs) = default;
            virtual ~Node()       = default;

            virtual void accept(Visitor& v) = 0;
        };

        using Node_ptr = std::shared_ptr<Node>;

        template<typename Derived>
        class Visitable : public Node{
        public:
            using Node::Node;
            void accept(Visitor& v) override
            {
                v.visit(*static_cast<Derived*>(this));
            }
        };

        struct Module : public Visitable<Module>{
        public:
            using Visitable<Module>::Visitable;

            Module()                  = default;
            Module(const Module& rhs) = default;
            virtual ~Module()         = default;

            Module(const std::shared_ptr<Qualified_id>& name,
                   const std::shared_ptr<Block>&        body,
                   const std::shared_ptr<Imports>&      used_modules) :
                name_{name}, body_{body}, used_modules_{used_modules}
            {
            }

            std::shared_ptr<Qualified_id> name_;
            std::shared_ptr<Block>        body_;
            std::shared_ptr<Imports>      used_modules_;
        };

        struct Qualified_id : public Visitable<Qualified_id>{
        public:
            using Visitable<Qualified_id>::Visitable;

            Qualified_id()                        = default;
            Qualified_id(const Qualified_id& rhs) = default;
            virtual ~Qualified_id()               = default;

            Qualified_id(const std::list<Token>& name_components) :
                name_components_{name_components}
            {
            }

            std::list<Token> name_components_;
        };

        struct Imports : public Visitable<Imports>{
        public:
            using Visitable<Imports>::Visitable;

            Imports()                   = default;
            Imports(const Imports& rhs) = default;
            virtual ~Imports()          = default;

            Imports(const std::list<std::shared_ptr<Qualified_id>>& imports) :
                imports_{imports}
            {
            }

            std::list<std::shared_ptr<Qualified_id>> imports_;
        };

        struct Block : public Visitable<Block>{
        public:
            using Visitable<Block>::Visitable;

            Block()                 = default;
            Block(const Block& rhs) = default;
            virtual ~Block()        = default;
        };

        class AST{
        public:
            AST()               = default;
            AST(const AST& rhs) = default;
            virtual ~AST()      = default;

            explicit AST(const Node_ptr& root) : root_{root} {}

            void traverse(Visitor& v) const
            {
                if(root_){
                    root_->accept(v);
                }
            }

            void traverse(Visitor& v)
            {
                if(root_){
                    root_->accept(v);
                }
            }
        private:
            Node_ptr root_;
        };
    };
};
#endif